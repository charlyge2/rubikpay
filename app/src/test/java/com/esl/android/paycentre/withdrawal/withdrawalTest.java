package com.esl.android.paycentre.withdrawal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.esl.android.paycentre.BuildConfig;
import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.withdrawal.WithdrawalAmountActivity;
import com.esl.android.paycentre.controllers.activationControllers.ActivationController;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.repo.PosAccountRepo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Iterator;

import androidx.annotation.NonNull;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
public class withdrawalTest {

    WithdrawalAmountActivity withdrawalAmountActivity;
    Context context;
    View view;

    ActivationController activationController = new ActivationController();
    String amount = "";

    @Test
    public void testWithdrawalAmount() {
        assertEquals("",amount);
    }

    @Test
    public void testActivation() {
        String activationCode = "";
        String result = activationController.validateActivation(context, "73");
        assertEquals("Activation Code Must be 8 digits",result);
    }

}