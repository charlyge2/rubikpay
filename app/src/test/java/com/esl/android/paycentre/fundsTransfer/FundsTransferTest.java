package com.esl.android.paycentre.fundsTransfer;

import android.content.Context;
import android.view.View;
import com.esl.android.paycentre.controllers.FundsTransferController;
import com.esl.android.paycentre.services.HttpService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(HttpService.class)
public class FundsTransferTest {
    Context context;
    View view;
    @Test
    public void checkIfAccountLengthIsLessThan10() {
        int acccountNumberLength = 5;
        String result = FundsTransferController.validateFundsTransfer(acccountNumberLength);
        assertEquals("invalid",result);
    }

    @Test
    public void checkIfAccountLenghtIsEqualsTo10() {
        int acccountNumberLength = 10;
        String result = FundsTransferController.validateFundsTransfer(acccountNumberLength);
        assertEquals("valid",result);
    }

    @Test
    public void checkIfAccountNumberFieldIsMoreThan10() {
        int acccountNumberLength = 122;
        String result = FundsTransferController.validateFundsTransfer(acccountNumberLength);
        assertEquals("inactive",result);
    }


}