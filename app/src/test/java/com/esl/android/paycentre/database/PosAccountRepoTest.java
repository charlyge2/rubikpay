package com.esl.android.paycentre.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.esl.android.paycentre.BuildConfig;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.repo.PosAccountRepo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Iterator;

import androidx.annotation.NonNull;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DatabaseManager.class })
public class PosAccountRepoTest {

    SQLiteDatabase db;
    PosAccountRepo repo;

    @Before
    public void setup(){
        repo = new PosAccountRepo();
        db = mock(SQLiteDatabase.class);

        mockStatic(DatabaseManager.class);
        DatabaseManager dbManager = mock(DatabaseManager.class);
        when(DatabaseManager.getInstance()).thenReturn(dbManager);
        when(dbManager.openDatabase()).thenReturn(db);
    }

    @Test
    public void testCreateTable() {
        String createSql = String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY,%s TEXT UNIQUE,%s TEXT,%s TEXT,%s TEXT,%s TEXT)", PosAccount.TABLE, PosAccount.KEY_ID, PosAccount.KEY_USERNAME, PosAccount.KEY_FIRSTNAME, PosAccount.KEY_LASTNAME, PosAccount.KEY_PASSWORD, PosAccount.KEY_ACCOUNTID);

        assertEquals("Create table statement is not correct!", createSql, repo.createTable());
    }

    @Test
    public void testInsertIsCalledOnDbObject() {
        PosAccount posAccount = new PosAccount();
        posAccount.setFirstname("TestFirstname");
        posAccount.setLastname("TestLastname");
        posAccount.setUsername("TestUsername");
        posAccount.setPassword("TestPassword");
        posAccount.setPosAccountId("TestAccountId");

        ContentValues values = new ContentValues();

        values.put(PosAccount.KEY_USERNAME, posAccount.getUsername());
        values.put(PosAccount.KEY_FIRSTNAME, posAccount.getFirstname());
        values.put(PosAccount.KEY_LASTNAME, posAccount.getLastname());
        values.put(PosAccount.KEY_PASSWORD, posAccount.getPassword());
        values.put(PosAccount.KEY_ACCOUNTID, posAccount.getPosAccountId());

        verify(db, atMost(1)).insert(PosAccount.TABLE, null, values);

        repo.insert(posAccount);
    }

    @Test
    public void testReturnPosAccountForValidLogin() {
        PosAccount posAccount = new PosAccount();
        posAccount.setFirstname("TestFirstname");
        posAccount.setLastname("TestLastname");
        posAccount.setUsername("TestUsername");
        posAccount.setPassword("TestPassword");
        posAccount.setPosAccountId("TestAccountId");

        Cursor cursor = createMockCursorOverPosAccount(posAccount);
        when(db.query(PosAccount.TABLE,
                null,
                PosAccount.KEY_USERNAME + "=? AND " + PosAccount.KEY_PASSWORD + "=?",
                new String[]{posAccount.getUsername(), posAccount.getPassword()},
                null,
                null,
                null)).thenReturn(cursor);

        PosAccount authAccount = repo.authenticateAccount(posAccount.getUsername(), posAccount.getPassword());
        assertEquals(posAccount.getUsername(), authAccount.getUsername());
    }

    @Test(expected = RuntimeException.class)
    public void testThrowsRuntimeExceptionForInvalidLogin() {
        String username = "TestUsername";
        String password = "TestPassword";

        Cursor cursor = createMockForEmptyCursor();

        when(db.query(PosAccount.TABLE,
                null,
                PosAccount.KEY_USERNAME + "=? AND " + PosAccount.KEY_PASSWORD + "=?",
                new String[]{username, password},
                null,
                null,
                null)).thenReturn(cursor);

        PosAccount authAccount = repo.authenticateAccount(username, password);
    }

    @Test
    public void testIterableIsEmptyForEmptyTable(){
        Cursor cursor = createMockForEmptyCursor();
        when(db.query(PosAccount.TABLE,
                null,
                null,
                null,
                null,
                null,
                null)).thenReturn(cursor);

        Iterator<PosAccount> iterator = repo.getPosAccounts();

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testIteratorSizeIsTableSize() {
        int posAccountCount = 4;
        int count = 1;
        Cursor cursor = createMockForNonEmptyCursor(posAccountCount);
        when(db.query(PosAccount.TABLE,
                null,
                null,
                null,
                null,
                null,
                null)).thenReturn(cursor);

        //verify(cursor, times(4)).moveToNext();
        Iterator<PosAccount> iterator = repo.getPosAccounts();

        assertTrue(iterator.hasNext());
        assertTrue(iterator.next() instanceof PosAccount);

        while(iterator.hasNext()) {
            iterator.next();
            count++;
        }

        assertEquals(posAccountCount, count);
    }

    private Cursor createMockForEmptyCursor() {
        Cursor cursor = mock(Cursor.class);
        when(cursor.moveToFirst()).thenReturn(false);

        return cursor;
    }

    private Cursor createMockCursorOverPosAccount(final PosAccount posAccount) {
        Cursor cursor = mock(Cursor.class);

        when(cursor.moveToFirst()).thenReturn(true);
        when(cursor.getColumnIndex(anyString())).thenAnswer(new Answer<Integer>() {

            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                switch((String)invocation.getArgument(0)) {
                    case PosAccount.KEY_ID:
                        return 0;

                    case PosAccount.KEY_USERNAME:
                        return 1;

                    case PosAccount.KEY_FIRSTNAME:
                        return 2;

                    case PosAccount.KEY_LASTNAME:
                        return 3;

                    case PosAccount.KEY_PASSWORD:
                        return 4;

                    case PosAccount.KEY_ACCOUNTID:
                        return 5;
                }

                return null;
            }
        });

        when(cursor.getString(anyInt())).then(new Answer<String>() {

            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {

                switch((int)invocation.getArgument(0)) {
                    case 1:
                        return posAccount.getUsername();

                    case 2:
                        return posAccount.getFirstname();

                    case 3:
                        return posAccount.getLastname();

                    case 4:
                        return posAccount.getPassword();

                    case 5:
                        return posAccount.getPosAccountId();
                }

                return null;
            }
        });


        when(cursor.getInt(anyInt())).then(new Answer<Integer>() {

            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                switch((int)invocation.getArgument(0)) {
                    case 0:
                        return posAccount.getId();
                }

                return null;
            }
        });

        return cursor;
    }

    private Cursor createMockForNonEmptyCursor(final int posAccountCount) {
        Cursor cursor = mock(Cursor.class);
        final HashMap<String, Integer> state = new HashMap<>();
        state.put("position", 1);
        state.put("state", 1);

        when(cursor.getColumnIndex(anyString())).thenReturn(0);
        when(cursor.moveToFirst()).thenReturn(true);
        when(cursor.moveToNext()).then(new Answer<Boolean>() {

            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                //System.out.println(state.get("position") + " " + state.get("state"));
                if(state.get("position") < posAccountCount) {
                    state.put("position", state.get("position") + 1);
                    return true;
                }

                state.put("state", 0);
                return false;
            }
        });

        when(cursor.getString(anyInt())).then(new Answer<String>() {

            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                if(state.get("state") == 1) {
                    return "TestData" + state.get("position");
                }

                return null;
            }
        });


        when(cursor.getInt(anyInt())).then(new Answer<Integer>() {

            @Override
            public Integer answer(InvocationOnMock invocation) throws Throwable {
                if(state.get("state") == 1) {
                    return state.get("position");
                }

                return null;
            }
        });

        return cursor;
    }
}