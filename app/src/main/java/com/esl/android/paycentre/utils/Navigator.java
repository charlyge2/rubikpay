package com.esl.android.paycentre.utils;

import android.content.Context;
import android.content.Intent;

import com.esl.android.paycentre.activities.agentTransfer.AgentTransferAmountActivity;
import com.esl.android.paycentre.activities.airtimeVending.ValidateAirtimeAmountActivity;
import com.esl.android.paycentre.activities.billsPayment.BillsActivity;
import com.esl.android.paycentre.activities.billsPayment.startimes.StartimesNumberActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.activities.fundsTransfer.FundsTransferAmountActivity;
import com.esl.android.paycentre.activities.transactionHistory.HistoryActivity;
import com.esl.android.paycentre.activities.withdrawal.WithdrawalAmountActivity;

public class Navigator {

    private static Intent intent;

    public static void moveToDashboard(Context context){
        intent = new Intent(context, DashboardActivity.class);
        context.startActivity(intent);
    }

    public static void moveTo(Context context, String page){
        switch (page){
            case "withdrawal":
                intent = new Intent(context, WithdrawalAmountActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                break;
            case "history":
                intent = new Intent(context, HistoryActivity.class);
                context.startActivity(intent);
                break;
            case "agentTransfer":
                intent = new Intent(context, AgentTransferAmountActivity.class);
                context.startActivity(intent);
                break;
            case "fundsTransfer":
                intent = new Intent(context, FundsTransferAmountActivity.class);
                context.startActivity(intent);
                break;
            case "airtime":
                intent = new Intent(context, ValidateAirtimeAmountActivity.class);
                context.startActivity(intent);
                break;
            case "bills":
                intent = new Intent(context, BillsActivity.class);
                context.startActivity(intent);
                break;
            case "startimes":
                intent = new Intent(context, StartimesNumberActivity.class);
                context.startActivity(intent);
                break;
        }
    }
}
