package com.esl.android.paycentre.interfaces;

public interface InactivityTimerListener {
    public void onTimerCompleted();
}
