package com.esl.android.paycentre.adapters;

public class ProductModel {

    private int id;
    private String plan;
    private String planCode;
    private String price;
    private boolean isSelected;

    public ProductModel(int id, String plan, String planCode, String price) {
        this.id = id;
        this.plan = plan;
        this.planCode = planCode;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getPlan() {
        return plan;
    }

    public String getPlanCode() {
        return planCode;
    }

    public String getPrice() {
        return price;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


}
