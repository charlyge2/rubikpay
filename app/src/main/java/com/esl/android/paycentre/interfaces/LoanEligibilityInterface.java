package com.esl.android.paycentre.interfaces;

import org.json.JSONObject;

public interface LoanEligibilityInterface {
    public void sendLoanEligibilityDetails(JSONObject params);
}
