package com.esl.android.paycentre.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.ArrayList;
import java.util.List;

public class PinpadListAdapter extends ArrayAdapter<PinpadList> {

    private Context mcontext;
    private List<PinpadList> pinpadList = new ArrayList<>();

    public PinpadListAdapter(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<PinpadList> list) {
        super(context, 0 , list);
        mcontext = context;
        pinpadList = list;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mcontext).inflate(R.layout.adapt,parent,false);

        PinpadList currentPinpad = pinpadList.get(position);

        TextView name = (TextView) listItem.findViewById(R.id.name);
        name.setText(currentPinpad.getDeviceName());

        ImageView image = (ImageView)listItem.findViewById(R.id.bluetooth);
        image.setImageResource(currentPinpad.getImageDrawable());

        //TextView address = (TextView) listItem.findViewById(R.id.address);
        //address.setText(currentPinpad.getDeviceAddress());

        return listItem;
    }


}
