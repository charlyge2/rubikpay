package com.esl.android.paycentre.config;


import android.content.Context;

import com.esl.android.paycentre.utils.SharedPreferenceManager;

public class ConfigurationClass {

    private static SharedPreferenceManager sharedPreferenceManager;

    private String url;
    private String ACTIVATION_URL;
    private String AUTHENTICATION_URL;
    private String POS_ACCOUNT_URL;
    private String FETCH_POS_ACCOUNT_URL;
    private String CHANGE_POS_PIN_URL;
    private String CHANGE_PASSWORD_URL;
    private String ACCOUNT_NAME_URL;
    private String FUNDS_TRANSFER_PAYMENT_URL;
    private String FUNDS_REQUERY_URL;
    private String PURSE_BALANCE_URL;
    private String VERIFY_AGENT_URL;
    private String AGENT_TRANSFER_URL;
    private String LOG_CARD_TRANSACTION_URL;
    private String IBETA_TRANSACTION_URL;
    private String CONVENIENCE_FEE_URL;
    private String DSTV_INQUIRY_URL;
    private String UNIFIED_DSTV_URL;
    private String DSTV_PRODUCT_AND_ADDON_URL;
    private String DSTV_PAYMENT_URL;
    private String GOTV_INQUIRY_URL;
    private String UNIFIED_GOTV_URL;
    private String GOTV_PLAN_URL;
    private String GOTV_PAYMENT_URL;
    private String PAYU_PAYMENT;
    private String PAYU_UPGRADE;
    private String STARTIMES_INQUIRY_URL;
    private String STARTIMES_PAYMENT_URL;
    private String FRSC_INQUIRY_URL;
    private String FRSC_PAYMENT_URL;
    private String ELIGIBILITY_URL;
    private String PICTURE_PLACEHOLDER;
    private String LOAN_URL;
    private String ONE_SIGNAL_APP_ID;
    private String NEW_ONE_SIGNAL_APP_ID;
    private String ANONYMOUS_APP_ID;
    private String PLAYER_ID_URL;
    private String VAS_LIST_URL;
    private String VAS_PAY_URL,POWER_URL,ELECTRICITY_INQUIRY_URL,PAY_POWER_URL,REQUERY_POWER_URL;
    private String TIN_PAYMENT_URL;
    private String KEYS_REDOWNLOAD_URL;
    private String SERVER_TIME_URL;
    private String PLAYSTORE_VERSIONING_URL;
    private String PORT_CHANGE_URL;
    private String TEST_SERVER;
    private String LIVE_SERVER;
    private String APP_VERSION;
    private String SELECTED_BANK;
    private String ONE_SIGNAL_API_KEY;
    private String TERMS_AND_CONDITIONS_URL;
    private String LOAN_TERMS_AND_CONDITIONS_URL;
    private String BANK_LIST_URL;
    private String TOKEN_URL;
    private String DELETE_POS_URL;
    private Double WITHDRAWAL_LIMIT;
    private Double AGENT_FEE_LIMIT;
    private Double WITHDRAWAL_PLUS_AGENT_FEE_LIMIT;
    private String LIVE_BASE_URL ="https://rubikcube.paypad.com.ng/";
    private String TEST_BASE_URL = "http://thor.paypad.com.ng:9004/";


    public ConfigurationClass(String enviroment) {
        this.setEnvironment(enviroment);
    }

    private void setEnvironment(String enviroment) {

        if (enviroment.equals("live")){

            TOKEN_URL = LIVE_BASE_URL + "api/v1/token/refresh";
            ACTIVATION_URL = LIVE_BASE_URL + "api/v1/terminal/activate";
            AUTHENTICATION_URL = LIVE_BASE_URL + "api/v1/login";
            POS_ACCOUNT_URL = LIVE_BASE_URL + "api/v1/createpos";
            FETCH_POS_ACCOUNT_URL = LIVE_BASE_URL +"api/v1/posaccount";
            CHANGE_POS_PIN_URL = LIVE_BASE_URL +"api/v1/posaccount/update";
            CHANGE_PASSWORD_URL = LIVE_BASE_URL +"api/v1/password";
            DELETE_POS_URL = LIVE_BASE_URL + "api/v1/posaccount?username=";

            //    FUNDS TRANSFER ENDPOINTS
            ACCOUNT_NAME_URL = LIVE_BASE_URL +"api/v1/verify/";
            FUNDS_TRANSFER_PAYMENT_URL = LIVE_BASE_URL +"api/v1/fundstransfer";
            FUNDS_REQUERY_URL = LIVE_BASE_URL +"api/v1/transaction/detail";
            BANK_LIST_URL = LIVE_BASE_URL + "api/v1/transfer/banks";

            // PURSE ENDPOINTS
            PURSE_BALANCE_URL = LIVE_BASE_URL +"api/v1/purse/balance";
            VERIFY_AGENT_URL = LIVE_BASE_URL +"api/v1/agenttransfer/verify";
            AGENT_TRANSFER_URL = LIVE_BASE_URL +"api/v1/agenttransfer";

            //    CARD TRANSACTION ENDPOINT
            LOG_CARD_TRANSACTION_URL = LIVE_BASE_URL +"api/v1/transaction/card";

            // IBETA SYNC ENDPOINT
            IBETA_TRANSACTION_URL = "https://ibeta.paypad.com.ng/paypad/webapi/v2/logtransactions";

            //    CONVENIENCE FEE URL
            CONVENIENCE_FEE_URL = LIVE_BASE_URL +"api/v1/convenience/fee";

            //    DSTV ENDPOINTS
            DSTV_INQUIRY_URL = LIVE_BASE_URL +"api/v1/dstv/inquiry";
            UNIFIED_DSTV_URL = LIVE_BASE_URL +"api/v1/unified/dstv/inquiry";
            DSTV_PRODUCT_AND_ADDON_URL = LIVE_BASE_URL +"api/v1/dstv/product";
            DSTV_PAYMENT_URL = LIVE_BASE_URL +"api/v1/dstv/advice";

            //    GOTV ENDPOINTS
            GOTV_INQUIRY_URL = LIVE_BASE_URL +"api/v1/gotv/inquiry";
            UNIFIED_GOTV_URL = LIVE_BASE_URL +"api/v1/unified/gotv/inquiry";
            GOTV_PLAN_URL = LIVE_BASE_URL +"api/v1/gotv/product";
            GOTV_PAYMENT_URL = LIVE_BASE_URL +"api/v1/gotv/advice";

            //    PAYU ENDPOINTS
            PAYU_PAYMENT = LIVE_BASE_URL +"api/v1/payu/payment";
            PAYU_UPGRADE = LIVE_BASE_URL +"api/v1/payu/upgrade";

            //    STARTIMES URL
            STARTIMES_INQUIRY_URL = LIVE_BASE_URL +"api/v1/startimes/inquiry";
            STARTIMES_PAYMENT_URL = LIVE_BASE_URL +"api/v1/startimes/advice";

            // FRSC

            FRSC_INQUIRY_URL = LIVE_BASE_URL +"api/v1/frsc/reference";
            FRSC_PAYMENT_URL = LIVE_BASE_URL +"api/v1/frsc/pay";

            // LOAN

            ELIGIBILITY_URL = LIVE_BASE_URL +"api/v1/loan/checkeligibility";
            PICTURE_PLACEHOLDER = "http://placehold.it/300x300";
            LOAN_URL = LIVE_BASE_URL +"api/v1/loan/request";

            // PUSH NOTIFICATIONS ENDPOINT

            ONE_SIGNAL_APP_ID = "ffadb1a9-b314-4494-b663-0bc40fc18c1d";
            NEW_ONE_SIGNAL_APP_ID = "351526fd-4f32-425f-8101-92b406249bcf";
            ANONYMOUS_APP_ID = "bdd6c22b-baf7-4b6c-8dc8-3e4e0e7a3d3b";
            PLAYER_ID_URL = LIVE_BASE_URL +"api/v1/agent/playerid";

            // AIRTIME VENDING
            VAS_LIST_URL = LIVE_BASE_URL +"api/v1/airtime/fetch";
            VAS_PAY_URL = LIVE_BASE_URL +"api/v1/airtime/vend";
            POWER_URL = LIVE_BASE_URL +"api/v1/power/billers";
            ELECTRICITY_INQUIRY_URL = LIVE_BASE_URL +"api/v1/power/inquiry";
            PAY_POWER_URL = LIVE_BASE_URL +"api/v1/power/pay";
            REQUERY_POWER_URL = LIVE_BASE_URL +"api/v1/power/requery?transactionId=";



            // WITHDRAWAL LIMITS

            WITHDRAWAL_LIMIT = 40000.00;
            AGENT_FEE_LIMIT = 2000.00;
            WITHDRAWAL_PLUS_AGENT_FEE_LIMIT = 41000.00;

            // MISCELLANOUS

            TIN_PAYMENT_URL = "https://paycentre.paypad.com.ng:8000/api/v1/billpayment/tin";
            KEYS_REDOWNLOAD_URL = LIVE_BASE_URL +"api/v1/terminal/reactivate";
            SERVER_TIME_URL = LIVE_BASE_URL +"api/v1/time";
            PLAYSTORE_VERSIONING_URL = LIVE_BASE_URL +"api/v1/app/version";
            PORT_CHANGE_URL = "https://logger.paypad.com.ng/nibssportchange/getparams";
            TEST_SERVER = "http://206.189.204.54:8000";
            LIVE_SERVER = "https://paycentre.paypad.com.ng";
            APP_VERSION = "2.0";
            SELECTED_BANK = "ESL";
            ONE_SIGNAL_API_KEY = "android_sdk-90bcae0b73d7ddca11dec0b24028f56e4c6e717a";
            ONE_SIGNAL_APP_ID = "u4o9lizg";
            TERMS_AND_CONDITIONS_URL = "https://paycentre-terms.firebaseapp.com/";
            LOAN_TERMS_AND_CONDITIONS_URL = "https://paycentre-loans-terms.firebaseapp.com/";


        }else{

            TOKEN_URL = TEST_BASE_URL + "api/v1/token/refresh";
            ACTIVATION_URL = TEST_BASE_URL + "api/v1/terminal/activate";
            AUTHENTICATION_URL = TEST_BASE_URL + "api/v1/login";
            POS_ACCOUNT_URL = TEST_BASE_URL + "api/v1/createpos";
            FETCH_POS_ACCOUNT_URL = TEST_BASE_URL +"api/v1/posaccount";
            CHANGE_POS_PIN_URL = TEST_BASE_URL +"api/v1/posaccount/update";
            CHANGE_PASSWORD_URL = TEST_BASE_URL +"api/v1/password";
            DELETE_POS_URL = TEST_BASE_URL + "api/v1/posaccount?username=";

            //    FUNDS TRANSFER ENDPOINTS
            ACCOUNT_NAME_URL = TEST_BASE_URL +"api/v1/verify/";
            FUNDS_TRANSFER_PAYMENT_URL = TEST_BASE_URL +"api/v1/fundstransfer";
            FUNDS_REQUERY_URL = TEST_BASE_URL +"api/v1/transaction/detail";
            BANK_LIST_URL = TEST_BASE_URL + "api/v1/transfer/banks";

            // PURSE ENDPOINTS
            PURSE_BALANCE_URL = TEST_BASE_URL +"api/v1/purse/balance";
            VERIFY_AGENT_URL = TEST_BASE_URL +"api/v1/transfer/agent/verify";
            AGENT_TRANSFER_URL = TEST_BASE_URL +"api/v1/transfer";

            //    CARD TRANSACTION ENDPOINT
            LOG_CARD_TRANSACTION_URL = TEST_BASE_URL +"api/v1/transaction/card";

            // IBETA SYNC ENDPOINT
            IBETA_TRANSACTION_URL = "https://ibeta.paypad.com.ng/paypad/webapi/v1/logtransactions";

            //    CONVENIENCE FEE URL
            CONVENIENCE_FEE_URL = TEST_BASE_URL +"api/v1/convenience/fee";

            //    DSTV ENDPOINTS
            DSTV_INQUIRY_URL = TEST_BASE_URL +"api/v1/dstv/inquiry";
            UNIFIED_DSTV_URL = TEST_BASE_URL +"api/v1/unified/dstv/inquiry";
            DSTV_PRODUCT_AND_ADDON_URL = TEST_BASE_URL +"api/v1/dstv/product";
            DSTV_PAYMENT_URL = TEST_BASE_URL +"api/v1/dstv/advice";

            //    GOTV ENDPOINTS
            GOTV_INQUIRY_URL = TEST_BASE_URL +"api/v1/gotv/inquiry";
            UNIFIED_GOTV_URL = TEST_BASE_URL +"api/v1/unified/gotv/inquiry";
            GOTV_PLAN_URL = TEST_BASE_URL +"api/v1/gotv/product";
            GOTV_PAYMENT_URL = TEST_BASE_URL +"api/v1/gotv/advice";

            //    PAYU ENDPOINTS
            PAYU_PAYMENT = TEST_BASE_URL +"api/v1/payu/payment";
            PAYU_UPGRADE = TEST_BASE_URL +"api/v1/payu/upgrade";

            //    STARTIMES URL
            STARTIMES_INQUIRY_URL = TEST_BASE_URL +"api/v1/startimes/inquiry";
            STARTIMES_PAYMENT_URL = TEST_BASE_URL +"api/v1/startimes/advice";

            // FRSC

            FRSC_INQUIRY_URL = TEST_BASE_URL +"api/v1/frsc/reference";
            FRSC_PAYMENT_URL = TEST_BASE_URL +"api/v1/frsc/pay";

            // LOAN

            ELIGIBILITY_URL = TEST_BASE_URL +"api/v1/loan/checkeligibility";
            PICTURE_PLACEHOLDER = "http://placehold.it/300x300";
            LOAN_URL = TEST_BASE_URL +"api/v1/loan/request";

            // PUSH NOTIFICATIONS ENDPOINT

            ONE_SIGNAL_APP_ID = "ffadb1a9-b314-4494-b663-0bc40fc18c1d";
            NEW_ONE_SIGNAL_APP_ID = "351526fd-4f32-425f-8101-92b406249bcf";
            ANONYMOUS_APP_ID = "bdd6c22b-baf7-4b6c-8dc8-3e4e0e7a3d3b";
            PLAYER_ID_URL = TEST_BASE_URL +"api/v1/agent/playerid";

            // AIRTIME VENDING
            VAS_LIST_URL = TEST_BASE_URL +"api/v1/airtime/fetch";
            VAS_PAY_URL = TEST_BASE_URL +"api/v1/airtime/vend";
            POWER_URL = TEST_BASE_URL +"api/v1/power/billers";
            ELECTRICITY_INQUIRY_URL = TEST_BASE_URL +"api/v1/power/inquiry";
            PAY_POWER_URL = TEST_BASE_URL +"api/v1/power/pay";
            REQUERY_POWER_URL = LIVE_BASE_URL +"api/v1/power/requery?transactionId=";


            // WITHDRAWAL LIMITS

            WITHDRAWAL_LIMIT = 40000.00;
            AGENT_FEE_LIMIT = 2000.00;
            WITHDRAWAL_PLUS_AGENT_FEE_LIMIT = 41000.00;

            // MISCELLANOUS

            TIN_PAYMENT_URL = "https://paycentre.paypad.com.ng:8000/api/v1/billpayment/tin";
            KEYS_REDOWNLOAD_URL = TEST_BASE_URL +"api/v1/terminal/reactivate";
            SERVER_TIME_URL = TEST_BASE_URL +"api/v1/time";
            PLAYSTORE_VERSIONING_URL = TEST_BASE_URL +"api/v1/app/version";
            PORT_CHANGE_URL = "https://logger.paypad.com.ng/nibssportchange/getparams";
            TEST_SERVER = "http://206.189.204.54:8000";
            LIVE_SERVER = "https://paycentre.paypad.com.ng";
            APP_VERSION = "2.0";
            SELECTED_BANK = "ESL";
            ONE_SIGNAL_API_KEY = "android_sdk-90bcae0b73d7ddca11dec0b24028f56e4c6e717a";
            ONE_SIGNAL_APP_ID = "u4o9lizg";
            TERMS_AND_CONDITIONS_URL = "https://paycentre-terms.firebaseapp.com/";
            LOAN_TERMS_AND_CONDITIONS_URL = "https://paycentre-loans-terms.firebaseapp.com/";

        }
    }

    public String getEnviromentUrl(String type){

        switch (type){
            case "activation":
                url = ACTIVATION_URL;
                break;
            case "adminLogin":
                url = AUTHENTICATION_URL;
                break;
            case "createPos":
                url = POS_ACCOUNT_URL;
                break;
            case "fetchPos":
                url = FETCH_POS_ACCOUNT_URL;
                break;
            case "changePosPin":
                url = CHANGE_POS_PIN_URL;
                break;
            case "changeAdminPassword":
                url = CHANGE_PASSWORD_URL;
                break;

            case "electricityInquiryUrl":
                url = ELECTRICITY_INQUIRY_URL;
                break;
            case "REQUERYPOWER":
                url = REQUERY_POWER_URL;
                break;


            case "payPowerUrl":
                url = PAY_POWER_URL;
                break;

            case "deletePos":
                url = DELETE_POS_URL;
                break;
            case "fundsTransferNameVerification":
                url = ACCOUNT_NAME_URL;
                break;
            case "fundsTransferPayment":
                url = FUNDS_TRANSFER_PAYMENT_URL;
                break;
            case "fundsTransferRequery":
                url = FUNDS_REQUERY_URL;
                break;
            case "bankList":
                url = BANK_LIST_URL;
                break;
            case "purseBalance":
                url = PURSE_BALANCE_URL;
                break;
            case "verifyAgentAccount":
                url = VERIFY_AGENT_URL;
                break;
            case "agentTransferPayment":
                url = AGENT_TRANSFER_URL;
                break;
            case "paycentreTransaction":
                url = LOG_CARD_TRANSACTION_URL;
                break;
            case "ibetaTransaction":
                url = IBETA_TRANSACTION_URL;
                break;
            case "convenienceFee":
                url = CONVENIENCE_FEE_URL;
                break;
            case "dstvInquiry":
                url = DSTV_INQUIRY_URL;
                break;
            case "unifiedDstvUrl":
                url = UNIFIED_DSTV_URL;
                break;
            case "dstvPlan":
                url = DSTV_PRODUCT_AND_ADDON_URL;
                break;
            case "dstvPayment":
                url = DSTV_PAYMENT_URL;
                break;
            case "gotvInquiry":
                url = GOTV_INQUIRY_URL;
                break;
            case "unifiedGotv":
                url = UNIFIED_GOTV_URL;
                break;
            case "gotvPlan":
                url = GOTV_PLAN_URL;
                break;
            case "gotvPayment":
                url = GOTV_PAYMENT_URL;
                break;
            case "payuPayment":
                url = PAYU_PAYMENT;
                break;
            case "payuUpgrade":
                url = PAYU_UPGRADE;
                break;
            case "startimesInquiry":
                url = STARTIMES_INQUIRY_URL;
                break;
            case "startimesPayment":
                url = STARTIMES_PAYMENT_URL;
                break;
            case "frscInquiry":
                url = FRSC_INQUIRY_URL;
                break;
            case "frscPayment":
                url = FRSC_PAYMENT_URL;
                break;
            case "eligibility":
                url = ELIGIBILITY_URL;
                break;
            case "picturePlaceholder":
                url = PICTURE_PLACEHOLDER;
                break;
            case "loan":
                url = LOAN_URL;
                break;
            case "oneSignalAppId":
                url = ONE_SIGNAL_APP_ID;
                break;
            case "newOneSignalAppId":
                url = NEW_ONE_SIGNAL_APP_ID;
                break;
            case "anonymousAppId":
                url = ANONYMOUS_APP_ID;
                break;
            case "playerId":
                url = PLAYER_ID_URL;
                break;
            case "vasList":
                url = VAS_LIST_URL;
                break;
            case "airtimePayment":
                url = VAS_PAY_URL;
                break;
            case "powerUrl":
                url = POWER_URL;
                break;

            case "tinPayment":
                url = TIN_PAYMENT_URL;
                break;
            case "keysDownload":
                url = KEYS_REDOWNLOAD_URL;
                break;
            case "serverTime":
                url = SERVER_TIME_URL;
                break;
            case "playstoreVersioning":
                url = PLAYSTORE_VERSIONING_URL;
                break;
            case "portChange":
                url = PORT_CHANGE_URL;
                break;
            case "testServer":
                url = TEST_SERVER;
                break;
            case "liveServer":
                url = LIVE_SERVER;
                break;
            case "appVersion":
                url = APP_VERSION;
                break;
            case "selectedBank":
                url = SELECTED_BANK;
                break;
            case "oneSignalApiKey":
                url = ONE_SIGNAL_API_KEY;
                break;
            case "termsAndCondition":
                url = TERMS_AND_CONDITIONS_URL;
                break;
            case "loanTermsAndConditions":
                url = LOAN_TERMS_AND_CONDITIONS_URL;
                break;
            case "refreshToken":
                url = TOKEN_URL;
                break;
        }

        return url;
    }

    public static String getAccessToken(Context context){
        return "Bearer " +sharedPreferenceManager.getString(context, "accessToken", "");
    }
}
