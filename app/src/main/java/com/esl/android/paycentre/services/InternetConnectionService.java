package com.esl.android.paycentre.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetConnectionService {

    public static boolean doInternetConnectionCheck(Context context){

        boolean state = false;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED) {

            state = true;
        }

        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (networkInfo.isConnected()) {
            state = true;
        }

        return state;

    }


}
