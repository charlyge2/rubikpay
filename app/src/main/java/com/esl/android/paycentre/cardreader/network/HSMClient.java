package com.esl.android.paycentre.cardreader.network;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class HSMClient extends IntentService {

    public static final int UPDATE_PROGRESS = 2;
    public static final int UPDATE_FAILED = 3;

    private PinpadFacade pinpadFacade;

    public HSMClient() {
        super("HSMClient");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        ResultReceiver receiver = (ResultReceiver) intent
                .getParcelableExtra("receiver");

        try {

            System.out.println("Start here");
            SSLContext context = SSLContext.getInstance("TLS");

            KeyStore keyStore = null;

            // This is needed for non ics phones
            String nonICS = "/system/etc/security/cacerts.bks";

            try {

                keyStore = KeyStore.getInstance("BKS");
                FileInputStream inputStream = new FileInputStream(nonICS);
                keyStore.load(inputStream, null);

            } catch (Exception ex) {
                try {
                    // Enters here if OS is ICS
                    keyStore = KeyStore.getInstance("AndroidCAStore");
                    keyStore.load(null, null);
                } catch (Exception ex1) {

                }

            }

            TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
            tmf.init(keyStore);

            context.init(null, tmf.getTrustManagers(), null);

            SSLSocketFactory socketFactory = context.getSocketFactory();

            ReferenceList.config = getSharedPreferences(ReferenceList.preference, 0);

            String ip = ReferenceList.config.getString(ReferenceList.hsmIp, "");
            int port = ReferenceList.config.getInt(ReferenceList.hsmPort, 0);

            SSLSocket socket = (SSLSocket) socketFactory.createSocket(ip, port);

            socket.startHandshake();

            socket.setSoTimeout(120 * 1000);

            if (socket.isConnected()) {
                System.out.println("Client is connected successfully");
            }

            PinpadKeysRequestProto.PinpadKeysRequest.Builder builder = PinpadKeysRequestProto.PinpadKeysRequest
                    .newBuilder();

            String preferencesName = ReferenceList.preference;
            MiscUtils.initContext(this);

            String terminalId = MiscUtils.getFromSharedPreferences(
                    preferencesName, ReferenceList.terminalId, "");
            String srn = MiscUtils.getFromSharedPreferences(preferencesName,
                    ReferenceList.pinpadSerialNumber, "");

            System.out.println(terminalId + " " + srn);

            builder.setTerminalId(terminalId);
            builder.setSerialNo(srn);
            builder.setVas(false);

            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();

            builder.build().writeDelimitedTo(outputStream);

            PinpadKeysResponseProto.PinpadKeysResponse response = PinpadKeysResponseProto.PinpadKeysResponse
                    .parseDelimitedFrom(inputStream);

            if (response == null || response.getStatus() == false) {

                Bundle resultData = new Bundle();
                receiver.send(UPDATE_FAILED, resultData);

            } else {

                System.out.println("BDK key: " + response.getBdk());
                System.out.println("Data key: " + response.getDataKey());
                System.out.println("Data key TR-31: " + response.getDataTr31());
                System.out.println("DUPKT key : " + response.getDupktTr31());
                System.out.println("Pin key : " + response.getTmkTr31());
                System.out.println("Pin key1 : " + response.getPinKey());
                System.out.println("TSK: " + response.getTsk());
                System.out.println("Parameter: " + response.getParameter());

                String dupkt = response.getDupktTr31();
                String tmk = response.getTmkTr31();
                String data = response.getDataTr31();
                String tsk = response.getTsk().replace(" ", "");
                String tpk = response.getPinKey().replace(" ", "");

                System.out.println("Before getParameter ");
                parseField62(response.getParameter());
                System.out.println("After getParameter ");

                ReferenceList.config = getSharedPreferences(
                        ReferenceList.preference, 0);

                SharedPreferences.Editor editor = ReferenceList.config.edit();


                editor.putString(ReferenceList.TSK, tsk);
                editor.putString(ReferenceList.TPK, tpk);
                //TO DO: check correct data key for new pinpad
                editor.putString(ReferenceList.bdk, response.getBdk());
                editor.putString(ReferenceList.dataKey, response.getDataKey());

                editor.putString(ReferenceList.TR31dupktKey, dupkt);
                editor.putString(ReferenceList.TR31tmkKey, tmk);
                editor.putString(ReferenceList.TR31dataKey, data);

                editor.putString(ReferenceList.acceptorID, Globals.acceptorID);
                editor.putString(ReferenceList.acceptorName,
                        Globals.acceptorName);
                editor.putString(ReferenceList.merchantType,
                        Globals.merchantType);

                if (Globals.redownloading == false) {

                    editor.putBoolean(ReferenceList.nibssKeysDownloaded, true);
                }



                editor.apply();

//                pinpadFacade = new PinpadFacade(getApplicationContext());
//
//                pinpadFacade.doKeyLoad();

                Bundle resultData = new Bundle();
                receiver.send(UPDATE_PROGRESS, resultData);
            }

        } catch (SocketTimeoutException e) {

            Bundle resultData = new Bundle();
            receiver.send(UPDATE_FAILED, resultData);
            e.printStackTrace();

        } catch (Exception e) {
            Bundle resultData = new Bundle();
            receiver.send(UPDATE_FAILED, resultData);
            e.printStackTrace();

        }

    }

    public static void parseField62(String msg) {

        String message = msg;
        String[] subMsg = new String[8];

        do {

            int i = 0;

            String value = message.substring(2, 5);
            int lengthValue = Integer.parseInt(value);
            subMsg[i] = message.substring(5, 5 + lengthValue);

            switch (message.substring(0, 2)) {
                case "02":
                    // System.out.print("Time and date: ");
                    // System.out.println(subMsg[i]);
                    break;
                case "03":
                    Globals.acceptorID = subMsg[i];
                    break;
                case "04":
                    System.out.print("Timeout: ");
                    System.out.println(subMsg[i]);
                    break;
                case "05":
                    System.out.print("Currency Code: ");
                    System.out.println(subMsg[i]);
                    break;
                case "06":
                    System.out.print("Currency Code: ");
                    System.out.println(subMsg[i]);
                    break;
                case "07":
                    System.out.print("Call Timeout: ");
                    System.out.println(subMsg[i]);
                    break;
                case "08":
                    Globals.merchantType = subMsg[i];
                    break;
                case "52":
                    Globals.acceptorName = subMsg[i];
                    break;
            }
            message = message.substring(5 + lengthValue);

            i++;
        } while (!message.isEmpty());

    }

}
