package com.esl.android.paycentre.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.ArrayList;
import java.util.List;

public class RecentTransactionAdapter extends ArrayAdapter<RecentTransactionList> {

    private Context mcontext;
    private List<RecentTransactionList> recentTransactionList = new ArrayList<>();

    public RecentTransactionAdapter(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<RecentTransactionList> list) {
        super(context, 0 , list);
        mcontext = context;
        recentTransactionList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mcontext).inflate(R.layout.recent_transaction_template,parent,false);

        RecentTransactionList recentTransaction = recentTransactionList.get(position);

        TextView name = (TextView) listItem.findViewById(R.id.transactionName);
        name.setText(recentTransaction.getTransactionName());

        TextView time = (TextView) listItem.findViewById(R.id.transactionTime);
        time.setText(recentTransaction.getTime());

        TextView amount = (TextView) listItem.findViewById(R.id.transactionAmount);

        if(recentTransaction.getResponseCode().equals("00")){
            amount.setText(recentTransaction.getTransactionAmount());
            amount.setTextColor(Color.parseColor("#00B653"));
        }else{
            amount.setText(recentTransaction.getTransactionAmount());
            amount.setTextColor(Color.parseColor("#B6042F"));
        }
        TextView datee = (TextView) listItem.findViewById(R.id.transactionDate);
        datee.setText(recentTransaction.getTransactionDate());

        TextView status = (TextView) listItem.findViewById(R.id.transactionStatus);
        if(recentTransaction.getResponseCode().equals("00")){
            status.setText(recentTransaction.getTransactionStatus());
            status.setTextColor(Color.parseColor("#00B653"));
        }else{
            status.setText(recentTransaction.getTransactionStatus());
            status.setTextColor(Color.parseColor("#B6042F"));
        }

        return listItem;
    }

    @Override
    public int getCount() {
        if(recentTransactionList==null){
            return 0;
        }
        else if(recentTransactionList.size()>15){
            return 15;
        }
            return recentTransactionList.size();
    }
}
