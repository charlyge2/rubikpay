package com.esl.android.paycentre.controllers.activationControllers;

import android.content.Context;
import android.text.TextUtils;

import com.esl.android.paycentre.services.InternetConnectionService;

public class ActivationController {

    public String validateActivation(final Context context, final String activationCode){

        if(TextUtils.isEmpty(activationCode)){

            return  "Please Enter Activation Code";

        }else if(activationCode.length() != 8){

            return  "Activation Code Must be 8 digits";

        }else if(!InternetConnectionService.doInternetConnectionCheck(context)){

            return "Please Check Your Internet Connection";

        }else{

            return "valid";

        }

    }
}
