package com.esl.android.paycentre.activities.notifications;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.adapters.Notification;
import com.esl.android.paycentre.adapters.NotificationAdapter;
import com.esl.android.paycentre.database.repo.NotificationRepo;
import com.esl.android.paycentre.database.repo.PosAccountRepo;
import com.esl.android.paycentre.utils.TransactionFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class NotificationListActivity extends BaseActivity {

    //a list to store all the NotificationModels
    List<Notification> notificationList;

    private PosAccountRepo posAccountRepo = new PosAccountRepo();

    private NotificationRepo notificationRepo = new NotificationRepo();

    //the recyclerview
    RecyclerView recyclerView;

    private View.OnClickListener onNotificationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Step 4 of 4: Finally call getTag() on the view.
            // This viewHolder will have all required values.
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            Notification thisItem = notificationList.get(position);

            String title = thisItem.getTitle();
            String message = thisItem.getShortdesc();
            String time = thisItem.getTime();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NotificationListActivity.this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);

        //getting the recyclerview from xml
        recyclerView = findViewById(R.id.notificationRecycleView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

        //initializing the notificationList
        notificationList = new ArrayList<>();

        Iterator<com.esl.android.paycentre.database.model.Notification> notification = notificationRepo.getNotifications();

        while(notification.hasNext()) {

            com.esl.android.paycentre.database.model.Notification message = notification.next();
            Date date = message.getDate();
            String dateTime = TransactionFormatter.formatDate(date) + " " + TransactionFormatter.formatTime(date);

            notificationList.add(
                    new Notification(
                            message.getId(),
                            message.getTitle(),
                            message.getMessage(),
                            dateTime));
        }

        if(notificationList.isEmpty()){
            notificationList.add(
                    new Notification(
                            1,
                            "Push Notifications",
                            "Welcome to Rubikcube push notifications.All messages sent from Rubikcube will appear here subsequently. You dont have to miss any update again. You can click on each message to view the details. Thank you for using Rubikcube",
                            ""));
        }

        NotificationAdapter adapter = new NotificationAdapter(this, notificationList);
        NotificationAdapter.setOnItemClickListener(onNotificationClickListener);
        recyclerView.setAdapter(adapter);
    }
}
