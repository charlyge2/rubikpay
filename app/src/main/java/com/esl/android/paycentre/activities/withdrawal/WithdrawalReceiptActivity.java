package com.esl.android.paycentre.activities.withdrawal;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.activities.dispute.CreateDisputeActivity;
import com.esl.android.paycentre.cardreader.serializers.Receipt;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.services.RetrySaveTransToDbService;
import com.esl.android.paycentre.services.SyncService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;

public class WithdrawalReceiptActivity extends BaseActivity {

    private TextView amountTextView;
    private TextView responseTextView;
    private TextView rrnTextView;
    private TextView stanTextView;
    private TextView panTextView;
    private TextView terminalIdTextView;
    private TextView transactionIdTextView;
    private TextView dateTextView;
    private TextView transactionTypeTextView;
    private TextView versionTextView;
    private TextView responseCodeTextView;
    private ImageView responseImageView;
    private LinearLayout optionsLayout;
    private Button proceedButton;
    private Button downloadBtn;
    private Button sendBtn;

    private Button downloadButton;
    private Button sendButton;
    private ImageView downloadImageView;
    private ImageView sendImageView;
    private TextView logDisputeTextView;
    private static SharedPreferenceManager sharedPreferenceManager;

    private AlertDialog alertDialog = null;
    private boolean isPending = false;
    String responseCode = "";
    String response = "";
    String amount = "";
    String rrn = "";
    String stan = "";
    String pan = "";
    String terminalId = "";
    String transactionId = "";
    String date = "";
    String transactionType = "";
    String version = "";
    String accountType = "";
    String aid = "";
    String authCode = "";
    String cardHolder = "";
    String expiry = "";
    String tenderType = "";
    String sequenceNo = "";

    public static final String TAG = "Permissions";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal_receipt);
        Intent intent = getIntent();
        Receipt receipt = intent.getParcelableExtra("receipt");

        accountType = receipt.getAccountType();
        aid = receipt.getAid();
        authCode = receipt.getAuthenticationCode();
        cardHolder = receipt.getCardholder();
        expiry = receipt.getCardExpiry();
        tenderType = receipt.getTenderType();
        sequenceNo = receipt.getSequenceNumber();
        responseCode = receipt.getResponseCode();
        response = receipt.getResponse();
        amount = receipt.getAmount();
        rrn = receipt.getRrn();
        stan = receipt.getStan();
        pan = receipt.getPan();
        terminalId = receipt.getTerminalId();
        transactionId = receipt.getTransactionId();
        date = receipt.getTransactionDate();
        transactionType = receipt.getTransactionType();
        version = receipt.getApplicationVersion();

        amountTextView = findViewById(R.id.amountTextView);
        responseTextView = findViewById(R.id.responseTextView);
        rrnTextView = findViewById(R.id.rrnTextView);
        stanTextView = findViewById(R.id.stanTextView);
        panTextView = findViewById(R.id.panTextView);
        terminalIdTextView = findViewById(R.id.terminalIdTextView);
        transactionIdTextView = findViewById(R.id.transactionIdTextView);
        dateTextView = findViewById(R.id.dateTextView);
        transactionTypeTextView = findViewById(R.id.transactionTypeTextView);
        versionTextView = findViewById(R.id.versionTextView);
        optionsLayout = findViewById(R.id.optionsLayout);
        responseCodeTextView = findViewById(R.id.responseCodeTextView);

        responseImageView = findViewById(R.id.responseImageView);
        updateTransaction();

        if (Globals.isValueDifferent) {
            confirmAmount();
        }

    }

    private void confirmAmount() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Alert");
        alert.setIcon(android.R.drawable.ic_dialog_info);
        SpannableStringBuilder builder = new SpannableStringBuilder();
        RelativeSizeSpan sizeSpan = new RelativeSizeSpan(2.0f);
        builder.append("Please confirm cashout amount is: \n")
                .append("₦ " + amount + "0", sizeSpan, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        alert.setMessage(builder);

        alert.setCancelable(false);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            takeScreenshot();
        }
    }

    private void init() {

        if (isPending) {
            responseImageView.setImageResource(R.drawable.ic_pending);
            amountTextView.setTextColor(getResources().getColor(R.color.colorBlack));
            responseTextView.setTextColor(getResources().getColor(R.color.apply));
        } else if ("00".equals(responseCode)) {

            responseImageView.setImageResource(R.drawable.ic_check);
            amountTextView.setTextColor(getResources().getColor(R.color.colorBlack));
            responseTextView.setTextColor(getResources().getColor(R.color.apply));

        } else {
            responseImageView.setImageResource(R.drawable.ic_error);
            amountTextView.setTextColor(getResources().getColor(R.color.colorBlack));
            responseTextView.setTextColor(getResources().getColor(R.color.reset));
        }


//        if (responseCode.equalsIgnoreCase("XX") || responseCode.equalsIgnoreCase("05")
//                || responseCode.equalsIgnoreCase("06") || responseCode.equalsIgnoreCase("91")
//                || responseCode.equalsIgnoreCase("96")) {
//
//            disputeLayout.setVisibility(View.VISIBLE);
//        }

        responseCodeTextView.setText(responseCode);
        if (Globals.isValueDifferent) {
            amountTextView.setText(String.format("₦%s0", amount));
        } else {
            amountTextView.setText(String.format("₦%s", amount));
        }
        if (isPending) {
            responseTextView.setText("PENDING");
        } else {
            responseTextView.setText(response);
        }

        rrnTextView.setText(rrn);
        stanTextView.setText(stan);
        panTextView.setText(pan);
        terminalIdTextView.setText(terminalId);
        transactionIdTextView.setText(transactionId);
        dateTextView.setText(date);
        transactionTypeTextView.setText(transactionType);
        versionTextView.setText(version);

        proceedButton = findViewById(R.id.proceedButton);
        proceedButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        revertGlobalVariables();
                        startActivity(new Intent(WithdrawalReceiptActivity.this, WithdrawalAmountActivity.class));
                    }
                }
        );

        optionsLayout.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(WithdrawalReceiptActivity.this);
                        // Get custom login form view.
                        final View loginFormView = getLayoutInflater().inflate(R.layout.transaction_receipt_layout, null);
                        // Set above view in alert dialog.
                        builder.setView(loginFormView);

                        builder.setCancelable(true);
                        alertDialog = builder.create();
                        alertDialog.show();

                        downloadBtn = alertDialog.findViewById(R.id.downloadBtn);
                        sendBtn = alertDialog.findViewById(R.id.sendBtn);
                        downloadBtn.setOnClickListener(
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        takeScreenshot();
                                        alertDialog.hide();
                                    }
                                }
                        );
                        sendBtn.setOnClickListener(
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        emailReceipt();
                                        alertDialog.hide();
                                    }
                                }
                        );
                    }
                }
        );


    }

    private void revertGlobalVariables() {
        Globals.rrn = "";
        Globals.stan = "";
        Globals.pan = "";
        Globals.transactionId = "";
        Globals.applicationVersion = "";
        Globals.transactionDate = "";
        Globals.databaseId = -1;
        Globals.convenienceFee = "";
    }

    public void dispute(View view) {

        Intent intent = new Intent(getApplicationContext(), CreateDisputeActivity.class);
        intent.putExtra("transactionId", transactionId);
        if (response.equalsIgnoreCase("FAILED")) {
            intent.putExtra("response", "1");
        } else {
            intent.putExtra("response", "0");
        }
        Globals.disputeType = "CASHOUT";
        startActivity(intent);
    }

    private void updateTransaction() {

        TransactionRepo transactionRepo = new TransactionRepo();

        Transaction transaction = transactionRepo.getTransaction((int) Globals.databaseId);

        transaction.setResponseCode(responseCode);
        transaction.setResponseMsg(response);

        //PAYCENTRE PARAMS
        transaction.set("rrn", rrn);
        transaction.set("stan", stan);
        transaction.set("pan", pan);
        transaction.set("accountType", accountType);
        transaction.set("aid", aid);
        transaction.set("authCode", authCode);
        transaction.set("cardHolder", cardHolder);
        transaction.set("expiry", expiry);
        transaction.set("tenderType", tenderType);
        transaction.set("sequenceNo", sequenceNo);
        transaction.set("statusCode", responseCode);
        transaction.set("vat", Globals.vat);
        transaction.set("statusMessage", response);
        transaction.set("version", MainActivity.getAppVersion(this));
        transaction.set("stampDuty", Globals.stampDuty);

        //IBETA PARAMS
        transaction.set("cardNo", pan);
        transaction.set("response", response);
        transaction.set("responseCode", responseCode);

        if (Globals.isValueDifferent) {
            transaction.setAmount(Float.parseFloat(String.valueOf(Globals.amount)));
            transaction.setConvenienceFee(Float.parseFloat(Globals.convenienceFee));
        }


        int updated = transactionRepo.update(transaction);

        if (updated < 1) {
            Log.d("TRANSACTION_UPDATE", "Transaction could not be updated " + updated);
            isPending = true;
            saveToFile(transaction);
            startService(new Intent(WithdrawalReceiptActivity.this, RetrySaveTransToDbService.class));
        }

        SyncService.syncTransaction(getApplicationContext(), transaction);
        init();

    }

    private void saveToFile(Transaction transaction) {
        try {

            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "PaycentreTrans";
            File myDir = new File(root);
            myDir.mkdirs();
            String fname = "PaycentreTrans-" + Globals.transactionId + ".txt";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            FileOutputStream fileOut =
                    new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(transaction);
            out.close();
            fileOut.close();
            Log.d("saveToFile", "Serialized data is saved  ");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    private void takeScreenshot() {

        isStoragePermissionGranted();

        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            //Environment.
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpeg";
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + "Camera/Paycentre";
            File myDir = new File(root);
            myDir.mkdirs();
            String fname = "Image-" + Globals.transactionId + ".jpeg";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            FileOutputStream outputStream = new FileOutputStream(file);

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            //FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            MediaScannerConnection.scanFile(getApplicationContext(), new String[]{file.getPath()}, new String[]{"image/jpeg"}, null);

            Alert.showSuccess(getApplicationContext(), "Screenshot downloaded successfully");

        } catch (Throwable e) {

            Alert.showFailed(getApplicationContext(), "Screenshot could not be saved");
            e.printStackTrace();
        }
    }

    private void emailReceipt() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Email Receipt");
        alert.setMessage("Enter Customer email");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                String value = input.getText().toString();

                try {

                    JSONObject emailPayload = new JSONObject();

                    emailPayload.put("transactionId", Globals.transactionId);
                    emailPayload.put("email", value);

                    HttpService.sendEmail(WithdrawalReceiptActivity.this, emailPayload);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();

    }

    public void newTransactionn(View view) {
        Intent intent = new Intent(getApplicationContext(), WithdrawalAmountActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        revertGlobalVariables();
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }

}
