package com.esl.android.paycentre.interfaces;

import org.json.JSONArray;

public interface PlanInterface {
    public void sendPlans(JSONArray products);
}
