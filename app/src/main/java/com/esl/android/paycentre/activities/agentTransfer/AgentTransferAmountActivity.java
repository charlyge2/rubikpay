package com.esl.android.paycentre.activities.agentTransfer;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.controllers.AgentTransferController;
import com.esl.android.paycentre.interfaces.ConvenienceFeeInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;
import com.esl.android.paycentre.utils.NumberTextWatcher;

import org.json.JSONObject;

import java.text.DecimalFormat;

public class AgentTransferAmountActivity extends BaseActivity implements ConvenienceFeeInterface {

    private Button agentTransferBtn;

    private EditText agentTransferAmount;

    private String amount;

    private String formattedAmount;

    private DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_transfer_amount);

        agentTransferAmount = findViewById(R.id.agtTransAmount);
        agentTransferAmount.addTextChangedListener(new NumberTextWatcher());
        agentTransferBtn = findViewById(R.id.agentTransferBtn);
        agentTransferBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               amount = agentTransferAmount.getText().toString();

               String result = AgentTransferController.validateAmount(getApplicationContext(),amount);

               if(result.equals("Empty")){
                   Alert.showWarning(getApplicationContext(),"Please Enter Amount to Proceed");
               }else if(result.equals("Valid")){

                   formattedAmount = amount.replace(",","");

                   if(Double.parseDouble(formattedAmount) > 250000){
                       Alert.showWarning(getApplicationContext(),"The maximum limit for agent transfer is ₦250,000" );
                       return;
                   }

                   try{

                       JSONObject jsonObject = new JSONObject();
                       jsonObject.put("amount",formattedAmount);
                       jsonObject.put("transactionType","Agent Transfer");

                       HttpService.fetchConvenienceFee(AgentTransferAmountActivity.this,jsonObject);

                   }catch (Exception e){
                       e.printStackTrace();
                   }

               }

            }
        });

    }

    @Override
    public void sendConvenienceFee(String fee) {

    }

    @Override
    public void sendConvenienceFee(String fee, String StampDuty, boolean showStampDuty, String vat, boolean showVat) {
        HttpService.progressDialog.dismiss();

        Intent intent = new Intent(getApplicationContext(), AgentTransferPreviewActivity.class);
        intent.putExtra("amount",amount);
        intent.putExtra("convenienceFee",fee);
        intent.putExtra("showVat", showVat);
        intent.putExtra("vat", vat);
        startActivity(intent);
    }

    public void goBack(View view) {
        Navigator.moveToDashboard(AgentTransferAmountActivity.this);
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(AgentTransferAmountActivity.this);
    }
}

