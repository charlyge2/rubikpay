package com.esl.android.paycentre.interfaces;

public interface NameLookupInterface {
    public void sendAccountDetails(String customerName,String invoicePeriod,String customerNumber);
}
