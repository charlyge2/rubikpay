package com.esl.android.paycentre.activities.loans;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.adapters.TransactionHistory;
import com.esl.android.paycentre.adapters.TransactionHistoryAdapter;
import com.esl.android.paycentre.utils.TransactionFormatter;

import java.util.ArrayList;
import java.util.List;

public class LoanHistory extends AppCompatActivity {

    List<TransactionHistory> transactionHistoryList;

    //the recyclerview
    RecyclerView recyclerView;


    private View.OnClickListener onNotificationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Step 4 of 4: Finally call getTag() on the view.
            // This viewHolder will have all required values.
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            TransactionHistory history = transactionHistoryList.get(position);

            String amount = history.getTransactionAmount();
            String transactionTime = history.getTransactionTime();
            String transactionName = history.getTransactionName();
            String transactionStatus = history.getTransactionStatus();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoanHistory.this);
            alertDialogBuilder.setTitle("Loan Details");
            alertDialogBuilder
                    .setMessage("\nAmount : " +amount + "\n\nStatus : " +transactionStatus + "\n\nDate : "+transactionTime + "\n\nTransaction : "+transactionName)
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_history);

        //getting the recyclerview from xml
        recyclerView = (RecyclerView) findViewById(R.id.loanHistoryRecycleView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

        //initializing the transactionHistoryList
        transactionHistoryList = new ArrayList<>();

        transactionHistoryList.add(new TransactionHistory(
                "",
                "Loan",
                "FAILED",
                "₦16,000.00",
                TransactionFormatter.generateAppTime(),
                "04"));

        transactionHistoryList.add(new TransactionHistory(
                "",
                "Loan",
                "SUCCESSFUL",
                "₦200,000.00",
                TransactionFormatter.generateAppTime(),
                "00"));

        transactionHistoryList.add(new TransactionHistory(
                "",
                "Loan",
                "PENDING",
                "₦10,000.00",
                TransactionFormatter.generateAppTime(),
                "XY"));

        transactionHistoryList.add(new TransactionHistory(
                "",
                "Loan",
                "SUCCESSFUL",
                "₦10,000.00",
                TransactionFormatter.generateAppTime(),
                "00"));

        transactionHistoryList.add(new TransactionHistory(
                "",
                "Loan",
                "FAILED",
                "₦10,000.00",
                TransactionFormatter.generateAppTime(),
                "04"));

        transactionHistoryList.add(new TransactionHistory(
                "",
                "Loan",
                "SUCCESSFUL",
                "₦1,000.00",
                TransactionFormatter.generateAppTime(),
                "00"));

        transactionHistoryList.add(new TransactionHistory(
                "",
                "Loan",
                "PENDING",
                "₦10,500.00",
                TransactionFormatter.generateAppTime(),
                "XY"));

        TransactionHistoryAdapter adapter = new TransactionHistoryAdapter(this, transactionHistoryList);
        TransactionHistoryAdapter.setOnItemClickListener(onNotificationClickListener);
        recyclerView.setAdapter(adapter);
    }
}
