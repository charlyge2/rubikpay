package com.esl.android.paycentre.cardreader.serializers;


public enum EmvTransactionType {
	
	EmvGoods("00");
	
    
    private EmvTransactionType(final String text) {
        this.text = text;
    }

    private final String text;

    @Override
    public String toString() {
        return text;
    }

	

}
