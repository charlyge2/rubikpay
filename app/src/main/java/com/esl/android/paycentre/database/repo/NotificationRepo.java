package com.esl.android.paycentre.database.repo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.Notification;

import java.util.Iterator;

public class NotificationRepo {

    public static String createTable(){
        return "CREATE TABLE " + Notification.TABLE  + "("
                + Notification.KEY_ID  + " INTEGER PRIMARY KEY,"
                + Notification.KEY_TITLE + " TEXT,"
                + Notification.KEY_MESSAGE + " TEXT,"
                + Notification.KEY_TYPE + " TEXT,"
                + Notification.KEY_DATE + " TEXT)";
    }

    public int insert(Notification Notification) {
        int insertId;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        ContentValues values = new ContentValues();

        values.put(Notification.KEY_TITLE, Notification.getTitle());
        values.put(Notification.KEY_MESSAGE, Notification.getMessage());
        values.put(Notification.KEY_TYPE, Notification.getType());
        values.put(Notification.KEY_DATE, Notification.getDateLong());

        // Inserting Row
        insertId = (int) db.insert(Notification.TABLE, null, values);
        DatabaseManager.getInstance().closeDatabase();

        return insertId;

    }

    private void isEmpty(){
//        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
//        String count = "SELECT count(*) FROM table";
//        Cursor mcursor = db.rawQuery(count, null);
//        mcursor.moveToFirst();
//        int icount = mcursor.getInt(0);
//        if(icount>0){
//
//        }else{
//
//        }
    }

    public void delete( ) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        db.delete(Notification.TABLE,null,null);
        DatabaseManager.getInstance().closeDatabase();
    }

    private void deleteStaleNotifications() {
    }

    public Iterator<Notification> getNotifications() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        final Cursor cursor = db.query(Notification.TABLE, null, null, null, null, null, Notification.KEY_ID + " DESC", "10");

        if(cursor.getCount() > 10 ) {
            this.deleteStaleNotifications();
        }

        return new Iterator<Notification>() {

            boolean hasNext = !this.isEmpty();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public Notification next() {
                return this.getNotification();
            }

            private boolean isEmpty() {
                return !cursor.moveToFirst();
            }

            private Notification getNotification() {
                if(hasNext) {
                    Notification notification = new Notification();
                    notification.setId(cursor.getInt(cursor.getColumnIndex(Notification.KEY_ID)));
                    notification.setTitle(cursor.getString(cursor.getColumnIndex(Notification.KEY_TITLE)));
                    notification.setMessage(cursor.getString(cursor.getColumnIndex(Notification.KEY_MESSAGE)));
                    notification.setType(cursor.getString(cursor.getColumnIndex(Notification.KEY_TYPE)));
                    notification.setDateLong(cursor.getLong(cursor.getColumnIndex(Notification.KEY_DATE)));

                    if(!(hasNext = cursor.moveToNext())) {
                        cursor.close();
                        DatabaseManager.getInstance().closeDatabase();
                    }

                    return notification;
                }

                return null;
            }

        };
    }

}