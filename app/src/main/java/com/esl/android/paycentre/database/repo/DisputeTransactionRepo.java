package com.esl.android.paycentre.database.repo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.DisputeTransaction;

import java.util.Iterator;


public class DisputeTransactionRepo {

    public static String createTable(){
        return "CREATE TABLE " + DisputeTransaction.TABLE  + "("
                + DisputeTransaction.KEY_ID  + " INTEGER PRIMARY KEY,"
                + DisputeTransaction.KEY_TRANSID + " TEXT UNIQUE,"
                + DisputeTransaction.KEY_NAME + " TEXT,"
                + DisputeTransaction.KEY_PHONE + " TEXT,"
                + DisputeTransaction.KEY_STATUS + " TEXT,"
                + DisputeTransaction.KEY_DATE + " TEXT)";
    }

    public int insert(DisputeTransaction transaction) {
        int insertId;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues values = this.buildValues(transaction);

        // Inserting Row
        insertId = (int) db.insert(DisputeTransaction.TABLE, null, values);
        DatabaseManager.getInstance().closeDatabase();

        return insertId;

    }

    public int update(DisputeTransaction transaction) {

        int result;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        ContentValues values = this.buildValues(transaction);
        // Update Row
        result = db.update(DisputeTransaction.TABLE, values, DisputeTransaction.KEY_TRANSID + " = ? ",new String[]{transaction.getTransactionId()});
        DatabaseManager.getInstance().closeDatabase();

        return result;

    }

    private ContentValues buildValues(DisputeTransaction transaction) {
        ContentValues values = new ContentValues();

        values.put(DisputeTransaction.KEY_TRANSID, transaction.getTransactionId());
        values.put(DisputeTransaction.KEY_NAME, transaction.getName());
        values.put(DisputeTransaction.KEY_PHONE, transaction.getPhone());
        values.put(DisputeTransaction.KEY_STATUS, transaction.getStatus());
        values.put(DisputeTransaction.KEY_DATE, transaction.getDateLong());;

        return values;
    }

    private DisputeTransaction buildTransaction(Cursor cursor) {
        DisputeTransaction transaction = new DisputeTransaction();

        transaction.setId(cursor.getInt(cursor.getColumnIndex(DisputeTransaction.KEY_ID)));
        transaction.setTransactionId(cursor.getString(cursor.getColumnIndex(DisputeTransaction.KEY_TRANSID)));
        transaction.setName(cursor.getString(cursor.getColumnIndex(DisputeTransaction.KEY_NAME)));
        transaction.setPhone(cursor.getString(cursor.getColumnIndex(DisputeTransaction.KEY_PHONE)));
        transaction.setStatus(cursor.getString(cursor.getColumnIndex(DisputeTransaction.KEY_STATUS)));
        transaction.setDateLong(cursor.getLong(cursor.getColumnIndex(DisputeTransaction.KEY_DATE)));

        return transaction;
    }

    public DisputeTransaction getDispute(int id) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        DisputeTransaction transaction = null;
        Cursor cursor = db.query(DisputeTransaction.TABLE, null, DisputeTransaction.KEY_ID + " = ? ",new String[]{String.valueOf(id)}, null, null, null);

        if(cursor.moveToFirst()) {
            transaction = this.buildTransaction(cursor);
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();
        return transaction;
    }

    public Iterator<DisputeTransaction> getDisputes() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        final Cursor cursor = db.query(DisputeTransaction.TABLE, null, null, null, null, null, null);
        
        return new Iterator<DisputeTransaction>() {

            boolean hasNext = !this.isEmpty();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public DisputeTransaction next() {
                return this.getDisputeTransaction();
            }

            private boolean isEmpty() {
                return !cursor.moveToFirst();
            }

            private DisputeTransaction getDisputeTransaction() {

                if(hasNext) {

                    DisputeTransaction transaction = DisputeTransactionRepo.this.buildTransaction(cursor);

                    if(!(hasNext = cursor.moveToNext())) {
                        cursor.close();
                        DatabaseManager.getInstance().closeDatabase();
                    }

                    return transaction;
                }

                return null;
            }

        };
    }
}