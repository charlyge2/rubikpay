package com.esl.android.paycentre.activities.activation;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.controllers.activationControllers.FetchPosAccountController;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.repo.PosAccountRepo;
import com.esl.android.paycentre.services.FetchBankListService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;


public class  PosLoginActivity extends AppCompatActivity {

    private ArrayList<String> usernames = new ArrayList<>();

    private PosAccountRepo posAccountRepo = new PosAccountRepo();

    private boolean doubleBackToExitPressedOnce = false;

    private EditText pin;

    private FetchPosAccountController fetchPosAccountController;

    private SharedPreferenceManager sharedPreferenceManager;

    Spinner mySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        //getSupportActionBar().hide(); // hide the title back
        setContentView(R.layout.activity_pos_login);
        Intent intent = new Intent(this, FetchBankListService.class);
        startService(intent);


        pin = findViewById(R.id.loginPin);
        mySpinner = (Spinner) findViewById(R.id.dateSpinner);

        Iterator posaccounts = posAccountRepo.getPosAccounts();

        while(posaccounts.hasNext()) {
            PosAccount account = (PosAccount) posaccounts.next();
            //Toast.makeText(getApplicationContext(),account.getUsername() + " - " +account.getPassword() ,Toast.LENGTH_LONG).show();
            usernames.add(account.getUsername());
        }

        Spinner spinner = (Spinner) findViewById(R.id.dateSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, usernames);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(),""+adapterView.getItemAtPosition(i), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        SpannableString spannableString = new SpannableString("Click to login as administrator");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(getApplicationContext(), PosAdminLoginActivity.class);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(clickableSpan, 0, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = findViewById(R.id.adminLoginTextView);
        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    public void login(View view) {
        String stateCode = SharedPreferenceManager.getString(getApplicationContext(), "stateCode", "");
        if(stateCode == null ||TextUtils.isEmpty(stateCode)){
            Alert.showFailed(this,"Clear App data and Reactivate account");
            return;
        }
        String pospin = pin.getText().toString();

        if(TextUtils.isEmpty(pospin) || mySpinner.getCount() == 0){
            Alert.showWarning(getApplicationContext(),"Please Select Username and Pin");
            return;
        }

        String username = mySpinner.getSelectedItem().toString();

        sharedPreferenceManager.save(getApplicationContext(), "posAccountUsername", username);

        savePosAccount(username);

        try {

            posAccountRepo.authenticateAccount(username,pospin);

            String viewedNewWalkghthrough = sharedPreferenceManager.getString(getApplicationContext(),"viewedNewWalkthrough","false");

            Date date = new Date();
            long timeMilli = date.getTime();

            System.out.println("Time in milliseconds using Date class: " + timeMilli);

            String savedTime = sharedPreferenceManager.getString(getApplicationContext(), "savedTime", String.valueOf(timeMilli));
            int count = sharedPreferenceManager.getInt(getApplicationContext(), "count", 0);

            if(viewedNewWalkghthrough.equals("true")){
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), NewUpdateWalkthroughActivity.class);
                startActivity(intent);
            }

        }catch (Exception e){
            Alert.showFailed(getApplicationContext(),"Invalid Credentials");
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
        }

        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);

        Alert.showWarning(getApplicationContext(),"Click back again to exit");
    }

    private void savePosAccount(String username) {

        ReferenceList.config = this.getSharedPreferences(
                ReferenceList.preference, 0);
        SharedPreferences.Editor editor = ReferenceList.config.edit();
        editor.putString(ReferenceList.posAccountUsername, username);
        editor.apply();
    }
}
