package com.esl.android.paycentre.adapters;

import android.widget.ImageView;

public class BillList {

    private int billImage;

    private String billName;

    public BillList(int billImage,String billName) {
        this.billName = billName;
        this.billImage = billImage;
    }


    public String getBillName() {
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public void setBillImage(int billImage){
        this.billImage = billImage;
    }

    public int getBillImage(){
        return billImage;
    }

}
