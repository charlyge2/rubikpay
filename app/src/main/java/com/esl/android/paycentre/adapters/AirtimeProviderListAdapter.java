package com.esl.android.paycentre.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.ArrayList;
import java.util.List;

public class AirtimeProviderListAdapter extends ArrayAdapter<AirtimeProviderList> {


  private Context mcontext;
  private List<AirtimeProviderList> airtimeProviderList = new ArrayList<>();

  public AirtimeProviderListAdapter(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<AirtimeProviderList> list) {
    super(context, 0 , list);
    mcontext = context;
    airtimeProviderList = list;
  }


  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    View listItem = convertView;
    if(listItem == null)
      listItem = LayoutInflater.from(mcontext).inflate(R.layout.adapt,parent,false);

    AirtimeProviderList currentNetwork = airtimeProviderList.get(position);

    TextView name = (TextView) listItem.findViewById(R.id.name);
    name.setText(currentNetwork.getNetwork());

    ImageView image = (ImageView)listItem.findViewById(R.id.bluetooth);
    image.setImageResource(currentNetwork.getImageDrawable());

    return listItem;
  }


}


