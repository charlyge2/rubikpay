package com.esl.android.paycentre.activities.airtimeVending;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.utils.Alert;

import org.json.JSONObject;

import java.util.Date;

public class AirtimeResultActivity extends BaseActivity {

    private ImageView responseImage;

    private TextView responseText;

    private Button homeBtn;

    private String responseCode;

    private String responseMessage;

    private String transactionId;

    private String amount;

    private String convenienceFee;

    private String posAccount;

    private String total;

    private String appTime;

    private String transactionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aritime_result);

        responseImage = findViewById(R.id.responseImage);
        responseText = findViewById(R.id.transactionStatus);
        homeBtn = findViewById(R.id.homeBtn);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(getApplicationContext(),DashboardActivity.class);
               startActivity(intent);
            }
        });

        responseCode = getIntent().getStringExtra("responseCode");
        responseMessage = getIntent().getStringExtra("responseMessage");

        responseCode = getIntent().getStringExtra("responseCode");
        responseMessage = getIntent().getStringExtra("responseMessage");
        amount = getIntent().getStringExtra("amount");
        transactionId = getIntent().getStringExtra("transactionId");
        convenienceFee = getIntent().getStringExtra("convenienceFee");
        posAccount = getIntent().getStringExtra("posAccount");
        total = getIntent().getStringExtra("total");
        appTime = getIntent().getStringExtra("appTime");

        if(responseCode.equals("00")){

            responseImage.setImageResource(R.drawable.ic_check);
            responseText.setText(responseMessage);
        }else {
            responseImage.setImageResource(R.drawable.ic_error);
            responseText.setText(responseMessage);
        }


        long check = saveAirtimeTransaction();

        if(check < 0){
            Alert.showWarning(getApplicationContext(), "Airtime transaction could not be saved");
        }

    }

    public long saveAirtimeTransaction(){

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("phone",AirtimeAmountPreview.phone);
            jsonObject.put("network", AirtimeAmountPreview.network);
        }catch (Exception e){
            e.printStackTrace();
        }

        Transaction transaction = new Transaction();
        TransactionRepo transactionRepo = new TransactionRepo();

        float floatAmount = Float.parseFloat(amount.replace(",", ""));
        float floatConvenience = Float.valueOf(convenienceFee);

        transaction.setAgentCut(0);
        transaction.setAmount(floatAmount);
        transaction.setConvenienceFee(floatConvenience);
        transaction.setDate(new Date());
        transaction.setPosAccount(posAccount);
        transaction.setTransactionId(transactionId);
        transaction.setResponseCode(responseCode);
        transaction.setResponseMsg(responseMessage);
        transaction.setVersion(MainActivity.getAppVersion(getApplicationContext()));
        transaction.setAgentCut(Float.valueOf("0.0"));
        transaction.setTransactionType(Transaction.Type.getByName("AIRTIME"));
        transaction.setData(jsonObject);

        long result = transactionRepo.insert(transaction);

        return result;

    }

    @Override
    public void onBackPressed() {

    }
}
