package com.esl.android.paycentre.activities.billsPayment.startimes;

import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.interfaces.StartimesNameLookupInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;

import org.json.JSONObject;

public class StartimesNumberActivity extends BaseActivity implements StartimesNameLookupInterface {

    private EditText startimesCardNumber;

    private Button startimesAmountBtn;

    public static String cardNumber;

    public static String startimeCustomerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startimes_number);

        startimesCardNumber = findViewById(R.id.startimesCardNumber);
        startimesAmountBtn = findViewById(R.id.startimesAmountBtn);

        startimesAmountBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cardNumber = startimesCardNumber.getText().toString();
                        if (TextUtils.isEmpty(cardNumber)) {
                            Alert.showWarning(getApplicationContext(), "Please enter your smart card number");
                            return;
                        }

                        try {
                            JSONObject params = new JSONObject();

                            params.put("cardNumber", cardNumber);

                            String url = MainActivity.startimesInquiryUrl;

                            HttpService.doStartimesNameLookup(StartimesNumberActivity.this, params, url);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }

    @Override
    public void sendStartimesDetails(String customerName) {
        startimeCustomerName = customerName;

        HttpService.progressDialog.dismiss();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(StartimesNumberActivity.this);
        alertDialogBuilder.setTitle("Startimes Account Details");
        alertDialogBuilder
                .setMessage("\nNAME : " + customerName + "\n\n\nCARD NUMBER : " + cardNumber)
                .setCancelable(false)
                .setPositiveButton("PROCEED",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Intent intent = new Intent(getApplicationContext(),StartimesAmountActivity.class);
                                startActivity(intent);

                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Alert.showInfo(getApplicationContext(), "CANCELLED");
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void goBack(View view) {
        Navigator.moveTo(StartimesNumberActivity.this, "bills");
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(StartimesNumberActivity.this);
    }
}
