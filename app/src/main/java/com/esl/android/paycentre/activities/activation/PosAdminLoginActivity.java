package com.esl.android.paycentre.activities.activation;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.controllers.activationControllers.PosAdminLoginController;
import com.esl.android.paycentre.services.InternetConnectionService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import static com.esl.android.paycentre.activities.insurance.InsuranceActivity.sharedPreferenceManager;


public class PosAdminLoginActivity extends AppCompatActivity {

    PosAdminLoginController posAdminLoginController = new PosAdminLoginController();

    SharedPreferenceManager sharedPreferenceManager;

    private Button btn;

    private EditText phoneNum;

    private EditText password;

    private boolean doubleBackToExitPressedOnce = false;

    public static String agentPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        //getSupportActionBar().hide(); // hide the title back
        setContentView(R.layout.activity_adminpos_login);

        sharedPreferenceManager.save(getApplicationContext(), "adminLogin", "true");

        btn = findViewById(R.id.changePasswdBtn);
        phoneNum = findViewById(R.id.phoneNum);
        password = findViewById(R.id.password);

/**
 *
 *  Hi, charles. I want
 */


//        phoneNum.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                if (phoneNum.getText().toString().trim().length() < 11) {
//                    phoneNum.setError("Input is not a valid phone number");
//                } else {
//                    phoneNum.setError(null);
//                }
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        SpannableString spannableString = new SpannableString("Click to login with pos account");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(getApplicationContext(), PosLoginActivity.class);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(clickableSpan, 0, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = findViewById(R.id.posLoginTextView);
        if(!Globals.refreshToken){
            textView.setVisibility(View.GONE);
        }else{
            textView.setText(spannableString);
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setHighlightColor(Color.TRANSPARENT);
        }
    }

    public void adminAccess(View view) {
        //  sharedPreferenceManager.save(context, "username", dataObject.getString("username"));
        String serverUsername = sharedPreferenceManager.getString(getApplicationContext(), "username", "");

        String username = sharedPreferenceManager.getString(getApplicationContext(), "username", "").replace(" ","").toLowerCase();
        String agentPhoneNumber = String.valueOf(phoneNum.getText().toString()).replace(" ","").toLowerCase();
        agentPassword = String.valueOf(password.getText().toString());

        if (TextUtils.isEmpty(agentPassword.trim()) || TextUtils.isEmpty(agentPassword.trim())) {
            Alert.showWarning(getApplicationContext(),"Username and Password fields cannot be Empty");
            return;
        } else if (!username.equals(String.valueOf(agentPhoneNumber))) {
            Alert.showFailed(getApplicationContext()," Incorrect Username");
            return;
        } else if (!InternetConnectionService.doInternetConnectionCheck(getApplicationContext())) {
            Alert.showWarning(getApplicationContext(),"Please Check Your Internet Connection");
            return;
        } else {
            posAdminLoginController.doLogin(PosAdminLoginActivity.this, String.valueOf(serverUsername), String.valueOf(agentPassword), view);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
        }

        this.doubleBackToExitPressedOnce = true;

        Alert.showWarning(getApplicationContext(),"Click back again to exit");


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
