package com.esl.android.paycentre.activities.loans;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.controllers.LoanController;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class NewLoanActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<String> identityArray = new ArrayList<>();

    private String meansOfId;

    private TextView interestTextView, paybackTextView, dailyPayableTextView, totalPayableTextView;

    private EditText issueDate;

    private EditText expiryDate;

    private Button selectBtn;

    private DatePickerDialog.OnDateSetListener issueDateListener;

    private DatePickerDialog.OnDateSetListener expiryDateListener;

    private ImageView imageView;

    private String currentDate, previousDate;

    private Calendar cal;

    private int year;

    private int month;

    private int day;

    private DatePickerDialog dialog;

    public static EditText newLoanAmount;

    private Double interest;

    private Double dailyPayable;

    private Double totalPayable;

    private Double interestRate;

    private int payBackDays;

    private Double eligibilityAmount;

    private DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    private Button loanApplication;

    private String encoded;

    private TextView loanBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_loan);

        interestTextView = findViewById(R.id.interestTextView);
        paybackTextView = findViewById(R.id.paybackDTextView);
        totalPayableTextView = findViewById(R.id.totalPayableTextView);
        dailyPayableTextView = findViewById(R.id.dPayableTextView);

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

        final int calendarYear = calendar.get(Calendar.YEAR);
        final int calendarMonth = calendar.get(Calendar.MONTH) + 1;
        final int today = calendar.get(Calendar.DAY_OF_MONTH);

        Intent intent = getIntent();
        interestRate = Double.valueOf(intent.getStringExtra("interestRate"));
        payBackDays = Integer.valueOf(intent.getStringExtra("payBackDays"));
        eligibilityAmount = Double.valueOf(intent.getStringExtra("eligibleAmount"));


        issueDate = (EditText) findViewById(R.id.issueDate);
        expiryDate = (EditText) findViewById(R.id.expiryDate);
        imageView = (ImageView) findViewById(R.id.imageContainer);
        loanApplication = (Button) findViewById(R.id.loanApplication);
        loanBalance = (TextView) findViewById(R.id.newLoanBalance);

        double formattedBalance = Double.valueOf(eligibilityAmount);

        String formattedLoanBalance = "₦" + formatter.format(formattedBalance);

        loanBalance.setText(formattedLoanBalance);

        issueDate.setOnClickListener(this);
        expiryDate.setOnClickListener(this);
        loanApplication.setOnClickListener(this);
        issueDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d("SELECTED_DATE", "YEAR = " + year + " MONTH = " + month + " DAY = " + day);
                Log.d("CALENDAR_DATE", "YEAR = " + calendarYear + " MONTH = " + calendarMonth + " DAY = " + today);

                if (!isBeforeToday(year, month, day)) {
                    Alert.showWarning(getApplicationContext(), "Please make sure issue date is less than today's date");
                    issueDate.setText("");
                    return;
                }

                String appTime = year + "/" + month + "/" + day;
                issueDate.setText(appTime);
            }
        };

        expiryDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d("SELECTED_DATE", "YEAR = " + year + " MONTH = " + month + " DAY = " + day);
                Log.d("CALENDAR_DATE", "YEAR = " + calendarYear + " MONTH = " + calendarMonth + " DAY = " + today);

                if (isBeforeToday(year, month, day)) {
                    Alert.showWarning(getApplicationContext(), "Please make sure expiry date is more than today's date");
                    expiryDate.setText("");
                    return;
                }

                String appTime = year + "/" + month + "/" + day;
                expiryDate.setText(appTime);
            }
        };

        identityArray.add("International Passport");
        identityArray.add("National ID Card");
        identityArray.add("Driver's License");
        identityArray.add("Voter's Card");

        Spinner spinner = (Spinner) findViewById(R.id.meansOfIdSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, identityArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                meansOfId = (String) adapterView.getItemAtPosition(i);
                if (meansOfId.equals("National ID Card")) {
                    issueDate.setVisibility(View.GONE);
                    issueDate.setHint("");
                    expiryDate.setVisibility(View.GONE);
                    expiryDate.setHint("");
                } else {
                    issueDate.setVisibility(View.VISIBLE);
                    expiryDate.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        selectBtn = (Button) findViewById(R.id.selectBtn);
        selectBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        PickImageDialog.build(new PickSetup())
                                .setOnPickResult(new IPickResult() {
                                    @Override
                                    public void onPickResult(PickResult r) {
                                        //TODO: do what you have to...
                                        imageView.setImageBitmap(r.getBitmap());

                                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                        r.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                                        byte[] byteArray = byteArrayOutputStream.toByteArray();

                                        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

                                        Log.i("BASE64", encoded);
                                    }
                                })
                                .setOnPickCancel(new IPickCancel() {
                                    @Override
                                    public void onCancelClick() {
                                        //Alert.showInfo(getApplicationContext(),"CANCEL");
                                        //TODO: do what you have to if user clicked cancel
                                    }
                                }).show(getSupportFragmentManager());
                    }
                }
        );

        newLoanAmount = (EditText) findViewById(R.id.loanAmt);
        //newLoanAmount.addTextChangedListener(new NumberTextWatcher());
        newLoanAmount.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String value = "".equals(String.valueOf(s)) ? "0" : String.valueOf(s);

                        interest = (interestRate / 100) * Double.valueOf(value);
                        totalPayable = interest + Double.valueOf(value);
                        dailyPayable = totalPayable / payBackDays;

                        interestTextView.setText("₦" + formatter.format(interest));
                        dailyPayableTextView.setText("₦" + formatter.format(dailyPayable));
                        totalPayableTextView.setText("₦" + formatter.format(totalPayable));
                        paybackTextView.setText(String.valueOf(payBackDays));

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                }
        );

        interestTextView.setText("₦0.00");
        dailyPayableTextView.setText("₦0.00");
        totalPayableTextView.setText("₦0.00");
        paybackTextView.setText(String.valueOf(payBackDays));

    }


    @Override
    public void onClick(View v) {

        cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);

        switch (v.getId()) {

            case R.id.issueDate:

                dialog = new DatePickerDialog(
                        NewLoanActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        issueDateListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                break;

            case R.id.expiryDate:

                dialog = new DatePickerDialog(
                        NewLoanActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        expiryDateListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                break;

            case R.id.loanApplication:

                try {

                    JSONObject payload = new JSONObject();

                    payload.put("meansOfId", meansOfId);
                    payload.put("issueDate", issueDate.getText().toString());
                    payload.put("expiryDate", expiryDate.getText().toString());
                    payload.put("newLoanAmount", newLoanAmount.getText().toString());

                    String result = LoanController.validateNewLoanApplication(getApplicationContext(), payload);

                    if (result.equals("empty")) {
                        Alert.showWarning(getApplicationContext(), "Please fill all fields");
                        return;
                    } else if (result.equals("failed")) {
                        Alert.showWarning(getApplicationContext(), "Please Fill All Fields");
                        return;
                    } else {

                        try {

                            JSONObject loanPayload = new JSONObject();

                            String base64String = "data:image/jpeg;base64," + encoded;

                            loanPayload.put("amount", Double.parseDouble(newLoanAmount.getText().toString()));
                            loanPayload.put("kycDocument", base64String);
                            loanPayload.put("documentType", meansOfId);
                            loanPayload.put("idNumber", "");
                            loanPayload.put("issuingDate", issueDate.getText().toString());
                            loanPayload.put("expiryDate", expiryDate.getText().toString());

                            HttpService.doLoanApplication(NewLoanActivity.this, loanPayload);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            default:
                Alert.showWarning(getApplicationContext(), "DEFAULT");
                break;
        }
    }

//    public static boolean isBeforeToday(int year, int month, int day) {
//        Calendar today = Calendar.getInstance();
//        Calendar myDate = Calendar.getInstance();
//
//        myDate.set(year, month, day);
//
//        if (myDate.before(today)) return true;
//        return false;
//    }

    public static boolean isBeforeToday(int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
       // set the calendar to start of today
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        // and get that as a Date
        Date today = c.getTime();

      // user-specified date which you are testing
      // let's say the components come from a form or something

       // reuse the calendar to set user specified date
        Log.d("DATE ", "YEAR = "+year+" MONTH = "+month+ " DAY = "+ dayOfMonth);
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

       // and get that as a Date
        Date dateSpecified = c.getTime();

        // test your condition
        if (dateSpecified.before(today)) {
            System.err.println("Date specified [" + dateSpecified + "] is before today [" + today + "]");
            return true;
        } else {
            System.err.println("Date specified [" + dateSpecified + "] is NOT before today [" + today + "]");
            return false;
        }
    }


    public void dashboard(View view) {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }

}
