package com.esl.android.paycentre.cardreader.serializers;


public enum TransResult {
	TransError,
	TransOnlineRequest,
	TransOfflineApprove,
	TransOfflineDecline,
	TransCancelled,
	TransTransactionfailed
}
