package com.esl.android.paycentre.activities.billsPayment.startimes;

import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.interfaces.ConvenienceFeeInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;
import com.esl.android.paycentre.utils.NumberTextWatcher;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.esl.android.paycentre.utils.TransactionFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class StartimesAmountActivity extends BaseActivity implements ConvenienceFeeInterface {

    private EditText startimesAmt;

    private Button startimesBtn;

    private String amount;

    private DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    SharedPreferenceManager sharedPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startimes_amount);

        startimesAmt = findViewById(R.id.startimesAmount);
        startimesBtn = findViewById(R.id.startimesAmountBtn);
        startimesAmt.addTextChangedListener(new NumberTextWatcher());

        final String posAccountUsername = sharedPreferenceManager.getString(getApplicationContext(), "posAccountUsername", "");

        startimesBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        amount = startimesAmt.getText().toString();
                        if(TextUtils.isEmpty(amount)){
                            Alert.showWarning(getApplicationContext(),"Please enter amount");
                            return;
                        }

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(StartimesAmountActivity.this);
                        alertDialogBuilder.setTitle("Transaction Details");
                        alertDialogBuilder
                                .setMessage("\nSTARTIMES PLAN PRICE : ₦" +formatter.format(Double.parseDouble(amount.replace(",",""))))
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                try {
                                                    JSONObject params = new JSONObject();
                                                    params.put("version", MainActivity.getAppVersion(getApplicationContext()));
                                                    params.put("addons",new JSONArray());
                                                    params.put("smartCardNumber", StartimesNumberActivity.cardNumber);
                                                    params.put("amount",String.valueOf(Double.valueOf(amount.replace(",", "")).intValue()));
                                                    params.put("posAccount",posAccountUsername);
                                                    params.put("convenienceFee","0");
                                                    params.put("customerName", StartimesNumberActivity.startimeCustomerName);
                                                    params.put("transactionType","Startimes");
                                                    params.put("transactionId", TransactionFormatter.generateTransactionId());
                                                    params.put("appTime",TransactionFormatter.generateAppTime());


                                                    String url = MainActivity.startimesPaymentUrl;

                                                    String type = "startimes";

                                                    HttpService.makePayment(StartimesAmountActivity.this,params,url,type);


                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                }

                                            }
                                        })
                                .setNegativeButton("CANCEL",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                });
    }

    @Override
    public void sendConvenienceFee(String fee) {

    }

    @Override
    public void sendConvenienceFee(String fee, String StampDuty, boolean showStampDuty, String vat, boolean showVat) {
        HttpService.progressDialog.dismiss();
        //        Intent intent = new Intent(getApplicationContext(), StartimesConfirmActivity.class);
//        intent.putExtra("totalAmount",amount);
//        intent.putExtra("cFee",String.valueOf(fee));
//
//        startActivity(intent);
    }


    public void goBack(View view) {
        Navigator.moveTo(StartimesAmountActivity.this, "startimes");
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(StartimesAmountActivity.this);
    }
}
