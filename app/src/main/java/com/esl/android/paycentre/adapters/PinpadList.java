package com.esl.android.paycentre.adapters;

public class PinpadList {

    // bluetooth icon
    private int imageDrawable;
    // Store the address of device
    private String address;
    // Store the name of the device
    private String name;

    // Constructor that is used to create an instance of the PinpadList object
    public PinpadList(int imageDrawable,String name, String address) {
        this.imageDrawable = imageDrawable;
        this.name = name;
        this.address = address;
    }


    public int getImageDrawable() {
        return imageDrawable;
    }

    public void setImageDrawable(int imageDrawable) {
        this.imageDrawable = imageDrawable;
    }


    public String getDeviceName() {
        return name;
    }

    public void setDeviceName(String name) {
        this.name = name;
    }

    public String getDeviceAddress() {
        return address;
    }

    public void setDeviceAddress(String address) {
        this.address = address;
    }

}
