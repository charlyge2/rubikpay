package com.esl.android.paycentre.cardreader.emv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.datecs.barcode.Barcode;
import com.datecs.crypto.SHA256;
import com.datecs.crypto.TripleDES;
import com.datecs.emv.EmvTags;
import com.datecs.pinpad.DeviceInfo;
import com.datecs.pinpad.Pinpad;
import com.datecs.pinpad.PinpadException;
import com.datecs.tlv.BerTlv;
import com.datecs.tlv.Tag;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.withdrawal.WithdrawalAmountDetailsActivity;
import com.esl.android.paycentre.activities.withdrawal.WithdrawalReceiptActivity;
import com.esl.android.paycentre.cardreader.controllers.PinpadHelper;
import com.esl.android.paycentre.cardreader.controllers.PinpadManager;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.cardreader.network.NibssIsoProcessor;
import com.esl.android.paycentre.cardreader.serializers.EmvResponse;
import com.esl.android.paycentre.cardreader.serializers.PurchaseRequest;
import com.esl.android.paycentre.cardreader.serializers.Receipt;
import com.esl.android.paycentre.cardreader.utils.CryptoUtil;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.cardreader.utils.HexUtil;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.esl.android.paycentre.cardreader.utils.HexUtil.byteArrayToHexString;

public class NewEMVController {

    private static final String LOG_TAG = "Paycentre";

    // Default transaction timeout in milliseconds.
    private static int DEFAULT_TIMEOUT = 30000;

    // Thread synchronization.
    private final Object mSyncRoot = new Object();

    private Context context;
    private PinpadManager mPinpadManager;

    private byte[] sessionKey = null;
    private byte[] pinKey = null;
    private byte[] dataKey = null;

    private int bankIconCheck = 0;

    private Receipt receipt;


    public NewEMVController(Context context) {

        this.context = context;

        mPinpadManager = PinpadManager.getInstance(this.context);

        ReferenceList.config = this.context.getSharedPreferences(ReferenceList.preference, 0);

        String tsk = ReferenceList.config.getString(ReferenceList.TSK, "none");

        if (!tsk.equals("none")) {

            sessionKey = HexUtil.hexStringToByteArray(tsk);
        }

        String tpk = ReferenceList.config.getString(ReferenceList.TPK, "none");

        if (!tpk.equals("none")) {

            pinKey = HexUtil.hexStringToByteArray(tpk);
        }

        String data = ReferenceList.config.getString(ReferenceList.dataKey, "none");

        System.out.println("DATA: " + data);

        String srn = ReferenceList.config.getString(ReferenceList.pinpadSerialNumber, "");
        System.out.println("Serial number: " + srn);

        // if (!data.equals("none")) {

        dataKey = CryptoUtil.encrypt3DESCBC(HexUtil.hexStringToByteArray("2CE234E4067F511B26FCCE580D3BA4CF"), HexUtil.hexStringToByteArray("0000000000000000"), HexUtil.hexStringToByteArray(srn));
        // }


    }

    public interface PinpadRunnable {
        void run(Pinpad pinpad) throws IOException;
    }

    private void runAsync(final PinpadRunnable r) {


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    Pinpad pinpad = mPinpadManager.getPinpad();

                    try {

                        r.run(pinpad);

                        PinpadHelper.initScreen(pinpad);

                    } catch (PinpadException e) {

                        e.printStackTrace();
                        StringWriter sw = new StringWriter();
                        e.printStackTrace(new PrintWriter(sw));
                        String stacktrace = sw.toString();
                        warn("Pinpad error: " + stacktrace);

                    } catch (IOException e) {

                        e.printStackTrace();
                        fail("I/O error: " + e.getMessage());

                    } catch (Exception e) {

                        e.printStackTrace();
                        fail("Critical error: " + e.getMessage());

                    }

                } finally {


                }
            }
        });
        thread.start();
    }


    private void initNewPinpad() {
        log("*** Init Pinpad ***", true);

        // Enable debug information
        Pinpad.setDebug(true);

        // Set listener not notify when Pinpad is disconnected.
        mPinpadManager.getPinpad().setPinpadListener(new Pinpad.PinpadListener() {
            @Override
            public void onPinpadRelease() {
                Globals.isPinpadConnected = false;
                if (!((Activity) context).isFinishing()) {
                    // ((Activity)Context).showToast(Context.getString(R.string.msg_pinpad_connection_is_closed));

                }
            }
        });

        runAsync(new PinpadRunnable() {
            @Override
            public void run(final Pinpad pinpad) throws IOException {

                String srn = pinpad.getIdentification().getCPUSerialNumber();
                String preferencesName = ReferenceList.preference;
                MiscUtils.initContext(context);

                MiscUtils.storeInSharedPreferences(preferencesName, ReferenceList.pinpadSerialNumber, srn);
                MiscUtils.storeInSharedPreferences(ReferenceList.preference, ReferenceList.pinpadMACAddress, mPinpadManager.getBluetoothAddress());

                log("Get device information");
                DeviceInfo devInfo = pinpad.getIdentification();
                log("  Serial number: " + devInfo.getDeviceSerialNumber());
                log("  Loader name: " + devInfo.getLoaderName());
                log("  Loader version: " + devInfo.getLoaderVersion());
                log("  Firmware name: " + devInfo.getFirmwareName());
                log("  Firmware version: " + devInfo.getFirmwareVersion());
                log("  Application name: " + devInfo.getApplicationName());
                log("  Application version: " + devInfo.getApplicationVersion());

                log("Init display");
                PinpadHelper.initScreen(pinpad);

                log("Set time");
                pinpad.sysSetDate(Calendar.getInstance());

                log("Enable events");
                pinpad.sysEnableEvents(Pinpad.ENABLE_BARCODE);

                // Register callback for terminals with barcode engines
                pinpad.setBarcodeListener(new Pinpad.BarcodeListener() {
                    @Override
                    public void onBarcodeRead() {
                        Barcode barcode;
                        try {
                            barcode = pinpad.barGetBarcodeData();
                        } catch (IOException e) {
                            e.printStackTrace();
                            return;
                        }

                        String barcodeType = barcode.getTypeString();
                        String barcodeData = barcode.getDataString();
                        log("  Barcode: (" + barcodeType + ") " + barcodeData);
                    }
                });
            }
        });
    }


    public void startTransaction() {
        log("*** Start transaction ***", true);


        runAsync(new PinpadRunnable() {
            @Override
            public void run(final Pinpad pinpad) throws IOException {
                final ByteArrayOutputStream transactionBuffer = new ByteArrayOutputStream();

                log("Initialize Payment Engine");
                pinpad.emv2Initialize();

                log("Lock keyboard");
                pinpad.uiKeyboardControl(true);

                pinpad.setEmv2Listener(new Pinpad.Emv2Listener() {

                    @Override
                    public void onUpdateUserInterface(int id, int status, int hold) {

                        log("Update user interface");

                        Intent v1 = null;

                        final String message;
                        switch (id) {
                            // EMV2 message codes
                            case Pinpad.EMV2_MESSAGE_NOT_WORKING:
                                message = "NOT WORKING";
                                break;
                            case Pinpad.EMV2_MESSAGE_APPROVED:

                                message = "APPROVED";
                                break;
                            case Pinpad.EMV2_MESSAGE_DECLINED:

                                message = "DECLINED";
                                break;
                            case Pinpad.EMV2_MESSAGE_PLEASE_ENTER_PIN:

                                v1 = new Intent();

                                v1.setAction("com.esl.paycenter");
                                v1.putExtra("response", "enterPIN");

                                context.sendBroadcast(v1);

                                message = "PLEASE ENTER PIN";
                                break;

                            case Pinpad.EMV2_MESSAGE_ERROR_PROCESSING:
                                message = "ERROR PROCESSING";
                                break;
                            case Pinpad.EMV2_MESSAGE_REMOVE_CARD:
                                message = "REMOVE CARD";
                                break;
                            case Pinpad.EMV2_MESSAGE_PRESENT_CARD:
                                message = "PRESENT CARD";
                                break;
                            case Pinpad.EMV2_MESSAGE_IDLE:
                                message = "IDLE";
                                break;
                            case Pinpad.EMV2_MESSAGE_PROCESSING:

                                v1 = new Intent();

                                v1.setAction("com.esl.paycenter");

                                v1.putExtra("response", "pinpadProcessing");

                                context.sendBroadcast(v1);

                                message = "PROCESSING";
                                break;

                            case Pinpad.EMV2_MESSAGE_CARD_READ_OK_REMOVE:
                                message = "CARD READ OK REMOVE";
                                break;
                            case Pinpad.EMV2_MESSAGE_TRY_OTHER_INTERFACE:
                                message = "TRY OTHER INTERFACE";
                                break;
                            case Pinpad.EMV2_MESSAGE_CARD_COLLISION:
                                message = "CARD COLLISION";
                                break;
                            case Pinpad.EMV2_MESSAGE_SIGN_APPROVED:
                                message = "SIGN APPROVED";
                                break;
                            case Pinpad.EMV2_MESSAGE_ONLINE_AUTHORISATION:

                                v1 = new Intent();

                                v1.setAction("com.esl.paycenter");
                                v1.putExtra("response", "nibssProcessing");

                                context.sendBroadcast(v1);

                                message = "ONLINE AUTHORISATION";
                                break;

                            case Pinpad.EMV2_MESSAGE_TRY_OTHER_CARD:
                                message = "TRY OTHER CARD";
                                break;
                            case Pinpad.EMV2_MESSAGE_INSERT_CARD:

                                message = "INSERT CARD";

                                v1 = new Intent();

                                v1.setAction("com.esl.paycenter");
                                v1.putExtra("response", "insertCard");

                                context.sendBroadcast(v1);

                                break;

                            case Pinpad.EMV2_MESSAGE_CLEAR_DISPLAY:
                                message = "CLEAR DISPLAY";
                                break;
                            case Pinpad.EMV2_MESSAGE_SEE_PHONE:
                                message = "SEE PHONE";
                                break;
                            case Pinpad.EMV2_MESSAGE_PRESENT_CARD_AGAIN:
                                message = "PRESENT CARD AGAIN";
                                break;
                            case Pinpad.EMV2_MESSAGE_ENTER_PIN_ON_PHONE:
                                message = "ENTER PIN ON PHONE";
                                break;
                            case Pinpad.EMV2_MESSAGE_NA:
                                message = "N/A";
                                break;
                            default:
                                message = "N/A " + id;
                        }

                        log("  Message: " + message);

                    }

                    @Override
                    public void onTransactionFinish(byte[] data) {
                        log("Finish transaction");

                        synchronized (mSyncRoot) {
                            log("  @Tag: ", BerTlv.createList(data));
                            try {
                                transactionBuffer.write(data);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            mSyncRoot.notify();
                        }


                    }

                    @Override
                    public void onApplicationSelection(byte[] data) {
                        log("onApplicationSelection");
                        log("  @Tag: ", BerTlv.createList(data));
                        try {
                            pinpad.emv2SelectApplication(0);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onDCCRequest(byte[] data) {
                        log("onDCCRequest");
                        log("  @Tag: ", BerTlv.createList(data));
                    }

                    @Override
                    public void onPinRequest(byte[] data) {
                        log("onPinRequest");
                        log("  @Tag: ", BerTlv.createList(data));
                        BerTlv tagC6 = BerTlv.find(data, 0xC6);
                        if (tagC6 != null) {
                            //Mpea.sendMessage(PinpadActivity.this, REQUEST_PIN, tagC6.getValue());
                        }
                    }

                    @Override
                    public void onOnlineAuthorisationRequest(byte[] data) {


                        EmvResponse response = new EmvResponse();

                        log("Process ONLINE request");
                        log("  @Tag: ", BerTlv.createList(data));

                        // Sample tag list: 5F2A,5F34,82,95,9A,9C,9F02,...
                        int[] tagList = Globals.ONLINE_REQUEST_TAGS;

                        log("Read EMV tags");
                        byte[] encTags;
                        try {
                            encTags = pinpad.emv2GetTagsEncrypted(tagList,
                                    Pinpad.TAGS_FORMAT_DATECS,
                                    Pinpad.KEY_TYPE_3DES_CBC,
                                    PinpadFacade.KEY_DATA_INDEX,
                                    0xAABBCCDD);
                        } catch (IOException e) {
                            e.printStackTrace();
                            fail("Pinpad error: " + e.getMessage());
                            try {
                                pinpad.emv2CancelTransaction();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            return;
                        }

                        // Decryption is made with demonstration purpose only.
                        byte[] decTags = null;

                        try {

                            log("Encrpted EMV DATA: " + byteArrayToHexString(encTags));

                            decTags = decryptData(encTags, dataKey, response);

                            log("  EMV DATA: " + byteArrayToHexString(decTags));

                        } catch (Exception e) {

                            warn("Failed to decrypt data");
                        }

                        // Get Cardholder Verification Result
                        byte[] pinKSN = null, pinBlock = null;
                        BerTlv tagC2 = BerTlv.find(data, EmvTags.TAG_C2_CVM_RESULT);
                        int cvmResult = tagC2.getValueInt();
                        if (cvmResult == Pinpad.EMV2_CVM_ONLINE_PIN) {
                            log("  CVM Result: ONLINE PIN");

                            // Get ISO0 pin block
                            try {


                                pinBlock = pinpad.uiVerifyPINOnline(Pinpad.MKSK,
                                        Pinpad.ISO0, pinKey,
                                        PinpadFacade.KEY_PIN_INDEX, null);

                                String pinBlockHex = HexUtil.byteArrayToHexString(
                                        pinBlock).replace(" ", "");

                                Log.i("EMVPROCESSOR", "  ENCRTYPTED PIN: "
                                        + pinBlockHex + "\n");

                                if (pinBlockHex != null) {

                                    response.setPinData(pinBlockHex);
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                warn("Failed to get PIN");
                            }
                        } else if (cvmResult == Pinpad.EMV2_CVM_CONFIG_CODE_VERIFIED) {
                            log("  CVM Result: CONFIG CODE VERIFIED");
                        } else if (cvmResult == Pinpad.EMV2_CVM_OBTAIN_SIGNATURE) {
                            log("  CVM Result: OBTAIN SIGNATURE");
                        } else if (cvmResult == Pinpad.EMV2_CVM_NO_CVM) {
                            log("  CVM Result: NO CVM");
                        } else if (cvmResult == Pinpad.EMV2_CVM_OFFLINE_PIN) {
                            log("  CVM Result: OFFLINE PIN");
                        }

                        // Get Payment interface
                        BerTlv tagC3 = BerTlv.find(data, EmvTags.TAG_C3_PAYMENT_INTERFACE);
                        int paymentInterface = tagC3.getValueInt();
                        if (paymentInterface == Pinpad.EMV2_INTERFACE_CONTACT) {
                            log("  Payment Interface: CONTACT");
                        } else if (paymentInterface == Pinpad.EMV2_INTERFACE_CONTACTLESS) {
                            log("  Payment Interface: CONTACTLESS");
                        } else if (paymentInterface == Pinpad.EMV2_INTERFACE_MAGNETIC) {
                            log("  Payment Interface: INTERFACE_MAGNETIC");

                            // Get track2 equivalent data.
//                            if (decTags != null) {
//                                BerTlv tag57 = BerTlv.find(decTags, EmvTags.TAG_57_TRACK_2_EQUIVALENT_DATA);
//                                String track = tag57.getValueHexString();
//                                // Convert from EMV track data to magnetic track data.
//                                if (track.endsWith("F")) {
//                                    track = track.substring(0, track.length() - 1);
//                                }
//                                track = ";" + track.replaceAll("D", "=") + "?";
//                                log("  TRACK2: " + track + "\n");
//                            }
                        } else if (paymentInterface == Pinpad.EMV2_INTERFACE_MANUAL) {
                            log("  Payment Interface: MANUAL");
                        }

                        // Get transaction host result
                        byte[] responseByte = processEMVOnlineTransaction(response);


                        List<BerTlv> onlineResult = new ArrayList<>();

                        if (response != null) {

                            onlineResult.add(new BerTlv(EmvTags.TAG_C2_ONLINE_RESULT, "01"));
                            onlineResult.add(new BerTlv(EmvTags.TAG_E6_ONLINE_DATA, responseByte));

                        } else {

                            onlineResult.add(new BerTlv(EmvTags.TAG_C2_ONLINE_RESULT, "00"));

                        }

                        try {

                            log("Set online result");
                            log("  @Tag: ", onlineResult);

                            pinpad.emv2SetOnlineResult(BerTlv.listToByteArray(onlineResult));
                            String amount = Globals.isValueDifferent ? String.valueOf(Globals.amount) : Globals.currencyAmount;


                            receipt = new Receipt(Globals.responseCode, Globals.appResponse,amount, Globals.pan, Globals.rrn,
                                    Globals.stan, Globals.terminalId, Globals.transactionId, Globals.transactionDate,
                                    Globals.withdrawalType, Globals.applicationVersion, "Default", Globals.aid, Globals.authCode,
                                    Globals.cardholder, Globals.cardExpiry, Globals.tenderType, Globals.sequenceNumber);

                            Intent intent = new Intent(context, WithdrawalReceiptActivity.class);
                            intent.putExtra("receipt", receipt);

                            context.startActivity(intent);

                        } catch (IOException e) {
                            e.printStackTrace();
                            log("Pinpad error: " + e.getMessage());
                        }
                    }
                });

                Long amountLong = Long.parseLong(Globals.pinpadAmount);

                String transactionAmount = String.format("%012d", amountLong);

                // Constructs input parameters
                List<BerTlv> inputParams = new ArrayList<>();
                // Mandatory tags
                inputParams.add(new BerTlv(EmvTags.TAG_9C_TRANSACTION_TYPE, "00"));
                inputParams.add(new BerTlv(EmvTags.TAG_9F02_AMOUNT_AUTHORISED, transactionAmount));
                inputParams.add(new BerTlv(EmvTags.TAG_5F2A_TRANSACTION_CURRENCY_CODE, "0566"));
                inputParams.add(new BerTlv(EmvTags.TAG_9F1A_TERMINAL_COUNTRY_CODE, "0566"));
                inputParams.add(new BerTlv(EmvTags.TAG_9F03_AMOUNT_OTHER, "000000000000"));
                // Optional tags
                //inputParams.add(new BerTlv(EmvTags.TAG_9F33_TERMINAL_CAPABILITIES, "6060C8"));
                inputParams.add(new BerTlv(EmvTags.TAG_C1_TRANSACTION_CONFIGURATION, "01"));
                inputParams.add(new BerTlv(0xC2, "20"));
                //inputParams.add(new BerTlv(0xCC, "01"));
                // C3 tag is 4 bytes, one for CHIP, NFC, MSR, MANUAL. The value of that bytes can
                // be:
                // 00 - Auto
                // 01 - Force pin online
                // 02 disable
                inputParams.add(new BerTlv(0xC3, "00"));

//                List<BerTlv> msg = new ArrayList<>();
//                msg.add(new BerTlv(0xC1,"01"));
//
//                msg.add(new BerTlv(0xC0,"800300456E7465722050494E203B0A414D4F554E543A0A4000"));
//
//                msg.add(new BerTlv(0xC0, "001D01414D4F554E543A0A400A496E73657274206361726400"));
//
//                inputParams.add(new BerTlv(0xE6,byteArrayToHexString(BerTlv.listToByteArray(msg))));

//                if (Mpea.isAvailable(PinpadActivity.this)) {
//                    String pairToken = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("pairToken", "");
//                    String shuffle = "01";
//                    String issueChallenge = "01";
//                    // MPEA tag: 16 byte pair token + 1 byte shuffle + 1 byte issue challenge
//                    String data = pairToken + shuffle + issueChallenge;
//                    inputParams.add(new BerTlv(0xD1, data));
//                    log("MPEA is enabled: " + data);
//                }

                log("Start transaction");
                log("  @Tag: ", inputParams);

                int paymentInterface = Pinpad.EMV2_INTERFACE_CONTACT |
                        //Pinpad.EMV2_INTERFACE_MAGNETIC |
                        Pinpad.EMV2_INTERFACE_MANUAL;

                pinpad.emv2StartTransaction(paymentInterface,
                        0 /* Flags */,
                        BerTlv.listToByteArray(inputParams),
                        DEFAULT_TIMEOUT / 1000 /* Seconds */);

                synchronized (mSyncRoot) {
                    while (transactionBuffer.size() == 0) {
                        if (pinpad.isActive()) {
                            try {
                                mSyncRoot.wait(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } else {
                            throw new IOException("Connection closed");
                        }
                    }
                }
                byte[] transactionData = transactionBuffer.toByteArray();

                BerTlv tagC1 = BerTlv.find(transactionData, EmvTags.TAG_C1_TRANSACTION_RESULT);
                int transactionResult = tagC1.getValueInt();
                if (transactionResult == Pinpad.EMV2_RESULT_ABORTED) {

                    Intent v1 = new Intent();

                    v1.setAction("com.esl.paycenter");
                    v1.putExtra("response", "stop");

                    context.sendBroadcast(v1);

                    warn("  Result: ABORTED");
                } else if (transactionResult == Pinpad.EMV2_RESULT_APPROVED) {


                } else if (transactionResult == Pinpad.EMV2_RESULT_DECLINED) {


                } else if (transactionResult == Pinpad.EMV2_RESULT_TRY_ANOTHER_INTERFACE) {

                    Intent v1 = new Intent();

                    v1.setAction("com.esl.paycenter");
                    v1.putExtra("response", "stop");

                    context.sendBroadcast(v1);
                    warn("  Result: TRY ANOTHER INTERFACE");

                } else if (transactionResult == Pinpad.EMV2_RESULT_END_APPLICATION) {

                    Intent v1 = new Intent();

                    v1.setAction("com.esl.paycenter");
                    v1.putExtra("response", "stop");

                    context.sendBroadcast(v1);

                    warn("  Result: END APPLICATION");
                }

                if (transactionResult != Pinpad.EMV2_RESULT_APPROVED) {
                    // Use tag C4 to determinate failure reason.
                    BerTlv tagC4 = BerTlv.find(transactionData, EmvTags.TAG_C4_TRANSACTION_FAILURE_REASON);
                    if (tagC4 != null) {
                        // First byte from tag C4 specify transaction failure result.
                        int failureReason = tagC4.getValueInt() >> 8;

                        if (failureReason == Pinpad.EMV2_FAILURE_REASON_FAILED) {
                            log("  Failure reason: Transaction failed");

                            // Second byte from tag C4 specify Pinpad error code
                            int errorCode = tagC4.getValueInt() & 0xff;
                            log("  Error code: " + errorCode);


                        } else if (failureReason == Pinpad.EMV2_FAILURE_REASON_TIMEOUT) {

                            Intent v1 = new Intent();

                            v1.setAction("com.esl.paycenter");
                            v1.putExtra("response", "stop");

                            context.sendBroadcast(v1);

                            log("  Failure reason: Transaction timeout");

                        } else if (failureReason == Pinpad.EMV2_FAILURE_REASON_CANCELED) {

                            Intent v1 = new Intent();

                            v1.setAction("com.esl.paycenter");
                            v1.putExtra("response", "stop");

                            context.sendBroadcast(v1);

                            log("  Failure reason: Transaction is canceled");
                        }
                    }
                }

                // Unlock keyboard
                pinpad.uiKeyboardControl(false);

                // C3
                // 1 - Contact
                // 2 - Contactless
                // 4 Magnetic
                // 8 - Manuol
                log("Deinitialise Payment Engine");
                pinpad.emv2Deinitialise();
            }
        });
    }

    private byte[] processEMVOnlineTransaction(EmvResponse response) {
        Log.i("EMVPROCESSOR", "» Process online\n");

        Intent v1 = new Intent();

        v1.setAction("com.esl.paycenter");
        v1.putExtra("response", "nibssProcessing");

        context.sendBroadcast(v1);

        // Database date
        Calendar date = Calendar.getInstance();
        Date transdate = date.getTime();
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String currentDateTime = String.valueOf(System.currentTimeMillis());
        String retrievalReferenceNumber = currentDateTime;

        int len = retrievalReferenceNumber.length();

        if (len < 12) {

            int pad = 12 - len;

            for (int i = 0; i < pad; i++) {

                retrievalReferenceNumber = "0" + retrievalReferenceNumber;

            }
        }

        if (len > 12) {

            retrievalReferenceNumber = retrievalReferenceNumber.substring(len - 12);

        }


        String stan = retrievalReferenceNumber.substring(retrievalReferenceNumber.length() - 6);

        Globals.rrn = retrievalReferenceNumber;
        Globals.stan = stan;


        // TODO: Build & transmit online request to issuer
        String pan = response.getPan();

        if (pan.charAt(pan.length() - 1) == 'F') {
            pan = pan.replaceAll("F", "");
        }

        String purchaseProcessingCode = "000000";

        switch (Globals.accountType) {
            case "10":

                purchaseProcessingCode = "00" + "10" + "00";

                break;
            case "20":

                purchaseProcessingCode = "00" + "20" + "00";

                break;
            case "30":

                purchaseProcessingCode = "00" + "30" + "00";

                break;
        }


        String transactionAmount = response.getAmountAuthorized();

        SimpleDateFormat format = new SimpleDateFormat("hhmmss");
        String localTransactionTime = format.format(transdate);
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        String stringDay = String.valueOf(day);
        String stringMonth = String.valueOf(month);
        if (stringDay.length() == 1) {
            stringDay = "0" + stringDay;
        }

        if (stringMonth.length() == 1) {
            stringMonth = "0" + stringMonth;
        }
        String localTransactionDate = stringMonth + stringDay;
        String transmissionDateTime = localTransactionDate
                + localTransactionTime;

        //String cardExpirationDate = response.getExpiry();


        String merchantType = ReferenceList.config.getString(ReferenceList.merchantType, "none");

        String posEntryMode = "051";

        String cardSequenceNumber = response.getPanSequenceNo();

        if (cardSequenceNumber.length() == 2) {
            cardSequenceNumber = "0" + cardSequenceNumber;
        }
        Globals.sequenceNumber = cardSequenceNumber;

        String posConditionCode = "00";
        String posPinCaptureCode = "04";
        String transactionFee = "C00000000";
        String acquiringInstIdCode = "111129";


        String terminalId = ReferenceList.config.getString(ReferenceList.terminalId, "none");
//        if(terminalId.startsWith("2044")){
//            purchaseProcessingCode = "010000";
//        }

        //Globals.terminalId = terminalId;

        String cardAcceptorIdCode = ReferenceList.config.getString(
                ReferenceList.acceptorID, "none");
        String cardAcceptorNameLocation = ReferenceList.config.getString(
                ReferenceList.acceptorName, "none");

        String currencyCode = "566";

        String pinData = null;
        if (response.getPinData() != null) {
            pinData = response.getPinData();
        }

        String iccData = "";

        String posDataCode = "511101512344101";

        String track2Data = response.getTrack2();

        String getSRC = "";
        int index = 0;
        if (track2Data.contains("D")) {
            index = track2Data.indexOf('D');
        } else if (track2Data.contains("=")) {
            index = track2Data.indexOf('=');
        }
        getSRC = track2Data.substring(index + 1, index + 8);

        String cardExpirationDate = getSRC.substring(0, 4);

        Globals.cardExpiry = cardExpirationDate;

        String serviceRestrictionCode = getSRC.substring(4);

        String securityRelatedInformation = "";
        String additionalAmounts = "000000000000";
        String messageReasonCode = "";

        String replacementAmounts = String.format("%s%s%s%s",
                transactionAmount, transactionAmount, "C00000000", "C00000000");
        String transportData = "MPOS";
        String paymentInformation = "";
        String secondaryHashValue = "";

        String iccData2 = packICCDataJpos(response);

        PurchaseRequest purchaseRequest = null;
        purchaseRequest = new PurchaseRequest();
        purchaseRequest.setPan(pan);
        purchaseRequest.setProcessingCode(purchaseProcessingCode);
        purchaseRequest.setTransactionAmount(transactionAmount);
        purchaseRequest.setTransmissionDateTime(transmissionDateTime);
        purchaseRequest.setStan(stan);
        purchaseRequest.setLocalTransactionDate(localTransactionDate);
        purchaseRequest.setLocalTransactionTime(localTransactionTime);
        purchaseRequest.setCardExpirationDate(cardExpirationDate);
        purchaseRequest.setMerchantType(merchantType);
        purchaseRequest.setPosEntryMode(posEntryMode);
        purchaseRequest.setCardSequenceNumber(cardSequenceNumber);
        purchaseRequest.setPosConditionCode(posConditionCode);
        purchaseRequest.setPosPinCaptureCode(posPinCaptureCode);
        purchaseRequest.setTransactionFee(transactionFee);
        purchaseRequest.setAcquiringInstIdCode(acquiringInstIdCode);
        purchaseRequest.setTrack2Data(track2Data);
        purchaseRequest.setRetrievalReferenceNumber(retrievalReferenceNumber);
        purchaseRequest.setTerminalId(terminalId);
        purchaseRequest.setCardAcceptorIdCode(cardAcceptorIdCode);
        purchaseRequest.setCardAcceptorNameLocation(cardAcceptorNameLocation);
        purchaseRequest.setCurrencyCode(currencyCode);
        purchaseRequest.setPinData(pinData);
        purchaseRequest.setIccData(iccData2.replace(" ", ""));
        purchaseRequest.setPosDataCode(posDataCode);
        purchaseRequest.setServiceRestrictionCode(serviceRestrictionCode);
        purchaseRequest.setSecurityRelatedInformation(securityRelatedInformation);
        purchaseRequest.setAdditionalAmounts(additionalAmounts);
        purchaseRequest.setMessageReasonCode(messageReasonCode);
        purchaseRequest.setTransportData(transportData);
        purchaseRequest.setPaymentInformation(paymentInformation);
        purchaseRequest.setOriginalDataElements(""); // set


        long check = saveTransaction(retrievalReferenceNumber, stan, getMaskedPan(pan));


        List<BerTlv> responseByte = new ArrayList<>();

        if (check < 0) {

            Alert.showWarning(context, "TRANSACTION COULD NOT BE SAVED");

            responseByte.add(new BerTlv(EmvTags.TAG_8A_AUTHORISATION_RESPONSE_CODE, "4040"));

            return BerTlv.listToByteArray(responseByte);

        } else {

            Globals.databaseId = check;

            String responseCode = NibssIsoProcessor.process(purchaseRequest, sessionKey, context);

            System.out.println("Response Code: " + responseCode);



            if ("00".equals(responseCode)) {
                responseByte.add(new BerTlv(EmvTags.TAG_8A_AUTHORISATION_RESPONSE_CODE, "3030"));
            } else {
                responseByte.add(new BerTlv(EmvTags.TAG_8A_AUTHORISATION_RESPONSE_CODE, "4040"));
            }


            return BerTlv.listToByteArray(responseByte);
        }


    }

    public String packICCDataJpos(EmvResponse response) {

        String iccdata = "";
        String temp = "";

        temp = response.getCryptogram();
        iccdata += String.format("9F26%02x%s", temp.length() / 2, temp);

        temp = response.getCryptogramInformationData();
        iccdata += String.format("9F27%02x%s", temp.length() / 2, temp);

        temp = response.getIad();
        iccdata += String.format("9F10%02x%s", temp.length() / 2, temp);

        temp = response.getUnpredictableNo();
        iccdata += String.format("9F37%02x%s", temp.length() / 2, temp);

        temp = response.getAtc();
        iccdata += String.format("9F36%02x%s", temp.length() / 2, temp);

        temp = response.getTvr();
        iccdata += String.format("95%02x%s", temp.length() / 2, temp);

        temp = response.getTransDate();
        iccdata += String.format("9A%02x%s", temp.length() / 2, temp);

        temp = response.getTransactionType();
        iccdata += String.format("9C%02x%s", temp.length() / 2, temp);

        Long amountLong = Long.parseLong(response.getAmountAuthorized());
        temp = String.format("%012d", amountLong);
        iccdata += String.format("9F02%02x%s", temp.length() / 2, temp);

        temp = response.getAip();
        iccdata += String.format("82%02x%s", temp.length() / 2, temp);

        temp = "0566";
        iccdata += String.format("9F1A%02x%s", temp.length() / 2, temp);

        temp = "0566";
        iccdata += String.format("5F2A%02x%s", temp.length() / 2, temp);

        temp = response.getCvmResult();
        iccdata += String.format("9F34%02x%s", temp.length() / 2, temp);

        temp = response.getTerminalCapabilities();
        iccdata += String.format("9F33%02x%s", temp.length() / 2, temp);

        temp = response.getTerminalType();
        iccdata += String.format("9F35%02x%s", temp.length() / 2, temp);

        temp = "000000000000";
        iccdata += String.format("9F03%02x%s", temp.length() / 2, temp);

        return iccdata;
    }

    public static byte[] decryptData(byte[] data, byte[] keyData, EmvResponse response) {
        ByteBuffer buffer = ByteBuffer.wrap(data);
        buffer.order(ByteOrder.BIG_ENDIAN);

        try {
            if (buffer.getInt() == Pinpad.TAGS_FORMAT_DATECS) {
                byte[] tmp = new byte[data.length - 4];
                buffer.get(tmp);

                byte[] dec = TripleDES.decryptCBC(keyData, tmp);
                buffer = ByteBuffer.wrap(dec);
                buffer.order(ByteOrder.BIG_ENDIAN);

                // Read format identifier
                buffer.getInt();

                // Read data length;
                int dataLength = buffer.getShort() & 0xffff;

                // read packet identifier
                buffer.getInt(); // Packet identifier

                // Read random data
                byte[] random = new byte[4];
                buffer.get(random);

                // Read device serial number
                byte[] serial = new byte[16];
                buffer.get(serial);

                // Read tags
                byte[] tags = new byte[dataLength - 24];
                buffer.get(tags);

                // Read SHA256
                byte[] hash = new byte[32];
                buffer.get(hash);

                // Verify packet integrity
                boolean ok = SHA256.verify(hash, dec, 6, dataLength);
                if (ok) {

                    Map<Tag, byte[]> tagMap = BerTlv.createMap(tags);

                    Tag tag57 = new Tag(0x57);
                    if (tagMap.containsKey(tag57)) {
                        String track2 = HexUtil.byteArrayToHexString(tagMap.get(tag57));
                        track2 = track2.replace(" ", "");

                        if (track2.charAt(track2.length() - 1) == 'F') {
                            track2 = track2.replace("F", "");
                        }

                        response.setTrack2(track2);

                        System.out.println("Track2: " + track2);

                    }

                    Tag tag5A = new Tag(0x5A);
                    if (tagMap.containsKey(tag5A)) {
                        String pan = HexUtil.byteArrayToHexString(tagMap.get(tag5A));
                        pan = pan.replace(" ", "");

                        if (pan.charAt(pan.length() - 1) == 'F') {
                            pan = pan.replace("F", "");
                        }

                        response.setPan(pan);

                        Globals.pan = getMaskedPan(pan);

                        System.out.println("Pan: " + getMaskedPan(pan));

                    }

                    Tag tag5F24 = new Tag(0x5F24);
                    if (tagMap.containsKey(tag5F24)) {
                        String expirydate = HexUtil.byteArrayToHexString(tagMap.get(tag5F24));
                        expirydate = expirydate.replace(" ", "");

                        response.setExpiry(expirydate);

                    }

                    Tag tag5F20 = new Tag(0x5F20);
                    if (tagMap.containsKey(tag5F20)) {
                        String cardholder = HexUtil.byteArrayToHexString(tagMap.get(tag5F20));
                        cardholder = cardholder.replace(" ", "");

                        System.out.println("Holder: " + cardholder);

                        cardholder = hexToAscii(cardholder);

                        response.setCardholder(cardholder);

                        Globals.cardholder = cardholder;

                    }

                    Tag tag9F02 = new Tag(0x9F02);
                    if (tagMap.containsKey(tag9F02)) {
                        String amount = HexUtil.byteArrayToHexString(tagMap.get(tag9F02));
                        amount = amount.replace(" ", "");

                        response.setAmountAuthorized(amount);

                    }

                    Tag tag95 = new Tag(0x95);
                    if (tagMap.containsKey(tag95)) {
                        String tvr = HexUtil.byteArrayToHexString(tagMap.get(tag95));
                        tvr = tvr.replace(" ", "");

                        response.setTvr(tvr);
                    }

                    Tag tag5F34 = new Tag(0x5F34);
                    if (tagMap.containsKey(tag5F34)) {
                        String sequenceNo = HexUtil.byteArrayToHexString(tagMap.get(tag5F34));
                        sequenceNo = sequenceNo.replace(" ", "");

                        response.setPanSequenceNo(sequenceNo);
                    }

                    Tag tag82 = new Tag(0x82);
                    if (tagMap.containsKey(tag82)) {
                        String aip = HexUtil.byteArrayToHexString(tagMap.get(tag82));
                        aip = aip.replace(" ", "");

                        response.setAip(aip);
                    }

                    Tag tag9F36 = new Tag(0x9F36);
                    if (tagMap.containsKey(tag9F36)) {

                        String atc = HexUtil.byteArrayToHexString(tagMap.get(tag9F36));
                        atc = atc.replace(" ", "");

                        response.setAtc(atc);
                    }

                    Tag tag9F26 = new Tag(0x9F26);
                    if (tagMap.containsKey(tag9F26)) {
                        String cryptogram = HexUtil.byteArrayToHexString(tagMap.get(tag9F26));
                        cryptogram = cryptogram.replace(" ", "");

                        response.setCryptogram(cryptogram);
                    }

                    Tag tag9F27 = new Tag(0x9F27);
                    if (tagMap.containsKey(tag9F27)) {
                        String cryptInfodata = HexUtil.byteArrayToHexString(tagMap.get(tag9F27));
                        cryptInfodata = cryptInfodata.replace(" ", "");

                        response.setCryptogramInformationData(cryptInfodata);
                    }

                    Tag tag9F10 = new Tag(0x9F10);
                    if (tagMap.containsKey(tag9F10)) {
                        String iad = HexUtil.byteArrayToHexString(tagMap.get(tag9F10));
                        iad = iad.replace(" ", "");

                        response.setIad(iad);
                    }

                    Tag tag9C = new Tag(0x9C);
                    if (tagMap.containsKey(tag9C)) {

                        String txnType = HexUtil.byteArrayToHexString(tagMap.get(tag9C));
                        txnType = txnType.replace(" ", "");

                        response.setTransactionType(txnType);
                    }

                    Tag tag9F37 = new Tag(0x9F37);
                    if (tagMap.containsKey(tag9F37)) {
                        String unpredictableNumber = HexUtil.byteArrayToHexString(tagMap.get(tag9F37));
                        unpredictableNumber = unpredictableNumber.replace(" ", "");

                        response.setUnpredictableNo(unpredictableNumber);
                    }

                    Tag tag9F35 = new Tag(0x9F35);
                    if (tagMap.containsKey(tag9F35)) {
                        String terminalType = HexUtil.byteArrayToHexString(tagMap.get(tag9F35));
                        terminalType = terminalType.replace(" ", "");

                        response.setTerminalType(terminalType);
                    }

                    Tag tag9F33 = new Tag(0x9F33);
                    if (tagMap.containsKey(tag9F33)) {
                        String terminalCapabilities = HexUtil.byteArrayToHexString(tagMap.get(tag9F33));
                        terminalCapabilities = terminalCapabilities.replace(" ", "");

                        response.setTerminalCapabilities(terminalCapabilities);
                    }

                    Tag tag9A = new Tag(0x9A);
                    if (tagMap.containsKey(tag9A)) {
                        String date = HexUtil.byteArrayToHexString(tagMap.get(tag9A));
                        date = date.replace(" ", "");

                        response.setTransDate(date);
                    }
                    Tag tag9F34 = new Tag(0x9F34);
                    if (tagMap.containsKey(tag9F34)) {
                        String cvmResult = HexUtil.byteArrayToHexString(tagMap.get(tag9F34));
                        cvmResult = cvmResult.replace(" ", "");

                        response.setCvmResult(cvmResult);
                    }

                    Tag tag9F06 = new Tag(0x9F06);
                    if (tagMap.containsKey(tag9F06)) {
                        String aid = HexUtil.byteArrayToHexString(tagMap.get(tag9F06));
                        aid = aid.replace(" ", "");

                        response.setAid(aid);

                        Globals.aid = aid;
                    }

                    Tag tag50 = new Tag(0x50);
                    if (tagMap.containsKey(tag50)) {
                        String appLabel = HexUtil.byteArrayToHexString(tagMap.get(tag50));
                        appLabel = appLabel.replace(" ", "");

                        appLabel = hexToAscii(appLabel);

                        System.out.println("tender: " + appLabel);

                        response.setApplabel(appLabel);

                        Globals.tenderType = appLabel;
                    }

                    response.setAmountOther("000000000000");

                    return tags;
                }
            } else {
                throw new IllegalArgumentException("Invalid encryption format");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String getMaskedPan(String pan) {
        int panLength = pan.length();
        int asterisk = panLength - 10;

        String asteriskString = "";

        for (int i = 0; i < asterisk; i++) {

            asteriskString += "*";

        }

        final String pan1 = pan.substring(0, 6) + asteriskString
                + pan.substring(panLength - 4, panLength);
        Globals.pan = pan1;

        return pan1;
    }

    private void fail(final String text) {
        Log.e(LOG_TAG, text);

    }

    private void warn(final String text) {
        Log.w(LOG_TAG, text);

    }

    private void log(final String text) {
        log(text, false);
    }

    private void log(final String text, final boolean bold) {
        Log.d(LOG_TAG, text);

    }

    private void log(String text, List<BerTlv> data) {
        for (BerTlv tlv : data) {
            Tag tag = tlv.getTag();

            if (tag.isConstructed()) {
                String s = Integer.toHexString(tag.toIntValue()) + " (CONSTRUCTED)";

                try {
                    List<BerTlv> list = BerTlv.createList(tlv.getValue());
                    log(text + s, false);
                    log("  " + text, list);
                } catch (Exception e) {
                    log(text + s + tlv.getValueHexString(), false);
                }
            } else {

                if (Integer.toHexString(tag.toIntValue()).equals("5A")) {
                    System.out.println("PAN IS HERE: " + tlv.getValueHexString() + " END HERE");
                }
                String s = Integer.toHexString(tag.toIntValue()) + ", " + tlv.getValueHexString();
                log(text + s, false);
            }
        }
    }

    public static String hexToAscii(String hexStr) {
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < hexStr.length(); i += 2) {
            String str = hexStr.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }

        return output.toString();
    }


    public static String getPosAccount() {
        String posAccount = ReferenceList.config.getString(ReferenceList.posAccountUsername, "none");
        if (posAccount != null && posAccount.equals("")) {
            posAccount = Globals.posAccount;
        }
        return posAccount;
    }

    public static String getTerminalId() {
        String terminalId = ReferenceList.config.getString(ReferenceList.terminalId, "none");
        if (terminalId != null && terminalId.equals("")) {
            terminalId = Globals.terminalId;
        }
        return terminalId;
    }


    private long saveTransaction(String rrn, String stan, String pan) {

        Transaction transaction = new Transaction();
        TransactionRepo transactionRepo = new TransactionRepo();

        Double floatAmount = Globals.amount;
        float floatConvenience = Float.valueOf(Globals.convenienceFee);


        String terminalId = getTerminalId();
        String posAccount = getPosAccount();

        JSONObject miscellanyInfo = new JSONObject();

        try {

            //PAYCENTRE PARAMS;
            miscellanyInfo.put("rrn", rrn);
            miscellanyInfo.put("stan", stan);
            miscellanyInfo.put("pan", pan);
            miscellanyInfo.put("stampDuty", Globals.stampDuty);
            miscellanyInfo.put("terminalId", terminalId);
            miscellanyInfo.put("accountType", "NIL");
            miscellanyInfo.put("aid", "NIL");
            miscellanyInfo.put("authCode", "NIL");
            miscellanyInfo.put("cardHolder", "NIL");
            miscellanyInfo.put("expiry", "NIL");
            miscellanyInfo.put("tenderType", "NIL");
            miscellanyInfo.put("sequenceNo", "NIL");
            miscellanyInfo.put("transactionTypeId", Globals.transactionTypeId);
            miscellanyInfo.put("statusCode", "XX");
            miscellanyInfo.put("statusMessage", "FAILED");
            miscellanyInfo.put("version",MainActivity.getAppVersion(context));
            miscellanyInfo.put("vat", Globals.vat);

            //IBETA PARAMS
            miscellanyInfo.put("createdOn", Calendar.getInstance().getTime());
            miscellanyInfo.put("email", Globals.email);
            miscellanyInfo.put("username", Globals.username);
            miscellanyInfo.put("cardNo", pan);
            miscellanyInfo.put("response", "FAILED");
            miscellanyInfo.put("responseCode", "XX");
            miscellanyInfo.put("purchase", Globals.purchase);
            miscellanyInfo.put("total", Globals.purchase);
            miscellanyInfo.put("transactionType", "card");
            miscellanyInfo.put("phone", WithdrawalAmountDetailsActivity.phone);

            transaction.setAgentCut(0);
            transaction.setAmount(floatAmount.floatValue());
            transaction.setConvenienceFee(floatConvenience);
            transaction.setDate(new Date());
            transaction.setPosAccount(posAccount);
            transaction.setTransactionId(Globals.transactionId);
            transaction.setResponseCode(Globals.responseCode);
            transaction.setResponseMsg(Globals.appResponse);
            transaction.setVersion(MainActivity.getAppVersion(context));
            transaction.setAgentCut(Float.valueOf("0.0"));
            transaction.setTransactionType(Transaction.Type.getByName(Globals.withdrawalType.toUpperCase()));
            transaction.setSynced(Transaction.Sync.SYNCREQBOTH.getValue());
            transaction.setData(miscellanyInfo);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return transactionRepo.insert(transaction);

    }


}
