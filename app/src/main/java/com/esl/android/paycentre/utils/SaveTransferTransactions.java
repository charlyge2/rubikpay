package com.esl.android.paycentre.utils;

import android.content.Context;

import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;

import org.json.JSONObject;

import java.util.Date;

public class SaveTransferTransactions {

    public static long initialSaveTransactions(Context context, String accountName, String accountNumber, String bankName, String phone, String vat, String posAccount, String transactionId, String amount, String convenienceFee,String transactionType){
        JSONObject params = new JSONObject();
        try {
            params.put("accountName", accountName);
            params.put("accountNumber", accountNumber);
            params.put("bankName",bankName);
            params.put("phone",phone);
            params.put("vat",vat);
        }

        catch (Exception e){
              e.printStackTrace();
        }

        Transaction transaction = new Transaction();
        TransactionRepo transactionRepo = new TransactionRepo();

        float floatAmount = Float.parseFloat(amount.replace(",", ""));
        float floatConvenience = Float.valueOf(convenienceFee);

        transaction.setAgentCut(0);
        transaction.setAmount(floatAmount);
        transaction.setConvenienceFee(floatConvenience);
        transaction.setDate(new Date());
        transaction.setPosAccount(posAccount);
        transaction.setTransactionId(transactionId);
        transaction.setResponseCode("00");
        transaction.setResponseMsg("SUCCESSFUL");
        transaction.setVersion(MainActivity.getAppVersion(context));
        transaction.setAgentCut(Float.valueOf("0.0"));
         transaction.setTransactionType(Transaction.Type.getByName(transactionType));
        transaction.setData(params);

        long result = transactionRepo.insert(transaction);

        return result;


    }


    public static long updateTransferTransactions(Context context, String accountName, String accountNumber, String bankName, String phone, String vat, String posAccount, String transactionId, String responseCode, String responseMessage, String amount, String convenienceFee,String transactionType){
        JSONObject params = new JSONObject();
        try {
            params.put("accountName", accountName);
            params.put("accountNumber", accountNumber);
            params.put("bankName",bankName);
            params.put("phone",phone);
            params.put("vat",vat);
        }

        catch (Exception e){
            e.printStackTrace();
        }

        Transaction transaction = new Transaction();
        TransactionRepo transactionRepo = new TransactionRepo();

        float floatAmount = Float.parseFloat(amount.replace(",", ""));
        float floatConvenience = Float.valueOf(convenienceFee);

        transaction.setAgentCut(0);
        transaction.setAmount(floatAmount);
        transaction.setConvenienceFee(floatConvenience);
        transaction.setDate(new Date());
        transaction.setPosAccount(posAccount);
        transaction.setTransactionId(transactionId);
        transaction.setResponseCode(responseCode);
        transaction.setResponseMsg(responseMessage);
        transaction.setVersion(MainActivity.getAppVersion(context));
        transaction.setAgentCut(Float.valueOf("0.0"));
        transaction.setTransactionType(Transaction.Type.getByName(transactionType));
        transaction.setData(params);

        long result = transactionRepo.update(transaction);

        return result;


    }
}
