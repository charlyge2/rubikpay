package com.esl.android.paycentre.activities.billsPayment.others;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.billsPayment.BillsActivity;
import com.esl.android.paycentre.adapters.WaecList;
import com.esl.android.paycentre.adapters.WaecListAdapter;
import com.esl.android.paycentre.constants.Constant;
import com.esl.android.paycentre.controllers.BillsController;
import com.esl.android.paycentre.utils.TransactionFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WaecListingActivity extends AppCompatActivity implements WaecListAdapter.onWaecItemClickListener {

    private ArrayList<WaecList> waecList = new ArrayList<>();

    private AlertDialog alertDialog = null;

    Button makePaymentButton;

    WaecListAdapter waecListAdapter;

    ListView waecListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waec_listing);

        Intent intent = getIntent();
        String listings = intent.getStringExtra("waec_listings");

        waecListview = findViewById(R.id.waecListView);

        populateWaecListings(listings);

    }

    private void populateWaecListings(String listings) {

        waecList.clear();

        Log.d("LISTINGS",listings);

        JSONArray jsonArr = new JSONArray();

        try {

            jsonArr = new JSONArray(listings);

            for (int i = 0; i < jsonArr.length(); i++)
            {
                JSONObject jsonObj = new JSONObject();

                try {

                    String count = jsonArr.getJSONObject(i).getString("count");
                    String amount = jsonArr.getJSONObject(i).getString("amount");

                    waecList.add(new WaecList(count,amount));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println(jsonObj);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        waecListview.setDivider(null);
        waecListAdapter = new WaecListAdapter(this,this, waecList);
        waecListview.setAdapter(waecListAdapter);

    }

    public void goBack(View view) {
        Intent intent = new Intent(getApplicationContext(), BillsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemClick(WaecList waecList) {
        processWaecPayment(waecList);
    }


    private void processWaecPayment(WaecList waecList) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(WaecListingActivity.this);
        final View loginFormView = getLayoutInflater().inflate(R.layout.amount_confirmation_layout, null);
        // Set above view in alert dialog.
        builder.setView(loginFormView);

        builder.setCancelable(true);
        alertDialog = builder.create();
        alertDialog.show();

        makePaymentButton = alertDialog.findViewById(R.id.makePaymentButton);
        TextView waecAmount_hint = alertDialog.findViewById(R.id.waecAmount_hint);
        if (waecAmount_hint != null) {
            if (waecList != null) {
                waecAmount_hint.setText("You are about to purchase Waec pin at a price of " + waecList.getAmount() +" Naira. \n Do you want to proceed ?");
            }
        }

        makePaymentButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.hide();

                        JSONObject params = new JSONObject();

                        try {
                            if (waecList != null) {
                                params.put("amount",waecList.getAmount());
                            }
                            params.put("transactionId", TransactionFormatter.generateTransactionId());
                            params.put("appTime", TransactionFormatter.generateAppTime());
                            params.put("agentFee","0");
                            params.put("convenienceFee","0");
                            //params.put("posAccount", ConfigurationClass.getPosAccount(getApplicationContext()));

                         //   BillsController.billsPaymentProcessor(WaecListingActivity.this, Constant.WAEC_PAYMENT_URL,"WAEC",params);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
        );
    }
}
