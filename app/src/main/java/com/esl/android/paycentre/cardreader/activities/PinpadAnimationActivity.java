package com.esl.android.paycentre.cardreader.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;

import pl.droidsonroids.gif.GifImageView;

public class PinpadAnimationActivity extends AppCompatActivity {

    private GifImageView animationImageView;
    private TextView actionTextView;
    private PinpadMessageReceiver messageReceiver;

    private PinpadFacade pinpadFacade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinpad_animation);

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        pinpadFacade = new PinpadFacade(PinpadAnimationActivity.this);

        animationImageView = findViewById(R.id.animationImageView);
        actionTextView = findViewById(R.id.actionTextView);

        animationImageView.setImageResource(R.drawable.insert);
        actionTextView.setText("PLEASE INSERT CARD");

        messageReceiver = new PinpadMessageReceiver();

        registerReceiver(messageReceiver, new IntentFilter("com.esl.paycenter"));

        pinpadFacade.makePayment();

    }



    @Override
    public void onBackPressed() {


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(messageReceiver);
    }

    public class PinpadMessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null && intent.getAction() != null) {

                String s = intent.getAction();

                if (s.equals("com.esl.paycenter")) {

                    //TO DO
                    // change result names
                    //insert card, processing, enter pin, nibss processing

                    String result = intent.getStringExtra("response");

                    Log.d("PINPAD RESPONSES", result);

                    if (result.equals("insertCard")) {

                        animationImageView.setImageResource(R.drawable.insert);
                        actionTextView.setText("PLEASE INSERT CARD");

                    }

                    if (result.equals("pinpadProcessing")) {

                        animationImageView.setImageResource(R.drawable.process);
                        actionTextView.setText("PINPAD PROCESSING");


                    }

                    if (result.equals("enterPIN")) {

                        animationImageView.setImageResource(R.drawable.enterpin);
                        actionTextView.setText("ENTER PIN");

                    }
                    if (result.equals("nibssProcessing")) {

                        animationImageView.setImageResource(R.drawable.bank);
                        actionTextView.setText("PROCESSING WITH BANK");
                        //SyncService.scheduleSync(getApplicationContext());

                    }
                    if (result.equals("stop")) {

                        Intent intent1 = new Intent(getApplicationContext(), AccountTypeActivity.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent1);

                    }

                }
            }

        }

    }
}
