package com.esl.android.paycentre.interfaces;

public interface TokenTimerListener {
    public void onTimerCompleted();
}
