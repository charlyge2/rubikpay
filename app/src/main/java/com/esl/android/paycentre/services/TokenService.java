package com.esl.android.paycentre.services;

import android.os.CountDownTimer;
import android.util.Log;

import com.esl.android.paycentre.interfaces.InactivityTimerListener;
import com.esl.android.paycentre.interfaces.TokenTimerListener;

public class TokenService {

    CountDownTimer timer;
    TokenTimerListener listener;

    long intervals;
    boolean finished = false;
    public static long timeLeft = 0;

    public TokenService(long intervals,TokenTimerListener listener) {
        this.intervals = intervals;
        this.listener = listener;
        this.timer = this.createNewTimer();
        this.start();

        Log.d("TIMER: ", "Timer started!");

    }

    public void restart() {
        if (finished)
            return;

        Log.d("TIMER: ", "Timer restarted!");
        this.start();
    }

    public void start() {
        Log.d("TIMER: ", "Timer started!");
        finished = false;
        timer.cancel();
        timer = createNewTimer();
        timer.start();
    }

    private CountDownTimer createNewTimer() {
        return new CountDownTimer(this.intervals, 1) {
            @Override
            public void onTick(long millisUntilFinished) {
               timeLeft =  millisUntilFinished / 1000;
            }

            @Override
            public void onFinish() {
                Log.d("TIMER: ", "Timer finished!");
                finished = true;
                TokenService.this.listener.onTimerCompleted();
            }
        };
    }
}
