package com.esl.android.paycentre.activities.dispute;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class CreateDisputeActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_ID = 234;

    private static final String TAG = "CreateDisputeActivity";

    private EditText customerNameEditText;
    private EditText customerPhoneEditText;
    private ImageButton uploadImageButton;
    private TextView uploadTextView;
    private ImageView uploadedImageView;
    private Button submitButton;
    private EditText panDigitsEditText;

    private String imagePath;
    private String transactionId;
    String customerName;
    String customerPhone;
    String panLastFour = "";
    String response = "";

    private ProgressDialog pDialog;

    private File destination;
    private String encoded = "";

    private static SharedPreferenceManager sharedPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_dispute);

        //init view
        init();

        uploadTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                takeUploadImage();

            }
        });

        uploadImageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                takeUploadImage();

            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                submitDispute();

            }
        });

    }

    private void init() {

        customerNameEditText = findViewById(R.id.customerNameTextView);
        customerPhoneEditText = findViewById(R.id.customerPhoneTextView);
        panDigitsEditText = findViewById(R.id.panDigitsEditText);
        uploadImageButton = findViewById(R.id.uploadImageButton);
        uploadTextView = findViewById(R.id.uploadTextView);
        uploadedImageView = findViewById(R.id.uploadedImageView);
        submitButton = findViewById(R.id.submitButton);



        if(Globals.disputeType.equals("FUNDS_TRANSFER")){
            uploadedImageView.setVisibility(View.GONE);
            uploadTextView.setVisibility(View.GONE);
            uploadImageButton.setVisibility(View.GONE);
        }

        uploadedImageView.setVisibility(View.GONE);

        Intent intent = getIntent();

        transactionId = intent.getStringExtra("transactionId");

        response = intent.getStringExtra("response");

        if (response.equals("1")){
            panDigitsEditText.setVisibility(View.VISIBLE);
        }

        isStoragePermissionGranted();

    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }

    private void takeUploadImage() {


        PickImageDialog.build(new PickSetup())
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        //TODO: do what you have to...
                        uploadedImageView.setImageBitmap(r.getBitmap());
                        uploadedImageView.setVisibility(View.VISIBLE);

                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        r.getBitmap().compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();

                        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

                        Log.i("BASE64", encoded);
                    }
                })
                .setOnPickCancel(new IPickCancel() {
                    @Override
                    public void onCancelClick() {
                        //Alert.showInfo(getApplicationContext(),"CANCEL");
                        //TODO: do what you have to if user clicked cancel
                    }
                }).show(getSupportFragmentManager());
    }

    private void submitDispute() {

        customerName = customerNameEditText.getText().toString();
        customerPhone = customerPhoneEditText.getText().toString();

        if (response.equals("1")){

            panLastFour = panDigitsEditText.getText().toString();

            if (panLastFour.isEmpty() || panLastFour.length() < 4){
                Alert.showWarning(getApplicationContext(), "Please add last four digits of PAN");
                submitButton.setEnabled(false);
            }
        }

        if (customerName.isEmpty() || customerPhone.isEmpty()) {

            Alert.showWarning(getApplicationContext(), "Kindly fill customer details");

        } else {

            if(Globals.disputeType.equals("CASHOUT")){

                if (uploadedImageView.getDrawable() == null || encoded.isEmpty()) {

                    Alert.showWarning(getApplicationContext(), "Kindly put evidence of debit");

                } else {

                    try{

                        JSONObject disputePayload = new JSONObject();

                        String base64String = "data:image/jpeg;base64,"+encoded;

                        disputePayload.put("fullName", customerName);
                        disputePayload.put("phoneNumber", customerPhone);
                        disputePayload.put("transactionId", transactionId);
                        disputePayload.put("proof", encoded);
                        if (response.equals("1")){
                            disputePayload.put("pan", panLastFour);
                        }

                        Log.i("Dispute Body", disputePayload.toString());


  //                      HttpService.createDispute(CreateDisputeActivity.this, disputePayload);

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }else{

                try{

                    JSONObject disputePayload = new JSONObject();

                    disputePayload.put("fullName", customerName);
                    disputePayload.put("phoneNumber", customerPhone);
                    disputePayload.put("transactionId", transactionId);

//                    HttpService.createDispute(CreateDisputeActivity.this, disputePayload);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

    public void moveHome(View view) {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }
}
