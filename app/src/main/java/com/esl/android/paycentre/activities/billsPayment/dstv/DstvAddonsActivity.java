package com.esl.android.paycentre.activities.billsPayment.dstv;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.adapters.DstvAddonAdapter;
import com.esl.android.paycentre.adapters.ProductModel;
import com.esl.android.paycentre.interfaces.ConvenienceFeeInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.esl.android.paycentre.utils.TransactionFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DstvAddonsActivity extends BaseActivity implements ConvenienceFeeInterface {

    //a list to store all the products
    private List<ProductModel> productList;

    private String formattedString;

    private String price;

    private ArrayList<String> addonCodeArray = new ArrayList<String>();

    private ArrayList<String> addonAmountArray = new ArrayList<String>();

    private CheckBox addonsCheckBox;

    private RecyclerView recyclerView;

    private Double productAmount;

    private TextView total;

    private String planCode;

    private String products;

    private DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    private Button dstvAddonBtn;

    private Double sum;

    private String formattedPrice;

    private Double initialPlanValue;

    private TextView addonDescription;

    SharedPreferenceManager sharedPreferenceManager;

    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Step 4 of 4: Finally call getTag() on the view.
            // This viewHolder will have all required values.
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            ProductModel thisItem = productList.get(position);

            String plan = thisItem.getPlan();
            price = thisItem.getPrice();
            String code = thisItem.getPlanCode();

            formattedPrice = price.replaceAll("[₦,]", "");

            int id = thisItem.getId();

            addonsCheckBox = view.findViewById(R.id.addonCheckbox);

            if (addonsCheckBox.isChecked()) {
                addonsCheckBox.setChecked(false);
                addonCodeArray.remove(code);
                addonAmountArray.remove(formattedPrice);
                productAmount -= Double.parseDouble(formattedPrice);
                formattedString = "TOTAL : ₦" + formatter.format(productAmount);
                total.setText(formattedString);
            } else {
                addonCodeArray.add(code);
                addonsCheckBox.setChecked(true);
                addonAmountArray.add(formattedPrice);
                productAmount += Double.parseDouble(formattedPrice);
                formattedString = "TOTAL : ₦" + formatter.format(productAmount);
                total.setText(formattedString);
            }

            sum = 0.0;

            for (int i = 0; i < addonAmountArray.size(); i++) {
                System.out.println(addonAmountArray.get(i));
                sum += Double.parseDouble(addonAmountArray.get(i));
            }

            System.out.println("SUM = " + sum);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dstv_addons);

        addonDescription = findViewById(R.id.addonDescription);
        total = findViewById(R.id.addonsTotal);
        dstvAddonBtn = findViewById(R.id.dstvAddonsBtn);
        final String posAccountUsername = sharedPreferenceManager.getString(getApplicationContext(), "posAccountUsername", "");

        dstvAddonBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!addonCodeArray.isEmpty()) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DstvAddonsActivity.this);
                            alertDialogBuilder.setTitle("Transaction Details");
                            alertDialogBuilder
                                    .setMessage("\nDSTV PLAN PRICE : ₦" + formatter.format(initialPlanValue) + "\n\nADDONS ADDED  : ₦" + formatter.format(sum) + "\n\nTOTAL : ₦" + formatter.format(productAmount))
                                    .setCancelable(false)
                                    .setPositiveButton("OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    try {

                                                        JSONObject params = new JSONObject();

                                                        params.put("version", MainActivity.appVersionUrl);
                                                        params.put("cardNumber", DstvNumberActivity.cardNumber);
                                                        params.put("productCode", planCode);
                                                        params.put("addOns", new JSONArray(addonCodeArray));
                                                        params.put("amount", String.valueOf(Double.valueOf(String.valueOf(productAmount)).intValue()));
                                                        params.put("posAccount", posAccountUsername);
                                                        params.put("convenienceFee", "0");
                                                        params.put("invoicePeriod", DstvNumberActivity.dstvInvoicePeriod);
                                                        params.put("customerName", DstvNumberActivity.dstvCustomerName);
                                                        params.put("customerNumber", DstvNumberActivity.dstvCustomerNumber);
                                                        params.put("transactionType", "DSTV");
                                                        params.put("transactionId", TransactionFormatter.generateTransactionId());
                                                        params.put("appTime", TransactionFormatter.generateAppTime());

                                                        String url = MainActivity.dstvPaymentUrl;

                                                        String type = "dstv";

                                                        HttpService.makePayment(DstvAddonsActivity.this, params, url, type);

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            })
                                    .setNegativeButton("CANCEL",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        } else {
                            try {

                                JSONObject params = new JSONObject();

                                params.put("version", MainActivity.appVersionUrl);
                                params.put("cardNumber", DstvNumberActivity.cardNumber);
                                params.put("productCode", planCode);
                                params.put("addOns", new JSONArray(addonCodeArray));
                                params.put("amount", String.valueOf(Double.valueOf(String.valueOf(productAmount)).intValue()));
                                params.put("posAccount", posAccountUsername);
                                params.put("convenienceFee", "0");
                                params.put("invoicePeriod", DstvNumberActivity.dstvInvoicePeriod);
                                params.put("customerName", DstvNumberActivity.dstvCustomerName);
                                params.put("customerNumber", DstvNumberActivity.dstvCustomerNumber);
                                params.put("transactionType", "DSTV");
                                params.put("transactionId", TransactionFormatter.generateTransactionId());
                                params.put("appTime", TransactionFormatter.generateAppTime());

                                String url = MainActivity.dstvPaymentUrl;

                                String type = "dstv";

                                HttpService.makePayment(DstvAddonsActivity.this, params, url, type);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        );

        recyclerView = (RecyclerView) findViewById(R.id.addonRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        DstvAddonAdapter.setOnItemClickListener(onItemClickListener);

        Intent intent = getIntent();
        products = intent.getStringExtra("products");
        planCode = intent.getStringExtra("code");
        productAmount = Double.valueOf(intent.getStringExtra("price").replaceAll("[₦,]", ""));

        Log.d("ADDONS", products);

        formattedString = "TOTAL : ₦" + formatter.format(productAmount);
        initialPlanValue = productAmount;
        total.setText(formattedString);


        JSONArray jsonArr = new JSONArray();

        productList = new ArrayList<>();

        try {

            jsonArr = new JSONArray(products);

            if (jsonArr.length() == 0 || jsonArr == null) {
                recyclerView.setVisibility(View.GONE);
                addonDescription.setText("PROCEED TO MAKE PAYMENT");
            }

            for (int i = 0; i < jsonArr.length(); i++) {

                JSONObject jsonObj = new JSONObject();

                try {

                    String plan = jsonArr.getJSONObject(i).getString("name");
                    String price = jsonArr.getJSONObject(i).getString("price");
                    String code = jsonArr.getJSONObject(i).getString("code");

                    double formattedPrice = Double.parseDouble(price);

                    String formattedString = "₦" + formatter.format(formattedPrice);

                    productList.add(
                            new ProductModel(
                                    i,
                                    plan,
                                    code,
                                    formattedString));

                    DstvAddonAdapter adapter = new DstvAddonAdapter(this, productList);

                    recyclerView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println(jsonObj);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendConvenienceFee(String fee) {

    }

    @Override
    public void sendConvenienceFee(String fee, String StampDuty, boolean showStampDuty, String vat, boolean showVat) {
        HttpService.progressDialog.dismiss();

        try {

            JSONObject params = new JSONObject();

            params.put("version", MainActivity.appVersionUrl);
            params.put("cardNumber", DstvNumberActivity.cardNumber);
            params.put("productCode", planCode);
            params.put("addOns", new JSONArray(addonCodeArray));
            params.put("amount", String.valueOf(Double.valueOf(String.valueOf(productAmount)).intValue()));
            params.put("posAccount", "");
            params.put("convenienceFee", fee);
            params.put("invoicePeriod", DstvNumberActivity.dstvInvoicePeriod);
            params.put("customerName", DstvNumberActivity.dstvCustomerName);
            params.put("customerNumber", DstvNumberActivity.dstvCustomerNumber);
            params.put("transactionType", "DSTV");
            params.put("transactionId", TransactionFormatter.generateTransactionId());
            params.put("appTime", TransactionFormatter.generateAppTime());

            String url = MainActivity.dstvPaymentUrl;

            String type = "dstv";

            HttpService.makePayment(DstvAddonsActivity.this, params, url, type);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
