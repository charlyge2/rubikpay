package com.esl.android.paycentre.cardreader.serializers;

public class _0200Request {
	private String pan;
	private String processingCode;
	private String transactionAmount;
	private String transmissionDateTime;
	private String stan;
	private String localTransactionTime;
	private String localTransactionDate;
	private String cardExpirationDate;
	private String merchantType;
	private String posEntryMode;
	private String cardSequenceNumber;
	private String posConditionCode;
	private String posPinCaptureCode;
	private String transactionFee;
	private String acquiringInstIdCode;
	private String track2Data;
	private String retrievalReferenceNumber;
	private String terminalId;
	private String cardAcceptorIdCode;
	private String cardAcceptorNameLocation;
	private String currencyCode;
	private String pinData;
	private String iccData;
	private String posDataCode;
	private String hashValue;
	private String serviceRestrictionCode;
	private String securityRelatedInformation;
	private String additionalAmounts;
	private String messageReasonCode;
	private String transportData;
	private String paymentInformation;
	private String secondaryHashValue;
	private String originalDataElements;
	private String replacementAmount;
	
	public String getReplacementAmount() {
		return replacementAmount;
	}
	public void setReplacementAmount(String replacementAmount) {
		this.replacementAmount = replacementAmount;
	}
	public String getOriginalDataElements() {
		return originalDataElements;
	}
	public void setOriginalDataElements(String originalDataElements) {
		this.originalDataElements = originalDataElements;
	}
	public String getSecondaryHashValue() {
		return secondaryHashValue;
	}
	public void setSecondaryHashValue(String secondaryHashValue) {
		this.secondaryHashValue = secondaryHashValue;
	}
	public String getPaymentInformation() {
		return paymentInformation;
	}
	public void setPaymentInformation(String paymentInformation) {
		this.paymentInformation = paymentInformation;
	}
	public String getTransportData() {
		return transportData;
	}
	public void setTransportData(String transportData) {
		this.transportData = transportData;
	}
	public String getMessageReasonCode() {
		return messageReasonCode;
	}
	public void setMessageReasonCode(String messageReasonCode) {
		this.messageReasonCode = messageReasonCode;
	}
	public String getAdditionalAmounts() {
		return additionalAmounts;
	}
	public void setAdditionalAmounts(String additionalAmounts) {
		this.additionalAmounts = additionalAmounts;
	}
	public String getSecurityRelatedInformation() {
		return securityRelatedInformation;
	}
	public void setSecurityRelatedInformation(String securityRelatedInformation) {
		this.securityRelatedInformation = securityRelatedInformation;
	}
	public String getServiceRestrictionCode() {
		return serviceRestrictionCode;
	}
	public void setServiceRestrictionCode(String serviceRestrictionCode) {
		this.serviceRestrictionCode = serviceRestrictionCode;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getProcessingCode() {
		return processingCode;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	public String getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getTransmissionDateTime() {
		return transmissionDateTime;
	}
	public void setTransmissionDateTime(String transmissionDateTime) {
		this.transmissionDateTime = transmissionDateTime;
	}
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public String getLocalTransactionTime() {
		return localTransactionTime;
	}
	public void setLocalTransactionTime(String localTransactionTime) {
		this.localTransactionTime = localTransactionTime;
	}
	public String getLocalTransactionDate() {
		return localTransactionDate;
	}
	public void setLocalTransactionDate(String localTransactionDate) {
		this.localTransactionDate = localTransactionDate;
	}
	public String getCardExpirationDate() {
		return cardExpirationDate;
	}
	public void setCardExpirationDate(String cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}
	public String getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}
	public String getPosEntryMode() {
		return posEntryMode;
	}
	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}
	public String getCardSequenceNumber() {
		return cardSequenceNumber;
	}
	public void setCardSequenceNumber(String cardSequenceNumber) {
		this.cardSequenceNumber = cardSequenceNumber;
	}
	public String getPosConditionCode() {
		return posConditionCode;
	}
	public void setPosConditionCode(String posConditionCode) {
		this.posConditionCode = posConditionCode;
	}
	public String getPosPinCaptureCode() {
		return posPinCaptureCode;
	}
	public void setPosPinCaptureCode(String posPinCaptureCode) {
		this.posPinCaptureCode = posPinCaptureCode;
	}
	public String getTransactionFee() {
		return transactionFee;
	}
	public void setTransactionFee(String transactionFee) {
		this.transactionFee = transactionFee;
	}
	public String getAcquiringInstIdCode() {
		return acquiringInstIdCode;
	}
	public void setAcquiringInstIdCode(String acquiringInstIdCode) {
		this.acquiringInstIdCode = acquiringInstIdCode;
	}
	public String getTrack2Data() {
		return track2Data.replace("?", "").replace(";", "").replace("=", "D");
	}
	public void setTrack2Data(String track2Data) {
		this.track2Data = track2Data;
	}
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public String getCardAcceptorIdCode() {
		return cardAcceptorIdCode;
	}
	public void setCardAcceptorIdCode(String cardAcceptorIdCode) {
		this.cardAcceptorIdCode = cardAcceptorIdCode;
	}
	public String getCardAcceptorNameLocation() {
	//	return "";
		return cardAcceptorNameLocation;
	}
	public void setCardAcceptorNameLocation(String cardAcceptorNameLocation) {
		this.cardAcceptorNameLocation = cardAcceptorNameLocation;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getPinData() {
		return pinData;
	}
	public void setPinData(String pinData) {
		this.pinData = pinData;
	}
	public String getIccData() {
		return iccData.replace(" ", "");
	}
	public void setIccData(String iccData) {
		this.iccData = iccData;
	}
	public String getPosDataCode() {
		return posDataCode;
	}
	public void setPosDataCode(String posDataCode) {
		this.posDataCode = posDataCode;
	}
	public String getHashValue() {
		return hashValue;
	}
	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}
	
	
	
	
}
