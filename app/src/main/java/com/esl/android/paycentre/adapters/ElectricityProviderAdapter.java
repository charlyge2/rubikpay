package com.esl.android.paycentre.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.esl.android.paycentre.R;

import java.util.List;

public class ElectricityProviderAdapter extends RecyclerView.Adapter<ElectricityProviderAdapter.ElectricityViewModel> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the NotificationModels in a list
    private List<BillList> billLists;
    private onBillItemClickListener onBillItemClickListener;

    public interface onBillItemClickListener{
        void onBillItemClicked(int position);
    }

    //getting the context and Notification list with constructor
    public ElectricityProviderAdapter(Context mCtx,onBillItemClickListener onBillItemClickListener, List<BillList> billLists) {
        this.mCtx = mCtx;
        this.billLists = billLists;
        this.onBillItemClickListener = onBillItemClickListener;
    }

    @Override
    public ElectricityViewModel onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.electricity_list_item, parent,false);
        return new ElectricityViewModel(view);
    }

    @Override
    public void onBindViewHolder(ElectricityViewModel holder, int position) {
        //getting the Notification of the specified position
        BillList billList = billLists.get(position);

        //binding the data with the viewholder views
        holder.billName.setText(billList.getBillName());
        if(billList.getBillName().equals("EKEDC")){
            holder.billImage.setImageResource(R.drawable.ekedc);
            return;

        }
        if(billList.getBillName().equals("IKEDC")){
            holder.billImage.setImageResource(R.drawable.ikedc);
         return;
        }
        holder.billImage.setImageResource(R.drawable.ic_power);


    }


    @Override
    public int getItemCount() {
        if(billLists==null)return 0;
        return billLists.size();
    }





    class ElectricityViewModel extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView billName;
        ImageView billImage;

        public ElectricityViewModel(View itemView) {
            super(itemView);

            billName = itemView.findViewById(R.id.billName);
            billImage = itemView.findViewById(R.id.billImage);
            itemView.setTag(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onBillItemClickListener.onBillItemClicked(getAdapterPosition());
        }
    }


    public void setBillLists(List<BillList> billLists) {
        this.billLists = billLists;
        notifyDataSetChanged();
    }
}
