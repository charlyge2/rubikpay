package com.esl.android.paycentre.database.model;

public class PosAccount {
    public static final String TAG = PosAccount.class.getSimpleName();
    public static final String TABLE = "pos_account";

    // Labels Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FIRSTNAME = "firstname";
    public static final String KEY_LASTNAME = "lastname";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_ACCOUNTID = "posaccountid";

    private int id;
    private String username;
    private String firstname ;
    private String lastname;
    private String password;
    private String posAccountId;

    public PosAccount() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosAccountId() {
        return posAccountId;
    }

    public void setPosAccountId(String posAccountId) {
        this.posAccountId = posAccountId;
    }
}
