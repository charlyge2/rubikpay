package com.esl.android.paycentre.database.repo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.utils.Display;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;

public class TransactionRepo implements RepositoryInterface<Transaction> {

    public static String createTable() {
        return "CREATE TABLE " + Transaction.TABLE + "("
                + Transaction.KEY_ID + " INTEGER PRIMARY KEY,"
                + Transaction.KEY_TRANSID + " TEXT UNIQUE,"
                + Transaction.KEY_TRANSTYPE + " TEXT,"
                + Transaction.KEY_AMOUNT + " TEXT,"
                + Transaction.KEY_TOTAL + " TEXT,"
                + Transaction.KEY_CONVFEE + " TEXT,"
                + Transaction.KEY_AGENTCUT + " TEXT,"
                + Transaction.KEY_AGENCYTRANS + " TEXT,"
                + Transaction.KEY_POSACCOUNT + " TEXT,"
                + Transaction.KEY_DATE + " TEXT,"
                + Transaction.KEY_DATA + " TEXT,"
                + Transaction.KEY_RESPONSECODE + " TEXT,"
                + Transaction.KEY_RESPONSEMSG + " TEXT,"
                + Transaction.KEY_SYNCED + " TEXT,"
                + Transaction.KEY_VERSION + " TEXT)";
    }

    public long insert(Transaction transaction) {

        try {
            long insertId;

            SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
            ContentValues values = this.buildValues(transaction);

            // Inserting Row
            insertId = db.insert(Transaction.TABLE, null, values);
            DatabaseManager.getInstance().closeDatabase();

            return insertId;

        }catch (Exception ex){

            //logging response from portal
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);

            ex.printStackTrace(pw);

            String stackTrace = sw.toString();

            Display.sendToErrorLog("DB Update", ex.getMessage(), stackTrace);
        }

        return 0;

    }

    public int update(Transaction transaction) {

        try {
            int result;

            SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

            ContentValues values = this.buildValues(transaction);
            Log.d("DATABASE_SYNC", String.valueOf(transaction.getSynced()));
            // Update Row
            result = db.update(Transaction.TABLE, values, Transaction.KEY_TRANSID + " = ? ", new String[]{transaction.getTransactionId()});
            DatabaseManager.getInstance().closeDatabase();

            return result;
        }catch (Exception ex){

            //logging response from portal
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);

            ex.printStackTrace(pw);

            String stackTrace = sw.toString();

            Display.sendToErrorLog("DB Update", ex.getMessage(), stackTrace);

        }
         return 0;

    }

    private ContentValues buildValues(Transaction transaction) {
        ContentValues values = new ContentValues();

        values.put(Transaction.KEY_TRANSID, transaction.getTransactionId());
        values.put(Transaction.KEY_TRANSTYPE, transaction.getTransactionType().getName());
        values.put(Transaction.KEY_AMOUNT, transaction.getAmount());
        values.put(Transaction.KEY_TOTAL, transaction.getTotal());
        values.put(Transaction.KEY_CONVFEE, transaction.getConvenienceFee());
        values.put(Transaction.KEY_AGENTCUT, transaction.getAgentCut());
        values.put(Transaction.KEY_AGENCYTRANS, transaction.getAgencyTransaction());
        values.put(Transaction.KEY_POSACCOUNT, transaction.getPosAccount());
        values.put(Transaction.KEY_DATE, transaction.getDateLong());
        values.put(Transaction.KEY_VERSION, transaction.getVersion());
        values.put(Transaction.KEY_RESPONSECODE, transaction.getResponseCode());
        values.put(Transaction.KEY_RESPONSEMSG, transaction.getResponseMsg());
        values.put(Transaction.KEY_SYNCED, transaction.getSynced());
        values.put(Transaction.KEY_DATA, transaction.getDataDB());


        return values;
    }

    public Transaction build(Cursor cursor) {
        Transaction transaction = new Transaction();

        transaction.setId(cursor.getInt(cursor.getColumnIndex(Transaction.KEY_ID)));
        transaction.setTransactionId(cursor.getString(cursor.getColumnIndex(Transaction.KEY_TRANSID)));
        transaction.setTransactionType(Transaction.Type.getByName(cursor.getString(cursor.getColumnIndex(Transaction.KEY_TRANSTYPE))));
        transaction.setAmount(cursor.getFloat(cursor.getColumnIndex(Transaction.KEY_AMOUNT)));
        transaction.setTotal(cursor.getFloat(cursor.getColumnIndex(Transaction.KEY_TOTAL)));
        transaction.setConvenienceFee(cursor.getFloat(cursor.getColumnIndex(Transaction.KEY_CONVFEE)));
        transaction.setAgentCut(cursor.getFloat(cursor.getColumnIndex(Transaction.KEY_AGENTCUT)));
        transaction.setAgencyTransaction(cursor.getString(cursor.getColumnIndex(Transaction.KEY_AGENCYTRANS)));
        transaction.setPosAccount(cursor.getString(cursor.getColumnIndex(Transaction.KEY_POSACCOUNT)));
        transaction.setDateLong(cursor.getLong(cursor.getColumnIndex(Transaction.KEY_DATE)));
        transaction.setVersion(cursor.getString(cursor.getColumnIndex(Transaction.KEY_VERSION)));
        transaction.setResponseCode(cursor.getString(cursor.getColumnIndex(Transaction.KEY_RESPONSECODE)));
        transaction.setResponseMsg(cursor.getString(cursor.getColumnIndex(Transaction.KEY_RESPONSEMSG)));
        transaction.setSynced(cursor.getInt(cursor.getColumnIndex(Transaction.KEY_SYNCED)));
        transaction.setDataDB(cursor.getString(cursor.getColumnIndex(Transaction.KEY_DATA)));

        return transaction;
    }

    public DatabaseIterator<Transaction> getUnsyncedTransactions(boolean transactionsPendingSync) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String pendingSyncClause = "";
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, (3 * -1));

        if (transactionsPendingSync) {
            pendingSyncClause = String.format("%s <= %d AND ",
                    Transaction.KEY_DATE,
                    c.getTime().getTime());
        }

        // (sync & req_pay = req_pay AND sync & sync_pay != sync_pay) OR (sync & req_ibeta = req_ibeta AND sync & sync_ibeta = sync_ibeta)
        String selection = String.format("%s ((%s & %d = %d AND %s & %d != %d) OR (%s & %d = %d AND %s & %d != %d))",
                pendingSyncClause,

                Transaction.KEY_SYNCED,
                Transaction.Sync.SYNCREQPAYCENTRE.getValue(),
                Transaction.Sync.SYNCREQPAYCENTRE.getValue(),

                Transaction.KEY_SYNCED,
                Transaction.Sync.SYNCPAYCENTRE.getValue(),
                Transaction.Sync.SYNCPAYCENTRE.getValue(),

                Transaction.KEY_SYNCED,
                Transaction.Sync.SYNCREQIBETA.getValue(),
                Transaction.Sync.SYNCREQIBETA.getValue(),

                Transaction.KEY_SYNCED,
                Transaction.Sync.SYNCIBETA.getValue(),
                Transaction.Sync.SYNCIBETA.getValue()

        );
        Log.d("DATABASE", selection);
        final Cursor cursor = db.query(Transaction.TABLE, null, selection, null, null, null, Transaction.KEY_ID + " DESC");

        return  new DatabaseIterator<>(cursor, this);
    }

    public DatabaseIterator<Transaction> getCardTransactions(boolean unsynced) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String pendingClause = "";

        if(unsynced) {
            pendingClause = String.format("AND (%s & %d = %d AND %s & %d != %d)",

                    Transaction.KEY_SYNCED,
                    Transaction.Sync.SYNCREQPAYCENTRE.getValue(),
                    Transaction.Sync.SYNCREQPAYCENTRE.getValue(),

                    Transaction.KEY_SYNCED,
                    Transaction.Sync.SYNCPAYCENTRE.getValue(),
                    Transaction.Sync.SYNCPAYCENTRE.getValue()
            );
        }

        String selection = String.format("(%s = \"%s\" OR %s = \"%s\") %s",
                Transaction.KEY_TRANSTYPE,
                Transaction.Type.WITHDRAWAL.getName(),
                Transaction.KEY_TRANSTYPE,
                Transaction.Type.FUNDPURSE.getName(),
                pendingClause
        );

        final Cursor cursor = db.query(Transaction.TABLE, null, selection, null, null, null, Transaction.KEY_DATE + " ASC");

        return new DatabaseIterator<>(cursor, this);
    }

    public Transaction getTransaction(String id) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        Transaction transaction = null;
        Cursor cursor = db.query(Transaction.TABLE, null, Transaction.KEY_ID + " = ? OR " + Transaction.KEY_TRANSID + " = ?", new String[]{id, id}, null, null, null);

        if (cursor.moveToFirst()) {
            transaction = this.build(cursor);
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();
        return transaction;
    }

    public Transaction getTransaction(int id) {
        return getTransaction(String.valueOf(id));
    }

    public DatabaseIterator<Transaction> getTransactions() {
        return getTransactions(null, null);
    }

    public DatabaseIterator<Transaction> getTransactions(Date from, Date to) {
        String selection = null;
        String selectionArgs[] = null;

        if (from == null || to == null) {

        } else {
            selection = Transaction.KEY_DATE + " >= ? AND " + Transaction.KEY_DATE + " <= ?";
            selectionArgs = new String[]{String.valueOf(from.getTime()), String.valueOf(to.getTime())};
        }

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        final Cursor cursor = db.query(Transaction.TABLE, null, selection, selectionArgs, null, null, Transaction.KEY_ID + " DESC");

        return new DatabaseIterator<>(cursor, this);
    }
}