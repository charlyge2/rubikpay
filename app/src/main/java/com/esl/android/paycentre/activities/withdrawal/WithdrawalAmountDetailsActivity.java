package com.esl.android.paycentre.activities.withdrawal;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.activities.AccountTypeActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.utils.Alert;

import java.text.DecimalFormat;

public class WithdrawalAmountDetailsActivity  extends BaseActivity implements View.OnClickListener{

    private DecimalFormat DigitFormatter = new DecimalFormat("#,###,###.00");

    private String formattedAmount;

    private Button button;

    public static Double total =0.0;
    public static String phone;
    private TextView cashoutTitle;

    private TextView cashoutDescription;

    private TextView amountTextView;

    private TextView phoneTextView;

    private TextView convenienceFeeTextView;

    private TextView totalTextView,stamp_dutyTextView,vat_Label,vat_TextView;

    private LinearLayout phonelayout,stamp_duty_layout,vat_layout;

    private View phoneView,stamp_duty_view,vat_view;

    public ImageView homeIcon;

    public ImageView backIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal_details);
        Globals.showStampDuty = false;
        stamp_duty_layout = findViewById(R.id.stamp_duty_layout);
        stamp_dutyTextView = findViewById(R.id.stamp_dutyTextView);
        stamp_duty_view = findViewById(R.id.stamp_duty_view);
        vat_layout = findViewById(R.id.vat_layout);
        vat_Label = findViewById(R.id.vat_Label);
        vat_TextView = findViewById(R.id.vat_TextView);
        vat_view = findViewById(R.id.vat_view);
        phone = getIntent().getStringExtra("phone");

        String amount = getIntent().getStringExtra("amount");
        String convenienceFee = getIntent().getStringExtra("convenienceFee");
        String stampDuty = getIntent().getStringExtra("StampDuty");
        boolean showStampDuty = getIntent().getBooleanExtra("showStampDuty",false);
        String vat = getIntent().getStringExtra("vat");
        boolean showVat = getIntent().getBooleanExtra("showVat",false);
        if(showStampDuty){
            stamp_duty_layout.setVisibility(View.VISIBLE);
            stamp_duty_view.setVisibility(View.VISIBLE);
        }

        phonelayout = findViewById(R.id.phoneLayout);
        phoneView = findViewById(R.id.phoneView);
        amountTextView = findViewById(R.id.amountTextView);
        phoneTextView = findViewById(R.id.phoneTextView);
        convenienceFeeTextView = findViewById(R.id.convenienceFeeTextView);
        totalTextView = findViewById(R.id.totalTextView);
        button = findViewById(R.id.withdawalAmtProceed);
        homeIcon = (ImageView) findViewById(R.id.homeIcon);
        backIcon = (ImageView) findViewById(R.id.refreshIcon);
        cashoutTitle = (TextView) findViewById(R.id.cashoutTitle);
        cashoutDescription = (TextView) findViewById(R.id.cashoutDescription);

        homeIcon.setOnClickListener(this);
        backIcon.setOnClickListener(this);
        button.setOnClickListener(this);

        if("Fund Purse".equalsIgnoreCase(Globals.withdrawalType)){

            cashoutTitle.setText("Fund Purse");
            cashoutDescription.setText("Fund your account using your atm card");

            Globals.withdrawalType = "FUND PURSE";
            Globals.transactionTypeId = 10;
        }

        if (!TextUtils.isEmpty(phone)) {
            phoneTextView.setText(phone);
        }else{
            phonelayout.setVisibility(View.GONE);
            phoneView.setVisibility(View.GONE);
        }

        formattedAmount = amount.replace(",", "");

        if(showStampDuty){
            Globals.showStampDuty = true;
            Globals.stampDuty = stampDuty;
            if(showVat){
                Globals.showVat = true;
                if(TextUtils.isEmpty(vat)){
                    Alert.showFailed(this,"An Error Occurred");
                    Intent intent = new Intent(this,DashboardActivity.class);
                    startActivity(intent);
                    return;
                }
                vat_layout.setVisibility(View.VISIBLE);
                vat_view.setVisibility(View.VISIBLE);
                double myStampDuty = Double.parseDouble(stampDuty);

               total = Double.parseDouble(formattedAmount) + myStampDuty;

                String totalResult = String.format("%.2f", Double.parseDouble(formattedAmount) + myStampDuty);
                Globals.currencyAmount = totalResult;

                Globals.pinpadAmount = totalResult.replace(".", "");
                Globals.vat = vat;
             //   Double amountTotal = Double.valueOf(formattedAmount)  - Double.valueOf(convenienceFee);

                Globals.amount =total - myStampDuty - Double.parseDouble(convenienceFee)-Double.parseDouble(vat);

                String formattedTotal = DigitFormatter.format(Globals.amount);

                stamp_dutyTextView.setText("₦" +myStampDuty);
                amountTextView.setText("₦" +formattedTotal);
                convenienceFeeTextView.setText("₦" +DigitFormatter.format(Double.parseDouble(convenienceFee)));
                totalTextView.setText("₦" +total);
                vat_TextView.setText("₦" + vat);
            }
            else {
                double myStampDuty = Double.parseDouble(stampDuty);

                total = Double.parseDouble(formattedAmount) + myStampDuty;
                String totalResult = String.format("%.2f", total);

                Globals.currencyAmount = totalResult;

                Globals.pinpadAmount = totalResult.replace(".", "");

                Double amountTotal = Double.parseDouble(formattedAmount)  - Double.parseDouble(convenienceFee);

                Globals.amount = total - myStampDuty - Double.parseDouble(convenienceFee);

                String formattedTotal = DigitFormatter.format(amountTotal);

                stamp_dutyTextView.setText("₦" +myStampDuty);
                amountTextView.setText("₦" +formattedTotal);
                convenienceFeeTextView.setText("₦" +DigitFormatter.format(Double.parseDouble(convenienceFee)));
                totalTextView.setText("₦" +total);
            }


        } else {
            if(showVat){
                Globals.showVat = true;
                if(TextUtils.isEmpty(vat)){
                    Alert.showFailed(this,"An Error Occurred");
                    Intent intent = new Intent(this,DashboardActivity.class);
                    startActivity(intent);
                    return;
                }
                vat_layout.setVisibility(View.VISIBLE);
                vat_view.setVisibility(View.VISIBLE);
                total = Double.valueOf(formattedAmount);
                //  String totalResult = String.format("%.2f", total);

                //  double displayAmount = Double.valueOf(formattedAmount) + Double.valueOf(vat);
                Globals.currencyAmount = amount;
                Globals.pinpadAmount = formattedAmount.replace(".", "");
                Globals.vat = vat;
                total = Double.parseDouble(formattedAmount) - Double.parseDouble(convenienceFee)-Double.parseDouble(vat);
                Globals.amount = total;

                //   String formattedTotal = DigitFormatter.format(total);

                amountTextView.setText("₦" +total);
                convenienceFeeTextView.setText("₦" +DigitFormatter.format(Double.parseDouble(convenienceFee)));
                totalTextView.setText("₦" +amount);
                vat_TextView.setText("₦" + vat);
            }
            else {
                Globals.currencyAmount = amount;
                Globals.pinpadAmount = formattedAmount.replace(".", "");

                total = Double.parseDouble(formattedAmount) - Double.parseDouble(convenienceFee);
                Globals.amount = total;

                String formattedTotal = DigitFormatter.format(total);

                amountTextView.setText("₦" +formattedTotal);
                convenienceFeeTextView.setText("₦" +DigitFormatter.format(Double.parseDouble(convenienceFee)));
                totalTextView.setText("₦" +DigitFormatter.format(Double.parseDouble(amount.replace(",",""))));

            }


        }

    }

    @Override
    public void onClick(View v) {

        Intent myIntent = null;

        switch (v.getId()){
            case R.id.withdawalAmtProceed:
                Intent intent = new Intent(getApplicationContext(), AccountTypeActivity.class);
                startActivity(intent);
                break;
            case R.id.homeIcon:
                myIntent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(myIntent);
                break;

            case R.id.refreshIcon:
                finish();
                break;

            default:
                myIntent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(myIntent);
                break;
        }
    }

    public void goBack(View view) {
        onBackPressed();
    }
}
