package com.esl.android.paycentre.adapters;

public class TransactionHistory {

    private String transactionId;
    private String transactionName;
    private String transactionStatus;
    private String transactionAmount;
    private String transactionTime;
    private String transactionResponseCode;

    public TransactionHistory(String transactionId, String transactionName, String transactionStatus , String transactionAmount, String transactionTime, String transactionResponseCode) {
        this.transactionId = transactionId;
        this.transactionName = transactionName;
        this.transactionStatus = transactionStatus;
        this.transactionAmount = transactionAmount;
        this.transactionTime = transactionTime;
        this.transactionResponseCode = transactionResponseCode;
    }


    public String getTransactionId() {
        return transactionId;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public String getTransactionResponseCode() {
        return transactionResponseCode;
    }
}
