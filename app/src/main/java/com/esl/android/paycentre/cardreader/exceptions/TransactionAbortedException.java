package com.esl.android.paycentre.cardreader.exceptions;

public class TransactionAbortedException extends TransactionException {
    private static final long serialVersionUID = 1L;

    public TransactionAbortedException(String reason) {
        super(reason);
    }
}
