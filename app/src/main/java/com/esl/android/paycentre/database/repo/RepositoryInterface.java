package com.esl.android.paycentre.database.repo;

import android.database.Cursor;

public interface RepositoryInterface<T> {

    public T build(Cursor cursor);
}
