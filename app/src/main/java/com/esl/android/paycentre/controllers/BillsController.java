package com.esl.android.paycentre.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.billsPayment.others.OtherBillsDetailsActivity;
import com.esl.android.paycentre.activities.billsPayment.others.OthersBillsResultActivity;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.services.InternetConnectionService;
import com.esl.android.paycentre.utils.Alert;

import org.json.JSONException;
import org.json.JSONObject;

import static com.esl.android.paycentre.services.HttpService.refreshToken;

public class BillsController {

    private static ProgressDialog progressDialog;

    public static void BillsInquiry(final Context context, JSONObject params, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Processing......");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        AndroidNetworking.post(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        progress.dismiss();

                try {

                    String responseCode = response.getString("responseCode");
                    JSONObject data = response.getJSONObject("data");

                    if (responseCode.equals("00")){

                        String address = data.optString("address");
                        String verificationCode = data.optString("verificationCode");
                        String name = data.optString("customerName");



//
//                                "responseMessage":"Request Successful",
//
//                                "status":true,
//                                "responseCode":"0",
//                                "verificationCode":"aHtXhTvY"

                        Intent intent = new Intent(context, OtherBillsDetailsActivity.class);
                        intent.putExtra("customerAddress",address);
                        intent.putExtra("customerName",name);
                        intent.putExtra("meterNumber",params.optString("meterNumber"));
                        intent.putExtra("verificationCode",verificationCode);
                        intent.putExtra("powerType",params.optString("powerType"));

                        context.startActivity(intent);

                    }else{
                        Alert.showWarning(context,response.optString("responseMessage"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//
                    }

                    @Override
                    public void onError(ANError error) {

                        progress.dismiss();

                        Alert.showFailed(context, "Your email could not be sent");
                        try {
                            Log.d("ERROR_LOGGING_DISPUTE",error.getErrorBody());
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

    }

    public static void payPower(final Context context, JSONObject params, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Processing......");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        AndroidNetworking.post(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String creditToken = "";
                        String exchangeReference = "";
                        progress.dismiss();

                        String responseCode = response.optString("responseCode");
                        String responseMessage = response.optString("responseMessage");
                        JSONObject data = response.optJSONObject("data");
                        if(data!=null){
                             creditToken = data.optString("creditToken");
                             exchangeReference = data.optString("exchangeReference");

                        }


                        Intent intent = new Intent(context, OthersBillsResultActivity.class);
                        intent.putExtra("responseCode",responseCode);
                        intent.putExtra("responseMessage",responseMessage);
                        intent.putExtra("creditToken",creditToken);
                        intent.putExtra("exchangeReference",exchangeReference);
                        intent.putExtra("params",params.toString());
                        context.startActivity(intent);


                        //
                    }

                    @Override
                    public void onError(ANError error) {

                        progress.dismiss();
                        try {
                            JSONObject object = new JSONObject(error.getErrorBody());
                            Alert.showFailed(context, object.optString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {

                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

    }



}
