package com.esl.android.paycentre.activities.billsPayment.others;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.constants.Constant;
import com.esl.android.paycentre.controllers.BillsController;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.esl.android.paycentre.utils.TransactionFormatter;

import org.json.JSONException;
import org.json.JSONObject;

public class OtherBillsDetailsActivity extends AppCompatActivity {

    private String
            address,
            name,
            meterNumber,
            verificationCode,
            powerType;

   private TextView addressTextView
            ,nameTextView,
            meterNumberTextView;


   private Button billsBtn;
    private EditText amount,
            phone,
            email;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_bills_details);

        billsBtn = findViewById(R.id.billsProceedBtn);

            amount = findViewById(R.id.billAmount);
            phone = findViewById(R.id.billPhone);
            email = findViewById(R.id.billEmail);

            address = getIntent().getStringExtra("customerAddress");
            name = getIntent().getStringExtra("customerName");
            meterNumber = getIntent().getStringExtra("meterNumber");
            verificationCode = getIntent().getStringExtra("verificationCode");
            powerType = getIntent().getStringExtra("powerType");
            // Specify the layout to use when the list of choices appears

            addressTextView = findViewById(R.id.addressTextView);
            nameTextView = findViewById(R.id.nameTextView);
            meterNumberTextView = findViewById(R.id.meterTextView);
            addressTextView.setText(address);
            nameTextView.setText(name);
            meterNumberTextView.setText(meterNumber);

            billsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(TextUtils.isEmpty(amount.getText().toString()) || TextUtils.isEmpty(phone.getText().toString()) || TextUtils.isEmpty(email.getText().toString())){
                        Alert.showWarning(getApplicationContext(),"Please fill all fields");
                        return;
                    }



                    JSONObject params = new JSONObject();
                    try {
                        final String posAccountUsername = SharedPreferenceManager.getString(getApplicationContext(), "posAccountUsername", "");


                        params.put("amount",Double.valueOf(amount.getText().toString()));
                        params.put("transactionId", TransactionFormatter.generateTransactionId());
                        params.put("appTime", TransactionFormatter.generateAppTime());
                        params.put("posAccount",posAccountUsername);
                        params.put("convenienceFee","0.0");
                        params.put("customerAddress",address);
                        params.put("customerName",name);
                        params.put("powerType",powerType);
                        params.put("verificationCode",verificationCode);
                        params.put("customerEmail",email.getText().toString());
                        params.put("phoneNumber",phone.getText().toString());
                        params.put("meterNumber",meterNumber);
                        params.put("version",MainActivity.getAppVersion(OtherBillsDetailsActivity.this));

                        BillsController.payPower(OtherBillsDetailsActivity.this,params, MainActivity.payPowerUrl);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }





    public void goBack(View view) {
    }

    public void goHome(View view) {
    }
}
