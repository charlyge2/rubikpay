package com.esl.android.paycentre.activities.billsPayment.others;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.TransactionFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class OthersBillsResultActivity extends AppCompatActivity {

    JSONObject jsonObject;

    private ImageView responseImageView;
    private TextView amountTextView,type,status;
    private TextView responseTextView;
    private View type_layout_view,status_View,token_view,meter_view,trans_id_view,meternumber_view,serial_no_view;

    private TextView tokenTextView;
    private TextView transactionIdTextView;
    private TextView meterNumberTextView;
    private TextView exchangeReferenceTv;
    private TextView customerNameTextView;
    private TextView customerAddressTextView;
    private TextView customerDtTextView;
    private TextView phoneTextView;
    private TextView customerAccountTypeTextView;
    private TextView thirdPartyTextView,waecpinTextView,waecserialNumber;

    String responseCode = "";
    String responseMessage = "";
    String amount = "";

    String creditToken = "";
    String transactionId = "";
    String meterNumber = "";

    // irregular parameters
    String customerName = "";
    String phoneNumber = "";
    String contactType = "";
    String customerDtNumber = "";
    String customerAccountType = "";
    String thirdPartyCode = "";
    String customerAddress = "";
    String customerDistrict = "";
    String serialNumber;
    String pin;
    private LinearLayout waec_pin_layout,type_layout,status_layout,meternumber_layout,token_layout,otherbils_layout,serial_no_layout;

    private Button proceedButton;

    private Button homeBtn;
    private String exchangeReference;

    public OthersBillsResultActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_bills_result);


        // View For IBDEC Payment
        type_layout = findViewById(R.id.type_layout);
        status_layout = findViewById(R.id.status_layout);
        waec_pin_layout = findViewById(R.id.waec_pin_layout);
        type_layout_view = findViewById(R.id.type_layout_view);
        status_View= findViewById(R.id.status_View);
        type= findViewById(R.id.type);
        trans_id_view = findViewById(R.id.trans_id_view);
        otherbils_layout = findViewById(R.id.otherbils_layout);
        waecserialNumber = findViewById(R.id.waecserialNumber);
        serial_no_layout = findViewById(R.id.serial_no_layout);
        waecpinTextView = findViewById(R.id.waecpinTextView);
        meternumber_layout = findViewById(R.id.meternumber_layout);
        token_layout = findViewById(R.id.token_layout);
        status = findViewById(R.id.status);
        token_view =findViewById(R.id.token_view);
        trans_id_view = findViewById(R.id.trans_id_view);
        exchangeReferenceTv = findViewById(R.id.exchangeReference);
        responseImageView = findViewById(R.id.responseImageView);
        amountTextView = findViewById(R.id.amountTextView);
        meternumber_view = findViewById(R.id.meternumber_view);
        responseTextView = findViewById(R.id.responseTextView);

        tokenTextView = findViewById(R.id.tokenTextView);
        transactionIdTextView = findViewById(R.id.transactionIdTextView);
        meterNumberTextView = findViewById(R.id.meterNumberTextView);
        customerNameTextView = findViewById(R.id.customerNameTextView);
        customerAddressTextView = findViewById(R.id.customerAddressTextView);
        customerDtTextView = findViewById(R.id.customerDtTextView);
        phoneTextView = findViewById(R.id.phoneTextView);
        customerAccountTypeTextView = findViewById(R.id.customerAccountTypeTextView);
        thirdPartyTextView = findViewById(R.id.thirdPartyTextView);
        serial_no_view = findViewById(R.id.serial_no_view);

        LinearLayout customerDtLayout = findViewById(R.id.customerDtLayout);
        View customerNameView = findViewById(R.id.customerNameView);
        LinearLayout customerNameLayout = findViewById(R.id.customerNameLayout);
        View customerAddressView = findViewById(R.id.customerAddressView);
        LinearLayout customerAddressLayout = findViewById(R.id.customerAddressLayout);
        View customerDistrictView = findViewById(R.id.customerDistrictView);


        try {
            jsonObject = new JSONObject(getIntent().getStringExtra("params"));
            Log.d("param_jsonObject",String.valueOf(jsonObject));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        responseCode = getIntent().getStringExtra("responseCode");
        responseMessage = getIntent().getStringExtra("responseMessage");
        creditToken = getIntent().getStringExtra("creditToken");
        exchangeReference = getIntent().getStringExtra("exchangeReference");
        exchangeReferenceTv.setText(exchangeReference);

        responseTextView.setText(responseMessage);

        amount = jsonObject.optString("amount");
        amountTextView.setText(amount);

        if ("00".equals(responseCode) || "0".equals(responseCode)) {

            responseImageView.setImageResource(R.drawable.ic_check);
            amountTextView.setTextColor(getResources().getColor(R.color.colorBlack));
            responseTextView.setTextColor(getResources().getColor(R.color.apply));

        }
        else{
            responseImageView.setImageResource(R.drawable.ic_error);
            amountTextView.setTextColor(getResources().getColor(R.color.colorBlack));
            responseTextView.setTextColor(getResources().getColor(R.color.reset));
        }

        transactionId = jsonObject.optString("transactionId");
        meterNumber = jsonObject.optString("meterNumber");

        tokenTextView.setText(creditToken);
        transactionIdTextView.setText(transactionId);
        meterNumberTextView.setText(meterNumber);

            otherbils_layout.setVisibility(View.VISIBLE);
            customerAddress = jsonObject.optString("customerAddress");
            customerName = jsonObject.optString("customerName");
            customerDistrict = jsonObject.optString("customerDistrict");

            customerAddressTextView.setText(customerAddress);
            customerNameTextView.setText(customerName);
            serial_no_view.setVisibility(View.GONE);
            customerAddressView.setVisibility(View.VISIBLE);
            customerAddressLayout.setVisibility(View.VISIBLE);
            customerNameView.setVisibility(View.VISIBLE);
            customerNameLayout.setVisibility(View.VISIBLE);
            customerDistrictView.setVisibility(View.VISIBLE);
            serial_no_layout.setVisibility(View.GONE);
            waec_pin_layout.setVisibility(View.GONE);




        long check = saveOtherBillsTransaction();

        if(check < 0){
            Alert.showWarning(getApplicationContext(), "Transaction could not be saved, Please try again later");
        }

        proceedButton = findViewById(R.id.proceedButton);
        proceedButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        startActivity(new Intent(OthersBillsResultActivity.this, DashboardActivity.class));
                    }
                }
        );
    }

    private long saveOtherBillsTransaction() {
        JSONObject params = new JSONObject();

        try {

                params.put("customerAddress",jsonObject.optString("customerAddress"));
                params.put("customerName",jsonObject.optString("customerName"));
                params.put("phoneNumber",jsonObject.optString("phoneNumber"));
                params.put("customerNumber",jsonObject.optString("customerNumber"));;
                params.put("meterNumber",jsonObject.optString("meterNumber"));
                params.put("creditToken",creditToken);





        }catch (Exception e){
            e.printStackTrace();
        }

        Transaction transaction = new Transaction();
        TransactionRepo transactionRepo = new TransactionRepo();

        transaction.setAgentCut(0);
        transaction.setAmount(Float.valueOf(jsonObject.optString("amount")));
        transaction.setConvenienceFee(0);
        transaction.setDate(new Date());
        transaction.setPosAccount(jsonObject.optString("posAccount"));
        transaction.setTransactionId(jsonObject.optString("transactionId"));
        transaction.setResponseCode(responseCode);
        transaction.setResponseMsg(responseMessage);
        transaction.setVersion(MainActivity.appVersionUrl);
        transaction.setAgentCut(Float.valueOf("0.0"));
        transaction.setTransactionType(Transaction.Type.getByName("POWER"));

        transaction.setData(params);

        return transactionRepo.insert(transaction);


    }

    @Override
    public void onBackPressed() {

    }
}
