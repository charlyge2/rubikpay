package com.esl.android.paycentre.activities.dashboard;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.esl.android.paycentre.PaycentreApplication;
import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.SettingsActivity;
import com.esl.android.paycentre.activities.activation.PosLoginActivity;
import com.esl.android.paycentre.activities.agentTransfer.AgentTransferAmountActivity;
import com.esl.android.paycentre.activities.notifications.NotificationListActivity;
import com.esl.android.paycentre.activities.withdrawal.WithdrawalAmountActivity;
import com.esl.android.paycentre.adapters.DashboardSlideAdapter;
import com.esl.android.paycentre.adapters.RecentTransactionAdapter;
import com.esl.android.paycentre.adapters.RecentTransactionList;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.controllers.DashboardController;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.esl.android.paycentre.utils.TextCasing;
import com.esl.android.paycentre.utils.TransactionFormatter;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;

//import com.crashlytics.android.Crashlytics;

public class DashboardActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;
    private static final int IN_APP_REQUEST = 2;
    private static Context context;
    private static SharedPreferences prefs;
    public static String currentBalance;

    public static TextView balance;
    public static String agentPhoneNumber, agentPassword, agentName, loggedInUser;
    public static ImageView refreshIcon;
    private static DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    private static String formattedString;
    SharedPreferenceManager sharedPreferenceManager;
    private RecentTransactionAdapter recentTransactionAdapter;
    private boolean doubleBackToExitPressedOnce = false;
    private DashboardController dashboardController = new DashboardController();
    private ArrayList<RecentTransactionList> recentTransactions = new ArrayList<>();

    //private int[] layouts = {R.layout.first_product_slider, R.layout.second_product_slider};
    private LinearLayout dotsLayout;
    private ImageView[] dots;
    private ViewPager viewPager;
    private int[] layouts = {R.layout.first_product_slider};
    private DashboardSlideAdapter dashboardSlideAdapter;
    private ListView listView;
    private Button fundPurseBtn;
    private Button accountTransferBtn;
    private MenuItem chat;
    private String email;

    private AppUpdateManager appUpdateManager;

    private String username;

    private String terminalId;

    private AlertDialog alertDialog = null;


    public static void setBalance(String purseBalance) {

        balance.setText(purseBalance);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dashboard);

        context = getApplicationContext();
        prefs = getSharedPreferences("PlayerId", MODE_PRIVATE);
        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        navigationView.setItemIconTintList(null);
        refreshIcon = findViewById(R.id.refreshIcon);
        accountTransferBtn = findViewById(R.id.accountTransferBtn);
        fundPurseBtn = findViewById(R.id.fundPurseBtn);
        accountTransferBtn.setOnClickListener(this);
        fundPurseBtn.setOnClickListener(this);
        listView = findViewById(R.id.recentView);
        balance = findViewById(R.id.pBalance);
        dotsLayout = findViewById(R.id.dotsLayout);
        Toolbar toolbar = findViewById(R.id.homeIcon);

//        Crashlytics.log("WELCOME TO PAYCENTRE CRASHLYTICS");

        toolbar.setTitle("");

        agentPhoneNumber = sharedPreferenceManager.getString(getApplicationContext(), "agentPhoneNumber", "");
        agentPassword = sharedPreferenceManager.getString(getApplicationContext(), "password", "");
        email = sharedPreferenceManager.getString(getApplicationContext(), "email", "");
        username = sharedPreferenceManager.getString(getApplicationContext(), "username", "");
        agentName = sharedPreferenceManager.getString(getApplicationContext(), "agentName", "");
        loggedInUser = sharedPreferenceManager.getString(getApplicationContext(), "posAccountUsername", "");
        terminalId = sharedPreferenceManager.getString(getApplicationContext(), "terminalId", "");


        Globals.terminalId = terminalId;
        Globals.email = email;
        Globals.username = username;
        Globals.posAccount = loggedInUser;
        Globals.phoneNumber = agentPhoneNumber;


        new fetchPurseBalance(this).execute("my string parameter");


        setSupportActionBar(toolbar);

        String memberId = sharedPreferenceManager.getString(getApplicationContext(), "memberId", "");

        String stateCode = SharedPreferenceManager.getString(getApplicationContext(), "stateCode", "");
        if(stateCode == null || TextUtils.isEmpty(stateCode)){
            Alert.showFailed(this,"Clear App data and Reactivate account");
            return;
        }
        Globals.agentID = stateCode + getMemberID(memberId);

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                // TODO Auto-generated method stubs
                //Alert.showSuccess(getApplicationContext(),"Transactions Refreshed");
                pushTransactionsToList();
                pullToRefresh.setRefreshing(false);
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.whiteBackground));
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.agentName);
        TextView navPosUser = headerView.findViewById(R.id.posAccount);
        TextView navMemberId = headerView.findViewById(R.id.agentID);
        navMemberId.setText(memberId);
        navPosUser.setText("POS Account : " + loggedInUser + "\nApp Version : " + MainActivity.getAppVersion(this));
        navUsername.setText(agentName);

        pushTransactionsToList();
//        sendPlayerId();

        viewPager = findViewById(R.id.dashboardViewAdapater);
        dashboardSlideAdapter = new DashboardSlideAdapter(layouts, DashboardActivity.this);
        viewPager.setAdapter(dashboardSlideAdapter);
        createDots(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //In app updates
        appUpdateManager = AppUpdateManagerFactory.create(DashboardActivity.this);

        inAppUpdate();
    }


    private void pushTransactionsToList() {

        recentTransactions.clear();

        TransactionRepo repo = new TransactionRepo();
        Iterator<Transaction> iterator = repo.getTransactions();

        while (iterator.hasNext()) {

            Transaction transaction = iterator.next();

            Date date = transaction.getDate();
            String dateTime = TransactionFormatter.formatDate(date) + " " + TransactionFormatter.formatTime(date);

            String formattedAmt = "₦" + formatter.format(transaction.getAmount());

            recentTransactions.add(new RecentTransactionList(TransactionFormatter.formatTime(date),
                    TextCasing.toTitleCase(String.valueOf(transaction.getTransactionType().getName())),
                    formattedAmt,
                    dateTime,
                    TextCasing.toTitleCase(String.valueOf(transaction.getResponseMsg())),
                    String.valueOf(transaction.getResponseCode())));

        }


        recentTransactionAdapter = new RecentTransactionAdapter(this, recentTransactions);
        listView.setAdapter(recentTransactionAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String transactionType = ((TextView) view.findViewById(R.id.transactionName)).getText().toString();
                String amt = ((TextView) view.findViewById(R.id.transactionAmount)).getText().toString();
                String dated = ((TextView) view.findViewById(R.id.transactionDate)).getText().toString();
                String status = ((TextView) view.findViewById(R.id.transactionStatus)).getText().toString();
                String time = ((TextView) view.findViewById(R.id.transactionTime)).getText().toString();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
                alertDialogBuilder.setTitle("Transaction Details");
                alertDialogBuilder
                        .setMessage("\nAmount : " + amt + "\n\nStatus : " + status + "\n\nDate : " + dated + "\n\nTransaction : " + transactionType)
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IN_APP_REQUEST) {
            if (resultCode != RESULT_OK) {
                Alert.showInfo(DashboardActivity.this, "App update was not successful, You would be logged out");
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        appUpdateInfo -> {
                            if (appUpdateInfo.updateAvailability()
                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                // If an in-app update is already running, resume the update.
                                try {
                                    appUpdateManager.startUpdateFlowForResult(
                                            appUpdateInfo,
                                            IMMEDIATE,
                                            this,
                                            IN_APP_REQUEST);
                                } catch (IntentSender.SendIntentException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent;
        int id = item.getItemId();

        if (id == R.id.nav_notification) {
            intent = new Intent(getApplicationContext(), NotificationListActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_call) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:+2349072776423"));
            //  Phone: +2349072776423, +2349072764603


            if (ContextCompat.checkSelfPermission(DashboardActivity.this,
                    Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(DashboardActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS_REQUEST_CALL_PHONE);

                // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            } else {
                //startActivity(callIntent);
            }

        } else if (id == R.id.nav_email) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:info@rubikpay.tech"));
            startActivity(Intent.createChooser(emailIntent, "Send feedback"));
        } else if (id == R.id.nav_home) {
            //intent = new Intent(getApplicationContext(), DashboardActivity.class);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void getPurseBalance(View view) {
        dashboardController.doFetchPurseBalance(DashboardActivity.this);
    }

    public void logout(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Confirmation!");
        alertDialogBuilder
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //finish();
                                Intent intent = new Intent(getApplicationContext(), PosLoginActivity.class);
                                startActivity(intent);
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void createDots(int current_position) {

        if (dotsLayout != null)
            dotsLayout.removeAllViews();

        dots = new ImageView[layouts.length];

        for (int i = 0; i < layouts.length; i++) {
            dots[i] = new ImageView(this);
            if (i == current_position) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.default_dots));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);

            dotsLayout.addView(dots[i], params);
        }
    }

    @Override
    public void onClick(View v) {

        Intent myIntent = null;

        switch (v.getId()) {
            case R.id.accountTransferBtn:
                myIntent = new Intent(getApplicationContext(), AgentTransferAmountActivity.class);
                startActivity(myIntent);
                break;
            case R.id.fundPurseBtn:
                myIntent = new Intent(getApplicationContext(), WithdrawalAmountActivity.class);
                Globals.withdrawalType = "Fund Purse".toUpperCase();
                startActivity(myIntent);
                break;
            default:
                Alert.showWarning(getApplicationContext(), "Could not navigate");
                break;
        }
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                finishAffinity();
            }

            this.doubleBackToExitPressedOnce = true;

            Alert.showWarning(getApplicationContext(), "Click back again to exit app");
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    public void alertBtn() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Set icon value.
        builder.setIcon(R.drawable.ic_tag_search_filter);
        // Set title value.
        builder.setTitle("TRANSACTIONS FILTER");
        // Get custom login form view.
        final View loginFormView = getLayoutInflater().inflate(R.layout.filterlayout, null);
        // Set above view in alert dialog.
        builder.setView(loginFormView);
        builder.setCancelable(true);
        alertDialog = builder.create();
        alertDialog.show();

    }

    private void inAppUpdate() {

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)) {

                try {
                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            IN_APP_REQUEST);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });
    }



    public static class fetchPurseBalance extends AsyncTask<String, Integer, String> {

        private final Context mContext;

        public fetchPurseBalance(final Context context) {
            mContext = context;
        }

        // Runs in UI before background thread is called
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Do something like display a progress bar
        }

        // This is run in a background thread
        @Override
        protected String doInBackground(String... params) {
            // get the string from params, which is an array
            String myString = params[0];

            // Do something that takes a long time, for example:
            final String usernamePassword = agentPhoneNumber + ":" + agentPassword;
            formattedString = "₦0.00";

            final String basicAuth = "Basic " + Base64.encodeToString(usernamePassword.getBytes(), Base64.NO_WRAP);

            AndroidNetworking.get(MainActivity.purseBalanceUrl)
                    .setPriority(Priority.IMMEDIATE)
                    .addHeaders("Authorization", ConfigurationClass.getAccessToken(mContext))
                    .addHeaders("x-phone-uuid", MainActivity.getUuid(mContext))
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {


                            Log.d("RESPONSE ", String.valueOf(response));

                            try {
                                String responseMessage = response.getString("responseMessage");
                                String responseCode = response.getString("responseCode");
                                final String balance = response.getString("balance");

                                double formattedBalance = Double.parseDouble(balance);

                                currentBalance = formatter.format(formattedBalance);

                                formattedString = "₦" + currentBalance;

                                if (responseCode.equals("00")) {
                                    Globals.purseBalance = formattedString;
                                    DashboardActivity.setBalance(formattedString);
                                } else {
                                    Alert.showWarning(PaycentreApplication.getAppContext(), responseMessage);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle error
                            Alert.showFailed(context, error.getErrorDetail() + " Please try again later");
                        }
                    });

            return formattedString;
        }

        // This is called from background thread but runs in UI
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        // This runs in UI when background thread finishes
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    private String getMemberID(String memberId) {
        String[] values = memberId.split("/");
        return values[values.length - 1];
    }
}