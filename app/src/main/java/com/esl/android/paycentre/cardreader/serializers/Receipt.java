package com.esl.android.paycentre.cardreader.serializers;

import android.os.Parcel;
import android.os.Parcelable;

public class Receipt implements Parcelable {

    private String responseCode;
    private String response;
    private String amount;
    private String pan;
    private String rrn;
    private String stan;
    private String terminalId;
    private String transactionId;
    private String transactionDate;
    private String transactionType;
    private String applicationVersion;
    private String accountType;
    private String aid;
    private String authenticationCode;
    private String cardholder;
    private String cardExpiry;
    private String tenderType;
    private String sequenceNumber;

    public Receipt(String responseCode, String response, String amount, String pan, String rrn, String stan, String terminalId, String transactionId, String transactionDate, String transactionType, String applicationVersion, String accountType, String aid, String authenticationCode, String cardholder, String cardExpiry, String tenderType, String sequenceNumber) {
        this.responseCode = responseCode;
        this.response = response;
        this.amount = amount;
        this.pan = pan;
        this.rrn = rrn;
        this.stan = stan;
        this.terminalId = terminalId;
        this.transactionId = transactionId;
        this.transactionDate = transactionDate;
        this.transactionType = transactionType;
        this.applicationVersion = applicationVersion;
        this.accountType = accountType;
        this.aid = aid;
        this.authenticationCode = authenticationCode;
        this.cardholder = cardholder;
        this.cardExpiry = cardExpiry;
        this.tenderType = tenderType;
        this.sequenceNumber = sequenceNumber;
    }

    protected Receipt(Parcel in) {
        responseCode = in.readString();
        response = in.readString();
        amount = in.readString();
        pan = in.readString();
        rrn = in.readString();
        stan = in.readString();
        terminalId = in.readString();
        transactionId = in.readString();
        transactionDate = in.readString();
        transactionType = in.readString();
        applicationVersion = in.readString();
        accountType = in.readString();
        aid = in.readString();
        authenticationCode = in.readString();
        cardholder = in.readString();
        cardExpiry = in.readString();
        tenderType = in.readString();
        sequenceNumber = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(responseCode);
        dest.writeString(response);
        dest.writeString(amount);
        dest.writeString(pan);
        dest.writeString(rrn);
        dest.writeString(stan);
        dest.writeString(terminalId);
        dest.writeString(transactionId);
        dest.writeString(transactionDate);
        dest.writeString(transactionType);
        dest.writeString(applicationVersion);
        dest.writeString(accountType);
        dest.writeString(aid);
        dest.writeString(authenticationCode);
        dest.writeString(cardholder);
        dest.writeString(cardExpiry);
        dest.writeString(tenderType);
        dest.writeString(sequenceNumber);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Receipt> CREATOR = new Creator<Receipt>() {
        @Override
        public Receipt createFromParcel(Parcel in) {
            return new Receipt(in);
        }

        @Override
        public Receipt[] newArray(int size) {
            return new Receipt[size];
        }
    };

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getAuthenticationCode() {
        return authenticationCode;
    }

    public void setAuthenticationCode(String authenticationCode) {
        this.authenticationCode = authenticationCode;
    }

    public String getCardholder() {
        return cardholder;
    }

    public void setCardholder(String cardholder) {
        this.cardholder = cardholder;
    }

    public String getCardExpiry() {
        return cardExpiry;
    }

    public void setCardExpiry(String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }

    public String getTenderType() {
        return tenderType;
    }

    public void setTenderType(String tenderType) {
        this.tenderType = tenderType;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }
}
