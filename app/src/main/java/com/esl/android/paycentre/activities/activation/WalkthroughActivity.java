package com.esl.android.paycentre.activities.activation;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.adapters.WalkthroughPageAdapter;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.utils.PreferenceManager;
import com.esl.android.paycentre.utils.SharedPreferenceManager;


public class WalkthroughActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout dotsLayout;
    private ImageView[] dots;
    private ViewPager viewPager;
    private Button btnSkip, btnNext;
    private PreferenceManager preferenceManager;
    private int[] layouts = {R.layout.first_slide, R.layout.second_slide};
    WalkthroughPageAdapter viewpagerAdapter;
    public static final int REQUEST_ENABLE_BT = 1;


    private boolean doubleBackToExitPressedOnce = false;

    SharedPreferenceManager sharedPreferenceManager;

    private PinpadFacade pinpadFacade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String agentPhoneNumber = sharedPreferenceManager.getString(getApplicationContext(), "agentPhoneNumber", "");
        String agentName = sharedPreferenceManager.getString(getApplicationContext(), "agentName", "");
        Log.d("agentName", agentName);

//        ChatService.init(getApplication(), agentPhoneNumber, agentName);

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title

        preferenceManager = new PreferenceManager(this);

        pinpadFacade = new PinpadFacade(this);

        if (preferenceManager.checkPreferences()) {
            // startMainActivity();
            connect();
        }
        setContentView(R.layout.activity_welcome);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewpagerAdapter = new WalkthroughPageAdapter(layouts, this);
        viewPager.setAdapter(viewpagerAdapter);

        dotsLayout = (LinearLayout) findViewById(R.id.dotsLayout);
        btnSkip = findViewById(R.id.btnSkip);
        btnNext = findViewById(R.id.btnNext);

        btnSkip.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        createDots(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);
                if (position == layouts.length - 1) {
                    btnNext.setText("Start");
                    btnSkip.setVisibility(View.INVISIBLE);
                } else {
                    btnNext.setText("Next");
                    btnSkip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void createDots(int current_position) {

        if (dotsLayout != null)
            dotsLayout.removeAllViews();

        dots = new ImageView[layouts.length];

        for (int i = 0; i < layouts.length; i++) {
            dots[i] = new ImageView(this);
            if (i == current_position) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.default_dots));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);

            dotsLayout.addView(dots[i], params);
        }
    }

    @Override
    public void onClick(View view) {
        preferenceManager = new PreferenceManager(this);
        switch (view.getId()) {
            case R.id.btnNext:
                loadNextSlide();
                break;

            case R.id.btnSkip:
                //startMainActivity();
//                Intent intent = new Intent(getApplicationContext(),AfterkeyExchangeActivity.class);
//                startActivity(intent);
                connect();
                preferenceManager.writePreferences();
                break;
        }
    }

    //enable bluetooth here to be able to handle when user cancels dialog
    public void connect() {

        try {

            BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBtAdapter != null) {
                if (mBtAdapter.isEnabled()) {

                    startMainActivity();

                } else {
                    enableBluetooth();
                }
            } else {
                Toast.makeText(getApplicationContext(),
                        R.string.msg_bluetooth_is_not_supported,
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //enable bluetooth here to be able to handle when user cancels dialog
    private void enableBluetooth() {

        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    public void startMainActivity() {

        sharedPreferenceManager.save(getApplicationContext(), "walkthrough", "true");


        pinpadFacade.connect();

    }

    private void loadNextSlide() {

        preferenceManager = new PreferenceManager(this);
        int nextSlide = viewPager.getCurrentItem() + 1;

        if (nextSlide < layouts.length) {
            viewPager.setCurrentItem(nextSlide);
        } else {
            //startMainActivity();
            connect();
            preferenceManager.writePreferences();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_CANCELED) {

            //DO NOTHING

        } else if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {

            startMainActivity();

        } else {

            pinpadFacade.onActivityResult(requestCode, resultCode, data);

            Intent intent = new Intent(this, AfterkeyExchangeActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {

//        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
//            finishAffinity();
//        }
//
//        this.doubleBackToExitPressedOnce = true;
//
//        Alert.showWarning(getApplicationContext(),"Click back again to exit");
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce=false;
//            }
//        }, 2000);
    }
}

