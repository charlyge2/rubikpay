package com.esl.android.paycentre;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import com.esl.android.paycentre.database.DBHelper;
import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.services.SyncService;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;

import java.util.Iterator;
import java.util.Locale;


public class PaycentreApplication extends Application implements Application.ActivityLifecycleCallbacks, OSSubscriptionObserver/*, OSSubscriptionObserver */{

    public static boolean isInBackground = false;
    private int activityReferences = 0;
    private boolean isActivityChangingConfigurations = false;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        String languageToLoad  = "en"; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

//        Fabric.with(this, new Crashlytics());
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        DatabaseManager.initializeInstance(dbHelper);
        PaycentreApplication.context = getApplicationContext();
        SyncService.scheduleSync(getApplicationContext());

        String agentPhoneNumber = SharedPreferenceManager.getString(getApplicationContext(), "agentPhoneNumber", "");
        String agentName = SharedPreferenceManager.getString(getApplicationContext(), "agentName", "");


        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        OneSignal.addSubscriptionObserver(this);

        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();

        this.updateTransactionTerminal();

        this.registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    public static Context getAppContext() {
        return PaycentreApplication.context;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (++activityReferences == 1 && !isActivityChangingConfigurations) {
            isInBackground = false;
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        isActivityChangingConfigurations = activity.isChangingConfigurations();
        if (--activityReferences == 0 && !isActivityChangingConfigurations) {
            isInBackground = true;
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
        if (!stateChanges.getFrom().getSubscribed() &&
                stateChanges.getTo().getSubscribed()) {

            // get player ID
            String userId = stateChanges.getTo().getUserId();

            savePlayerIdToSharedPreference(userId);
            Log.i("userId", "userId: " + userId);
        }


    }

    private void savePlayerIdToSharedPreference(String PlayerId){
        SharedPreferences.Editor editor = getSharedPreferences("PlayerId", MODE_PRIVATE).edit();
        editor.putString("PlayerId", PlayerId);
        editor.apply();
    }

    private void updateTransactionTerminal() {

        TransactionRepo repo = new TransactionRepo();
        Iterator cashoutCount = repo.getCardTransactions(true);

        if(cashoutCount.hasNext()) {
            Iterator<Transaction> transactions = repo.getCardTransactions(false);
            String terminalId = "";
            String posAccount = "";

            while(transactions.hasNext()) {
                Transaction transaction = transactions.next();

                int status = transaction.getSynced();
                if((transaction.getSynced() & Transaction.Sync.SYNCPAYCENTRE.getValue()) == Transaction.Sync.SYNCPAYCENTRE.getValue()) {
                    terminalId = (String)transaction.get("terminalId");
                    posAccount = transaction.getPosAccount();
                    continue;
                }else if((transaction.getSynced() & Transaction.Sync.SYNCPAYCENTRE.getValue()) == 0) {
                    boolean hasChanges = false;

                    if(transaction.getPosAccount() == null || "".equals(transaction.getPosAccount()) && !"".equals(posAccount)) {
                        transaction.setPosAccount(posAccount);
                        hasChanges = true;
                    }

                    if("".equals((String)transaction.get("terminalId")) && !"".equals(terminalId)) {
                        transaction.set("terminalId", terminalId);
                        hasChanges = true;
                    }

                    if(hasChanges) {
                        repo.update(transaction);
                    }
                }


            }
        }

    }
}