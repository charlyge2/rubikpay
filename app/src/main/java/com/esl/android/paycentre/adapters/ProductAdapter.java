package com.esl.android.paycentre.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<ProductModel> productList;

    private static View.OnClickListener mOnItemClickListener;

    //getting the context and product list with constructor
    public ProductAdapter(Context mCtx, List<ProductModel> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.dstv_products_layout, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //getting the product of the specified position
        ProductModel product = productList.get(position);

        //binding the data with the viewholder views
        holder.planTitle.setText(product.getPlan());
        holder.planCode.setText(product.getPlanCode());;
        holder.planPrice.setText(String.valueOf(product.getPrice()));


    }


    @Override
    public int getItemCount() {
        return productList.size();
    }

    //TODO: Step 2 of 4: Assign itemClickListener to your local View.OnClickListener variable
    public static void setOnItemClickListener(View.OnClickListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }



    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView planTitle, planCode, planPrice;

        public ProductViewHolder(View itemView) {
            super(itemView);

            planTitle = itemView.findViewById(R.id.planTitle);
            planCode = itemView.findViewById(R.id.planCode);
            planPrice = itemView.findViewById(R.id.planPrice);
            itemView.setTag(this);
            itemView.setOnClickListener(mOnItemClickListener);
        }
    }
}
