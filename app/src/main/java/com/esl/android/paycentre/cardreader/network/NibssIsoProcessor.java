package com.esl.android.paycentre.cardreader.network;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.serializers.PurchaseRequest;
import com.esl.android.paycentre.cardreader.serializers.PurchaseResponse;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.cardreader.utils.HexUtil;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Display;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import com.solab.iso8583.MessageFactory;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class NibssIsoProcessor {

    private static NibssSocketManager socketPluger;

    public NibssIsoProcessor() {

    }

    public static String process(PurchaseRequest request,
                                 byte[] sessionKey, Context context) {

        PurchaseResponse response = null;
        try {

            IsoMessage ismsg = new IsoMessage();
            ismsg.setType(0x200);
            IsoValue<String> field2 = new IsoValue<String>(IsoType.LLVAR,
                    request.getPan());
            Log.i("pan", request.getPan());
            IsoValue<String> field3 = new IsoValue<String>(IsoType.NUMERIC,
                    request.getProcessingCode(), 6);
            Log.i("pcode", request.getProcessingCode());
            IsoValue<String> field4 = new IsoValue<String>(IsoType.NUMERIC,
                    request.getTransactionAmount(), 12);
            Log.i("amt", request.getTransactionAmount());
            IsoValue<String> field7 = new IsoValue<String>(IsoType.DATE10,
                    request.getTransmissionDateTime());
            Log.i("dandt", request.getTransmissionDateTime());
            IsoValue<String> field11 = new IsoValue<String>(IsoType.NUMERIC,
                    request.getStan(), 6);
            Log.i("stan", request.getStan());
            IsoValue<String> field12 = new IsoValue<String>(IsoType.TIME,
                    request.getLocalTransactionTime());
            Log.i("time", request.getLocalTransactionTime());
            IsoValue<String> field13 = new IsoValue<String>(IsoType.DATE4,
                    request.getLocalTransactionDate());
            Log.i("date", request.getLocalTransactionDate());
            IsoValue<String> field14 = new IsoValue<String>(IsoType.DATE4,
                    request.getCardExpirationDate());
            Log.i("expdate", request.getCardExpirationDate());
            IsoValue<String> field18 = new IsoValue<String>(IsoType.ALPHA,
                    request.getMerchantType(), 4);
            Log.i("mertype", request.getMerchantType());
            IsoValue<String> field22 = new IsoValue<String>(IsoType.ALPHA,
                    request.getPosEntryMode(), 3);
            Log.i("posentrymode", request.getPosEntryMode());
            IsoValue<String> field23 = new IsoValue<String>(IsoType.ALPHA,
                    request.getCardSequenceNumber(), 3);
            Log.i("seqcardno", request.getCardSequenceNumber());
            IsoValue<String> field25 = new IsoValue<String>(IsoType.ALPHA,
                    request.getPosConditionCode(), 2);
            Log.i("poscondtncode", request.getPosConditionCode());
            IsoValue<String> field26 = new IsoValue<String>(IsoType.NUMERIC,
                    request.getPosPinCaptureCode(), 2);
            Log.i("pospincapturecode", request.getPosPinCaptureCode());
            IsoValue<String> field28 = new IsoValue<String>(IsoType.ALPHA,
                    request.getTransactionFee(), 9);
            Log.i("transactnfee", request.getTransactionFee());
            IsoValue<String> field32 = new IsoValue<String>(IsoType.LLVAR,
                    request.getAcquiringInstIdCode());
            Log.i("acqinstcode", request.getAcquiringInstIdCode());
            IsoValue<String> field35 = new IsoValue<String>(IsoType.LLVAR,
                    request.getTrack2Data());
            Log.i("track2", request.getTrack2Data());
            IsoValue<String> field37 = new IsoValue<String>(IsoType.ALPHA,
                    request.getRetrievalReferenceNumber(), 12);
            Log.i("rrn", Globals.rrn);
            IsoValue<String> field40 = new IsoValue<String>(IsoType.NUMERIC,
                    request.getServiceRestrictionCode(), 3);
            Log.i("src", request.getServiceRestrictionCode());
            IsoValue<String> field41 = new IsoValue<String>(IsoType.ALPHA,
                    request.getTerminalId(), 8);
            Log.i("tid", request.getTerminalId());
            IsoValue<String> field42 = new IsoValue<String>(IsoType.ALPHA,
                    request.getCardAcceptorIdCode(), 15);
            Log.i("cac", request.getCardAcceptorIdCode());
            IsoValue<String> field43 = new IsoValue<String>(IsoType.ALPHA,
                    request.getCardAcceptorNameLocation(), 40);
            Log.i("canc", request.getCardAcceptorNameLocation());
            IsoValue<String> field49 = new IsoValue<String>(IsoType.NUMERIC,
                    request.getCurrencyCode(), 3);
            Log.i("currcode", request.getCurrencyCode());
            IsoValue<String> field52 = new IsoValue<String>(IsoType.ALPHA,
                    request.getPinData(), 16);
            IsoValue<String> field55 = new IsoValue<String>(IsoType.LLLVAR,
                    request.getIccData());
            Log.i("icc", request.getIccData());
            IsoValue<String> field59 = new IsoValue<String>(IsoType.LLLVAR,
                    request.getTransportData());
            Log.i("icc", request.getTransportData());
            IsoValue<String> field123 = new IsoValue<String>(IsoType.LLLVAR,
                    request.getPosDataCode());
            Log.i("icc", request.getPosDataCode());
            IsoValue<String> field128 = new IsoValue<String>(IsoType.ALPHA,
                    new String(new byte[]{0x0}), 64);

            ismsg.setField(2, field2);
            ismsg.setField(3, field3);
            ismsg.setField(4, field4);
            ismsg.setField(7, field7);
            ismsg.setField(11, field11);
            ismsg.setField(12, field12);
            ismsg.setField(13, field13);
            ismsg.setField(14, field14);
            ismsg.setField(18, field18);
            ismsg.setField(22, field22);
            ismsg.setField(23, field23);
            ismsg.setField(25, field25);
            ismsg.setField(26, field26);
            ismsg.setField(28, field28);
            ismsg.setField(32, field32);
            ismsg.setField(35, field35);
            ismsg.setField(37, field37);
            ismsg.setField(40, field40);
            ismsg.setField(41, field41);
            ismsg.setField(42, field42);
            ismsg.setField(43, field43);
            ismsg.setField(49, field49);

            if (request.getPinData() != null) {
                ismsg.setField(52, field52);
            }

            ismsg.setField(55, field55);
            ismsg.setField(59, field59);
            ismsg.setField(123, field123);
            ismsg.setField(128, field128);

            byte[] bites = ismsg.writeData();

            int length = bites.length;

            /* Hash generation */
            byte[] temp = new byte[length - 64];

            if (length >= 64) {
                System.arraycopy(bites, 0, temp, 0, length - 64);
            }

            String hashHex = NibssIsoProcessor.generateHash256Value(temp,
                    sessionKey);

            IsoValue<String> field128update = new IsoValue<String>(
                    IsoType.ALPHA, hashHex, 64);

            ismsg.setField(128, field128update);

            byte[] rawMsg = ismsg.writeData();

            int lenght = rawMsg.length;
            byte[] bLenght = new byte[2];
            bLenght[1] = (byte) lenght;
            bLenght[0] = (byte) (lenght >> 8);

            byte[] toSend = concat(bLenght, rawMsg);

            Log.i("PAYPADSDK", new String(toSend));

            TrustManager tm = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            SSLContext sc = SSLContext.getInstance("TLSv1.2");

            sc.init(null, new TrustManager[]{tm}, null);

            ReferenceList.config = context.getSharedPreferences(ReferenceList.preference, 0);

            String ip = ReferenceList.config.getString(ReferenceList.nibssIp, "");
            int port = ReferenceList.config.getInt(ReferenceList.nibssPort, 0);

            SSLSocketFactory ssf = sc.getSocketFactory();
            Socket s = ssf.createSocket();
            s.connect(new InetSocketAddress(ip, port),
                    60 * 1000);

            s.setSoTimeout(75 * 1000);

            if (s.isConnected()) {
                System.out.println("I am connected");
                socketPluger = new NibssSocketManager(s);

            }


            socketPluger.sendData(toSend);

            byte[] responseBytes = socketPluger.receive();
            Log.i("responseBytes", String.valueOf(responseBytes));

            Log.i("PAYPADSDKRESPONSE", new String(responseBytes));

            MessageFactory<IsoMessage> responseMessageFactory = new MessageFactory<>();
            responseMessageFactory.addMessageTemplate(ismsg);
            responseMessageFactory.setAssignDate(true);
            responseMessageFactory.setUseBinaryBitmap(false);
            responseMessageFactory.setUseBinaryMessages(false);
            responseMessageFactory.setEtx(-1);
            responseMessageFactory.setIgnoreLastMissingField(false);

            responseMessageFactory.setConfigPath("assets/isoconfig.xml");

            IsoMessage responseMessage = responseMessageFactory.parseMessage(
                    responseBytes, 0);

            if (responseMessage != null) {

                // read response
                response = new PurchaseResponse();


                if (responseMessage.hasField(39)) {
                    response.setField39((String) responseMessage
                            .getObjectValue(39));
                }


                if (responseMessage.hasField(4)) {
                    BigDecimal amount = responseMessage.getObjectValue(4);
                    if ((Globals.purchase+"0").equals(String.valueOf(amount))) {
                        Log.i("PAYPADSDKAmount", String.valueOf(amount));
                    } else {

                        if(Globals.showStampDuty){
                            double myStampDuty = Double.parseDouble(Globals.stampDuty);
                            if(TextUtils.isEmpty(Globals.vat)){
                                Globals.amount = Double.parseDouble(String.valueOf(amount)) - myStampDuty - Double.parseDouble(Globals.convenienceFee);
                            }
                            else {
                                Globals.amount = Double.parseDouble(String.valueOf(amount)) - myStampDuty - Double.parseDouble(Globals.convenienceFee) - Double.parseDouble(Globals.vat);

                            }
                            Globals.isValueDifferent = true;
                        } else {
                            Globals.isValueDifferent = true;
                            if(TextUtils.isEmpty(Globals.vat)){
                                Globals.amount = Double.parseDouble(String.valueOf(amount)) - Double.parseDouble(Globals.convenienceFee);
                            }
                            else {
                                Globals.amount = Double.parseDouble(String.valueOf(amount)) - Double.parseDouble(Globals.convenienceFee)-Double.parseDouble(Globals.vat);
                            }

                            Log.i("PAYPADSDKAmount", String.valueOf(amount));
                        }


                    }
                }


                if (responseMessage.hasField(38)) {
                    Globals.authCode = (String) responseMessage
                            .getObjectValue(38);
                } else {
                    Globals.authCode = "Not authenticated";

                }

                if (responseMessage.hasField(55)) {
                    String icc = (String) responseMessage.getObjectValue(55);
                    Log.i("PAYPADSDKRESPONSEICC", icc);

                    //getICCResponse(Globals.icc);
                }

            }

            Log.i("PAYPADSDK39", response.getField39());

            response.returnMessage();

            String responseCode = response.getField39();

            Globals.responseCode = response.getField39();
            Globals.appResponse = response.messages.get(response.getField39());

            getPinpadResponse();


            return responseCode;

        } catch (ConnectTimeoutException e) {

            Globals.responseCode = "XX";
            Globals.appResponse = "TRANSACTION NOT DONE ";
            Globals.returnMessage = "TRANSACTION\n NOT DONE ";

            Globals.authCode = "Not authenticated";
            Log.i("nibbsExcep",e.getLocalizedMessage());

        } catch (ConnectException e) {

            Globals.responseCode = "XX";
            Globals.appResponse = "TRANSACTION NOT DONE ";
            Globals.returnMessage = "TRANSACTION\n NOT DONE ";

            Globals.authCode = "Not authenticated";
            Log.i("nibbsExcep",e.getLocalizedMessage());

        } catch (SocketTimeoutException e) {


            Globals.responseCode = "XX";
            Globals.appResponse = "TRANSACTION TIMED OUT ";
            Globals.returnMessage = "TRANSACTION\n TIMED OUT ";

            Globals.authCode = "Not authenticated";
            Log.i("nibbsExcep",e.getLocalizedMessage());

        } catch (Exception e) {

            e.printStackTrace();

            Globals.responseCode = "XX";
            Globals.appResponse = "ERROR OCCURRED";
            Globals.returnMessage = "ERROR OCCURRED";

            Globals.authCode = "Not authenticated";

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);

            e.printStackTrace(pw);

            String stackTrace = sw.toString();
            Log.i("nibbsExcep",e.getLocalizedMessage());

            Display.sendToErrorLog("Cashout", e.getMessage(), stackTrace);
        }

        return "XX";
    }


    public static String generateHash256Value(byte[] iso, byte[] key) {

        MessageDigest m = null;
        String hashText = null;

        try {
            m = MessageDigest.getInstance("SHA-256");
            m.update(key, 0, key.length);
            m.update(iso, 0, iso.length);
            // hashText = new BigInteger(1, m.digest()).toString(16);
            hashText = HexUtil.byteArrayToHexString(m.digest());
            hashText = hashText.replace(" ", "");

        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }

        if (hashText.length() < 64) {
            int numberOfZeroes = 64 - hashText.length();
            String zeroes = "";
            String temp = hashText.toString();

            for (int i = 0; i < numberOfZeroes; i++)
                zeroes = zeroes + "0";

            temp = zeroes + temp;


            return temp;
        }

        return hashText;
    }

    private static byte[] concat(byte[] A, byte[] B) {
        int aLen = A.length;
        int bLen = B.length;
        byte[] C = new byte[aLen + bLen];
        System.arraycopy(A, 0, C, 0, aLen);
        System.arraycopy(B, 0, C, aLen, bLen);
        return C;
    }

    public static void getPinpadResponse() {

        if (Globals.responseCode.equals("00")) {
            Globals.returnMessage = "TRANSACTION\n   SUCCESSFUL";
        } else if (Globals.responseCode.equals("01")) {
            Globals.returnMessage = "   REFER TO\n  CARD ISSUER";
        } else if (Globals.responseCode.equals("02")) {
            Globals.returnMessage = "   REFER TO\n  CARD ISSUER";
        } else if (Globals.responseCode.equals("03")) {
            Globals.returnMessage = "   INVALID\n    MERCHANT   ";
        } else if (Globals.responseCode.equals("04")) {
            Globals.returnMessage = "  PICK-UP\n      CARD   ";
        } else if (Globals.responseCode.equals("05")) {
            Globals.returnMessage = "    DO NOT\n   HONOUR";
        } else if (Globals.responseCode.equals("06")) {
            Globals.returnMessage = "      ERROR   ";
        } else if (Globals.responseCode.equals("07")) {
            Globals.returnMessage = "   PICK-UP\n      CARD   ";
        } else if (Globals.responseCode.equals("08")) {
            Globals.returnMessage = "     HONOUR\n    WITH ID";
        } else if (Globals.responseCode.equals("09")) {
            Globals.returnMessage = "    REQUEST IN\n    PROGRESS   ";
        } else if (Globals.responseCode.equals("10")) {
            Globals.returnMessage = "   APPROVED,\n   PARTIAL";
        } else if (Globals.responseCode.equals("11")) {
            Globals.returnMessage = "   APPROVED,\n     VIP";
        } else if (Globals.responseCode.equals("12")) {
            Globals.returnMessage = "   INVALID\n   TRANSACTION";
        } else if (Globals.responseCode.equals("13")) {
            Globals.returnMessage = "   INVALID\n     AMOUNT   ";
        } else if (Globals.responseCode.equals("14")) {
            Globals.returnMessage = " INVALID CARD\n    NUMBER   ";
        } else if (Globals.responseCode.equals("15")) {
            Globals.returnMessage = "  NO SUCH\n    ISSUER   ";
        } else if (Globals.responseCode.equals("16")) {
            Globals.returnMessage = "    APPROVED\n UPDATE TRACK3";
        } else if (Globals.responseCode.equals("17")) {
            Globals.returnMessage = "   CUSTOMER\n  CANCELLATION   ";
        } else if (Globals.responseCode.equals("18")) {
            Globals.returnMessage = "   CUSTOMER\n    DISPUTE   ";
        } else if (Globals.responseCode.equals("19")) {
            Globals.returnMessage = "      RE-ENTER\n   TRANSACTION";
        } else if (Globals.responseCode.equals("20")) {
            Globals.returnMessage = "     INVALID\n     RESPONSE   ";
        } else if (Globals.responseCode.equals("21")) {
            Globals.returnMessage = "  NO ACTION\n     TAKEN";
        } else if (Globals.responseCode.equals("22")) {
            Globals.returnMessage = "  SUSPECTED\n   MALFUNCTION";
        } else if (Globals.responseCode.equals("23")) {
            Globals.returnMessage = "UNACCEPTABLE FEE\n FOR TRANSACTION";
        } else if (Globals.responseCode.equals("24")) {
            Globals.returnMessage = " FILE UPDATE\n  NOT SUPPORTED    ";
        } else if (Globals.responseCode.equals("25")) {
            Globals.returnMessage = "  UNABLE TO\n LOCATE RECORD   ";
        } else if (Globals.responseCode.equals("26")) {
            Globals.returnMessage = "   DUPLICATE\n      RECORD   ";
        } else if (Globals.responseCode.equals("27")) {
            Globals.returnMessage = " FILE UPDATE\n  EDIT ERROR  ";
        } else if (Globals.responseCode.equals("28")) {
            Globals.returnMessage = " FILE UPDATE\n  FILE LOCKED  ";
        } else if (Globals.responseCode.equals("29")) {
            Globals.returnMessage = " FILE UPDATE\n     FAILED";
        } else if (Globals.responseCode.equals("30")) {
            Globals.returnMessage = "      FORMAT\n      ERROR";
        } else if (Globals.responseCode.equals("31")) {
            Globals.returnMessage = "    BANK NOT\n    SUPPORTED   ";
        } else if (Globals.responseCode.equals("32")) {
            Globals.returnMessage = "   COMPLETED\n    PARTIALLY";
        } else if (Globals.responseCode.equals("33")) {
            Globals.returnMessage = "  EXPIRED CARD\n    PICK-UP";
        } else if (Globals.responseCode.equals("34")) {
            Globals.returnMessage = " SUSPECTED FRAUD\n  PICK-UP   ";
        } else if (Globals.responseCode.equals("35")) {
            Globals.returnMessage = " CONTACT ACQUIRER\n  PICK-UP   ";
        } else if (Globals.responseCode.equals("36")) {
            Globals.returnMessage = " RESTRICTED CARD\n  PICK-UP   ";
        } else if (Globals.responseCode.equals("37")) {
            Globals.returnMessage = "CALL ACQUIRER SECURITY PICK-UP   ";
        } else if (Globals.responseCode.equals("38")) {
            Globals.returnMessage = "PIN TRIES EXCEEDED\n  PICK-UP   ";
        } else if (Globals.responseCode.equals("39")) {
            Globals.returnMessage = "   NO CREDIT\n    ACCOUNT   ";
        } else if (Globals.responseCode.equals("40")) {
            Globals.returnMessage = "  FUNCTION NOT\n   SUPPORTED   ";
        } else if (Globals.responseCode.equals("41")) {
            Globals.returnMessage = "   LOST CARD";
        } else if (Globals.responseCode.equals("42")) {
            Globals.returnMessage = "  NO UNIVERSAL\n     ACCOUNT   ";
        } else if (Globals.responseCode.equals("43")) {
            Globals.returnMessage = " STOLEN CARD";
        } else if (Globals.responseCode.equals("44")) {
            Globals.returnMessage = " NO INVESTMENT\n      ACCOUNT";
        } else if (Globals.responseCode.equals("51")) {
            Globals.returnMessage = "NOT SUFFICIENT\n FUNDS";
        } else if (Globals.responseCode.equals("52")) {
            Globals.returnMessage = "  NO CURRENT\n    ACCOUNT   ";
        } else if (Globals.responseCode.equals("53")) {
            Globals.returnMessage = "  NO SAVINGS\n    ACCOUNT   ";
        } else if (Globals.responseCode.equals("54")) {
            Globals.returnMessage = " EXPIRED CARD ";
        } else if (Globals.responseCode.equals("55")) {
            Globals.returnMessage = "   INCORRECT\n        PIN   ";
        } else if (Globals.responseCode.equals("56")) {
            Globals.returnMessage = "  NO CARD\n      RECORD   ";
        } else if (Globals.responseCode.equals("57")
                || Globals.responseCode.equals("58")) {
            Globals.returnMessage = "TRANSACTION\n  NOT PERMITTED";
        } else if (Globals.responseCode.equals("59")) {
            Globals.returnMessage = "    SUSPECTED\n      FRAUD ";
        } else if (Globals.responseCode.equals("60")) {
            Globals.returnMessage = "    CONTACT\n       ACQUIRER   ";
        } else if (Globals.responseCode.equals("61")) {
            Globals.returnMessage = "EXCEEDS WITHDRAWAL\n       LIMIT   ";
        } else if (Globals.responseCode.equals("62")) {
            Globals.returnMessage = "   RESTRICTED\n        CARD   ";
        } else if (Globals.responseCode.equals("63")) {
            Globals.returnMessage = "   SECURITY\n   VIOLATION   ";
        } else if (Globals.responseCode.equals("64")) {
            Globals.returnMessage = " ORIGINAL AMOUNT\n     INCORRECT   ";
        } else if (Globals.responseCode.equals("65")) {
            Globals.returnMessage = "EXCEEDS WITHDRAWAL\n   FREQUENCY   ";
        } else if (Globals.responseCode.equals("66")) {
            Globals.returnMessage = "  CALL ACQUIRER\n      SECURITY  ";
        } else if (Globals.responseCode.equals("67")) {
            Globals.returnMessage = "HARD CAPTURE";
        } else if (Globals.responseCode.equals("68")) {
            Globals.returnMessage = "RESPONSE RECEIVED\n   TOO LATE ";
        } else if (Globals.responseCode.equals("75")) {
            Globals.returnMessage = "   PIN TRY\n    EXCEEDED  ";
        } else if (Globals.responseCode.equals("77")) {
            Globals.returnMessage = "BANK INTERVENE";
        } else if (Globals.responseCode.equals("78")) {
            Globals.returnMessage = "BANK INTERVENE";
        } else if (Globals.responseCode.equals("90")) {
            Globals.returnMessage = "   CUT-OFF\n  IN PROGRESS  ";
        } else if (Globals.responseCode.equals("91")) {
            Globals.returnMessage = "   SWITCH\n  INOPERATIVE   ";
        } else if (Globals.responseCode.equals("92")) {
            Globals.returnMessage = "     ROUTING\n      ERROR";
        } else if (Globals.responseCode.equals("93")) {
            Globals.returnMessage = "LAW VIOLATION";
        } else if (Globals.responseCode.equals("94")) {
            Globals.returnMessage = "    DUPLICATE\n   TRANSACTION   ";
        } else if (Globals.responseCode.equals("95")) {
            Globals.returnMessage = "    RECONCILE\n      ERROR";
        } else if (Globals.responseCode.equals("96")) {
            Globals.returnMessage = "   SYSTEM\n   MALFUNCTION";
        } else if (Globals.responseCode.equals("98")) {
            Globals.returnMessage = "  EXCEEDS CASH\n      LIMIT";
        }

    }

}
