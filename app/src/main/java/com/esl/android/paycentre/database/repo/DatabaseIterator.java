package com.esl.android.paycentre.database.repo;

import android.database.Cursor;

import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.BaseModel;

import java.util.Iterator;

public class DatabaseIterator<T extends BaseModel> implements Iterator<T> {

    private Cursor cursor;
    private RepositoryInterface<T> repo;
    private boolean hasNext;

    public DatabaseIterator(Cursor cursor, RepositoryInterface<T> repo) {
        this.cursor = cursor;
        this.repo = repo;
        hasNext = !this.isEmpty();
    }

    @Override
    public boolean hasNext() {
        return !cursor.isClosed() && hasNext;
    }

    @Override
    public T next() {
        return this.getModel();
    }

    private T getModel() {
        if (this.hasNext()) {
            T t = this.repo.build(cursor);

            if (!(hasNext = cursor.moveToNext())) {
                this.close();
            }

            return t;
        }

        return null;
    }

    private boolean isEmpty() {
        return !cursor.isClosed() && !cursor.moveToFirst();
    }

    public void close() {
        this.cursor.close();
        DatabaseManager.getInstance().closeDatabase();
    }
}
