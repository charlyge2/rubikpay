package com.esl.android.paycentre.logs;

import com.esl.android.paycentre.services.HttpService;

import org.json.JSONObject;

public class PayCentreLogger {
    public static void error(JSONObject jsonObject,String url){
        HttpService.log(jsonObject,url);
    }
}
