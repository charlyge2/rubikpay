package com.esl.android.paycentre.controllers.activationControllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.activation.ChangePasswordActivity;
import com.esl.android.paycentre.activities.activation.PosOperationsActivity;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

public class PosAdminLoginController {


    SharedPreferenceManager sharedPreferenceManager;

    public void doLogin(final Context context, final String username, final String password, final View view) {
        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Logging in....");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("username", username);
            jsonObject.put("password", password);

            AndroidNetworking.post(MainActivity.adminLoginUrl)
                    .setPriority(Priority.IMMEDIATE)
                    .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                    .addHeaders("Content-type", "application/json")
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) { progress.dismiss();

                            Log.d("ADMIN_SUCCESS", response.toString());

                            try {
                                String responseMessage = response.getString("responseMessage");
                                String responseCode = response.getString("responseCode");
                                if (responseCode.equals("00")) {
                                    String accessToken = response.getJSONObject("data").getString("access_token");
                                    String refreshToken = response.getJSONObject("data").getString("refresh_token");
                                    String expiresIn = response.getJSONObject("data").getString("expires_in");
                                    sharedPreferenceManager.save(context, "posOperationsPage", "true");
                                    sharedPreferenceManager.save(context, "accessToken", accessToken);
                                    sharedPreferenceManager.save(context, "refreshToken", refreshToken);
                                    sharedPreferenceManager.save(context,"expiresIn",expiresIn);
                                    Intent myIntent = new Intent(context, PosOperationsActivity.class);
                                    context.startActivity(myIntent);
                                    Log.d("TOKEN", accessToken + " - " + refreshToken);
                                } else if ("03".equals(responseCode)) {
                                    Intent intent = new Intent(context, ChangePasswordActivity.class);
                                    context.startActivity(intent);
                                } else  {
                                    Alert.showFailed(context, responseMessage);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
//
                            try {

                                Log.d("ADMIN_ERROR", String.valueOf(error));
                                Log.d("ADMIN_ERROR", error.getErrorBody());

                                JSONObject response = new JSONObject(error.getErrorBody());

                                String responseMessage = response.optString("responseMessage");
                                String responseCode = response.optString("responseCode");

                                if ("03".equals(responseCode)) {
                                    Intent intent = new Intent(context, ChangePasswordActivity.class);
                                    context.startActivity(intent);
                                } else if("02".equals(responseCode)){
                                    Alert.showFailed(context, "Wrong Phone/Password Combination");
                                }else{
                                    Alert.showFailed(context, responseMessage);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
