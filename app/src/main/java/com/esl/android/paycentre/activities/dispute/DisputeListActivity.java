package com.esl.android.paycentre.activities.dispute;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.adapters.DisputeItemsAdapter;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class DisputeListActivity extends AppCompatActivity {

    private ListView disputeListView;
    private LinearLayout noResultLayout;

    private List<String[]> disputeRecordsList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispute_list);

        init();
    }

    private void init() {

        disputeListView = findViewById(R.id.disputeListView);
        noResultLayout = findViewById(R.id.noResultLayout);

        new GetDisputeList().execute(MainActivity.fetchDisputesUrl);

    }

    public void moveBack(View view) {
        Navigator.moveToDashboard(getApplicationContext());
    }

    class GetDisputeList extends AsyncTask<String, String, String> {

        private long result = 0;
        private JSONArray responseArray = null;
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(DisputeListActivity.this);

            progressDialog.setMessage("Fetching Disputes ...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            String urlString = strings[0];

            String code = "XX";

            try {
                TrustManager tm = new X509TrustManager() {
                    @Override
                    public int hashCode() {
                        return super.hashCode();
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                };

                SSLContext sc;

                sc = SSLContext.getInstance("TLS");

                sc.init(null, new TrustManager[]{tm}, null);

                SSLSocketFactory ssf = sc.getSocketFactory();

                URL url = new URL(urlString);

//                HttpsURLConnection conn = (HttpsURLConnection) url
//                        .openConnection();
//                conn.setSSLSocketFactory(ssf);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                conn.setRequestProperty("Authorization", ConfigurationClass.getAccessToken(DisputeListActivity.this));
                conn.setRequestProperty("x-phone-uuid", MainActivity.getUuid(getApplicationContext()));

                result = (long) conn.getResponseCode();

                System.out.print(result);

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                StringBuilder sb = new StringBuilder();

                String output;

                while ((output = br.readLine()) != null) {

                    sb.append(output);
                }

                JSONObject responseObject = new JSONObject(sb.toString());
                code = responseObject.getString("responseCode");
                Log.d("MESSAGE",responseObject.getString("responseMessage"));
                //responseArray = responseObject.getJSONArray("data");
                //Log.d("DISPUT_ARRAY",String.valueOf(responseArray));

                if(responseObject.has("data")){
                    responseArray = responseObject.getJSONArray("data");
                }



            } catch (Exception e) {

                e.printStackTrace();
            }

            return code;
        }

        @Override
        protected void onPostExecute(String code) {
            super.onPostExecute(code);

            progressDialog.dismiss();
            Log.d("JSON_RESULT", code + "\n"+result);
            if (result == 200 & "00".equals(code)) {
                try {

                    Log.d("JSON_RESULT", code + " - "+ result);

                    JSONObject disputeObject = null;

                    if (responseArray == null) {
                        Alert.showSuccess(getApplicationContext(),"No dispute created yet");
                        noResultLayout.setVisibility(View.VISIBLE);

                    } else {

                        disputeRecordsList = new ArrayList<>();

                        for (int i = 0; i < responseArray.length(); i++) {

                            disputeObject = responseArray.getJSONObject(i);

                            String status = disputeObject.getString("status");
                            String transactionId = disputeObject.getString("transactionId");
                            String fullName = disputeObject.getString("fullName");
                            String phoneNumber = disputeObject.getString("phoneNumber");
                            String transactionStatus = disputeObject.getString("initialResponse");
                            String amount = disputeObject.getString("amount");
                            String createdAt = disputeObject.getString("createdAt");
                            String updatedAt = disputeObject.getString("updatedAt");

                            disputeRecordsList.add(new String[]{status, transactionId, fullName, phoneNumber,
                            transactionStatus, "₦" +amount, createdAt, updatedAt});

                        }

                        disputeListView.setVisibility(View.VISIBLE);

                        DisputeItemsAdapter adapter = new DisputeItemsAdapter(disputeRecordsList);
                        disputeListView.setAdapter(adapter);
                        disputeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                String transactionId = ((TextView) view.findViewById(R.id.transactionId)).getText().toString();
                                String fullname = ((TextView) view.findViewById(R.id.customerName)).getText().toString();
                                String disputeStatus = ((TextView) view.findViewById(R.id.disputeStatus)).getText().toString();
                                String amount = ((TextView) view.findViewById(R.id.transactionAmount)).getText().toString();
                                String createdAt = ((TextView) view.findViewById(R.id.loggedTime)).getText().toString();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DisputeListActivity.this);
                                alertDialogBuilder.setTitle("Dispute Details");
                                alertDialogBuilder
                                        .setMessage("\nAmount : " + amount
                                                + "\n\nDispute Status : " + disputeStatus
                                                + "\n\nCreated : " + createdAt
                                                + "\n\nTransaction Id : " + transactionId
                                                + "\n\nCustomer Name : " + fullname)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        });
                    }

                } catch (Exception ex) {

                    Alert.showFailed(DisputeListActivity.this, "An error occurred");

                }
            } else if (result != 200) {

                Alert.showFailed(DisputeListActivity.this, "Please try again later");
            }
        }
    }

    public void moveHome(View view) {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }
}
