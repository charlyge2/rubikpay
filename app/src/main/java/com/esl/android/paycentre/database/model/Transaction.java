package com.esl.android.paycentre.database.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

public class Transaction implements BaseModel,Serializable {

    public enum Type{
        WITHDRAWAL("CASH OUT"), FUNDTRANSFER("FUNDS TRANSFER"), AGENTTRANSFER("AGENT TRANSFER"),
        POWER("POWER"),WAEC("WAEC"), FUNDPURSE("FUND PURSE"),
        DSTV("DSTV"), GOTV("GOTV"), AIRTIME("AIRTIME"), STARTIMES("STARTIMES");

        private static HashMap<String, Type> nameMap = new HashMap<>();
        private final String name;

        Type(String name) {
            this.name = name;
        }
        public String getName() {
            return name;
        }

        public static Type getByName(String name){
            for(Type type: values())
                nameMap.put(type.getName(), type);
            return nameMap.get(name);
        }
    }

    public enum Sync{
        SYNCREQBOTH(12), SYNCREQPAYCENTRE(8), SYNCREQIBETA(4),
        SYNCPAYCENTRE(2), SYNCIBETA(1);

        private static HashMap<String, Type> nameMap = new HashMap<>();
        private final int value;

        Sync(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public static final String TAG = Transaction.class.getSimpleName();
    public static final String TABLE = "transactions";

    // Labels Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_TRANSID = "transactionId";
    public static final String KEY_TRANSTYPE = "transactionType";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_TOTAL = "total";
    public static final String KEY_CONVFEE = "convenienceFee";
    public static final String KEY_AGENTCUT = "agentCut";
    public static final String KEY_AGENCYTRANS = "agencyTransaction";
    public static final String KEY_POSACCOUNT = "posAccount";
    public static final String KEY_DATE = "appTime";
    public static final String KEY_SYNCED = "synced";
    public static final String KEY_DATA = "data";
    public static final String KEY_VERSION = "version";
    public static final String KEY_RESPONSECODE = "statusCode";
    public static final String KEY_RESPONSEMSG = "statusMessage";


    private int id;
    private String transactionId;
    private Transaction.Type transactionType;
    private float amount;
    private float total;
    private float convenienceFee;
    private float agentCut;
    private String agencyTransaction;
    private String posAccount;
    private long date;
    private int synced;
    private JSONObject data;
    private String version;
    private String responseCode;
    private String responseMsg;
    private String time;


    public Transaction() {
        this.date = (new Date()).getTime();
        this.data = new JSONObject();
        this.synced = 0;
        this.responseCode = "YY";
        this.responseMsg = "FAILED";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime(){
        return time;
    }

    public void setTime(String time){
        this.time = time;
    }
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Transaction.Type getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Type transactionType) {
        this.transactionType = transactionType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public float getConvenienceFee() {
        return convenienceFee;
    }

    public void setConvenienceFee(float convenienceFee) {
        this.convenienceFee = convenienceFee;
    }

    public String getPosAccount() {
        return posAccount;
    }

    public void setPosAccount(String posAccount) {
        this.posAccount = posAccount;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getAgentCut() {
        return agentCut;
    }

    public void setAgentCut(float agentCut) {
        this.agentCut = agentCut;
    }

    public String getAgencyTransaction() {
        return agencyTransaction;
    }

    public void setAgencyTransaction(String agencyTransaction) {
        this.agencyTransaction = agencyTransaction;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public Date getDate() {
        return new Date(date);
    }

    public void setDate(Date date) {
        this.date = date.getTime();
    }

    public long getDateLong() {
        return date;
    }

    public void setDateLong(long date) {
        this.date = date;
    }

    public JSONObject getData() {
        return this.data;
    }

    public void setData(JSONObject data) {
        if(data != null)
            this.data = data;
    }

    public String getDataDB() {
        return this.data.toString();
    }

    public void setDataDB(String data) {
        try {
            if(data != null && !"".equals(data))
                this.data = new JSONObject(data);
        } catch (JSONException e) {
        }
    }

    public Object get(String key) {
        if(this.data != null && this.data.has(key)) {
            try {
                return this.data.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return "";
    }

    public void set(String key, Object value) {
        if(this.data != null) {
            try {
                this.data.put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public int getSynced() {
        return synced;
    }

    public void setSynced(int syncStatus) {
        this.synced = syncStatus;
    }

    public void requiresSync(int sync) {

    }
}
