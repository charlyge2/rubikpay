package com.esl.android.paycentre.activities.activation;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.controllers.activationControllers.ChangePosPinController;
import com.esl.android.paycentre.controllers.activationControllers.FetchPosAccountController;
import com.esl.android.paycentre.database.DBHelper;
import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.repo.PosAccountRepo;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class ChangePosPinActivity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<String> usernames = new ArrayList<>();

    DBHelper dbHelper;

    PosAccountRepo posAccountRepo = new PosAccountRepo();

    EditText newPin;

    EditText oldPin;

    FetchPosAccountController fetchPosAccountController;

    SharedPreferenceManager sharedPreferenceManager;

    Button changePin;

    Spinner spinner;

    ChangePosPinController changePosPinController = new ChangePosPinController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        //getSupportActionBar().hide(); // hide the title back
        setContentView(R.layout.activity_change_pos_pin);

        spinner = (Spinner) findViewById(R.id.dateSpinner);

        dbHelper = new DBHelper(getApplicationContext());

        newPin = findViewById(R.id.newPin);

        oldPin = findViewById(R.id.oldPin);

        changePin = findViewById(R.id.changePin);
        changePin.setOnClickListener(this);

        DatabaseManager.initializeInstance(dbHelper);

        Iterator posaccounts = posAccountRepo.getPosAccounts();

        while(posaccounts.hasNext()) {
            PosAccount account = (PosAccount) posaccounts.next();
            //Toast.makeText(getApplicationContext(),account.getUsername() + " ",Toast.LENGTH_LONG).show();
            usernames.add(account.getUsername());
        }

        // Create an ArrayAdapter using the string array and a default spinner layout

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, usernames);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(),""+adapterView.getItemAtPosition(i), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void changePin() {

        String agentPhoneNumber = sharedPreferenceManager.getString(getApplicationContext(), "agentPhoneNumber","");
        String agentPassword = sharedPreferenceManager.getString(getApplicationContext(), "password","");

        final JSONObject userObject = new JSONObject();

        Spinner mySpinner = (Spinner) findViewById(R.id.dateSpinner);
        String oldPn = oldPin.getText().toString().trim();
        String newPn = newPin.getText().toString().trim();

        if(spinner.getCount() == 0){
            Alert.showWarning(getApplicationContext(),"Please Select Username");
            return;
        }else if(TextUtils.isEmpty(oldPn) || TextUtils.isEmpty(newPn)){
            Alert.showWarning(getApplicationContext(),"Please enter old and new pin");
            return;
        }

        String username = mySpinner.getSelectedItem().toString();

        try {

            userObject.put("username",username);
            userObject.put("pin",newPn);
            userObject.put("agentPhoneNumber",agentPhoneNumber);
            userObject.put("agentPassword",agentPassword);

            changePosPinController.doChangePosPin(ChangePosPinActivity.this,userObject);

            newPin.setText("");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        changePin();
    }
}
