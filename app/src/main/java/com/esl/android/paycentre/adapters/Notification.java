package com.esl.android.paycentre.adapters;

public class Notification {

    private int id;
    private String title;
    private String shortdesc;
    private String time;

    public Notification(int id, String title, String shortdesc, String time) {
        this.id = id;
        this.title = title;
        this.shortdesc = shortdesc;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public String getTime() {
        return time;
    }

}
