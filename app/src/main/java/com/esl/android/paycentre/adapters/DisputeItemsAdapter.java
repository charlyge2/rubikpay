package com.esl.android.paycentre.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.List;

public class DisputeItemsAdapter extends BaseAdapter {

    private List<String[]> disputeRecordsList;

    public DisputeItemsAdapter(List<String[]> disputeRecordsList){

        this.disputeRecordsList =  disputeRecordsList;
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return disputeRecordsList.size();
    }

    @Override
    public Object getItem(int index) {
        // TODO Auto-generated method stub
        return disputeRecordsList.get(index);
    }

    @Override
    public long getItemId(int index) {
        // TODO Auto-generated method stub
        return index;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.dispute_item, parent, false);
        }

        TextView statusTextView = view.findViewById(R.id.disputeStatus);
        TextView transactionIdTextView = view.findViewById(R.id.transactionId);
        TextView fullNameTextView = view.findViewById(R.id.customerName);
        TextView transactionStatusTextView = view.findViewById(R.id.transactionStatus);
        TextView amountTextView = view.findViewById(R.id.transactionAmount);
        TextView createdAtTextView = view.findViewById(R.id.loggedTime);

        String[] transactionArr = disputeRecordsList.get(index);

        statusTextView.setText(transactionArr[0]);
        transactionIdTextView.setText(transactionArr[1]);
        fullNameTextView.setText(transactionArr[2]);
        transactionStatusTextView.setText(transactionArr[4]);
        amountTextView.setText(transactionArr[5]);
        createdAtTextView.setText(transactionArr[6]);

        if (transactionArr[0].equalsIgnoreCase("open")){
            statusTextView.setTextColor(Color.BLUE);
        }else if (transactionArr[0].equalsIgnoreCase("resolved")){
            statusTextView.setTextColor(Color.GREEN);
        }

        return view;
    }

}

