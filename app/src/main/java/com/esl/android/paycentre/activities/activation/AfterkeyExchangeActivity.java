package com.esl.android.paycentre.activities.activation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.cardreader.network.HSMClient;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

public class AfterkeyExchangeActivity extends AppCompatActivity {

    public static Context appContext;

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;

    private ProgressDialog mProgressDialog;

    private Button proceedBtn;

    private ImageView keyExhangeImage;

    private TextView terminalId, keyExhangeStatus,agentName;

    private boolean doubleBackToExitPressedOnce = false;

    private SharedPreferenceManager sharedPreferenceManager;

    private PinpadFacade pinpadFacade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        //getSupportActionBar().hide(); // hide the title back
        setContentView(R.layout.activity_afterkey_exchange);

        appContext = getApplicationContext();

        pinpadFacade = new PinpadFacade(AfterkeyExchangeActivity.this);

        keyExhangeImage = (ImageView) findViewById(R.id.successExchange);
        terminalId = (TextView) findViewById(R.id.terminalId);
        agentName = (TextView) findViewById(R.id.agentName);
        keyExhangeStatus = (TextView) findViewById(R.id.setupStatusMessage);
        proceedBtn = (Button) findViewById(R.id.keyExchangeBtn);

        mProgressDialog = new ProgressDialog(AfterkeyExchangeActivity.this);
        mProgressDialog.setMessage("Please wait...\n Downloading Keys");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        Intent msgIntent = new Intent(getApplicationContext(), HSMClient.class);
        msgIntent.putExtra("receiver", new DownloadReceiver(new Handler()));
        startService(msgIntent);

    }

    public void afterKeyExchange(View view) {
        Intent adminPosActivity = new Intent(this, PosAdminLoginActivity.class);
        startActivity(adminPosActivity);
    }

    private class DownloadReceiver extends ResultReceiver {
        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            String activatedTerminal = sharedPreferenceManager.getString(getApplicationContext(), "terminalId","");
            String activatedAgent = sharedPreferenceManager.getString(getApplicationContext(), "agentName","");

            if (resultCode == HSMClient.UPDATE_PROGRESS) {
                int progress = resultData.getInt("progress");
                mProgressDialog.setProgress(progress);

                mProgressDialog.dismiss();

                // set image
                keyExhangeImage.setImageResource(R.drawable.ic_check);

                // set text and terminal id

                terminalId.setText("TERMINAL ID : " +activatedTerminal);
                agentName.setText(activatedAgent);
                keyExhangeStatus.setText("Setup Successful");
                sharedPreferenceManager.save(getApplicationContext(), "initialization", "true");

                Globals.isSetup = true; //used to check if initialization was done during setup

                pinpadFacade.doKeyLoad();


            } else if (resultCode == HSMClient.UPDATE_FAILED) {

                int progress = resultData.getInt("progress");
                mProgressDialog.setProgress(progress);

                mProgressDialog.dismiss();

                // set image
                keyExhangeImage.setImageResource(R.drawable.ic_error);
                agentName.setText(activatedAgent);
                // set text and terminal id
                terminalId.setText(activatedTerminal);
                sharedPreferenceManager.save(getApplicationContext(), "initialization", "false");
                keyExhangeStatus.setText("Setup Failed");


            }


            proceedBtn.setVisibility(View.VISIBLE); //To set visible
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
        }

        this.doubleBackToExitPressedOnce = true;

        Alert.showWarning(getApplicationContext(),"Click back again to exit");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


}
