package com.esl.android.paycentre.activities.activation;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.esl.android.paycentre.R;
import com.esl.android.paycentre.controllers.activationControllers.FetchPosAccountController;
import com.esl.android.paycentre.services.InternetConnectionService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

public class PosOperationsActivity extends AppCompatActivity implements View.OnClickListener{

    FetchPosAccountController fetchPosAccountController = new FetchPosAccountController();

    SharedPreferenceManager sharedPreferenceManager;

    private Button fetchpos,changepospin;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        //getSupportActionBar().hide(); // hide the title back
        setContentView(R.layout.activity_posoperations);

        AndroidNetworking.initialize(getApplicationContext());

        fetchpos = (Button)findViewById(R.id.fetchpos);

        changepospin = (Button)findViewById(R.id.changePosPin);

        fetchpos.setOnClickListener(this);
        changepospin.setOnClickListener(this);


    }

    public void createPosAccount(View view) {
        sharedPreferenceManager.save(getApplicationContext(), "createPosPage", "true");
        Intent createPosActivity = new Intent(this, CreatePosAccountActivity.class);
        startActivity(createPosActivity);
    }

    public void fetchPosAccount() {

        String agentPhoneNumber = sharedPreferenceManager.getString(getApplicationContext(), "agentPhoneNumber","");
        String agentPassword = sharedPreferenceManager.getString(getApplicationContext(), "password","");

        final JSONObject userObject = new JSONObject();
        if(!InternetConnectionService.doInternetConnectionCheck(getApplicationContext())){
            Alert.showWarning(getApplicationContext(),"Please Check your internet connection");
            return;
        }

        try {

            userObject.put("agentPhoneNumber",agentPhoneNumber);
            userObject.put("agentPassword",agentPassword);

            fetchPosAccountController.doFetchPosAccount(PosOperationsActivity.this,userObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void changePosPin() {

        //customToastActivity.createToast("Failed", getApplicationContext());\
        sharedPreferenceManager.save(getApplicationContext(), "changePosPinPage", "true");
        Intent intent = new Intent(this , ChangePosPinActivity.class);
        startActivity(intent);
    }

    public void deletePosAccount(View view) {
        Intent intent = new Intent(this , DeletePosAccountActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fetchpos:
                fetchPosAccount();
                break;
            case R.id.changePosPin:
                changePosPin();
                break;
            default:
                //action
                break;
        }
    }

    public void gotoAdminLogin(View view) {
        Intent intent = new Intent(this , PosAdminLoginActivity.class);
        startActivity(intent);
    }
}
