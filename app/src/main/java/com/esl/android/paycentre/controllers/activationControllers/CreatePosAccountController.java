package com.esl.android.paycentre.controllers.activationControllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.activation.PosLoginActivity;
import com.esl.android.paycentre.activities.activation.PosOperationsActivity;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.database.DBHelper;
import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.repo.PosAccountRepo;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

public class CreatePosAccountController {

    String username, agentPassword, firstname, lastname, pin, agentPhoneNumber;

    PosOperationsActivity posOperationsActivity;

    FetchPosAccountController fetchPosAccountController;

    SharedPreferenceManager sharedPreferenceManager;

    PosAccount posAccount = new PosAccount();
    PosAccountRepo posAccountRepo = new PosAccountRepo();

    DatabaseManager databaseManager;

    public void doCreatePosAccount(final Context context, JSONObject userObject, final View view) {
        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Creating Pos Account....");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        try {

            username = userObject.getString("username");
            agentPassword = userObject.getString("agentPassword");
            agentPhoneNumber = userObject.getString("agentPhoneNumber");
            firstname = userObject.getString("firstname");
            lastname = userObject.getString("lastname");
            pin = userObject.getString("pin");

            final JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("username", username);
                jsonObject.put("pin", pin);
                jsonObject.put("firstname", firstname);
                jsonObject.put("lastname", lastname);

                AndroidNetworking.post(MainActivity.createPosUrl)
                        .setPriority(Priority.IMMEDIATE)
                        .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                        .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                        .addHeaders("Content-type", "application/json")
                        .addJSONObjectBody(jsonObject)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                progress.dismiss();

                                try {

                                    String responseMessage = response.getString("responseMessage");
                                    String responseCode = response.getString("responseCode");

                                    if (responseCode.equals("00")) {

                                        JSONObject data = response.optJSONObject("data");

                                        Log.d("CREATE_POS_OBJECT",String.valueOf(data));

                                        doUpdatePosAccount(context, data);

                                    } else {
                                        Alert.showWarning(context, responseMessage);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError error) {
                                // handle errorfr
                                progress.dismiss();
                                try {
                                    Log.d("FAIL ", String.valueOf(error.getErrorBody()));
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                Alert.showWarning(context, "Could not create pos account");
                            }
                        });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void doUpdatePosAccount(final Context context, JSONObject userObject) {

        DBHelper dbHelper = new DBHelper(context);
        DatabaseManager.initializeInstance(dbHelper);

        try {

            String username = userObject.getString("username");
            String firstname = userObject.getString("firstname");
            String pin = userObject.getString("pin");
            String lastname = userObject.getString("lastname");
            String posaccountid = userObject.getString("id");

            posAccount.setUsername(username);
            posAccount.setLastname(lastname);
            posAccount.setFirstname(firstname);
            posAccount.setPassword(pin);
            posAccount.setPosAccountId(posaccountid);

            try {

                posAccountRepo.insert(posAccount);

                sharedPreferenceManager.save(context, "posLoginPage", "true");
                Intent intent = new Intent(context, PosLoginActivity.class);
                context.startActivity(intent);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
