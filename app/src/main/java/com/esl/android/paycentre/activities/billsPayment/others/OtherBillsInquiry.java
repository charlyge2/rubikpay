package com.esl.android.paycentre.activities.billsPayment.others;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.billsPayment.BillsActivity;
import com.esl.android.paycentre.activities.billsPayment.gotv.GotvNumberActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.constants.Constant;
import com.esl.android.paycentre.controllers.BillsController;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;

import org.json.JSONObject;

public class OtherBillsInquiry extends AppCompatActivity {

    TextView titleTextView;

    ImageView imageView;

    Button proceedBtn;

    public static EditText inquiry;

    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_bills_inquiry);

        type = getIntent().getStringExtra("billName");

        titleTextView = findViewById(R.id.titleTextView);
        imageView = findViewById(R.id.imageContainer);
        proceedBtn = findViewById(R.id.billBtn);
        inquiry = findViewById(R.id.inquiryNumber);
                titleTextView.setText(type);
        if(type.equals("EKEDC")){
            imageView.setImageResource(R.drawable.ekedc);

        }
        if(type.equals("IKEDC")){
            imageView.setImageResource(R.drawable.ikedc);
        }

        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(inquiry.getText().toString())){
                    Alert.showWarning(getApplicationContext(),"Please enter inquiry number");
                    return;
                }

                try {
                    JSONObject params = new JSONObject();

                    params.put("meterNumber", inquiry.getText().toString());
                    params.put("powerType", type);
                    BillsController.BillsInquiry(OtherBillsInquiry.this, params,MainActivity.electricityInquiryUrl);


                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        });

    }

    public void goBack(View view) {
        Intent intent = new Intent(getApplicationContext(), BillsActivity.class);
        startActivity(intent);
    }

    public void goHome(View view) {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }
}
