package com.esl.android.paycentre.activities.transactionHistory;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.interfaces.ResponseProcessorInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Navigator;

import org.json.JSONException;
import org.json.JSONObject;

public class TransactionHistoryDetailActivity extends BaseActivity implements ResponseProcessorInterface<String> {

    private String transactionId;
    private String cardNumber,customerName,customerNumber;

    private TextView amountTextView,transactionIdTextView,transactionTypeTextView,dateTextView,statusTextView,rrnTextView
            ,stanTextView,panTextView,terminalIdTextView,accountNameTextView,accountNumberTextView,bankTextView,
            phoneTextView,cardNumberTextView,customerNameTextView,customerNumberTextView,telcoTextView,meterNumberTextView,
            creditTokenTextView,creditTokenLabel,meterNumberLabel;

    private LinearLayout rrnLayout,stanLayout,panLayout,terminalIdLayout,accountNameLayout,accountNumberLayout,bankLayout,
            phoneLayout,cardNumberLayout,customerNameLayout,customerNumberLayout,telcoLayout,disputeLayout,
            meterNumberLayout,creditTokenLayout;

    private View rrnView,stanView,panView,terminalIdView,accountNameView,accountNumberView,bankView,phoneView,
            cardNumberView,customerNameView,customerNumberView,telcoView,meterNumberView,creditTokenView ;

    private String transactionResponseCode;
    private String amount;

    TransactionRepo transactionRepo;

    private TextView getTokenTv;
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history_details);
        getTokenTv = findViewById(R.id.disputeTextView);
        Intent intent = getIntent();

        transactionId = intent.getStringExtra("transactionId");
        String amount = intent.getStringExtra("amount");
        String transactionName = intent.getStringExtra("transactionName");
        String transactionStatus = intent.getStringExtra("transactionStatus");
        String transactionTime = intent.getStringExtra("transactionTime");
        transactionResponseCode = intent.getStringExtra("transactionStatusCode");



        transactionRepo = new TransactionRepo();
        jsonObject = transactionRepo.getTransaction(transactionId).getData();
        transactionIdTextView = findViewById(R.id.transactionIdTextView);
        amountTextView = findViewById(R.id.amountTextView);
        transactionTypeTextView = findViewById(R.id.transactionTypeTextView);
        dateTextView = findViewById(R.id.dateTextView);
        statusTextView = findViewById(R.id.statusTextView);
        rrnTextView = findViewById(R.id.rrnTextView);
        stanTextView = findViewById(R.id.stanTextView);
        panTextView = findViewById(R.id.panTextView);
        terminalIdTextView = findViewById(R.id.terminalIdTextView);
        accountNameTextView = findViewById(R.id.accountNameTextView);
        accountNumberTextView = findViewById(R.id.accountNumberTextView);
        bankTextView = findViewById(R.id.bankTextView);
        phoneTextView = findViewById(R.id.phoneTextView);
        cardNumberTextView = findViewById(R.id.cardNumberTextView);
        customerNameTextView = findViewById(R.id.customerNameTextView);
        customerNumberTextView = findViewById(R.id.customerNumberTextView);
        telcoTextView = findViewById(R.id.telcoTextView);
        meterNumberTextView = findViewById(R.id.meterNumberTextView);
        creditTokenTextView = findViewById(R.id.creditTokenTextView);
        meterNumberLabel = findViewById(R.id.meterNumberLabel);
        creditTokenLabel = findViewById(R.id.creditTokenLabel);
        rrnLayout = findViewById(R.id.rrnLayout);
        stanLayout = findViewById(R.id.stanLayout);
        panLayout = findViewById(R.id.panLayout);
        terminalIdLayout = findViewById(R.id.terminalIdLayout);
        accountNameLayout = findViewById(R.id.accountNameLayout);
        accountNumberLayout = findViewById(R.id.accountNumberLayout);
        bankLayout = findViewById(R.id.bankLayout);
        phoneLayout = findViewById(R.id.phoneLayout);
        cardNumberLayout = findViewById(R.id.cardNumberLayout);
        customerNameLayout = findViewById(R.id.customerNameLayout);
        customerNumberLayout = findViewById(R.id.customerNumberLayout);
        telcoLayout = findViewById(R.id.telcoLayout);
        disputeLayout = findViewById(R.id.disputeLayout);
        meterNumberLayout = findViewById(R.id.meterNumberLayout);
        creditTokenLayout = findViewById(R.id.creditTokenLayout);


        rrnView = findViewById(R.id.rrnView);
        stanView = findViewById(R.id.stanView);
        panView = findViewById(R.id.panView);
        terminalIdView = findViewById(R.id.terminalIdView);
        accountNameView = findViewById(R.id.accountNameView);
        accountNumberView = findViewById(R.id.accountNumberView);
        bankView = findViewById(R.id.bankView);
        phoneView = findViewById(R.id.phoneView);
        cardNumberView = findViewById(R.id.cardNumberView);
        customerNameView = findViewById(R.id.customerNameView);
        customerNumberView = findViewById(R.id.customerNumberView);
        telcoView = findViewById(R.id.telcoView);
        meterNumberView = findViewById(R.id.meterNumberView);
        creditTokenView = findViewById(R.id.creditTokenView);

        detailsVisibility("GONE", "all");

        transactionIdTextView.setText(transactionId);
        transactionTypeTextView.setText(transactionName.toUpperCase());
        amountTextView.setText(amount);
        dateTextView.setText(transactionTime);
        statusTextView.setText(transactionStatus);
        if(transactionName.equalsIgnoreCase("POWER")){
            makePanInvisible();
            getTokenTv.setVisibility(View.VISIBLE);
        }
        getTokenTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          //  transactionId = "TT20210330004140252";
                HttpService.requeryPower(v.getContext(), MainActivity.requeryPowerUrl + transactionId,TransactionHistoryDetailActivity.this);
            }
        });

        switch (transactionName) {
            case "Cash Out":
                addToView("cashout");
                break;

            case "Agent Transfer":
                makePanInvisible();
                break;

            case "Fund Purse":
                addToView("fundPurse");
                break;
            case "Funds Transfer":
                addToView("fundsTransfer");
                break;
            case "Dstv":
                addToView("dstv");
                break;
            case "Gotv":
                addToView("gotv");
                break;
            case "Startimes":
                addToView("startimes");
                break;
            case "Airtime":
                addToView("airtime");
                break;
            case "Ikedc":
                addToView("ikedc");
                break;
            case "Power":
                makePanInvisible();
                addToView("power");break;
        }
    }

    public void newTransaction(View view) {
        Navigator.moveToDashboard(TransactionHistoryDetailActivity.this);
    }


    public void addToView(String type) {
        switch (type) {
            case "cashout": String rrnCashout = transactionRepo.getTransaction(transactionId).getData().optString("rrn");
                String stanCashout = transactionRepo.getTransaction(transactionId).getData().optString("stan");
                String panCashout = transactionRepo.getTransaction(transactionId).getData().optString("pan");
                String terminalIdCashout = transactionRepo.getTransaction(transactionId).getData().optString("terminalId");
                String phoneN = transactionRepo.getTransaction(transactionId).getData().optString("phone");
                    detailsVisibility("VISIBLE", "cashFund");
                    rrnTextView.setText(rrnCashout);
                    stanTextView.setText(stanCashout);
                    panTextView.setText(panCashout);
                    phoneTextView.setText(phoneN);
                    terminalIdTextView.setText(terminalIdCashout);

                break;
            case "fundPurse":
                String rrn = transactionRepo.getTransaction(transactionId).getData().optString("rrn");
                String stan = transactionRepo.getTransaction(transactionId).getData().optString("stan");
                String pan = transactionRepo.getTransaction(transactionId).getData().optString("pan");
                String terminalId = transactionRepo.getTransaction(transactionId).getData().optString("terminalId");

                    detailsVisibility("VISIBLE", "cashFund");
                    rrnTextView.setText(rrn);
                    stanTextView.setText(stan);
                    panTextView.setText(pan);
                    terminalIdTextView.setText(terminalId);

                break;
            case "fundsTransfer":
                String accountName = transactionRepo.getTransaction(transactionId).getData().optString("accountName");
                String accountNumber = transactionRepo.getTransaction(transactionId).getData().optString("accountNumber");
                String bankName = transactionRepo.getTransaction(transactionId).getData().optString("bankName");
                String phone = transactionRepo.getTransaction(transactionId).getData().optString("phone");

                    detailsVisibility("VISIBLE", "fundsTransfer");
                    accountNameTextView.setText(accountName);
                    accountNumberTextView.setText(accountNumber);
                    bankTextView.setText(bankName);
                    phoneTextView.setText(phone);

                break;
            case "dstv":
            case "gotv":

                cardNumber = transactionRepo.getTransaction(transactionId).getData().optString("cardNumber");
                customerName = transactionRepo.getTransaction(transactionId).getData().optString("customerName");
                customerNumber = transactionRepo.getTransaction(transactionId).getData().optString("customerNumber");

                if (!TextUtils.isEmpty(cardNumber) && !TextUtils.isEmpty(customerName) && !TextUtils.isEmpty(customerNumber)) {
                    detailsVisibility("VISIBLE","dstvAndGotv");
                    cardNumberTextView.setText(cardNumber);
                    customerNameTextView.setText(customerName);
                    customerNumberTextView.setText(customerNumber);
                }

                break;
            case "startimes":
                String smartCardNumber = transactionRepo.getTransaction(transactionId).getData().optString("smartCardNumber");
                customerName = transactionRepo.getTransaction(transactionId).getData().optString("customerName");

                if (!TextUtils.isEmpty(smartCardNumber) && !TextUtils.isEmpty(customerName)) {
                    detailsVisibility("VISIBLE","startimes");
                    cardNumberTextView.setText(smartCardNumber);
                    customerNameTextView.setText(customerName);
                }
                break;
            case "airtime":
                String airtimePhone = transactionRepo.getTransaction(transactionId).getData().optString("phone");
                String airtimeNetwork = transactionRepo.getTransaction(transactionId).getData().optString("network");

                if (!TextUtils.isEmpty(airtimeNetwork) && !TextUtils.isEmpty(airtimePhone)) {
                    detailsVisibility("VISIBLE","airtime");
                    phoneTextView.setText(airtimePhone);
                    telcoTextView.setText(airtimeNetwork);
                }
                break;
            case "power":
                String creditToken = transactionRepo.getTransaction(transactionId).getData().optString("creditToken");
                String meterNumber = transactionRepo.getTransaction(transactionId).getData().optString("meterNumber");

                detailsVisibility("VISIBLE","power");

                meterNumberTextView.setText(meterNumber);
                creditTokenTextView.setText(creditToken);


                break;
            case "WAEC":
                String serialNumber = transactionRepo.getTransaction(transactionId).getData().optString("serialNumber");
                String pin = transactionRepo.getTransaction(transactionId).getData().optString("pin");


               detailsVisibility("VISIBLE", "WAEC");
                meterNumberLabel.setText("SerialNumber");
                creditTokenLabel.setText("WAEC Pin");
                meterNumberTextView.setText(serialNumber);
                creditTokenTextView.setText(pin);
                break;

        }
    }


    private void detailsVisibility(String visibility, String type) {
        switch (visibility) {
            case "VISIBLE":
                switch (type) {
                    case "cashout": makeCardDetailsVisible(); break;
                    case "cashFund":
                        makeCardDetailsVisible();
                        break;
                    case "fundsTransfer":
                        makeTransferDetailsVisible();
                        break;
                    case "dstvAndGotv":
                        makeDstvAndGotvDetailsVisible();
                        break;
                    case "startimes":
                        makeStartimesDetailsVisible();
                        break;
                    case "airtime":
                        makeAirtimeDetailsVisible();
                        break;
                    case "WAEC":
                        makeWaecDetailsVisible();
                        break;
                    case "power":
                        makeDetailsVisible(5);
                        break;
                }
                break;
            case "GONE":
                switch (type) {
                    case "all":
                        makeAllAdditionalDetailsInvisible();
                        break;
                }
        }
    }

    private void makeDetailsVisible(int params) {

        if (params == 5){
            rrnLayout.setVisibility(View.GONE);
            stanLayout.setVisibility(View.GONE);
            panLayout.setVisibility(View.GONE);
            terminalIdLayout.setVisibility(View.GONE);

            rrnView.setVisibility(View.GONE);
            stanView.setVisibility(View.GONE);
            panView.setVisibility(View.GONE);
            terminalIdView.setVisibility(View.GONE);
            meterNumberLayout.setVisibility(View.VISIBLE);
            creditTokenLayout.setVisibility(View.VISIBLE);

            meterNumberView.setVisibility(View.VISIBLE);
            creditTokenView.setVisibility(View.VISIBLE);
        }
    }

    private void makeAirtimeDetailsVisible() {
        rrnLayout.setVisibility(View.GONE);
        stanLayout.setVisibility(View.GONE);
        panLayout.setVisibility(View.GONE);
        terminalIdLayout.setVisibility(View.GONE);

        rrnView.setVisibility(View.GONE);
        stanView.setVisibility(View.GONE);
        panView.setVisibility(View.GONE);
        terminalIdView.setVisibility(View.GONE);
        phoneLayout.setVisibility(View.VISIBLE);
        telcoLayout.setVisibility(View.VISIBLE);

        phoneView.setVisibility(View.VISIBLE);
        telcoView.setVisibility(View.VISIBLE);

    }

    public void makeCardDetailsVisible(){

        rrnLayout.setVisibility(View.VISIBLE);
        stanLayout.setVisibility(View.VISIBLE);
        panLayout.setVisibility(View.VISIBLE);
        terminalIdLayout.setVisibility(View.VISIBLE);
        phoneLayout.setVisibility(View.VISIBLE);

        rrnView.setVisibility(View.VISIBLE);
        stanView.setVisibility(View.VISIBLE);
        panView.setVisibility(View.VISIBLE);
        terminalIdView.setVisibility(View.VISIBLE);

    }


    private void makePanInvisible(){
        rrnLayout.setVisibility(View.GONE);
        stanLayout.setVisibility(View.GONE);
        panLayout.setVisibility(View.GONE);
        terminalIdLayout.setVisibility(View.GONE);
        rrnView.setVisibility(View.GONE);
        stanView.setVisibility(View.GONE);
        panView.setVisibility(View.GONE);
        terminalIdView.setVisibility(View.GONE);
    }
    public void makeTransferDetailsVisible(){
        rrnLayout.setVisibility(View.GONE);
        stanLayout.setVisibility(View.GONE);
        panLayout.setVisibility(View.GONE);
        terminalIdLayout.setVisibility(View.GONE);

        rrnView.setVisibility(View.GONE);
        stanView.setVisibility(View.GONE);
        panView.setVisibility(View.GONE);
        terminalIdView.setVisibility(View.GONE);

        accountNameLayout.setVisibility(View.VISIBLE);
        accountNumberLayout.setVisibility(View.VISIBLE);
        bankLayout.setVisibility(View.VISIBLE);
        phoneLayout.setVisibility(View.VISIBLE);
        //disputeLayout.setVisibility(View.VISIBLE);

        accountNameView.setVisibility(View.VISIBLE);
        accountNumberView.setVisibility(View.VISIBLE);
        bankView.setVisibility(View.VISIBLE);
        phoneView.setVisibility(View.VISIBLE);

    }

    public void makeWaecDetailsVisible(){
        rrnLayout.setVisibility(View.GONE);
        stanLayout.setVisibility(View.GONE);
        panLayout.setVisibility(View.GONE);
        terminalIdLayout.setVisibility(View.GONE);

        rrnView.setVisibility(View.GONE);
        stanView.setVisibility(View.GONE);
        panView.setVisibility(View.GONE);
        terminalIdView.setVisibility(View.GONE);
        meterNumberLayout.setVisibility(View.VISIBLE);
        creditTokenLayout.setVisibility(View.VISIBLE);

        meterNumberView.setVisibility(View.VISIBLE);
        creditTokenView.setVisibility(View.VISIBLE);


    }

    public void makeDstvAndGotvDetailsVisible(){
        rrnLayout.setVisibility(View.GONE);
        stanLayout.setVisibility(View.GONE);
        panLayout.setVisibility(View.GONE);
        terminalIdLayout.setVisibility(View.GONE);

        rrnView.setVisibility(View.GONE);
        stanView.setVisibility(View.GONE);
        panView.setVisibility(View.GONE);
        terminalIdView.setVisibility(View.GONE);
        customerNameLayout.setVisibility(View.VISIBLE);
        customerNumberLayout.setVisibility(View.VISIBLE);
        cardNumberLayout.setVisibility(View.VISIBLE);

        customerNameView.setVisibility(View.VISIBLE);
        customerNumberView.setVisibility(View.VISIBLE);
        cardNumberView.setVisibility(View.VISIBLE);

    }

    public void makeStartimesDetailsVisible(){
        rrnLayout.setVisibility(View.GONE);
        stanLayout.setVisibility(View.GONE);
        panLayout.setVisibility(View.GONE);
        terminalIdLayout.setVisibility(View.GONE);

        rrnView.setVisibility(View.GONE);
        stanView.setVisibility(View.GONE);
        panView.setVisibility(View.GONE);
        terminalIdView.setVisibility(View.GONE);
        customerNameLayout.setVisibility(View.VISIBLE);
        cardNumberLayout.setVisibility(View.VISIBLE);

        customerNameView.setVisibility(View.VISIBLE);
        cardNumberView.setVisibility(View.VISIBLE);

    }

    public void makeAllAdditionalDetailsInvisible(){

        accountNameLayout.setVisibility(View.GONE);
        accountNumberLayout.setVisibility(View.GONE);
        bankLayout.setVisibility(View.GONE);
        phoneLayout.setVisibility(View.GONE);

        accountNameView.setVisibility(View.GONE);
        accountNumberView.setVisibility(View.GONE);
        bankView.setVisibility(View.GONE);
        phoneView.setVisibility(View.GONE);

        customerNameLayout.setVisibility(View.GONE);
        customerNumberLayout.setVisibility(View.GONE);
        cardNumberLayout.setVisibility(View.GONE);

        customerNameView.setVisibility(View.GONE);
        customerNumberView.setVisibility(View.GONE);
        cardNumberView.setVisibility(View.GONE);

        telcoLayout.setVisibility(View.GONE);
        telcoView.setVisibility(View.GONE);

        meterNumberLayout.setVisibility(View.GONE);
        meterNumberView.setVisibility(View.GONE);

        creditTokenLayout.setVisibility(View.GONE);
        creditTokenView.setVisibility(View.GONE);

    }


    public void goBack(View view) {
        onBackPressed();
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(TransactionHistoryDetailActivity.this);
    }


    @Override
    public void processResponse(String response) {
        creditTokenTextView.setText(response);
        if(TextUtils.isEmpty(response)){return;}

        Transaction transaction = new Transaction();
        TransactionRepo transactionRepo = new TransactionRepo();
        transaction.setTransactionId(transactionId);
        transaction.setResponseCode("00");
        transaction.setResponseMsg("Successful");
        transaction.setAmount(Float.parseFloat(amount.substring(1).replace(",","")));
        transaction.setTransactionType(Transaction.Type.getByName("POWER"));
        try {
            jsonObject.put("creditToken", response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        transaction.setData(jsonObject);
        transactionRepo.update(transaction);
    }
}
