package com.esl.android.paycentre.adapters;

public class TransactionPreviewList {

    private String key;

    private String value;

    private String page;

    public TransactionPreviewList(String key,String value, String page) {
        this.key = key;
        this.value = value;
        this.page = page;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setPage(String page){
        this.page = page;
    }

    public String getPage(){
        return page;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
