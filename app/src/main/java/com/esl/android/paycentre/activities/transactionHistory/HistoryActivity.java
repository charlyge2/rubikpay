package com.esl.android.paycentre.activities.transactionHistory;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.adapters.TransactionHistory;
import com.esl.android.paycentre.adapters.TransactionHistoryAdapter;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.interfaces.ResponseProcessorInterface;
import com.esl.android.paycentre.services.InternetConnectionService;
import com.esl.android.paycentre.services.SyncService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.TextCasing;
import com.esl.android.paycentre.utils.TransactionFormatter;
import com.twigsntwines.daterangepicker.DatePickerSpinner;
import com.twigsntwines.daterangepicker.DateRangePickedListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class HistoryActivity extends BaseActivity implements View.OnClickListener, ResponseProcessorInterface<String> {

    //a list to store all the NotificationModels
    private List<TransactionHistory> transactionHistoryList;
    //the recyclerview
    private RecyclerView recyclerView;

    private Button applyBtn;

    private Button resetBtn;

    private Spinner dateSpinner;

    private EditText transactionId;
    private ProgressDialog progressDialog;
    private TransactionRepo repo = new TransactionRepo();

    private AlertDialog alertDialog = null;

    private Iterator<Transaction> iterator;

    boolean clicked = false;

    private static DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    private  Date filterFrom;

    private Date filterTo;

    private TextView successVolumeTextView;

    private TextView failedVolumeTextView;

    private TextView successValueTextView;

    private TextView failedValueTextView;

    private int successVolume = 0;
    private double sumSuccessValue = 0.0;
    private int failedVolume = 0;
    private double sumFailedValue = 0.0;
    private TransactionHistoryAdapter adapter;

    private LinearLayoutManager linearLayoutManager;
    private boolean isloading = true;
    private int pageNo;
    private ProgressBar dialog;
    private boolean hasAllList = false;

    private View.OnClickListener onNotificationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Step 4 of 4: Finally call getTag() on the view.
            // This viewHolder will have all required values.
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            TransactionHistory history = transactionHistoryList.get(position);

            String amount = history.getTransactionAmount();
            String transactionId = history.getTransactionId();
            String transactionTime = history.getTransactionTime();
            String transactionName = history.getTransactionName();
            String transactionStatus = history.getTransactionStatus();
            Intent intent = new Intent(getApplicationContext(), TransactionHistoryDetailActivity.class);
            intent.putExtra("amount", amount);
            intent.putExtra("transactionId", transactionId);
            intent.putExtra("transactionName", transactionName);
            intent.putExtra("transactionStatus", transactionStatus);
            intent.putExtra("transactionTime", transactionTime);
            intent.putExtra("transactionStatusCode",history.getTransactionResponseCode());
            startActivity(intent);

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        successVolumeTextView = findViewById(R.id.successfulVolumeTextView);
        successValueTextView = findViewById(R.id.successfulValueTextView);
        failedVolumeTextView = findViewById(R.id.failedVolumeTextView);
        failedValueTextView = findViewById(R.id.failedValueTextView);
        dialog = findViewById(R.id.progress_circular);
        progressDialog = new ProgressDialog(this);

        if (SyncService.shouldDoManualSync()) {
            if (!InternetConnectionService.doInternetConnectionCheck(getApplicationContext())) {
                Alert.showWarning(getApplicationContext(), "Please Check your internet connection");
                return;
            }
            //syntransactioins
            progressDialog.setMessage("Syncing Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            SyncService.doManualSync(HistoryActivity.this, HistoryActivity.this);
        }

        //getting the recyclerview from xml
        recyclerView = findViewById(R.id.transactionHistoryRecycleView);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

        //initializing the transactionHistoryList
        transactionHistoryList = new ArrayList<>();

        iterator = repo.getTransactions();

        while (iterator.hasNext()) {

            Transaction transaction = iterator.next();

            Date date = transaction.getDate();
            String dateTime = TransactionFormatter.formatDate(date) + " " + TransactionFormatter.formatTime(date);
            String formattedAmt = "₦" + formatter.format(transaction.getAmount());

            transactionHistoryList.add(new TransactionHistory(
                    transaction.getTransactionId(),
                    TextCasing.toTitleCase(transaction.getTransactionType().getName()),
                    TextCasing.toTitleCase(transaction.getResponseMsg()),
                    formattedAmt,
                    dateTime,
                    transaction.getResponseCode()));


            if(transaction.getResponseCode().equals("00")){
                sumSuccessValue+=transaction.getAmount();
                successVolume++;
            }else{
                sumFailedValue+=transaction.getAmount();
                failedVolume++;
            }

        }



        successVolumeTextView.setText(String.valueOf(successVolume));
        successValueTextView.setText(String.format("₦%s", formatter.format(sumSuccessValue)));
        failedVolumeTextView.setText(String.valueOf(failedVolume));
        failedValueTextView.setText(String.format("₦%s", formatter.format(sumFailedValue)));

        adapter = new TransactionHistoryAdapter(this, transactionHistoryList);
        TransactionHistoryAdapter.setOnItemClickListener(onNotificationClickListener);
        recyclerView.setAdapter(adapter);

        Log.d("N0_FILTER_RESULT","Success Volume "+successVolume + "\nSuccess Value "+sumSuccessValue
                +"\nFailed Volume "+failedVolume+ "\nFailed Value "+sumFailedValue);

    }

    public void filterTransactions(View view) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Set icon value.
        //builder.setIcon(R.drawable.ic_tag_search_filter);
        // Set title value.
        //builder.setTitle("Transactions Filter");

        // Get custom login form view.
        final View loginFormView = getLayoutInflater().inflate(R.layout.filterlayout, null);
        // Set above view in alert dialog.
        builder.setView(loginFormView);

        builder.setCancelable(true);
        alertDialog = builder.create();
        alertDialog.show();

        applyBtn = alertDialog.findViewById(R.id.applyBtn);
        resetBtn = alertDialog.findViewById(R.id.resetBtn);

        final DatePickerSpinner spinner = alertDialog.findViewById(R.id.datePickerSpinner); // Find Datepickerspinner declared in xml file
        spinner.setDateRangePickedListener(new DateRangePickedListener() {
            @Override
            public void OnDateRangePicked(Calendar fromDate, Calendar toDate) {
                filterFrom = fromDate.getTime();
                filterTo = toDate.getTime();

                if(filterFrom.after(filterTo)) {
                    toDate.add(Calendar.DAY_OF_WEEK, -(toDate.get(Calendar.DAY_OF_WEEK) - 1));
                    toDate.set(Calendar.HOUR_OF_DAY, 0);
                    toDate.set(Calendar.MINUTE, 0);
                    toDate.set(Calendar.SECOND, 0);
                    toDate.set(Calendar.MILLISECOND, 0);
                    filterFrom = toDate.getTime();
                }

            }
        });

        applyBtn.setOnClickListener(this);
        resetBtn.setOnClickListener(this);

    }

    private void showFilterResult(Date fromDate, Date toDate) {

        transactionHistoryList = new ArrayList<>();

        iterator = repo.getTransactions(fromDate, toDate);

        int successVolumeFilter = 0;
        double sumSuccessValueFilter = 0.0;
        int failedVolumeFilter = 0;
        double sumFailedValueFilter = 0.0;

        while (iterator.hasNext()) {

            Transaction transaction = iterator.next();

            Date date = transaction.getDate();
            String dateTime = TransactionFormatter.formatDate(date) + " " + TransactionFormatter.formatTime(date);
            String formattedAmt = "₦" + formatter.format(transaction.getAmount());

            transactionHistoryList.add(new TransactionHistory(
                    transaction.getTransactionId(),
                    transaction.getTransactionType().getName(),
                    transaction.getResponseMsg(),
                    formattedAmt,
                    dateTime,
                    transaction.getResponseCode()));

            if(transaction.getResponseCode().equals("00")){
                sumSuccessValueFilter+=transaction.getAmount();
                successVolumeFilter++;
            }else{
                sumFailedValueFilter+=transaction.getAmount();
                failedVolumeFilter++;
            }
        }

        successVolumeTextView.setText(String.valueOf(successVolumeFilter));
        successValueTextView.setText(String.format("₦%s", formatter.format(sumSuccessValueFilter)));
        failedVolumeTextView.setText(String.valueOf(failedVolumeFilter));
        failedValueTextView.setText(String.format("₦%s", formatter.format(sumFailedValueFilter)));


        TransactionHistoryAdapter adapter = new TransactionHistoryAdapter(getApplicationContext(), transactionHistoryList);
        TransactionHistoryAdapter.setOnItemClickListener(onNotificationClickListener);
        recyclerView.setAdapter(adapter);
        Log.d("FILTER_RESULT","Success Volume "+successVolumeFilter + "\nSuccess Value "+sumSuccessValueFilter
                +"\nFailed Volume "+failedVolumeFilter+ "\nFailed Value "+sumFailedValueFilter);

        //alertDialog.hide();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.applyBtn:
                showFilterResult(filterFrom, filterTo);
                alertDialog.hide();
                break;
            case R.id.resetBtn:
                filterFrom = filterTo = null;
                showFilterResult(filterFrom, filterTo);
                alertDialog.hide();
                break;
            default:
                Alert.showWarning(getApplicationContext(), "Hello");
                break;
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (alertDialog!=null && alertDialog.isShowing() ){
            showFilterResult(filterFrom, filterTo);
            alertDialog.dismiss();
        }
    }


    @Override
    public void processResponse(String response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(HistoryActivity.this, "Done", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    }

                }, 12000);
            }
        });

    }
}

