package com.esl.android.paycentre.activities.fundsTransfer;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SaveTransferTransactions;

public class FundsTransferResultActivity extends BaseActivity {

    private ImageView responseImage;

    private TextView responseText;

    private Button homeBtn;

    private String responseCode;

    private String responseMessage;

    private String transactionId;

    private String amount;

    private String convenienceFee;

    private String posAccount;

    private String total;

    private String appTime;

    private String accountName;

    private String accountNumber;

    private String bankName;

    private String phone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funds_transfer_result);

        responseImage = findViewById(R.id.responseImage);
        responseText = findViewById(R.id.transactionStatus);
        homeBtn = findViewById(R.id.homeBten);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),DashboardActivity.class);
                startActivity(intent);
            }
        });

        responseCode = getIntent().getStringExtra("responseCode");
        responseMessage = getIntent().getStringExtra("responseMessage");
        amount = getIntent().getStringExtra("amount");
        transactionId = getIntent().getStringExtra("transactionId");
        convenienceFee = getIntent().getStringExtra("convenienceFee");
        posAccount = getIntent().getStringExtra("posAccount");
        total = getIntent().getStringExtra("total");
        appTime = getIntent().getStringExtra("appTime");
        accountName =  getIntent().getStringExtra("accountName");
        accountNumber = getIntent().getStringExtra("accountNumber");
        bankName = getIntent().getStringExtra("bankName");
        phone = getIntent().getStringExtra("phone");

        if(responseCode.equals("00")){

            responseImage.setImageResource(R.drawable.ic_check);
            responseText.setText(responseMessage);
        }else {
            responseImage.setImageResource(R.drawable.ic_error);
            responseText.setText(responseMessage);
        }

        long check = SaveTransferTransactions.updateTransferTransactions(FundsTransferResultActivity.this, accountName, accountNumber, bankName, phone, Globals.vat, posAccount, transactionId, responseCode, responseMessage, amount, convenienceFee, Globals.fundsTransferType.toUpperCase());

        if (check < 0) {
            Alert.showWarning(getApplicationContext(), "Funds transfer transaction could not be saved");
        }

    }

    @Override
    public void onBackPressed() {

    }
}
