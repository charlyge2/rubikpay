package com.esl.android.paycentre.cardreader.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TwoLineListItem;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.activation.WalkthroughActivity;
import com.esl.android.paycentre.cardreader.controllers.PinpadManager;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;
import com.esl.android.paycentre.utils.Alert;

import java.io.IOException;
import java.util.ArrayList;

public class DeviceActivity extends Activity {

    private static int hasConnected = 0;
    private PinpadManager mPinpadManager;
    private BluetoothDeviceAdapter mListAdapter;
    private ListView mListView;
    private Handler deviceActivityHandler;
    private String btAddress = "";
    private PinpadFacade pinpadFacade;

    private Button pairButton;
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<BluetoothDevice> mDeviceList = new ArrayList<BluetoothDevice>();

    private int downloaderCounter = 0;

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;

    private ProgressDialog dialog;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.screen_device);

        pinpadFacade = new PinpadFacade(this);

        mProgressDialog = new ProgressDialog(DeviceActivity.this);

        setResult(RESULT_CANCELED);

        mPinpadManager = PinpadManager.getInstance(this);
        deviceActivityHandler = new Handler();
        mPinpadManager
                .setOnConnectionEstablishedListener(new PinpadManager.OnConnectionEstablishedListener() {
                    @Override
                    public void OnConnectionEstablished() {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                // set this to check if connected
                                Globals.isPinpadConnected = true;
                                showSuccessToast(getResources().getString(R.string.msg_pinpad_connected));

                                setResult(RESULT_OK);
                                finish();
                            }
                        });
                    }
                });

        MiscUtils.initContext(getApplicationContext());
        String pinpadMacAddress = MiscUtils.getFromSharedPreferences(
                ReferenceList.preference, ReferenceList.pinpadMACAddress, "");

        if (pinpadMacAddress.equals("") || pinpadMacAddress == null) {

            Globals.autoConnect = false;
            showConnectPinpadDialog();

        } else {

            Globals.autoConnect = true;
            btAddress = pinpadMacAddress;
            connectToDevice(btAddress);
        }

        pairButton = findViewById(R.id.pairButton);
        pairButton.setOnClickListener(v -> {
//            Intent bluetoothPicker = new Intent("android.bluetooth.devicepicker.action.LAUNCH");
//            startActivity(bluetoothPicker);

            goToBluetoothSettings();


        });
//        pairButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//
//                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//
//                mBluetoothAdapter.startDiscovery();
//
//                mProgressDialog.setMessage("Scanning...");
//                mProgressDialog.setCancelable(false);
//                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//
//                        mBluetoothAdapter.cancelDiscovery();
//                    }
//                });
//
//
//            }
//        });

//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
//
//        } else {
//
//
//            IntentFilter filter = new IntentFilter();
//
//            filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
//            filter.addAction(BluetoothDevice.ACTION_FOUND);
//            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
//            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//
//            registerReceiver(mReceiver, filter);
//        }

    }

    private void goToBluetoothSettings() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeviceActivity.this);
        alertDialogBuilder.setTitle("Pinpad Selection");
        alertDialogBuilder
                .setMessage("This will take you to the bluetooth settings\nPress back when you are done pairing the device")
                .setCancelable(true)
                .setPositiveButton("YES, PROCEED",
                        (dialog, id) -> {
                            Intent intentOpenBluetoothSettings = new Intent();
                            intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                            startActivity(intentOpenBluetoothSettings);
                            hasConnected = 1;
                        })
                .setNegativeButton("NO", (dialog, id) -> dialog.cancel());
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

//    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//
//            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
//
//                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
//
//            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
//
//                mDeviceList = new ArrayList<BluetoothDevice>();
//
//                mProgressDialog.show();
//
//            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
//
//                mProgressDialog.dismiss();
//
//                Intent newIntent = new Intent(DeviceActivity.this, DevicePairActivity.class);
//
//                newIntent.putParcelableArrayListExtra("device.list", mDeviceList);
//
//                startActivity(newIntent);
//
//                finish();
//
//            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
//
//                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//
//                mDeviceList.add(device);
//
//                //Toast.makeText(getApplicationContext(), "Scanned: " + device.getName(), Toast.LENGTH_SHORT).show();
//            }
//        }
//    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        pinpadFacade.onActivityResult(requestCode, resultCode, data);
        if (hasConnected == 1){
            mListAdapter.notifyDataSetChanged();
        }
    }

//    @Override
//    public void onPause() {
//        if (mBluetoothAdapter != null) {
//            if (mBluetoothAdapter.isDiscovering()) {
//                mBluetoothAdapter.cancelDiscovery();
//            }
//        }
//
//        super.onPause();
//
//        if (dialog != null){
//
//            dialog.dismiss();
//        }
//    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeviceActivity.this);
//        alertDialogBuilder.setTitle("Pinpad Selection");
//        alertDialogBuilder
//                .setMessage("Select Pinpad from list")
//                .setCancelable(false)
//                .setPositiveButton("OK",
//                        (dialog, id) -> {
//                            dialog.cancel();
//                            mListAdapter.notifyDataSetChanged();
//                        });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        alertDialog.show();
////        updateDeviceList();
//    }

    @Override
    public void onDestroy() {

        super.onDestroy();
//        unregisterReceiver(mReceiver);
//
//        super.onDestroy();
//
//
//        if (dialog != null){
//
//            dialog.dismiss();
//        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//
//        switch (requestCode) {
//            case 1: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    //permission granted!
//
//                    IntentFilter filter = new IntentFilter();
//
//                    filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
//                    filter.addAction(BluetoothDevice.ACTION_FOUND);
//                    filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
//                    filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//
//                    registerReceiver(mReceiver, filter);
//                }
//                return;
//            }
//        }
//    }


    private void showConnectPinpadDialog() {

        mListAdapter = new BluetoothDeviceAdapter(this);

        mListView = findViewById(R.id.deviceListView);

        mListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parentView, View view,
                                    final int position, long id) {

                String pinpadName = mListAdapter.getItem(position).name;

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeviceActivity.this);
                alertDialogBuilder.setTitle("Confirm Pinpad Selection");
                alertDialogBuilder
                        .setMessage("Activate pinpad with serial number " + pinpadName + " on your account?")
                        .setCancelable(false)
                        .setPositiveButton("YES, PROCEED",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        Globals.autoConnect = false;
                                        btAddress = mListAdapter.getItem(position).addr;
                                        connectToDevice(btAddress);
                                    }
                                })
                        .setNegativeButton("NO, CHOOSE AGAIN", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });

        mListView.setAdapter(mListAdapter);

//        findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });

        updateDeviceList();
    }

    // Show toast notification, running in UI thread.
    private void showSuccessToast(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Alert.showSuccess(getApplicationContext(), getResources().getString(R.string.msg_pinpad_connected));
            }
        });
    }

    private void showFailedToast(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Alert.showFailed(getApplicationContext(), getResources().getString(R.string.msg_failed_to_connect_to_pinpad));
            }
        });
    }

    // Populate list with paired bluetooth devices.
    private void updateDeviceList() {
        BluetoothAdapter bthAdapter = BluetoothAdapter.getDefaultAdapter();

        String pinpadBluetoothClassName = "1f00";

        if (bthAdapter != null) {
            for (BluetoothDevice device : bthAdapter.getBondedDevices()) {

                BluetootDevicePair pair = new BluetootDevicePair();
                pair.name = device.getName();
                pair.addr = device.getAddress();

                if (pinpadBluetoothClassName.equals(device.getBluetoothClass().toString())) {

                    mListAdapter.add(pair);

                }

            }
        }

        mListAdapter.notifyDataSetChanged();
    }

    // Connect to specific bluetooth device.
    private void connectToDevice(final String btAddress) {
        String dialogMessage;
        // Construct a progress dialog to prevent user from actions until
        // connection is finished.
        dialog = new ProgressDialog(DeviceActivity.this);

        if (Globals.autoConnect) {

            dialogMessage = getString(R.string.msg_auto_connect);

        } else {

            dialogMessage = getString(R.string.msg_please_wait);
        }

        dialog.setMessage(dialogMessage);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                return true;
            }
        });
        dialog.show();

        // Force connection to be execute in separate thread.
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    mPinpadManager.connect(btAddress, getApplicationContext());


                } catch (IOException e) {

                    e.printStackTrace();

                    showFailedToast(getResources().getString(R.string.msg_failed_to_connect_to_pinpad) + ": " + e.getMessage());

                    if (Globals.autoConnect) { // show dialog after auto
                        // connecting failed
                        deviceActivityHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                Globals.autoConnect = false;
                                showConnectPinpadDialog();
                            }
                        });

                    }
                } finally {

                    dialog.dismiss();


                }
            }
        });
        t.start();

    }

    private class BluetootDevicePair {
        String name;
        String addr;
    }

    private class BluetoothDeviceAdapter extends
            ArrayAdapter<BluetootDevicePair> {
        public BluetoothDeviceAdapter(Context context) {

            super(context, android.R.layout.simple_list_item_2);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TwoLineListItem v = (TwoLineListItem) convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = (TwoLineListItem) inflater.inflate(
                        android.R.layout.simple_list_item_2, null);
            }

            BluetootDevicePair device = getItem(position);
            v.getText1().setText(device.name);
            // v.getText2().setText(device.addr);

            return v;
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), WalkthroughActivity.class);
        startActivity(intent);

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        showConnectPinpadDialog();
//    }
//
//    @Override
//    protected void onRestart() {
//        super.onRestart();
//
//        showConnectPinpadDialog();
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        showConnectPinpadDialog();
//    }
}
