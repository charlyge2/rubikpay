package com.esl.android.paycentre.interfaces;

public interface AgentTransferVerificationInterface {
    public void sendAgentAccountDetails(String accountName,String accountNumber,String bank);
}
