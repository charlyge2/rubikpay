package com.esl.android.paycentre.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AlertDialog;
import androidx.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Method;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.activation.PosAdminLoginActivity;
import com.esl.android.paycentre.activities.activation.WalkthroughActivity;
import com.esl.android.paycentre.activities.agentTransfer.AgentTransferResultActivity;
import com.esl.android.paycentre.activities.airtimeVending.AirtimeResultActivity;
import com.esl.android.paycentre.activities.billsPayment.dstv.DstvAddonsActivity;
import com.esl.android.paycentre.activities.billsPayment.dstv.DstvResultActivity;
import com.esl.android.paycentre.activities.billsPayment.gotv.GotvResultActivity;
import com.esl.android.paycentre.activities.billsPayment.startimes.StartimesResultActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.activities.fundsTransfer.FundsTransferAmountActivity;
import com.esl.android.paycentre.activities.fundsTransfer.FundsTransferResultActivity;
import com.esl.android.paycentre.activities.loans.ExistingLoanActivity;
import com.esl.android.paycentre.activities.loans.NewLoanActivity;
import com.esl.android.paycentre.adapters.BillList;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.database.AndroidDatabaseManager;
import com.esl.android.paycentre.interfaces.AgentTransferVerificationInterface;
import com.esl.android.paycentre.interfaces.BankListInterface;
import com.esl.android.paycentre.interfaces.ConvenienceFeeInterface;
import com.esl.android.paycentre.interfaces.HttpResponseInterface;
import com.esl.android.paycentre.interfaces.LoanEligibilityInterface;
import com.esl.android.paycentre.interfaces.NameLookupInterface;
import com.esl.android.paycentre.interfaces.PlanInterface;
import com.esl.android.paycentre.interfaces.ResponseProcessorInterface;
import com.esl.android.paycentre.interfaces.StartimesNameLookupInterface;
import com.esl.android.paycentre.interfaces.TemplateInterface;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpService {

    private static DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    private static SharedPreferenceManager sharedPreferenceManager;

    private static ConvenienceFeeInterface convenienceFeeInterface;

    private static TemplateInterface templateInterface;

    private static NameLookupInterface nameLookupInterface;

    private static StartimesNameLookupInterface startimesNameLookupInterface;

    private static HttpResponseInterface httpResponseInterface;

    private static PlanInterface planInterface;

    private static BankListInterface bankListInterface;

    private static LoanEligibilityInterface loanEligibilityInterface;

    private static AgentTransferVerificationInterface agentTransferVerificationInterface;

    private static String result = "progress";

    public static String name;

    private static String convenienceFeeAmount;

    private static String convenienceFeeTransactionType;

    private static String activatedTerminal;

    public static ProgressDialog progressDialog;

    public static String TAG = "REPONSE";

    private static final JSONObject jsonObject = new JSONObject();

    public HttpService(TemplateInterface templateInterface) {
        this.templateInterface = templateInterface;
    }

    public static void activateUser(final Context context, String activationCode) {

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Please wait, Activating your account...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        final JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("activationCode", activationCode);
            jsonObject.put("phoneUUID", MainActivity.getUuid(context));

            Log.d("PARAMS", String.valueOf(jsonObject));

            AndroidNetworking.post(MainActivity.activationUrl)
                    .setPriority(Priority.IMMEDIATE)
                    .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                    .addHeaders("Content-type", "application/json")
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            progress.dismiss();

                            ReferenceList.config = context.getSharedPreferences(
                                    ReferenceList.preference, 0);

                            SharedPreferences.Editor editor = ReferenceList.config.edit();
                            Log.d("ACTIVATION", String.valueOf(response));

                            try {

                                String responseMessage = response.getString("responseMessage");
                                String responseCode = response.getString("responseCode");

                                if (responseCode.equals("00")) {

                                    JSONObject dataObject = response.getJSONObject("data");

                                    editor.putString(ReferenceList.terminalId, dataObject.getString("terminalId"));
                                    editor.putString(ReferenceList.activated, "true");

                                    editor.putString(ReferenceList.hsmIp, "178.62.57.125");
                                    editor.putInt(ReferenceList.hsmPort, 55528);

                                    editor.putString(ReferenceList.nibssIp, Globals.ipAddress);
                                    editor.putInt(ReferenceList.nibssPort, Globals.port);

                                    editor.apply();  //ends here

                                    sharedPreferenceManager.save(context, "terminalId", dataObject.getString("terminalId"));
                                    sharedPreferenceManager.save(context, "businessName", dataObject.getString("businessName"));
                                    sharedPreferenceManager.save(context, "agentName", dataObject.getString("agentName"));
                                    sharedPreferenceManager.save(context, "bankId", dataObject.getString("bankId"));
                                    sharedPreferenceManager.save(context, "merchantId", dataObject.getString("merchantId"));
                                    sharedPreferenceManager.save(context, "username", dataObject.getString("username"));
                                    sharedPreferenceManager.save(context, "merchantPhoneNumber", dataObject.getString("merchantPhoneNumber"));
                                    sharedPreferenceManager.save(context, "email", dataObject.getString("email"));
                                    sharedPreferenceManager.save(context, "memberId", dataObject.getString("memberId"));
                                    sharedPreferenceManager.save(context, "agentPhoneNumber", dataObject.getString("merchantPhoneNumber"));
                                    SharedPreferenceManager.save(context, "stateCode", dataObject.getString("stateCode"));
                                    Intent myIntent = new Intent(context, WalkthroughActivity.class);
                                    context.startActivity(myIntent);


                                } else {

                                    sharedPreferenceManager.save(context, "activated", "false");
                                    editor.putString(ReferenceList.activated, "false");
                                    Alert.showFailed(context, responseMessage);

                                }
                            } catch (JSONException e) {

                                e.printStackTrace();

                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle errorfr
                            progress.dismiss();
                            try {
                                Log.d("ACTIVATION_ERROR",error.getErrorBody());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            sharedPreferenceManager.save(context, "activated", "false");
                            Alert.showFailed(context, "Unable to Activate please try again");

                        }
                    });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void getAccountName(final Context context, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Fetching Account Name......");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        AndroidNetworking.get(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    //Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
                    @Override
                    public void onResponse(JSONObject response) {

                        progress.dismiss();

                        try {

                            String responseMessage = response.getString("responseMessage");
                            String responseCode = response.getString("responseCode");

                            if (responseCode.equals("00")) {

                                name = response.getString("accountName");

                                FundsTransferAmountActivity.firstLine.setVisibility(View.VISIBLE);
                                FundsTransferAmountActivity.accountName.setVisibility(View.VISIBLE);
                                FundsTransferAmountActivity.accountName.setText("NAME : " + name);

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Account Name");
                                alertDialogBuilder
                                        .setMessage("\n" + name)
                                        .setCancelable(false)
                                        .setPositiveButton("CONFIRM",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                        ;
                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } else {

                                FundsTransferAmountActivity.accountName.setText("");

                                if(FundsTransferAmountActivity.firstLine.getVisibility() == View.VISIBLE &&  FundsTransferAmountActivity.accountName.getVisibility() == View.VISIBLE){
                                    FundsTransferAmountActivity.firstLine.setVisibility(View.GONE);
                                    FundsTransferAmountActivity.accountName.setVisibility(View.GONE);
                                }

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Account Details");
                                alertDialogBuilder
                                        .setMessage(responseMessage)
                                        .setCancelable(false)
                                        .setPositiveButton("CONFIRM",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {

                        progress.dismiss();

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle("Account Name");
                        alertDialogBuilder
                                .setMessage("Unable to fetch Account Name")
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                ;
                                            }
                                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                        FundsTransferAmountActivity.accountName.setText("");

                        if(FundsTransferAmountActivity.firstLine.getVisibility() == View.VISIBLE && FundsTransferAmountActivity.accountName.getVisibility() == View.VISIBLE){
                            FundsTransferAmountActivity.firstLine.setVisibility(View.GONE);
                            FundsTransferAmountActivity.accountName.setVisibility(View.GONE);
                        }

                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }


                    }
                });


    }

    public static void fetchConvenienceFee(final Context context, JSONObject params) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Fetching Convenience Fee....");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        try {

            convenienceFeeAmount = params.getString("amount");
            convenienceFeeTransactionType = params.getString("transactionType");

            if (params.has("activatedTerminal")) {
                jsonObject.put("terminalId", params.getString("activatedTerminal"));
            }

            jsonObject.put("amount", convenienceFeeAmount);
            jsonObject.put("transactionType", convenienceFeeTransactionType);
            jsonObject.put("appVersion", MainActivity.getAppVersion(context));

            AndroidNetworking.post(MainActivity.convenienceFeeUrl)
                    .setPriority(Priority.IMMEDIATE)
                    .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                    .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                    .addHeaders("Content-type", "application/json")
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //progress.dismiss();

                            System.out.println("CFEE RESPONSE" + response.toString());

                            try {
                                String responseCode = response.getString("responseCode");
                                String responseMessage = response.getString("responseMessage");


                                if (responseCode.equals("00")) {
                                    JSONObject data = response.getJSONObject("data");
                                    String convenienceFee = data.optString("convenienceFee");
                                    String stampDuty = data.optString("stampDuty");
                                    boolean showStampDuty = data.optBoolean("showStampDuty");
                                    String vat = data.optString("vat");
                                    boolean showVat = data.optBoolean("showVat");
                                    convenienceFeeInterface = (ConvenienceFeeInterface) context;
                                    convenienceFeeInterface.sendConvenienceFee(convenienceFee, stampDuty, showStampDuty,vat,showVat);
//                                   convenienceFeeInterface.sendConvenienceFee(convenienceFee,"50",true);


                                } else {

//                                    String stampDuty = response.optString("stampDuty");
//                                    boolean showStampDuty = response.optBoolean("showStampDuty");
//                                    //Alert.showFailed(context, "from httpservice " + convenienceFee);
//                                    convenienceFeeInterface = (ConvenienceFeeInterface) context;
//                                    convenienceFeeInterface.sendConvenienceFee("100", stampDuty, showStampDuty);

                                    progressDialog.dismiss();
                                    Alert.showFailed(context, responseMessage);
                                }
                            } catch (Exception e) {
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle errorfr
                            progressDialog.dismiss();
                            Alert.showFailed(context, "Could not fetch convenience fee");
                            try {
                                if (error.getErrorCode() != 0) {
                                    if (error.getErrorCode() == 401) {
                                        refreshToken(context);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void doFetchPurseBalance(final Context context) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Fetching Purse Balance....");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        AndroidNetworking.get(MainActivity.purseBalanceUrl)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        progress.dismiss();
                        try {
                            String responseMessage = response.getString("responseMessage");
                            String responseCode = response.getString("responseCode");

                            if (responseCode.equals("00")) {

                                final String balance = response.getString("balance");

                                double formattedBalance = Double.parseDouble(balance);

                                final String formattedString = "₦" + formatter.format(formattedBalance);

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Purse Balance");
                                alertDialogBuilder
                                        .setMessage("\n" + formattedString)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                                Globals.purseBalance =formattedString;
                                DashboardActivity.setBalance(formattedString);
                                DashboardActivity.currentBalance = formatter.format(formattedBalance);

                            } else {
                                refreshToken(context);
//                                Alert.showFailed(context, responseMessage);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        Alert.showFailed(context, "Could not fetch purse balance");
                        try {
                           // Log.d("PURSE_BALANCE",error.getErrorBody());
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    public static void doChangePassword(final Context context, JSONObject params) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Processing....");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();


        AndroidNetworking.patch(MainActivity.changePasswordUrl)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progress.dismiss();
                        try {

                            String responseCode = response.getString("responseCode");
                            String responseMessage = response.getString("responseMessage");

                            if (responseCode.equals("00")) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Status");
                                alertDialogBuilder
                                        .setMessage("Your Password has been changed successfully")
                                        .setCancelable(false)
                                        .setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        Intent intent = new Intent(context, PosAdminLoginActivity.class);
                                                        context.startActivity(intent);

                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } else {
                                Alert.showWarning(context, responseMessage);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        Alert.showFailed(context, "Could not change admin password");
                    }
                });
    }

    public static void checkLoanEligibility(final Context context) {


        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Checking Eligibility......");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        AndroidNetworking.get(MainActivity.loanEligibilityUrl)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    //Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
                    @Override
                    public void onResponse(JSONObject response) {
                        progress.dismiss();
                        try {

                            String responseMessage = response.getString("responseMessage");
                            String responseCode = response.getString("responseCode");

                            Intent intent;

                            if (responseCode.equals("00")) {

                                JSONObject data = response.getJSONObject("data");

                                boolean documentUploaded = data.getBoolean("documentUploaded");

                                if (!documentUploaded) {
                                    intent = new Intent(context, NewLoanActivity.class);
                                    intent.putExtra("interestRate", data.getString("interestRate"));
                                    intent.putExtra("eligibleAmount", data.getString("eligibleAmount"));
                                    intent.putExtra("payBackDays", data.getString("payBackDays"));
                                    context.startActivity(intent);
                                } else {
                                    intent = new Intent(context, ExistingLoanActivity.class);
                                    intent.putExtra("interestRate", data.getString("interestRate"));
                                    intent.putExtra("eligibleAmount", data.getString("eligibleAmount"));
                                    intent.putExtra("payBackDays", data.getString("payBackDays"));
                                    context.startActivity(intent);
                                }

                            } else {

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Loan Status");
                                alertDialogBuilder
                                        .setMessage(responseMessage)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progress.dismiss();
                        Alert.showFailed(context, "Could not check loan eligibility");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                });

    }

    public static void doLoanApplication(final Context context, JSONObject params) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Processing......");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        AndroidNetworking.post(MainActivity.loanUrl)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progress.dismiss();
                        try {

                            String responseCode = response.getString("responseCode");
                            String responseMessage = response.getString("responseMessage");

                            if (responseCode.equals("00")) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Status");
                                alertDialogBuilder
                                        .setMessage(responseMessage)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        Intent intent = new Intent(context, DashboardActivity.class);
                                                        context.startActivity(intent);

                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } else {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Loan Status");
                                alertDialogBuilder
                                        .setMessage(responseMessage)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        Alert.showFailed(context, "Could not apply for loan");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

    }

    public static void verifyAgentAccountDetails(final Context context) {


        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Verifying Account ......");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        AndroidNetworking.get(MainActivity.verifyAgentAccountUrl)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    //Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
                    @Override
                    public void onResponse(JSONObject response) {

                        progressDialog.dismiss();

                        try {

                            String responseMessage = response.getString("responseMessage");
                            String responseCode = response.getString("responseCode");

                            if (responseCode.equals("00")) {
                                JSONObject data = response.getJSONObject("data");
                                String accountName = response.getString("accountName");
                                String accountNumber = data.getString("accountNumber");
                                String bank = data.getString("bankName");

                                agentTransferVerificationInterface = (AgentTransferVerificationInterface) context;
                                agentTransferVerificationInterface.sendAgentAccountDetails(accountName, accountNumber, bank);

                            } else {

                                Alert.showFailed(context, responseMessage);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {

                        progressDialog.dismiss();

                        Alert.showFailed(context, "Could not verify agent account details");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

    }

    public static void fetchPlans(final Context context, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }


        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Fetching Plans ......");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        AndroidNetworking.get(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    //Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
                    @Override
                    public void onResponse(JSONObject response) {

                        progressDialog.dismiss();

                        try {

                            String responseMessage = response.getString("responseMessage");
                            String responseCode = response.getString("responseCode");


                            if (responseCode.equals("00")) {
                                JSONObject data = response.getJSONObject("data");
                                JSONArray products = data.getJSONArray("products");

                                planInterface = (PlanInterface) context;
                                planInterface.sendPlans(products);

                            } else {

                                Alert.showFailed(context, responseMessage);

                            }


                        }
                        catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {

                        progressDialog.dismiss();

                        Alert.showFailed(context, "Could not fetch plans");

                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                });

    }

    public static void fetchBankList(final Context context, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            return;
        }

        AndroidNetworking.get(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    //Toast.makeText(context, String.valueOf(error), Toast.LENGTH_LONG).show();
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            String responseMessage = response.getString("responseMessage");
                            String responseCode = response.getString("responseCode");


                            if (responseCode.equals("00")) {

                                JSONObject data = response.getJSONObject("data");
                                try{


                                    SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
                                    SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

                                    Gson gson = new Gson();
                                    String jsonObject = gson.toJson(data);
                                    prefsEditor.putString("BankList", jsonObject);
                                    prefsEditor.apply();

                                    bankListInterface = (BankListInterface) context;
                                    bankListInterface.send(data);

                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }



                            } else {
                                  refreshToken(context);
                               // Alert.showFailed(context, responseMessage);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                    //   Alert.showFailed(context, "Could not fetch bank list");

                                       try {

                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

    }

    public static void doNameLookup(final Context context, final JSONObject params, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Processing....");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        AndroidNetworking.post(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {

                            String responseCode = response.getString("responseCode");
                            String responseMessage = response.getString("responseMessage");

                            if (responseCode.equals("00")) {
                                JSONObject data = response.getJSONObject("data");
                                String customerName = data.getString("customerName");
                                String customerNumber = data.getString("customerNumber");
                                String invoicePeriod = data.getString("invoicePeriod");

                                nameLookupInterface = (NameLookupInterface) context;
                                nameLookupInterface.sendAccountDetails(customerName, invoicePeriod, customerNumber);

                            } else {
                                progressDialog.dismiss();
                                refreshToken(context);
                               // Alert.showFailed(context, responseMessage);
                            }


                        }
                        catch (Exception e) {
                            Log.i("HttpService",e.getLocalizedMessage());
                            Log.i("HttpService",e.getLocalizedMessage());
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        Alert.showFailed(context, "Could not perform name enquiry");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    public static void doStartimesNameLookup(final Context context, final JSONObject params, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Processing....");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        AndroidNetworking.post(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            progressDialog.dismiss();
                            String responseCode = response.getString("responseCode");
                            String responseMessage = response.getString("responseMessage");

                            if (responseCode.equals("00")) {
                                JSONObject data = response.getJSONObject("data");
                                String customerName = data.getString("customerName");

                                startimesNameLookupInterface = (StartimesNameLookupInterface) context;
                                startimesNameLookupInterface.sendStartimesDetails(customerName);

                            } else {

                                Alert.showFailed(context, responseMessage);
                            }


                        } catch (Exception e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        Alert.showFailed(context, "Could not perform startimes name enquiry");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    public static void fetchDstvAddons(final Context context, final String code, final String price) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Fetching Plans ......");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();
        AndroidNetworking.get(MainActivity.dstvPlanUrl + "/" + code + "/addon")
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {

                        progressDialog.dismiss();

                        try {

                            String responseMessage = response.getString("responseMessage");
                            String responseCode = response.getString("responseCode");


                            if (responseCode.equals("00")) {
                                JSONObject data = response.getJSONObject("data");
                                JSONArray products = data.getJSONArray("products");

                                Log.i("DSTV ADDONS", String.valueOf(products));

                                Intent intent = new Intent(context, DstvAddonsActivity.class);
                                intent.putExtra("products", products.toString());
                                intent.putExtra("price", price);
                                intent.putExtra("code", code);
                                context.startActivity(intent);

                            } else {

                                Alert.showFailed(context, responseMessage);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {

                        progressDialog.dismiss();

                        Alert.showFailed(context, "Could not fetch dstv addons");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                });

    }

    public static void makePayment(final Context context, final JSONObject params, String url, final String type) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Processing your payment......");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        AndroidNetworking.post(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progress.dismiss();
                        //Alert.showInfo(context,String.valueOf(response));
                        try {

                            String responseCode = response.getString("responseCode");
                            String responseMessage = response.getString("responseMessage");

                            Intent intent = null;

                            switch (type) {
                                case "startimes":
                                    intent = new Intent(context, StartimesResultActivity.class);
                                    intent.putExtra("customerName", params.getString("customerName"));
                                    intent.putExtra("smartCardNumber", params.getString("smartCardNumber"));
                                    break;
                                case "dstv":
                                    intent = new Intent(context, DstvResultActivity.class);
                                    intent.putExtra("cardNumber", params.getString("cardNumber"));
                                    intent.putExtra("customerName", params.getString("customerName"));
                                    intent.putExtra("customerNumber", params.getString("customerNumber"));
                                    break;
                                case "agentTransfer":
                                    intent = new Intent(context, AgentTransferResultActivity.class);
                                    intent.putExtra("total", params.getString("total"));
                                    break;
                                case "fundsTransfer":
                                    intent = new Intent(context, FundsTransferResultActivity.class);
                                    intent.putExtra("accountName", params.getString("accountName"));
                                    intent.putExtra("accountNumber", params.getString("accountNumber"));
                                    intent.putExtra("bankName", params.getString("bankName"));
                                    intent.putExtra("phone", params.getString("phone"));
                                    break;
                                case "gotv":
                                    intent = new Intent(context, GotvResultActivity.class);
                                    intent.putExtra("cardNumber", params.getString("cardNumber"));
                                    intent.putExtra("customerName", params.getString("customerName"));
                                    intent.putExtra("customerNumber", params.getString("customerNumber"));
                                    break;
                                case "airtime":
                                    intent = new Intent(context, AirtimeResultActivity.class);
                                    intent.putExtra("telco", params.getString("telco"));
                                    break;
                                default:
                                    intent = new Intent(context, DashboardActivity.class);
                                    break;
                            }

                            intent.putExtra("responseMessage", responseMessage);
                            intent.putExtra("responseCode", responseCode);
                            intent.putExtra("transactionId", params.getString("transactionId"));
                            intent.putExtra("amount", params.getString("amount"));
                            intent.putExtra("convenienceFee", params.getString("convenienceFee"));
                            intent.putExtra("posAccount", params.getString("posAccount"));
                            intent.putExtra("appTime", params.getString("appTime"));

                            context.startActivity(intent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        Alert.showFailed(context, "Could not make payment");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(context, DashboardActivity.class);
                        context.startActivity(intent);
                    }
                });
    }

    public static void refreshToken(final Context context) {

        String refreshToken = sharedPreferenceManager.getString(context, "refreshToken", "");

        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("refreshToken", refreshToken);

            AndroidNetworking.post(MainActivity.refreshTokenUrl)
                    .setPriority(Priority.IMMEDIATE)
                    .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                    .addHeaders("Content-type", "application/json")
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                String responseMessage = response.getString("responseMessage");
                                String responseCode = response.getString("responseCode");
                                if (responseCode.equals("00")) {
                                    Globals.refreshToken = true;
                                    String accessToken = response.getJSONObject("data").getString("access_token");
                                    String refreshToken = response.getJSONObject("data").getString("refresh_token");
                                    String expiresIn = response.getJSONObject("data").getString("expires_in");
                                    Log.d("ACCESS_TOKEN",accessToken);
                                    Log.d("REFRESH_TOKEN",refreshToken);
                                    Log.d("EXPIRES_IN",expiresIn);
                                    sharedPreferenceManager.save(context, "accessToken", accessToken);
                                    sharedPreferenceManager.save(context, "refreshToken", refreshToken);
                                    sharedPreferenceManager.save(context,"expiresIn",expiresIn);

                                } else  {
                               //     Alert.showFailed(context, responseMessage);
                                    Globals.refreshToken = false;
                                }

                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle errorfr
                            //progress.dismiss();
                            try {
                                if (error.getErrorCode() != 0) {
                                    if(error.getErrorCode() == 401){
                                        Globals.refreshToken = false;
                                        Intent intent = new Intent(context, PosAdminLoginActivity.class);
                                        context.startActivity(intent);
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public static void deletePosAccount(final Context context, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Processing....");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        AndroidNetworking.delete(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response

                        Log.d("SUCCESS_DELETE",String.valueOf(response));
                        try {

                            String responseCode = response.getString("responseCode");
                            String responseMessage = response.getString("responseMessage");

                            if (responseCode.equals("00")) {

                                httpResponseInterface = (HttpResponseInterface) context;
                                httpResponseInterface.sendDefaultHttpResponse(responseCode,responseMessage);

                            } else {

                                progressDialog.dismiss();
                                Alert.showFailed(context, responseMessage);

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        Alert.showFailed(context, "Could not delete pos account");
                        try {
                            Log.d("DELETE_POS",error.getErrorBody());
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

    public static void sendEmail(final Context context, JSONObject params) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Processing......");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        AndroidNetworking.post(MainActivity.sendEmailUrl)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        progress.dismiss();

                        try {

                            String responseCode = response.getString("responseCode");
                            String responseMessage = response.getString("responseMessage");

                            if (responseCode.equals("00")) {

                                Alert.showSuccess(context, "Email has been sent to customer");

//                                Intent intent = new Intent(context, DisputeListActivity.class);
//                                context.startActivity(intent);

                            } else {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Email");
                                alertDialogBuilder
                                        .setMessage(responseMessage)
                                        .setCancelable(false)
                                        .setPositiveButton("OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {

                        progress.dismiss();

                        Alert.showFailed(context, "Your email could not be sent");
                        try {
                            Log.d("ERROR_LOGGING_DISPUTE",error.getErrorBody());
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

    }

    public static void log(JSONObject params, String url) {

        AndroidNetworking.post(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                         Log.d("LOG_SUCCESS_RESPONSE", String.valueOf(response));
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error

                        Log.d("LOG_FAILED_RESPONSE", error.getErrorBody());

                    }
                });

    }

    private static String getAuth(Context context) {
        return ConfigurationClass.getAccessToken(context);
    }

    private int getMethodFromString(String methodName) {
        int method = Method.GET;

        switch (methodName.toLowerCase()) {
            case "post":
                return Method.POST;

        }

        return method;
    }

    public static void makePost(Context context, String url, JSONObject body, Map<String, String> param, Map<String, String> headers, final boolean async, final ResponseProcessorInterface processor) {
        String auth = getAuth(context);

        if (headers == null) {
            headers = new HashMap<>();
        }

        headers.put("Authorization", auth);
        headers.put("x-phone-uuid", MainActivity.getUuid(context));


        ANRequest request = AndroidNetworking.post(url)
                .addJSONObjectBody(body)
                .addHeaders(headers)
                .addQueryParameter(param)
                .setPriority(Priority.IMMEDIATE)
                .build();

        processRequest(request, async, processor,context);
    }

    public static void makeGet(Context context, String url, Map<String, String> param, Map<String, String> headers, boolean async, final ResponseProcessorInterface processor) {
        String auth = getAuth(context);

        if (headers == null) {
            headers = new HashMap<>();
        }

        Log.i("UUID instance: ", MainActivity.getUuid(context));

        headers.put("Authorization", auth);
        headers.put("x-phone-uuid", MainActivity.getUuid(context));

        ANRequest request = AndroidNetworking.get(url)
                .addHeaders(headers)
                .addQueryParameter(param)
                .setPriority(Priority.IMMEDIATE)
                .build();

        processRequest(request, async, processor,context);

    }

    private static void processRequest(ANRequest request, boolean async, final ResponseProcessorInterface<JSONObject> processor, final Context context) {
        final String reqType = async ? "async" : "sync";

        if (async) {
            request.getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("ASYNC_SUCCESS", String.valueOf(response));
                    processor.processResponse(prepareResponse(false, reqType, response));
                }

                @Override
                public void onError(ANError error) {
                    // handle error
                    try {
                        Log.d("ASYNC_ERROR", error.getErrorBody());
                        if (error.getErrorCode() != 0) {
                            if(error.getErrorCode() == 401){
                                refreshToken(context);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    processor.processResponse(prepareResponse(true, reqType, null));
                }
            });
        }
    }

    private static JSONObject prepareResponse(boolean error, String reqType, JSONObject response) {
        JSONObject requestRes = new JSONObject();

        try {
            requestRes.put("error", error);
            requestRes.put("requestType", reqType);

            if (!error)
                requestRes.put("data", response);

            return requestRes;
        } catch (JSONException ex) {

        }

        return null;
    }

    public static void makePostRequest(Context context, String url, JSONObject body, final boolean async, final ResponseProcessorInterface processor) {
        String auth = getAuth(context);

        Map<String, String> headers;

        headers = new HashMap<>();
        headers.put("Authorization", auth);
        headers.put("x-phone-uuid", MainActivity.getUuid(context));

        ANRequest request = AndroidNetworking.post(url)
                .addJSONObjectBody(body)
                .addHeaders(headers)
                .setPriority(Priority.IMMEDIATE)
                .build();

        processRequest(request, async, processor,context);
    }

    public static void sendPlayerId(final Context context, final JSONObject params, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }
        Log.d("okh", "sendPlayerId: called");
        AndroidNetworking.post(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            Log.d(TAG, "Player ID: " + params.getString("playerId"));
                            Globals.playerId = null;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.d(TAG, "Player id onError: " + error.getErrorBody());
                        Log.d("okh", "Refresh token onError: " + ConfigurationClass.getAccessToken(context));
                    }
                });
    }

    public static void postDbPin(JSONObject params, String url,Context context) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Processing....");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();
        AndroidNetworking.post(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Content-type", "application/json")
                .addJSONObjectBody(params)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        Log.d("LOG_SUCCESS_RESPONSE", String.valueOf(response));

                        String responseCode = response.optString("responseCode");
                        String responseMessage = response.optString("responseMessage");

                        if (responseCode.equals("00")) {

                            Intent intent = new Intent(context, AndroidDatabaseManager.class);
                            context.startActivity(intent);

                        } else {

                            progressDialog.dismiss();
                            Alert.showFailed(context, responseMessage);

                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        Alert.showFailed(context, error.getErrorBody());
                        Log.d("LOG_FAILED_RESPONSE", error.getErrorBody());

                    }
                });

    }

    public static void getElectricityBillers(Context context, ResponseProcessorInterface<List<BillList> >responseProcessorInterface, String url) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Fetching  ......");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();
        AndroidNetworking.get(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {

                        progressDialog.dismiss();

                        try {

                            String responseMessage = response.getString("responseMessage");
                            String responseCode = response.getString("responseCode");

                            List<BillList> billLists = new ArrayList<>();
                            if (responseCode.equals("00")) {
                                JSONArray data = response.optJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    BillList bills = new BillList(R.drawable.ic_power,data.getString(i));
                                    billLists.add(bills);
                                }

                             //   BillList billList = new BillList(R.drawable.ic_bill,)
                                 responseProcessorInterface.processResponse(billLists);

//                                Log.i("DSTV ADDONS", String.valueOf(products));
//
//                                Intent intent = new Intent(context, DstvAddonsActivity.class);
//                                intent.putExtra("products", products.toString());
//                                intent.putExtra("price", price);
//                                intent.putExtra("code", code);
//                                context.startActivity(intent);

                            } else {

                                Alert.showFailed(context, responseMessage);

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {

                        progressDialog.dismiss();

                        Alert.showFailed(context, "Could not fetch");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                });

    }
    public static void requeryPower(final Context context, String url,ResponseProcessorInterface<String> responseProcessorInterface) {

        if (!InternetConnectionService.doInternetConnectionCheck(context)) {
            Alert.showWarning(context, "Please check your internet connection");
            return;
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Processing....");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        AndroidNetworking.get(url)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid", MainActivity.getUuid(context))
                .addHeaders("Content-type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progressDialog.dismiss();
                        try {

                            String responseCode = response.getString("responseCode");
                            String responseMessage = response.getString("responseMessage");

                            if (responseCode.equals("00")) {
                                JSONObject data = response.getJSONObject("data");
                                String creditToken = data.getString("creditToken");
                                 responseProcessorInterface.processResponse(creditToken);

                            } else {
                                progressDialog.dismiss();
                              //  refreshToken(context);
                                // Alert.showFailed(context, responseMessage);
                            }


                        }
                        catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        Alert.showFailed(context, "Could not perform name enquiry");
                        try {
                            if (error.getErrorCode() != 0) {
                                if(error.getErrorCode() == 401){
                                    refreshToken(context);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
    }

}
