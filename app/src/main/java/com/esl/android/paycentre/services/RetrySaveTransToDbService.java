package com.esl.android.paycentre.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Date;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * helper methods.
 */
public class RetrySaveTransToDbService extends IntentService {


    public RetrySaveTransToDbService() {
        super("RetrySaveTransToDbService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "PaycentreTrans";
            File myDir = new File(root);
            String fname = "PaycentreTrans-" + Globals.transactionId + ".txt";
            File file = new File(myDir, fname);
            Transaction transaction;
            try {
                FileInputStream fileIn = new FileInputStream(file);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                transaction = (Transaction) in.readObject();
                TransactionRepo transactionRepo = new TransactionRepo();

                transaction.setAgentCut(0);
                transaction.setAmount(transaction.getAmount());
                transaction.setConvenienceFee(transaction.getConvenienceFee());
                transaction.setDate(transaction.getDate());
                transaction.setPosAccount(transaction.getPosAccount());
                transaction.setTransactionId(transaction.getTransactionId());
                transaction.setResponseCode(transaction.getResponseCode());
                transaction.setResponseMsg(transaction.getResponseMsg());
                transaction.setVersion(MainActivity.getAppVersion(this));
                transaction.setAgentCut(Float.valueOf("0.0"));
                transaction.setTransactionType(Transaction.Type.getByName(Globals.fundsTransferType.toUpperCase()));
               // transaction.setData(params);

                long result = transactionRepo.update(transaction);
                if(result < 1){
                    Log.d("TRANSACTION_UPDATE","Transaction could not be updated "+result);
                }
                else {

                    myDir.delete();
                }

                in.close();
                fileIn.close();

            } catch (IOException | ClassNotFoundException i) {
                i.printStackTrace();
            }

        }
    }


}
