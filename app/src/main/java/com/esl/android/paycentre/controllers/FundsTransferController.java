package com.esl.android.paycentre.controllers;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.esl.android.paycentre.services.HttpService;

import org.json.JSONObject;
import org.w3c.dom.Text;

public class FundsTransferController {

    public static String validateFundsTransfer(int accountLength){

        String result = "";

        if(accountLength < 10){
            result = "invalid";
        }else if(accountLength == 10){
            result = "valid";
        }else{
            result ="inactive";
        }
        return result;
    }

    public static String validateTransferAmount(Context context, JSONObject params){

        String result = "";

        try {

            String accountName = params.getString("accountName");
            String bank = params.getString("bank");
            String amount = params.getString("amount");
            String narration = params.getString("narration");
            String accountNumber = params.getString("accountNumber");
            String phone = params.getString("phone");

            if(TextUtils.isEmpty(amount) || TextUtils.isEmpty(phone)){
                result = "emptyPhoneOrAmount";
            }else if(TextUtils.isEmpty(narration)){
                result = "emptyNarration";
            }else if(TextUtils.isEmpty(accountName) || TextUtils.isEmpty(bank) || TextUtils.isEmpty(accountNumber)){
                result = "fillAll";
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }
}
