package com.esl.android.paycentre.services;

import android.os.CountDownTimer;
import android.util.Log;

import com.esl.android.paycentre.interfaces.InactivityTimerListener;

public class InactivityService {

    CountDownTimer timer;
    InactivityTimerListener listener;

    long intervals;
    boolean finished = false;

    public InactivityService(long intervals, InactivityTimerListener listener) {
        this.intervals = intervals;
        this.listener = listener;
        this.timer = this.createNewTimer();
        this.start();

        Log.d("TIMER: ", "Timer started!");

    }

    public void cancel(){
        timer.cancel();
    }

    public void restart() {
        if (finished)
            return;

        Log.d("TIMER: ", "Timer restarted!");
        this.start();
    }

    public void start() {
        Log.d("TIMER: ", "Timer started!");
        finished = false;
        timer.cancel();
        timer = createNewTimer();
        timer.start();
    }

    private CountDownTimer createNewTimer() {
        return new CountDownTimer(this.intervals, 1) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Log.d("TIMER: ", "Timer finished!");
                finished = true;
                InactivityService.this.listener.onTimerCompleted();
            }
        };
    }
}
