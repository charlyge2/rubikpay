package com.esl.android.paycentre.activities.billsPayment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.billsPayment.dstv.DstvNumberActivity;
import com.esl.android.paycentre.activities.billsPayment.gotv.GotvNumberActivity;
import com.esl.android.paycentre.activities.billsPayment.others.OtherBillsInquiry;
import com.esl.android.paycentre.activities.billsPayment.startimes.StartimesNumberActivity;
import com.esl.android.paycentre.adapters.BillAdapter;
import com.esl.android.paycentre.adapters.BillList;
import com.esl.android.paycentre.constants.Constant;
import com.esl.android.paycentre.controllers.BillsController;
import com.esl.android.paycentre.utils.Navigator;

import java.util.ArrayList;

public class BillsActivity extends BaseActivity {

    private BillAdapter billAdapter;

    private ArrayList<BillList> billPreview = new ArrayList<>();


    private ListView billsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);

        billsView = findViewById(R.id.billListView);

        billPreview.add(new BillList(R.drawable.dstv,"DSTV"));

        billPreview.add(new BillList(R.drawable.gotv,"GOTV"));

        billPreview.add(new BillList(R.drawable.startimes,"STARTIMES"));
//
//        billPreview.add(new BillList(R.drawable.ikeja,"IKEDC"));
//
//        billPreview.add(new BillList(R.drawable.eko,"EKEDC"));
//
//        billPreview.add(new BillList(R.drawable.ibadan,"IBEDC"));
//
//        billPreview.add(new BillList(R.drawable.waec,"WAEC"));

        billAdapter = new BillAdapter(this, billPreview);

        billsView.setAdapter(billAdapter);

        billsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String billName = ((TextView) view.findViewById(R.id.billName)).getText().toString();

                Intent intent = null;

                switch (billName){
                    
                    case "DSTV":
                        intent = new Intent(getApplicationContext(), DstvNumberActivity.class);
                        startActivity(intent);
                        break;
                    case "GOTV":
                        intent = new Intent(getApplicationContext(), GotvNumberActivity.class);
                        startActivity(intent);
                        break;
                    case "STARTIMES":
                        intent = new Intent(getApplicationContext(), StartimesNumberActivity.class);
                        startActivity(intent);
                        break;
//                    case "IKEDC":
//                        intent = new Intent(getApplicationContext(), OtherBillsInquiry.class);
//                        intent.putExtra("type","IKEDC");
//                        startActivity(intent);
//                        break;
//                    case "EKEDC":
//                        intent = new Intent(getApplicationContext(), OtherBillsInquiry.class);
//                        intent.putExtra("type","EKEDC");
//                        startActivity(intent);
//                        break;
//                    case "IBEDC":
//                        intent = new Intent(getApplicationContext(), OtherBillsInquiry.class);
//                        intent.putExtra("type","IBEDC");
//                        startActivity(intent);
//                        break;
//                    case "WAEC":
//                        BillsController.billsPaymentProcessor(BillsActivity.this, Constant.WAEC_INQUIRY_URL,"WAEC");
//                        break;
                }
            }
        });
    }

    public void goBack(View view) {
        Navigator.moveToDashboard(BillsActivity.this);
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(BillsActivity.this);
    }
}
