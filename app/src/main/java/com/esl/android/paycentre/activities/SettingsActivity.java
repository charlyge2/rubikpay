package com.esl.android.paycentre.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.ResultReceiver;
import android.preference.PreferenceScreen;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.cardreader.network.HSMClient;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;
import com.esl.android.paycentre.fragments.SettingsFragment;
import com.esl.android.paycentre.utils.Alert;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

public class SettingsActivity extends AppCompatActivity {

    private PinpadFacade pinpadFacade;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        pinpadFacade = new PinpadFacade(this);

        mProgressDialog = new ProgressDialog(this);

        if (savedInstanceState == null) {
            SettingsFragment fragment = new SettingsFragment();

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, fragment);
            ft.commitAllowingStateLoss();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        pinpadFacade.onActivityResult(requestCode, resultCode, data);

        startDownloadKeys();
    }

    private void startDownloadKeys() {
        Globals.redownloading = true;

        mProgressDialog = new ProgressDialog(SettingsActivity.this);
        mProgressDialog.setMessage("Downloading Keys ............");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        Intent msgIntent = new Intent(SettingsActivity.this, HSMClient.class);
        msgIntent.putExtra("receiver", new DownloadReceiver(new Handler()));
        startService(msgIntent);

    }

    private class DownloadReceiver extends ResultReceiver {

        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if (resultCode == HSMClient.UPDATE_PROGRESS) {

                int progress = resultData.getInt("progress");
                mProgressDialog.setProgress(progress);

                mProgressDialog.dismiss();
                Alert.showSuccess(getApplicationContext(), "Initialization Successful");
                pinpadFacade.doKeyLoad();

                Globals.autoConnect = true;

                MiscUtils.initContext(getApplicationContext());
                String preferencesName = ReferenceList.preference;
                MiscUtils.storeInSharedPreferences(preferencesName,
                        ReferenceList.doKeyDownload, false);


            } else if (resultCode == HSMClient.UPDATE_FAILED) {

                int progress = resultData.getInt("progress");
                mProgressDialog.setProgress(progress);

                mProgressDialog.dismiss();

                String preferencesName = ReferenceList.preference;
                MiscUtils.initContext(getApplicationContext());
                MiscUtils.storeInSharedPreferences(preferencesName,
                        ReferenceList.loadKeys, false);

            }
        }
    }

    public boolean onPreferenceStartScreen(PreferenceFragmentCompat preferenceFragmentCompat, PreferenceScreen preferenceScreen) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(PreferenceFragmentCompat.ARG_PREFERENCE_ROOT, preferenceScreen.getKey());
        fragment.setArguments(args);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.fragment, fragment, preferenceScreen.getKey());
        ft.addToBackStack(preferenceScreen.getKey());
        ft.commitAllowingStateLoss();

        return true;
    }


    public void settingsBackBtn(View view) {
        onBackPressed();
    }
}
