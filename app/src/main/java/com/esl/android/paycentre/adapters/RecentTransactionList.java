package com.esl.android.paycentre.adapters;

public class RecentTransactionList {

    private String time;

    private String transactionName;

    private String transactionAmount;

    private String transactionDate;

    private String transactionStatus;

    private String responseCode;

    // Constructor that is used to create an instance of the RecentTransactionList object
    public RecentTransactionList(String time,String transactionName, String transactionAmount, String transactionDate, String transactionStatus, String responseCode) {
        this.time = time;
        this.transactionAmount = transactionAmount;
        this.transactionDate = transactionDate;
        this.transactionStatus = transactionStatus;
        this.transactionName = transactionName;
        this.responseCode = responseCode;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getResponseCode(){
        return responseCode;
    }

    public void setResponseCode(String responseCode){
        this.responseCode = responseCode;
    }

}
