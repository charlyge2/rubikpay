package com.esl.android.paycentre.activities.airtimeVending;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Navigator;
import com.esl.android.paycentre.utils.TransactionFormatter;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import org.json.JSONObject;
import java.text.DecimalFormat;

public class AirtimeAmountPreview extends BaseActivity {


    private Button airtimePaymentButton;

    private String formattedAmount;

    private String formattedTotal;

    private Double total;

    private TextView amountTextView;

    private TextView phoneTextView;

    private TextView networkTextView;

    public static String phone;

    public static String network;

    private DecimalFormat DigitFormatter = new DecimalFormat("#,###,###.00");

    SharedPreferenceManager sharedPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime_amount_preview);

        phone = getIntent().getStringExtra("phone");
        network = getIntent().getStringExtra("network");
        final String amount = getIntent().getStringExtra("amount");
        final String convenienceFee = getIntent().getStringExtra("convenienceFee");
        
        final String posAccountUsername = sharedPreferenceManager.getString(getApplicationContext(), "posAccountUsername", "");

        phoneTextView = findViewById(R.id.phoneTextView);
        amountTextView = findViewById(R.id.amountTextView);
        networkTextView = findViewById(R.id.networkTextView);
        airtimePaymentButton = findViewById(R.id.airtimePaymentBtn);
        airtimePaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AirtimeAmountPreview.this);
                alertDialogBuilder.setTitle("Verify Account");
                alertDialogBuilder
                        .setMessage("\nAmount : ₦" + formattedAmount + "\n\n\nNetwork : " + network  + "\n\n\nRecipient : " + phone +"\n\n\nAre you sure you want to send airtime to this network?")
                        .setCancelable(false)
                        .setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //Alert.showInfo(getApplicationContext(),"Got here");
                                        try {

                                            JSONObject airtimeParams = new JSONObject();

                                            String telco = "";

                                            switch (network){
                                                case "9MOBILE":
                                                    telco = "etisalat";
                                                    break;
                                                case "GLO":
                                                    telco = "glo";
                                                    break;
                                                case "AIRTEL":
                                                    telco = "airtel";
                                                    break;
                                                case "MTN":
                                                    telco = "mtn";
                                                    break;
                                            }

                                            airtimeParams.put("amount", amount);
                                            airtimeParams.put("telco", telco);
                                            airtimeParams.put("transactionId", TransactionFormatter.generateTransactionId());
                                            airtimeParams.put("convenienceFee", convenienceFee);
                                            airtimeParams.put("agentCut", "0");
                                            airtimeParams.put("transactionType", "Airtime");
                                            airtimeParams.put("phone", phone);
                                            airtimeParams.put("posAccount", posAccountUsername);
                                            airtimeParams.put("appTime", TransactionFormatter.generateAppTime());
                                            airtimeParams.put("version", MainActivity.getAppVersion(getApplicationContext()));


                                            String url = MainActivity.airtimePaymentUrl;
                                            String type = "airtime";

                                            HttpService.makePayment(AirtimeAmountPreview.this,airtimeParams,url,type);


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        formattedAmount = amount.replace(",", "");

        total = Double.valueOf(formattedAmount);

        formattedTotal = DigitFormatter.format(total);

        phoneTextView.setText(phone);
        networkTextView.setText(network);
        amountTextView.setText("₦"+formattedTotal);


    }

    public void goBack(View view) {
        finish();
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(AirtimeAmountPreview.this);
    }


}
