package com.esl.android.paycentre.activities.activation;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity {

    SharedPreferenceManager sharedPreferenceManager;

    private Button btn;

    private EditText oldpasswd;

    private EditText newpasswd;

    private EditText confirmpasswd;

    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        btn = findViewById(R.id.changePasswdBtn);
        oldpasswd = findViewById(R.id.defpasswd);
        newpasswd = findViewById(R.id.newpasswd);
        confirmpasswd = findViewById(R.id.confirmpasswd);
        username = sharedPreferenceManager.getString(getApplicationContext(), "username", "");

    }

    public void changePassword(View view) {



        if (TextUtils.isEmpty(oldpasswd.getText().toString()) || TextUtils.isEmpty(newpasswd.getText().toString())) {
            Alert.showWarning(getApplicationContext(), "Please Enter Old and New Passwords");
            return;
        } else if (oldpasswd.getText().toString().length() != 6 || newpasswd.getText().toString().length() != 6) {
            Alert.showWarning(getApplicationContext(), "Please Make Sure Password Length is 6");
            return;
        }

        else if (newpasswd.getText().toString().equals("123456")) {
            Alert.showWarning(getApplicationContext(), "password cannot be 123456");
            return;
        }
        else if (!newpasswd.getText().toString().equals(confirmpasswd.getText().toString())) {
            Alert.showWarning(getApplicationContext(), "Please Make Sure New Password and Confirm Password Matches");
            return;
        } else if (!oldpasswd.getText().toString().equals(PosAdminLoginActivity.agentPassword)) {
            Alert.showWarning(getApplicationContext(), "Default password incorrect");
            return;
        } else {

            try {

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("username", username);
                jsonObject.put("newPassword",newpasswd.getText().toString());
                jsonObject.put("oldPassword",oldpasswd.getText().toString());

                HttpService.doChangePassword(ChangePasswordActivity.this,jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
