package com.esl.android.paycentre.activities.fundsTransfer;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.goodiebag.pinview.Pinview;

public class FundsTransferPinActivity extends AppCompatActivity {

    Pinview pinview;

    Pinview confirmpinview;

    Button savePinBtn;

    ImageView backIcon;

    SharedPreferenceManager sharedPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funds_transfer_pin);

        pinview = findViewById(R.id.pinview);
        confirmpinview = findViewById(R.id.confirmPinview);
        savePinBtn = findViewById(R.id.savePinButton);
        savePinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(pinview.getValue()) || TextUtils.isEmpty(confirmpinview.getValue())){
                    Alert.showWarning(getApplicationContext(),"Please enter pin and confirm pin");
                    return;
                }

                if(!pinview.getValue().equals(confirmpinview.getValue())){
                    Alert.showWarning(getApplicationContext(),"Pin and Confirm Pin Does not match");
                    return;
                }

                sharedPreferenceManager.save(getApplicationContext(),"fundTransferPin","true");
                sharedPreferenceManager.save(getApplicationContext(),"fundTransferPinValue",pinview.getValue());

                Alert.showSuccess(getApplicationContext(),"Pin Saved Successfully");

//                Intent intent = new Intent(getApplicationContext(), FundsTransferAmountActivity.class);
//////                startActivity(intent);
                onBackPressed();

            }
        });

    }

    public void goBack(View view) {
//        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
//        startActivity(intent);
        onBackPressed();
    }
}
