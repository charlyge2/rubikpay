package com.esl.android.paycentre.notification;

import android.util.Log;

import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;
import com.esl.android.paycentre.database.model.Notification;
import com.esl.android.paycentre.database.repo.NotificationRepo;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationPayload;
import com.onesignal.OSNotificationReceivedResult;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;


public class PaycentreNotificationExtender extends NotificationExtenderService {

    private final int COMMAND_DOWNLOAD_KEYS = 1;
    private final int COMMAND_HSM_IP = 2;
    private final int COMMAND_HSM_PORT = 4;
    private final int COMMAND_NIBSS_IP = 8;
    private final int COMMAND_NIBSS_PORT = 16;

    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        // Read properties from result.
        OSNotificationPayload payload = receivedResult.payload;
        JSONObject data = payload.additionalData;
        int command = data != null ? data.optInt("command") : 0;

        Log.d("NOTIFICATION_RECEIVED", String.valueOf(payload));

        if (command > 0) {
            String preferencesName = ReferenceList.preference;
            MiscUtils.initContext(getApplicationContext());

            HashMap<String, String> commandData = new HashMap<>();
            Iterator<String> keys = data.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                String value = data.optString(key);
                commandData.put(key, value);
            }

            if ((command & COMMAND_DOWNLOAD_KEYS) == COMMAND_DOWNLOAD_KEYS) {
                MiscUtils.storeInSharedPreferences(preferencesName, ReferenceList.doKeyDownload, true);
                System.out.println(command);
            }

            if ((command & COMMAND_HSM_IP) == COMMAND_HSM_IP  && commandData.get("hsm_ip") != null) {
                MiscUtils.storeInSharedPreferences(preferencesName, ReferenceList.hsmIp, commandData.get("hsm_ip"));
            }

            if ((command & COMMAND_HSM_PORT) == COMMAND_HSM_PORT && commandData.get("hsm_port") != null) {
                MiscUtils.storeInSharedPreferences(preferencesName, ReferenceList.hsmPort, Integer.valueOf(commandData.get("hsm_port")));
            }

            if ((command & COMMAND_NIBSS_IP) == COMMAND_NIBSS_IP && commandData.get("nibss_ip") != null) {
                MiscUtils.storeInSharedPreferences(preferencesName, ReferenceList.nibssIp, commandData.get("nibss_ip"));
            }

            if ((command & COMMAND_NIBSS_PORT) == COMMAND_NIBSS_PORT && commandData.get("nibss_port") != null) {
                MiscUtils.storeInSharedPreferences(preferencesName, ReferenceList.nibssPort, Integer.valueOf(commandData.get("nibss_port")));
            }

            return true;
        }

//        new Handler(Looper.getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//                Context context = PaycentreApplication.getAppContext();
//                Alert.showSuccess(context, "You have a new Message From Paycentre. Please check your notifications");
//            }
//        });

        saveNotification(payload.title, payload.body);

        return false;
    }

    private void saveNotification(String title, String message) {
        NotificationRepo repo = new NotificationRepo();
        Notification notification = new Notification();
        notification.setTitle(title);
        notification.setMessage(message);
        notification.setType("message");
        notification.setDate(new Date());

        repo.insert(notification);
    }
}