package com.esl.android.paycentre.services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;

import com.esl.android.paycentre.activities.MainActivity;

public class FetchBankListService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public FetchBankListService() {
        super("FetchBankListService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        HttpService.fetchBankList(this, MainActivity.bankListUrl);
    }
}
