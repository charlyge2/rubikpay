package com.esl.android.paycentre.activities.activation;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.adapters.PinpadList;
import com.esl.android.paycentre.adapters.PinpadListAdapter;
import com.esl.android.paycentre.utils.Alert;

import java.util.ArrayList;
import java.util.Set;

public class BluetoothActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 0;

    private static final int REQUEST_DISCOVER_BT = 1;

    private ListView listView;

    private Button turnOff, turnOn, pairDevice, discoverable;

    private BluetoothAdapter bluetoothAdapter;

    private PinpadListAdapter pinpadListAdapter;

    private ArrayList<PinpadList> pinpadList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        final SwipeRefreshLayout pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                // TODO : Auto-generated method stubs
                showToast("List Updated");
                getPairedDevices();
                pullToRefresh.setRefreshing(false);
            }
        });


        listView = findViewById(R.id.loanListView);

        // adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Check if bluetooth is available
        if(bluetoothAdapter == null){
            showToast("Bluetooth not available on device");
        }else{
            //showToast("Bluetooth available on device");
            turnOnBluetooth();
        }
    }


    public void turnOnBluetooth() {
        if(!bluetoothAdapter.isEnabled()){
            //showToast("Turning On Bluetooth...");
            //intent to on bluetooth
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_ENABLE_BT);
        }else{
            //showToast("Bluetooth is already on");
            getPairedDevices();
        }
    }

    public void turnOffBluetooth(View view) {
        if(bluetoothAdapter.isEnabled()){
            showToast("Turning Bluetooth Off");
            bluetoothAdapter.disable();
        }else{
            showToast("Bluetooth already off");
        }
    }

    public void discoverBluetooth(View view) {
        if(!bluetoothAdapter.isDiscovering()){
            showToast("Making Your Device Discoverable");
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(intent, REQUEST_DISCOVER_BT);
        }
    }

    public void getPairedDevices() {
        // Name, address and RSSI
        String pinpadBluetoothClassName = "1f00";
        pinpadList.clear();
        if(bluetoothAdapter.isEnabled()){
            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device: devices){
                if(pinpadBluetoothClassName.equals(device.getBluetoothClass().toString())){
                    pinpadList.add(new PinpadList(R.drawable.ic_langue,device.getName(),device.getAddress()));
                }
            }
        }else{
            turnOnBluetooth();
        }

        pinpadListAdapter = new PinpadListAdapter(this,pinpadList);

        listView.setAdapter(pinpadListAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PinpadList item = (PinpadList) parent.getItemAtPosition(position);
                // Use item.getDeviceAddress() to get device address
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BluetoothActivity.this);
                alertDialogBuilder.setTitle("Confirm Pinpad Selection");
                alertDialogBuilder
                        .setMessage("Activate pinpad with serial number " + item.getDeviceName() + " on your account?" )
                        .setCancelable(false)
                        .setPositiveButton("YES, PROCEED",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(BluetoothActivity.this, AfterkeyExchangeActivity.class);
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton("NO, CHOOSE AGAIN", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK){
                    //bluetooth is on
                    getPairedDevices();
                }
                else {
                    //user denied to turn bluetooth on
                    Alert.showWarning(getApplicationContext(),"Could not turn on bluetooth");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //toast message function
    public void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
