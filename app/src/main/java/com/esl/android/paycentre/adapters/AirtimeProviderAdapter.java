package com.esl.android.paycentre.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;

public class AirtimeProviderAdapter extends BaseAdapter {
    Context context;
    int images[];
    String[] provider;
    LayoutInflater inflter;

    public  AirtimeProviderAdapter(Context applicationContext, int[] flags, String[] provider) {
        this.context = applicationContext;
        this.images = flags;
        this.provider = provider;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.provider_dropdown_layout, null);
        ImageView icon = view.findViewById(R.id.imageView);
        TextView names = view.findViewById(R.id.textView);
        icon.setImageResource(images[i]);
        names.setText(provider[i]);
        return view;
    }
}