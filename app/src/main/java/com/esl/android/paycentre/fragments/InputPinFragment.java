package com.esl.android.paycentre.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;

import org.json.JSONException;
import org.json.JSONObject;

public class InputPinFragment extends DialogFragment {

    public InputPinFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inputpinfragment_xml,container,false);
        Button processPin = view.findViewById(R.id.processPin);
        EditText enter_pin = view.findViewById(R.id.enter_pin);
        processPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(enter_pin.getText().toString())){
                    Alert.showInfo(getActivity(),"Please Enter Pin");
                    return;
                }

                //make post request
                String pinUrl = "https://pocketmoni.paypad.com.ng/paycentretrace/verifypassword";
                JSONObject requestBody = new JSONObject();

                try {
                    requestBody.put("password", enter_pin.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
               //  requestBody.put("date", formattedDate);
                HttpService.postDbPin(requestBody,pinUrl,getActivity());
                dismiss();
            }
        });
        return view;
    }
}
