package com.esl.android.paycentre.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.ArrayList;
import java.util.List;

public class WaecListAdapter extends ArrayAdapter<WaecList> {

    private onWaecItemClickListener clickListener;
   public interface onWaecItemClickListener{
       void onItemClick(WaecList waecList);
    }

    private Context mcontext;
    private List<WaecList> waecList;

    public WaecListAdapter(@NonNull Context context,onWaecItemClickListener clickListener ,@SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<WaecList> list) {
        super(context, 0 , list);
        mcontext = context;
        waecList = list;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mcontext).inflate(R.layout.waec_listing_template,parent,false);

        WaecList waecList1 = waecList.get(position);
        Button buy_btn = listItem.findViewById(R.id.buy_btn);
        TextView amount = listItem.findViewById(R.id.amountTextView);
        amount.setText(waecList1.getAmount());
        buy_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WaecList newWaecList = getItem(position);
                clickListener.onItemClick(newWaecList);
            }
        });

        return listItem;
    }

    @Override
    public int getCount() {
        if(waecList.size() > 20){
            return 20;
        }else{
            return waecList.size();
        }
    }
}
