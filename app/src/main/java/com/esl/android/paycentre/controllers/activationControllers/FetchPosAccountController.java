package com.esl.android.paycentre.controllers.activationControllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.activation.PosLoginActivity;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.constants.Constant;
import com.esl.android.paycentre.database.DBHelper;
import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.repo.PosAccountRepo;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class FetchPosAccountController {

    SharedPreferenceManager sharedPreferenceManager;

    PosAccount posAccount = new PosAccount();
    PosAccountRepo posAccountRepo = new PosAccountRepo();

    DatabaseManager databaseManager;

    int result;

    public void doFetchPosAccount(final Context context, JSONObject userObject){

        DBHelper dbHelper = new DBHelper(context);
        DatabaseManager.initializeInstance(dbHelper);

        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Fetching Pos Accounts....");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        Log.d("URL",MainActivity.fetchPosUrl);

        Log.d("UUID",MainActivity.getUuid(context));

        AndroidNetworking.get(MainActivity.fetchPosUrl)
                .setPriority(Priority.IMMEDIATE)
                .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                .addHeaders("x-phone-uuid",MainActivity.getUuid(context))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        progress.dismiss();

                        String responseMessage = response.optString("responseMessage");
                        String responseCode = response.optString("responseCode");

                        if("00".equals(responseCode)){

                            try {

                                JSONArray data = response.getJSONArray("data");

                                for(int i = 0; i < data.length(); i++){

                                    String firstname = data.getJSONObject(i).getString("firstname");
                                    String lastname = data.getJSONObject(i).getString("lastname");
                                    String username = data.getJSONObject(i).getString("username");
                                    String pin = data.getJSONObject(i).getString("pin");
                                    String posaccountId = data.getJSONObject(i).getString("id");

                                    posAccount.setUsername(username);
                                    posAccount.setLastname(lastname);
                                    posAccount.setFirstname(firstname);
                                    posAccount.setPassword(pin);
                                    posAccount.setPosAccountId(posaccountId);

                                    try {
                                        posAccountRepo.insertOrUpdate(posAccount);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }

                                sharedPreferenceManager.save(context, "posLoginPage", "true");
                                Intent intent = new Intent(context, PosLoginActivity.class);
                                context.startActivity(intent);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Alert.showFailed(context,responseMessage);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        progress.dismiss();

                        try {
                            Log.d("ERROR_MESSAGE",String.valueOf(anError.getErrorBody()));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        //Log.d("ERR",String.valueOf(anError.getErrorCode()));
                        Alert.showFailed(context,"Could not fetch pos account");
                    }
                });
    }

}
