package com.esl.android.paycentre.utils;

import android.util.Log;

import com.esl.android.paycentre.cardreader.utils.Globals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class TransactionFormatter {

    public static String generateTransactionId() {

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);


        String transactionID = "TT" + String.valueOf(year) + appendZeroWhenLengthIsEqualsToOne(String.valueOf(month))
                + appendZeroWhenLengthIsEqualsToOne(String.valueOf(day))
                + appendZeroWhenLengthIsEqualsToOne(String.valueOf(hour))
                + appendZeroWhenLengthIsEqualsToOne(String.valueOf(minutes))
                + appendZeroWhenLengthIsEqualsToOne(String.valueOf(seconds))
                + Globals.agentID;

        return transactionID;
    }
    public static String appendZeroWhenLengthIsEqualsToOne(String value){

        String formattedValue = value + "";

        if (formattedValue.length() == 1) {
            formattedValue = "0" + value;
        }

        return formattedValue;
    }

    public static String generateAppTime(){

        String appTime = String.valueOf(android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", new java.util.Date()));

        return appTime;

    }

    public static String generateDate(){

        String date = String.valueOf(android.text.format.DateFormat.format("dd/MM/yyyy", new java.util.Date()));

        return date;
    }

    public static String formatTime(Date date){

        String time = String.valueOf(android.text.format.DateFormat.format("HH:mm:ss", date));

        return time;
    }

    public static String formatDate(Date date){

        String dated = String.valueOf(android.text.format.DateFormat.format("yyyy-MM-dd", date));

        return dated;
    }

    public static String formatDateTime(String appTime){
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
        String dateTime = null;
        try {
            Date date = format.parse(appTime);
            dateTime = TransactionFormatter.formatDate(date)+ " " +TransactionFormatter.formatTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateTime;
    }

    public static int generateRandomNumber(int min,int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }
}
