package com.esl.android.paycentre.services;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;

import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.DatabaseIterator;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.interfaces.ResponseProcessorInterface;
import com.esl.android.paycentre.utils.Display;
import com.esl.android.paycentre.utils.TransactionFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public final class SyncService extends JobService {

    private static int JOB_ID = 1234;
    private static int DEFERRED_JOB_ID = 12345;

    Thread transactionProcessor;

    private static final String TAG = SyncService.class.getSimpleName();
    private static TransactionRepo repo = new TransactionRepo();

    private static HashMap<Transaction.Type, HashMap<String, ArrayList<String>>> requiredMap = new HashMap<>();

    public static void syncTransaction(final Context context, final Transaction transaction) {
        syncTransaction(context, transaction, true);
    }

    public static void syncTransaction(final Context context, final Transaction transaction, boolean async) {

        ResponseProcessorInterface<JSONObject> ibetaProcessor = new ResponseProcessorInterface<JSONObject>() {
            @Override
            public void processResponse(JSONObject response) {
                Log.d("ASYNC_IBETA_PROCESSOR", String.valueOf(response));
                if (!response.optBoolean("error")) {
                    JSONObject data = response.optJSONObject("data");
                    boolean resCode = data == null? false: data.optBoolean("status", false);
                    if(resCode) {
                        Transaction dbTransaction = repo.getTransaction(transaction.getId());
                        int syncStatus = dbTransaction.getSynced();
                        Log.d("ASYNC_IBETA_PRE", String.valueOf(syncStatus));
                        dbTransaction.setSynced(syncStatus | Transaction.Sync.SYNCIBETA.getValue());
                        int status = repo.update(dbTransaction);
                        Log.d("ASYNC_IBETA_STATUS", String.valueOf(status));
                    }
                }else {
                    SyncService.scheduleSync(context, JOB_ID);
                }

                //logging response from portal
                Display.sendToErrorLog("Ibeta Sync", String.valueOf(response), "");
            }
        };

        ResponseProcessorInterface<JSONObject> paycentreProcessor = new ResponseProcessorInterface<JSONObject>() {
            @Override
            public void processResponse(JSONObject response) {

                Log.d("ASYNC_PAYCENT_PROCESSOR", String.valueOf(response));
                if (!response.optBoolean("error")) {
                    JSONObject data = response.optJSONObject("data");
                    String resCode = data == null? null: data.optString("responseCode");
                    String resMsg = data == null? null: data.optString("responseMessage");

                    if("00".equals(resCode) || "DUPLICATE TRANSACTION".equals(resMsg)) {
                        Transaction dbTransaction = repo.getTransaction(transaction.getId());
                        int syncStatus = dbTransaction.getSynced();
                        Log.d("ASYNC_PAYCENT_PRE", String.valueOf(syncStatus));
                        dbTransaction.setSynced(syncStatus | Transaction.Sync.SYNCPAYCENTRE.getValue());
                        int status = repo.update(dbTransaction);
                        Log.d("ASYNC_PAYCENT_STATUS", String.valueOf(status));
                    }
                }else {
                    SyncService.scheduleSync(context, JOB_ID);
                }

                //logging response from portal
                Display.sendToErrorLog("Paycentre Sync", String.valueOf(response), "");
            }
        };

        int id = transaction.getId();
        int syncStatus = transaction.getSynced();
        Log.d("ASYNC_BEFORE_SYNC", String.valueOf(syncStatus));

        if ((syncStatus & Transaction.Sync.SYNCREQIBETA.getValue()) == Transaction.Sync.SYNCREQIBETA.getValue() && (syncStatus & Transaction.Sync.SYNCIBETA.getValue()) != Transaction.Sync.SYNCIBETA.getValue()) {
            ArrayList<String> requiredFields = getRequiredFileds(transaction.getTransactionType(), "ibeta");
            JSONObject params = new JSONObject();
            JSONObject requestBody = buildRequestBody(transaction, requiredFields);
            Log.d("IBETA_REQUEST_BODY",String.valueOf(requestBody));
            HttpService.makePost(context, MainActivity.ibetaTransactionUrl, requestBody, new HashMap<String, String>(), new HashMap<String, String>(), async, ibetaProcessor);
        }

        if ((syncStatus & Transaction.Sync.SYNCREQPAYCENTRE.getValue()) == Transaction.Sync.SYNCREQPAYCENTRE.getValue() && (syncStatus & Transaction.Sync.SYNCPAYCENTRE.getValue()) != Transaction.Sync.SYNCPAYCENTRE.getValue()) {
            ArrayList<String> requiredFields = getRequiredFileds(transaction.getTransactionType(), "paycentre");
            JSONObject requestBody = buildRequestBody(transaction, requiredFields);
            try {
                String appTime = requestBody.optString("appTime");
                String dateTime = TransactionFormatter.formatDateTime(appTime);
                requestBody.put("appTime",dateTime);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("PAYCENTRE_REQUEST_BODY",String.valueOf(requestBody));
            HttpService.makePost(context, MainActivity.paycentreTransactionUrl, requestBody, new HashMap<String, String>(), new HashMap<String, String>(), true, paycentreProcessor);
        }
    }

    public static ArrayList<String> getRequiredFileds(Transaction.Type transactionType, String service) {
        buildRequireMap();
        HashMap<String, ArrayList<String>> map = requiredMap.get(transactionType);

        if (map != null && map.containsKey(service))
            return map.get(service);

        return new ArrayList<>();

    }

    private static void buildRequireMap() {
        String[] field = new String[]{"posAccount", "transactionId", "amount", "convenienceFee", "agentCut", "appTime", "version",
                "statusCode", "statusMessage", "rrn", "pan", "stan", "aid", "authCode", "cardHolder", "expiry", "sequenceNo", "transactionTypeId",
                "terminalId", "tenderType", "stampDuty","accountType", "createdOn", "email", "username", "cardNo", "response", "responseCode", "purchase", "total","transactionType","vat"};

        ArrayList<String> fields = new ArrayList<>();

        Collections.addAll(fields, field);

        ArrayList<String> ibetaFields = (ArrayList<String>) fields.clone();
        ArrayList<String> paycentreFields = (ArrayList<String>) fields.clone();

        paycentreFields.remove("createdOn");
        paycentreFields.remove("email");
        paycentreFields.remove("username");
        paycentreFields.remove("cardNo");
        paycentreFields.remove("response");
        paycentreFields.remove("responseCode");
        paycentreFields.remove("purchase");
        paycentreFields.remove("transactionType");

        ibetaFields.remove("posAccount");
        ibetaFields.remove("amount");
        ibetaFields.remove("stampDuty");
        ibetaFields.remove("agentCut");
        ibetaFields.remove("pan");
        ibetaFields.remove("appTime");
        ibetaFields.remove("vat");
        ibetaFields.remove("version");
        ibetaFields.remove("statusCode");
        ibetaFields.remove("statusMessage");
        ibetaFields.remove("sequenceNo");
        ibetaFields.remove("transactionTypeId");

        // Fill in required fields
        HashMap<String, ArrayList<String>> withMap = new HashMap<>();
        withMap.put("ibeta", ibetaFields);
        withMap.put("paycentre", paycentreFields);

        requiredMap.put(Transaction.Type.FUNDPURSE, withMap);
        requiredMap.put(Transaction.Type.WITHDRAWAL, withMap);
    }

    private static JSONObject buildRequestBody(Transaction transaction, ArrayList<String> requiredFields) {

        JSONObject requestBody = new JSONObject();
        JSONObject params = transaction.getData();

        for (String rField : requiredFields) {
            Object value = JSONObject.NULL;

            if (params.has(rField)) {
                value = params.opt(rField);
            } else {
                switch (rField) {
                    case Transaction.KEY_TRANSID:
                        value = transaction.getTransactionId();
                        break;

                    case Transaction.KEY_TRANSTYPE:
                        value = transaction.getTransactionType();
                        break;


                    case Transaction.KEY_AMOUNT:
                        value = transaction.getAmount();
                        break;


                    case Transaction.KEY_TOTAL:
                        value = transaction.getTotal();
                        break;


                    case Transaction.KEY_CONVFEE:
                        value = transaction.getConvenienceFee();
                        break;


                    case Transaction.KEY_AGENTCUT:
                        value = transaction.getAgentCut();
                        break;


                    case Transaction.KEY_POSACCOUNT:
                        value = transaction.getPosAccount();
                        break;


                    case Transaction.KEY_DATE:
                        value = transaction.getDate().toString();
                        break;


                    case Transaction.KEY_VERSION:
                        value = transaction.getVersion();
                        break;
                }
            }

            try {
                requestBody.put(rField, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return requestBody;
    }

    public void processUnsyncedTransaction(final Context context, final JobService jobService, final JobParameters params) {

        transactionProcessor = new Thread(new Runnable(){

            @Override
            public void run() {
                Iterator<Transaction> iterator = SyncService.getUnsyncedTransactions(context, true);

                while(iterator.hasNext()) {
                    Transaction transaction = iterator.next();
                    Log.d("SYNC TRANS", String.valueOf(transaction.getId()));
                    syncTransaction(context, transaction, false);
                }

                jobService.jobFinished(params, requiresSync(context));
            }
        });

        transactionProcessor.start();
    }

    public static boolean requiresSync(Context context) {
        Iterator<Transaction> iterator = getUnsyncedTransactions(context, false);
        boolean requiresSync = iterator.hasNext();
        DatabaseManager.getInstance().closeDatabase();
        return requiresSync;
    }


    public static boolean shouldDoManualSync() {
        DatabaseIterator<Transaction> iterator = repo.getCardTransactions(true);
        boolean shouldSync = iterator.hasNext();
        iterator.close();
        return shouldSync;
    }

    public static void doManualSync(Context context,ResponseProcessorInterface<String> responseProcessorInterface) {
        DatabaseIterator<Transaction> iterator = repo.getCardTransactions(true);

        while(iterator.hasNext()) {
            syncTransaction(context, iterator.next());
        }
        responseProcessorInterface.processResponse("finished");
    }

    public static Iterator<Transaction> getUnsyncedTransactions(Context context, boolean onlyPending) {
        TransactionRepo repo = new TransactionRepo();
        return repo.getUnsyncedTransactions(onlyPending);
    }

    public static void scheduleSync(Context context) {
        scheduleSync(context, JOB_ID);
    }

    public static void scheduleSync(Context context, int jobId) {
        Log.d(TAG, "Scheduling job!");
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        JobInfo jobInfo = createJob(context, jobId);
        List<JobInfo> pendingJobs = scheduler.getAllPendingJobs();

        if(pendingJobs.isEmpty()) {
            scheduleJob(scheduler, jobInfo);
            return;
        } else{
            boolean jobRunning = false;
            for(JobInfo jInfo: pendingJobs) {
                Log.d(TAG, String.valueOf(jInfo.getId()));
                if(jInfo.getId() == JOB_ID) {
                    jobRunning = true;
                    Log.d(TAG, "Job currently running!");
//                    break;
                }
            }

            if(!jobRunning) {
                scheduler.schedule(jobInfo);
            }
        }
    }

    public static int scheduleJob(JobScheduler scheduler, JobInfo jobInfo) {
        int result = scheduler.schedule(jobInfo);
        Log.d(TAG, String.format("Job %sscheduled successfully!", result == JobScheduler.RESULT_FAILURE? "not ": ""));
        return result;
    }

    public static JobInfo createJob(Context context, int jobId) {
        ComponentName serviceName = new ComponentName(context, SyncService.class);
        JobInfo.Builder jobBuilder = new JobInfo.Builder(JOB_ID, serviceName)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        //.setBackoffCriteria(1 * 60 * 1000, JobInfo.BACKOFF_POLICY_LINEAR);

        return jobBuilder.build();
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        processUnsyncedTransaction(getApplicationContext(), this, params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if(transactionProcessor != null) {
            transactionProcessor.interrupt();
            return true;
        }

        return false;
    }
}
