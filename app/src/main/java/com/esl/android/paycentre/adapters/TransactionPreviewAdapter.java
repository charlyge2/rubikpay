package com.esl.android.paycentre.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.ArrayList;
import java.util.List;

public class TransactionPreviewAdapter extends ArrayAdapter<TransactionPreviewList> {

    private Context mcontext;

    private List<TransactionPreviewList> transactionPreviewList = new ArrayList<>();

    public TransactionPreviewAdapter(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<TransactionPreviewList> list) {
        super(context, 0 , list);
        mcontext = context;
        transactionPreviewList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mcontext).inflate(R.layout.transaction_previe_template,parent,false);

        TransactionPreviewList transactionPreview = transactionPreviewList.get(position);

        TextView key = (TextView) listItem.findViewById(R.id.transactionKey);
        TextView value = (TextView) listItem.findViewById(R.id.transactionValue);
        key.setText(transactionPreview.getKey());
        value.setText(transactionPreview.getValue());

        if(transactionPreview.getPage().equals("receipt")){
            key.setTextSize(TypedValue.COMPLEX_UNIT_SP,13);
            value.setTextSize(TypedValue.COMPLEX_UNIT_SP,13);
            value.setText(transactionPreview.getValue());
            key.setText(transactionPreview.getKey());
        }

        return listItem;
    }

}
