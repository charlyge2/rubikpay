package com.esl.android.paycentre.constants;

public class Constant {

    public static final String CONTENT_TYPE = "application/json; charset=utf-8";

    public static final String HEADER_CONTENT_TYPE = "Content-Type";

    //    ACTIVATION ENDPOINTS
    public static final String ACTIVATION_URL = "https://paycentre.paypad.com.ng/api/v3/terminal/activate";

    public static final String AUTHENTICATION_URL = "https://paycentre.paypad.com.ng/api/v3/auth";

    public static final String POS_ACCOUNT_URL = "https://paycentre.paypad.com.ng/api/v3/createpos";

    public static final String FETCH_POS_ACCOUNT_URL = "https://paycentre.paypad.com.ng/api/v3/posaccount";

    public static final String CHANGE_POS_PIN_URL = "https://paycentre.paypad.com.ng/api/v3/posaccount/update";

    public static final String CHANGE_PASSWORD_URL = "https://paycentre.paypad.com.ng/api/v3/changepassword";

    //    FUNDS TRANSFER ENDPOINTS
    public static final String ACCOUNT_NAME_URL = "https://paycentre.paypad.com.ng/api/v3/transfer/inquiry/";

    public static final String FUNDS_TRANSFER_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v3/transfer/fund";

    public static final String FUNDS_REQUERY_URL = "https://paycentre.paypad.com.ng/api/v3/transaction/detail";

    // PURSE ENDPOINTS
    public static final String PURSE_BALANCE_URL = "https://paycentre.paypad.com.ng/api/v3/purse/balance";

    public static final String VERIFY_AGENT_URL = "https://paycentre.paypad.com.ng/api/v3/transfer/agent/verify";

    public static final String AGENT_TRANSFER_URL = "https://paycentre.paypad.com.ng/api/v3/transfer";

    //    CARD TRANSACTION ENDPOINT
    public static final String LOG_CARD_TRANSACTION_URL = "https://paycentre.paypad.com.ng/api/v3/logCardTransaction";

    // IBETA SYNC ENDPOINT
    public static final String IBETA_TRANSACTION_URL = "https://ibeta.paypad.com.ng/paypad/webapi/v2/logtransactions";

    //    CONVENIENCE FEE URL
    public static final String CONVENIENCE_FEE_URL = "https://paycentre.paypad.com.ng/api/v3/convenience";

    //    DSTV ENDPOINTS
    public static final String DSTV_INQUIRY_URL = "https://paycentre.paypad.com.ng/api/v3/dstv/inquiry";

    public static final String UNIFIED_DSTV_URL = "https://paycentre.paypad.com.ng/api/v3/unified/dstv/inquiry";

    public static final String DSTV_PRODUCT_AND_ADDON_URL = "https://paycentre.paypad.com.ng/api/v3/dstv/product";

    public static final String DSTV_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v3/dstv/advice";

    //    GOTV ENDPOINTS
    public static final String GOTV_INQUIRY_URL = "https://paycentre.paypad.com.ng/api/v3/gotv/inquiry";

    public static final String UNIFIED_GOTV_URL = "https://paycentre.paypad.com.ng/api/v3/unified/gotv/inquiry";

    public static final String GOTV_PLAN_URL = "https://paycentre.paypad.com.ng/api/v3/gotv/product";

    public static final String GOTV_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v3/gotv/advice";

    //    PAYU ENDPOINTS
    public static final String PAYU_PAYMENT = "https://paycentre.paypad.com.ng/api/v3/payu/payment";

    public static final String PAYU_UPGRADE = "https://paycentre.paypad.com.ng/api/v3/payu/upgrade";

    //    STARTIMES URL
    public static final String STARTIMES_INQUIRY_URL = "https://paycentre.paypad.com.ng/api/v3/startimes/inquiry";

    public static final String STARTIMES_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v3/startimes/advice";

// FRSC

    public static final String FRSC_INQUIRY_URL = "https://paycentre.paypad.com.ng/api/v3/frsc/reference";

    public static final String FRSC_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v3/frsc/pay";

    // LOAN

    public static final String ELIGIBILITY_URL = "https://paycentre.paypad.com.ng/api/v3/loan/checkeligibility";

    public static final String PICTURE_PLACEHOLDER = "http://placehold.it/300x300";

    public static final String LOAN_URL = "https://paycentre.paypad.com.ng/api/v3/loan/request";

    // PUSH NOTIFICATIONS ENDPOINT

    public static final String ONE_SIGNAL_APP_ID = "ffadb1a9-b314-4494-b663-0bc40fc18c1d";

    public static final String NEW_ONE_SIGNAL_APP_ID = "351526fd-4f32-425f-8101-92b406249bcf";

    public static final String ANONYMOUS_APP_ID = "bdd6c22b-baf7-4b6c-8dc8-3e4e0e7a3d3b";

    public static final String PLAYER_ID_URL = "https://paycentre.paypad.com.ng/api/v3/agent/playerid";

    // AIRTIME VENDING
    public static final String VAS_LIST_URL = "https://paycentre.paypad.com.ng/api/v3/airtime/fetch";

    public static final String VAS_PAY_URL = "https://paycentre.paypad.com.ng/api/v3/airtime/vend";

    // MISCELLANOUS

    public static final String TIN_PAYMENT_URL = "https://paycentre.paypad.com.ng:8000/api/v2/billpayment/tin";

    public static final String KEYS_REDOWNLOAD_URL = "https://paycentre.paypad.com.ng/api/v4/terminal/reactivate";

    public static final String SERVER_TIME_URL = "https://paycentre.paypad.com.ng/api/v3/time";

    public static final String PLAYSTORE_VERSIONING_URL = "https://paycentre.paypad.com.ng/api/v3/app/version";

    public static final String PORT_CHANGE_URL = "https://logger.paypad.com.ng/nibssportchange/getparams";

    public static final String TEST_SERVER = "http://206.189.204.54:8000";

    public static final String LIVE_SERVER = "https://paycentre.paypad.com.ng";

    public static final String APP_VERSION = "2.0";

    public static final String selectedbank = "ESL";

    public static final String INTERCOM_API_KEY = "android_sdk-90bcae0b73d7ddca11dec0b24028f56e4c6e717a";

    public static final String INTERCOM_APP_ID = "u4o9lizg";

    public static final String TERMS_AND_CONDITIONS_URL = "https://paycentre-terms.firebaseapp.com/";

    public static final String LOAN_TERMS_AND_CONDITIONS_URL = "https://paycentre-loans-terms.firebaseapp.com/";

    // WITHDRAWAL LIMITS

    public static final double WITHDRAWAL_LIMIT = 41000.00;

   // public static final double WITHDRAWAL_LIMIT = 41000.00;

    public static final double AGENT_FEE_LIMIT = 2000.00;

    public static final double WITHDRAWAL_PLUS_AGENT_FEE_LIMIT = 41000.00;

    // New bill payment API's
    
    //thor.paypad.com.ng:8000

    // NEW BILLS ENDPOINTS
//    public static final String IKEDC_INQUIRY_URL = "https://paycentre.paypad.com.ng/api/v4/ikedc/reference/";
//
//    public static final String IKEDC_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v4/ikedc/vend";
//
//    public static final String EKEDC_INQUIRY_URL = "https://paycentre.paypad.com.ng/api/v4/ekedc/reference/";
//
//    public static final String EKEDC_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v4/ekedc/vend";
//
//    public static final String IBEDC_INQUIRY_URL = "https://paycentre.paypad.com.ng/api/v4/ibedc/reference/";
//
//    public static final String IBEDC_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v4/ibedc/vend";

    public static final String WAEC_INQUIRY_URL = "https://paycentre.paypad.com.ng/api/v4/waec/scratchcard/denomination/";

    public static final String WAEC_PAYMENT_URL = "https://paycentre.paypad.com.ng/api/v4/waec/scratchcard/vend";

    // INSURANCE ENDPOINTS

    public static final String INSURANCE_SUBSCRIPTION = "https://paycentre.paypad.com.ng/api/v4/insurance/subscribe";

    public static final String INSURANCE_UNSUBSCRIPTION = "https://paycentre.paypad.com.ng/api/v4/insurance/unsubscribe";


}

