package com.esl.android.paycentre.activities.insurance;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.constants.Constant;
import com.esl.android.paycentre.interfaces.ResponseProcessorInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class InsuranceActivity extends AppCompatActivity implements View.OnClickListener {

    public ImageView homeIcon;

    public ImageView backIcon;

    public static ImageView statusImage;

    private static ProgressDialog progressDialog;

    private static ToggleButton toggle;

    private static TextView status;

    public static Button proceed;

    public static SharedPreferenceManager sharedPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance);

        statusImage = findViewById(R.id.statusImage);
        //homeIcon = findViewById(R.id.homeeIcon);
        backIcon = findViewById(R.id.backIcon);
        status = findViewById(R.id.subscriptionStatusIndicator);

        //homeIcon.setOnClickListener(this);
        backIcon.setOnClickListener(this);

        proceed = findViewById(R.id.proceedButton);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.getText().toString().equals("OFF")) {
                    insuranceProcessor(InsuranceActivity.this, Constant.INSURANCE_SUBSCRIPTION, "SUBSCRIBE");
                } else if(status.getText().toString().equals("ON")){
                    insuranceProcessor(InsuranceActivity.this, Constant.INSURANCE_UNSUBSCRIPTION, "UNSUBSCRIBE");
                }
            }
        });

        String subsribed = sharedPreferenceManager.getString(getApplicationContext(),"subscription_status","false");

        if(subsribed.equals("false")){
            status.setText("OFF");
            status.setTextColor(Color.parseColor("#B6042F"));
            proceed.setBackground(getResources().getDrawable(R.drawable.rounded_unsubscribed_insurance));
            proceed.setText("Subscribe");
            statusImage.setImageResource(R.drawable.ic_uncheck);
        }else{
            status.setText("ON");
            status.setTextColor(Color.parseColor("#00B653"));
            proceed.setBackground(getResources().getDrawable(R.drawable.rounded_btn_account_type));
            proceed.setText("Unsubscribe");
            statusImage.setImageResource(R.drawable.ic_check);
        }

    }

    @Override
    public void onClick(View v) {
        Intent myIntent = null;
        switch (v.getId()) {
            case R.id.backIcon:
                myIntent = new Intent(this, DashboardActivity.class);
                startActivity(myIntent);
                break;
        }
    }

    public static void insuranceProcessor(final Context context, String url, String type) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Processing....");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        ResponseProcessorInterface<JSONObject> subscriptionProcessor = new ResponseProcessorInterface<JSONObject>() {
            @Override
            public void processResponse(JSONObject response) {
                progressDialog.dismiss();

                Log.d("SUBSCRIPTION_RESPONSE", String.valueOf(response));
                try {
                    JSONObject datum = response.getJSONObject("data");

                    String responseCode = datum.optString("responseCode");
                    String responseMessage = datum.optString("responseMessage");

                    if ("00".equals(responseCode)) {
                        Alert.showSuccess(context, responseMessage);
//                        status.setText("ON");
//                        status.setTextColor(Color.parseColor("#00B653"));
//                        proceed.setBackground(context.getResources().getDrawable(R.drawable.rounded_btn_account_type));
//                        proceed.setText("Unsubscribe");
//                        statusImage.setImageResource(R.drawable.ic_check);
                        sharedPreferenceManager.save(context,"subscription_status","true");
                    } else {
                        Alert.showFailed(context, responseMessage);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        ResponseProcessorInterface<JSONObject> unSubscriptionProcessor = new ResponseProcessorInterface<JSONObject>() {
            @Override
            public void processResponse(JSONObject response) {
                progressDialog.dismiss();


                Log.d("UNSUBSCRIPTION_RESPONSE", String.valueOf(response));

                try {
                    JSONObject datum = response.getJSONObject("data");

                    String responseCode = datum.optString("responseCode");
                    String responseMessage = datum.optString("responseMessage");

                    if ("00".equals(responseCode)) {
                        Alert.showSuccess(context, responseMessage);
//                        status.setText("OFF");
//                        status.setTextColor(Color.parseColor("#B6042F"));
//                        proceed.setBackground(context.getResources().getDrawable(R.drawable.rounded_unsubscribed_insurance));
//                        proceed.setText("Subscribe");
//                        statusImage.setImageResource(R.drawable.ic_uncheck);
                        sharedPreferenceManager.save(context,"subscription_status","false");
                    } else {
                        Alert.showSuccess(context, responseMessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        switch (type) {
            case "SUBSCRIBE":
                HttpService.makeGet(context, url, new HashMap<String, String>(), new HashMap<String, String>(), true, subscriptionProcessor);
                break;
            case "UNSUBSCRIBE":
                HttpService.makeGet(context, url, new HashMap<String, String>(), new HashMap<String, String>(), true, unSubscriptionProcessor);
                break;
        }

    }
}
