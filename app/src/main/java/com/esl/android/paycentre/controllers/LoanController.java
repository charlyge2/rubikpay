package com.esl.android.paycentre.controllers;

import android.content.Context;
import android.text.TextUtils;

import com.esl.android.paycentre.interfaces.LoanEligibilityInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;

import org.json.JSONObject;

public class LoanController implements LoanEligibilityInterface {

    public static String validateNewLoanApplication(Context context, JSONObject params){

        String result = "";

        try {

            String meansOfId = params.getString("meansOfId");
            String issueDate = params.getString("issueDate");
            String expiryDate = params.getString("expiryDate");
            String newLoanAmount = params.getString("newLoanAmount");

            if(TextUtils.isEmpty(newLoanAmount) || TextUtils.isEmpty(meansOfId)
            ){
                result = "empty";
            }else {
                result = "valid";

            }

        }catch (Exception e){
            e.printStackTrace();
            result = "failed";
        }

        return result;

    }

    public static String validateExistingLoanApplication(Context context,JSONObject params){
        return "";
    }

    public static void checkLoanEligibility(Context context){
        HttpService.checkLoanEligibility(context);
    }

    @Override
    public void sendLoanEligibilityDetails(JSONObject params) {

    }
}
