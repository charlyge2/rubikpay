package com.esl.android.paycentre.activities.billsPayment.gotv;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.adapters.ProductAdapter;
import com.esl.android.paycentre.adapters.ProductModel;
import com.esl.android.paycentre.interfaces.ConvenienceFeeInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.esl.android.paycentre.utils.TransactionFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class GotvProductsActivity extends BaseActivity implements ConvenienceFeeInterface {

    //a list to store all the products
    private List<ProductModel> productList;
    //the recyclerview
    private RecyclerView recyclerView;

    private String price;

    private String code;

    private ArrayList<String> planCodeArray;

    private DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    private String posAccountUsername;

    SharedPreferenceManager sharedPreferenceManager;

    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Step 4 of 4: Finally call getTag() on the view.
            // This viewHolder will have all required values.
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();

            int position = viewHolder.getAdapterPosition();

            ProductModel thisItem = productList.get(position);

            String plan = thisItem.getPlan();
            price = thisItem.getPrice();
            code = thisItem.getPlanCode();

            System.out.println(Double.valueOf(price.replaceAll("[₦,]", "")));

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GotvProductsActivity.this);
            alertDialogBuilder.setTitle("Transaction Details");
            alertDialogBuilder
                    .setMessage("\nGOTV PLAN PRICE : " +price)
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {

                                        JSONObject params = new JSONObject();

                                        params.put("version", MainActivity.appVersionUrl);
                                        params.put("cardNumber", GotvNumberActivity.cardNumber);
                                        params.put("productCode",code);
                                        params.put("addOns",new JSONArray(planCodeArray));
                                        params.put("amount",String.valueOf(Double.valueOf(price.replaceAll("[₦,]", "")).intValue()));
                                        params.put("posAccount",posAccountUsername);
                                        params.put("convenienceFee","0");
                                        params.put("invoicePeriod", GotvNumberActivity.gotvInvoicePeriod);
                                        params.put("customerName", GotvNumberActivity.gotvCustomerName);
                                        params.put("customerNumber", GotvNumberActivity.gotvCustomerNumber);
                                        params.put("transactionType","gotv");
                                        params.put("transactionId", TransactionFormatter.generateTransactionId());
                                        params.put("appTime", TransactionFormatter.generateAppTime());

                                        Log.d("params", String.valueOf(params));

                                        String url = MainActivity.gotvPaymentUrl;

                                        String type = "gotv";

                                        HttpService.makePayment(GotvProductsActivity.this,params,url,type);


                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            })
                    .setNegativeButton("CANCEL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gotv_products);

        recyclerView = (RecyclerView) findViewById(R.id.gotvRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        posAccountUsername = sharedPreferenceManager.getString(getApplicationContext(), "posAccountUsername", "");

        ProductAdapter.setOnItemClickListener(onItemClickListener);

        Intent intent = getIntent();
        String products = intent.getStringExtra("products");

        JSONArray jsonArr = new JSONArray();

        productList = new ArrayList<>();

        try {

            jsonArr = new JSONArray(products);

            for (int i = 0; i < jsonArr.length(); i++)
            {
                JSONObject jsonObj = new JSONObject();

                try {

                    String plan = jsonArr.getJSONObject(i).getString("name");
                    String price = jsonArr.getJSONObject(i).getString("price");
                    String code = jsonArr.getJSONObject(i).getString("code");

                    double formattedPrice = Double.parseDouble(price);

                    String formattedString = "₦" +formatter.format(formattedPrice);

                    productList.add(
                            new ProductModel(
                                    i,
                                    plan,
                                    code,
                                    formattedString));

                    //creating recyclerview adapter
                    ProductAdapter adapter = new ProductAdapter(this, productList);

                    //setting adapter to recyclerview
                    recyclerView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println(jsonObj);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendConvenienceFee(String fee) {

    }

    @Override
    public void sendConvenienceFee(String fee, String StampDuty, boolean showStampDuty, String vat, boolean showVat) {
        HttpService.progressDialog.dismiss();
//
//        Intent intent = new Intent(getApplicationContext(), GotvConfirmActivity.class);
//        intent.putExtra("planCode",code);
//        intent.putExtra("totalAmount",String.valueOf(price));
//        intent.putExtra("cFee",String.valueOf(fee));
//
//        startActivity(intent);
    }


}
