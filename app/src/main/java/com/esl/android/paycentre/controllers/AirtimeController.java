package com.esl.android.paycentre.controllers;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONObject;

public class AirtimeController {

    public static String validateAmount(Context context, JSONObject params){

        try {
            String amount = params.getString("phone");
            String phone = params.getString("amount");
            String network = params.getString("network");

            if(TextUtils.isEmpty(amount) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(network)){
                return "Please fill all fields";
            }else if(network.equals("Select Network")){
                return "Please select a network";
            } else{
                 return "valid";
            }
        }catch (Exception e){
            e.printStackTrace();
            return "failed";
        }
    }
}
