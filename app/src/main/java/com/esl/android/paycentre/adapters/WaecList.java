package com.esl.android.paycentre.adapters;

public class WaecList {

    private String amount;

    private String count;

    public WaecList(String count, String amount) {
        this.amount = amount;
        this.count = count;
    }


    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
