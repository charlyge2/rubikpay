package com.esl.android.paycentre.cardreader.serializers;


public enum EmvTerminalDecision {
	EmvTerminalForceOnline,
	EmvTerminalForceDecline,
	EmvTerminalNoDecision

}
