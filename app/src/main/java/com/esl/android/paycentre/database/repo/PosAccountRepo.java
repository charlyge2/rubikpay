package com.esl.android.paycentre.database.repo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.model.Transaction;

import java.util.Iterator;

public class PosAccountRepo {

    public static String createTable(){
        return "CREATE TABLE " + PosAccount.TABLE  + "("
                + PosAccount.KEY_ID  + " INTEGER PRIMARY KEY,"
                + PosAccount.KEY_USERNAME + " TEXT UNIQUE,"
                + PosAccount.KEY_FIRSTNAME + " TEXT,"
                + PosAccount.KEY_LASTNAME + " TEXT,"
                + PosAccount.KEY_PASSWORD + " TEXT,"
                + PosAccount.KEY_ACCOUNTID + " TEXT)";
    }

    public int insert(PosAccount posAccount) {
        int insertId;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        ContentValues values = new ContentValues();

        values.put(PosAccount.KEY_USERNAME, posAccount.getUsername());
        values.put(PosAccount.KEY_FIRSTNAME, posAccount.getFirstname());
        values.put(PosAccount.KEY_LASTNAME, posAccount.getLastname());
        values.put(PosAccount.KEY_PASSWORD, posAccount.getPassword());
        values.put(PosAccount.KEY_ACCOUNTID, posAccount.getPosAccountId());

        // Inserting Row
        insertId = (int) db.insert(PosAccount.TABLE, null, values);
        DatabaseManager.getInstance().closeDatabase();

        return insertId;

    }

    public int insertOrUpdate(PosAccount posAccount) {
        int id;

        if(this.getPosAccount(posAccount.getUsername()) != null) {
            id = this.update(posAccount);
        }else {
            id = this.insert(posAccount);
        }

        return id;

    }

    public int update(PosAccount posAccount) {

        int result;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

        ContentValues values = new ContentValues();

        values.put(PosAccount.KEY_PASSWORD, posAccount.getPassword());
        values.put(PosAccount.KEY_FIRSTNAME, posAccount.getFirstname());
        values.put(PosAccount.KEY_LASTNAME, posAccount.getLastname());

        // Update Row
        result = (int) db.update(PosAccount.TABLE, values, posAccount.KEY_USERNAME+ " = ? ",new String[]{posAccount.getUsername()});
        DatabaseManager.getInstance().closeDatabase();

        return result;

    }

    public void delete(PosAccount posAccount) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        db.delete(PosAccount.TABLE,PosAccount.KEY_USERNAME+ " = ? ",new String[]{posAccount.getUsername()});
        DatabaseManager.getInstance().closeDatabase();
    }

    public PosAccount getPosAccount(String username) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        PosAccount posAccount = null;
        Cursor cursor = db.query(PosAccount.TABLE, null, PosAccount.KEY_USERNAME + " = ?", new String[]{ username }, null, null, null);

        if (cursor.moveToFirst()) {
            posAccount = new PosAccount();
            posAccount.setId(cursor.getInt(cursor.getColumnIndex(PosAccount.KEY_ID)));
            posAccount.setFirstname(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_FIRSTNAME)));
            posAccount.setLastname(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_LASTNAME)));
            posAccount.setPassword(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_PASSWORD)));
            posAccount.setPosAccountId(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_ACCOUNTID)));
            posAccount.setUsername(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_PASSWORD)));
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();
        return posAccount;
    }

    public PosAccount authenticateAccount(String username, String password) {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        PosAccount posAccount = null;

        Cursor cursor = db.query(PosAccount.TABLE, null, PosAccount.KEY_USERNAME + "=? AND " + PosAccount.KEY_PASSWORD + "=?", new String[]{username, password}, null, null, null);

        if(cursor.moveToFirst()) {
            posAccount = new PosAccount();
            posAccount.setId(cursor.getInt(cursor.getColumnIndex(PosAccount.KEY_ID)));
            posAccount.setUsername(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_USERNAME)));
            posAccount.setFirstname(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_FIRSTNAME)));
            posAccount.setLastname(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_LASTNAME)));
            posAccount.setPosAccountId(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_ACCOUNTID)));
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        if(posAccount == null) {
            throw new RuntimeException("Invalid Credentials!");
        }

        return posAccount;
    }

    public Iterator<PosAccount> getPosAccounts() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        final Cursor cursor = db.query(PosAccount.TABLE, null, null, null, null, null, null);
        
        return new Iterator<PosAccount>() {

            boolean hasNext = !this.isEmpty();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public PosAccount next() {
                return this.getPosAccount();
            }

            private boolean isEmpty() {
                return !cursor.moveToFirst();
            }

            private PosAccount getPosAccount() {
                if(hasNext) {
                    PosAccount posAccount = new PosAccount();
                    posAccount.setId(cursor.getInt(cursor.getColumnIndex(PosAccount.KEY_ID)));
                    posAccount.setUsername(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_USERNAME)));
                    posAccount.setFirstname(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_FIRSTNAME)));
                    posAccount.setLastname(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_LASTNAME)));
                    posAccount.setPosAccountId(cursor.getString(cursor.getColumnIndex(PosAccount.KEY_ACCOUNTID)));

                    if(!(hasNext = cursor.moveToNext())) {
                        cursor.close();
                        DatabaseManager.getInstance().closeDatabase();
                    }

                    return posAccount;
                }

                return null;
            }

        };
    }

}