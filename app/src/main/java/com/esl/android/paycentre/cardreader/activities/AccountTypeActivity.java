package com.esl.android.paycentre.cardreader.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.ResultReceiver;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.activities.withdrawal.WithdrawalAmountActivity;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.cardreader.network.HSMClient;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;
import com.esl.android.paycentre.utils.Alert;

public class AccountTypeActivity extends AppCompatActivity {

    private Button savingsButton, currentButton, creditButton;
    private RadioButton savingsRadioButton, currentRadioButton, creditRadioButton;
    private Button proceedButton;
    private TextView title;
    private ProgressDialog mProgressDialog;

    private PinpadFacade pinpadFacade;
    private int downloaderCounter = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_type);

        title = findViewById(R.id.titleTextView);

        init();

        savingsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                selectAccount(1);

            }
        });

        savingsRadioButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                selectAccount(1);


            }
        });

        currentButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                selectAccount(2);

            }
        });

        currentRadioButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                selectAccount(2);


            }
        });

        creditButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                selectAccount(3);

            }
        });

        creditRadioButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                selectAccount(3);


            }
        });

        if ("Fund Purse".equalsIgnoreCase(Globals.withdrawalType)) {

            title.setText("Fund Purse");

            Globals.withdrawalType = "FUND PURSE";
            Globals.transactionTypeId = 10;
        }

        proceedButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!savingsRadioButton.isChecked() && !currentRadioButton.isChecked() && !creditRadioButton.isChecked()) {
                    Alert.showWarning(getApplicationContext(), "Please select account type");
                    return;
                } else if (savingsRadioButton.isChecked() || currentRadioButton.isChecked() || creditRadioButton.isChecked()) {

                    if (!Globals.isPinpadConnected) {

                        pinpadFacade.connect();
                        Globals.checkFinalConnection = true;

                    } else {

                        Intent intent = new Intent(getApplicationContext(), PinpadAnimationActivity.class);
                        startActivity(intent);
                        finish();

                    }
                }

            }
        });


    }

    private void init() {

        savingsButton = findViewById(R.id.savingsButton);
        currentButton = findViewById(R.id.currentButton);
        creditButton = findViewById(R.id.creditButton);

        savingsRadioButton = findViewById(R.id.savingsRadioButton);
        currentRadioButton = findViewById(R.id.currentRadioButton);
        creditRadioButton = findViewById(R.id.creditRadioButton);

        proceedButton = findViewById(R.id.proceedButton);

        pinpadFacade = new PinpadFacade(AccountTypeActivity.this);

        mProgressDialog = new ProgressDialog(AccountTypeActivity.this);

        launchPinpad();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

            pinpadFacade.onActivityResult(requestCode, resultCode, data);
    }

    private void selectAccount(int accountType) {

        if (accountType == 1) {

            savingsRadioButton.setChecked(true);
            currentRadioButton.setChecked(false);
            creditRadioButton.setChecked(false);

            Globals.accountType = "10";


        } else if (accountType == 2) {

            currentRadioButton.setChecked(true);
            savingsRadioButton.setChecked(false);
            creditRadioButton.setChecked(false);

            Globals.accountType = "20";

        } else if (accountType == 3) {

            creditRadioButton.setChecked(true);
            savingsRadioButton.setChecked(false);
            currentRadioButton.setChecked(false);

            Globals.accountType = "30";

        }


        if (Globals.isPinpadConnected) {

            if (!Globals.autoConnect && !Globals.isSetup) {
                startDownloadKeys();
            }
        }


    }

    private void launchPinpad() {


        if (!Globals.isPinpadConnected) {// pinpad not connected

            pinpadFacade.connect();


        } else { // if pinpad is connected

            // And if the connection came from Initialization and keys not yet
            // loaded
            if (!isKeyLoaded()) {

                pinpadFacade.doKeyLoad();

                String preferencesName = ReferenceList.preference;
                MiscUtils.initContext(getApplicationContext());

                MiscUtils.storeInSharedPreferences(preferencesName,
                        ReferenceList.loadKeys, true);
            }

            if (!Globals.autoConnect) { //check if pinpad auto connected and download keys if it did not and it was not first time


                if (!Globals.isSetup) {

                    startDownloadKeys();
                }
            }


        }

        if (isDownloadKeysNotificationOn()) {

            startDownloadKeys();

        }


    }

    private void startDownloadKeys() {
        Globals.redownloading = true;

        mProgressDialog = new ProgressDialog(AccountTypeActivity.this);
        mProgressDialog.setMessage("Downloading keys ............");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        Intent msgIntent = new Intent(getApplicationContext(), HSMClient.class);
        msgIntent.putExtra("receiver", new DownloadReceiver(new Handler()));
        startService(msgIntent);

    }

    private boolean isKeyLoaded() {
        MiscUtils.initContext(getApplicationContext());
        boolean isLoaded = MiscUtils.getFromSharedPreferences(
                ReferenceList.preference, ReferenceList.loadKeys, false);
        if (isLoaded) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isDownloadKeysNotificationOn() {

        MiscUtils.initContext(getApplicationContext());

        boolean isLoaded = MiscUtils.getFromSharedPreferences(
                ReferenceList.preference, ReferenceList.doKeyDownload, false);

        if (isLoaded) {

            return true;

        } else {

            return false;
        }

    }

    private class DownloadReceiver extends ResultReceiver {

        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if (resultCode == HSMClient.UPDATE_PROGRESS) {

                int progress = resultData.getInt("progress");
                mProgressDialog.setProgress(progress);

                mProgressDialog.dismiss();
                Alert.showSuccess(getApplicationContext(), "Initialization Successful");
                pinpadFacade.doKeyLoad();

                Globals.autoConnect = true;

                MiscUtils.initContext(getApplicationContext());
                String preferencesName = ReferenceList.preference;
                MiscUtils.storeInSharedPreferences(preferencesName,
                        ReferenceList.doKeyDownload, false);


            } else if (resultCode == HSMClient.UPDATE_FAILED) {

                int progress = resultData.getInt("progress");
                mProgressDialog.setProgress(progress);

                mProgressDialog.dismiss();

                String preferencesName = ReferenceList.preference;
                MiscUtils.initContext(getApplicationContext());
                MiscUtils.storeInSharedPreferences(preferencesName,
                        ReferenceList.loadKeys, false);

                if (downloaderCounter > 3) {

                    Alert.showFailed(getApplicationContext(), "Setup Failed, Try again Later");
                    MiscUtils.initContext(getApplicationContext());
                    MiscUtils.storeInSharedPreferences(preferencesName,
                            ReferenceList.loadKeys, false);

                } else {

                    showNewPinpadDialog();
                }

            }
        }
    }

    public void showNewPinpadDialog() {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(AccountTypeActivity.this);
        View promptView = layoutInflater.inflate(R.layout.new_pinpad_dialog,
                null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                AccountTypeActivity.this);
        alertDialogBuilder.setView(promptView);

        final TextView textView1 = (TextView) promptView
                .findViewById(R.id.textview1);
        final TextView textView2 = (TextView) promptView
                .findViewById(R.id.textview2);

        textView1.setText("Your new device did not set up");
        textView2.setText("Kindly retry later");

        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startDownloadKeys();
                        downloaderCounter += 1;
                        return;
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setBackgroundColor(Color.parseColor("#EEEEEE"));
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setBackgroundColor(Color.parseColor("#EEEEEE"));

    }

    public void moveBack(View view) {
        Intent intent = new Intent(getApplicationContext(), WithdrawalAmountActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }

}
