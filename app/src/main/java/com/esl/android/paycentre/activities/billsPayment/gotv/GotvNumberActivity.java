package com.esl.android.paycentre.activities.billsPayment.gotv;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.interfaces.NameLookupInterface;
import com.esl.android.paycentre.interfaces.PlanInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;

import org.json.JSONArray;
import org.json.JSONObject;

public class GotvNumberActivity extends BaseActivity implements NameLookupInterface, PlanInterface {

    private EditText gotvCardNumber;

    private Button gotvAmountBtn;

    public static String cardNumber;

    public static String gotvInvoicePeriod;

    public static String gotvCustomerName;

    public static String gotvCustomerNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gotv_number);

        gotvCardNumber = findViewById(R.id.gotvCardNumber);
        gotvAmountBtn = findViewById(R.id.gotvAmountBtn);

        gotvAmountBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cardNumber = gotvCardNumber.getText().toString();
                        if (TextUtils.isEmpty(cardNumber)) {
                            Alert.showWarning(getApplicationContext(), "Please enter gotv card number");
                            return;
                        }

                        try {
                            JSONObject params = new JSONObject();

                            params.put("cardNumber", cardNumber);

                            String url = MainActivity.gotvInquiryUrl;

                            HttpService.doNameLookup(GotvNumberActivity.this, params, url);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }

    @Override
    public void sendAccountDetails(String customerName, String invoicePeriod, String customerNumber) {

        gotvCustomerName = customerName;
        gotvCustomerNumber = customerNumber;
        gotvInvoicePeriod = invoicePeriod;

        HttpService.progressDialog.dismiss();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GotvNumberActivity.this);
        alertDialogBuilder.setTitle("Gotv Account Details");
        alertDialogBuilder
                .setMessage("\nNAME : " + customerName + "\n\n\nCARD NUMBER : " + cardNumber + "\n\n\nINVOICE PERIOD : " + invoicePeriod)
                .setCancelable(false)
                .setPositiveButton("PROCEED",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                String url = MainActivity.gotvPlanUrl;

                                HttpService.fetchPlans(GotvNumberActivity.this, url);

                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Alert.showInfo(getApplicationContext(), "CANCELLED");
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void sendPlans(JSONArray products) {

        HttpService.progressDialog.dismiss();

        Intent intent = new Intent(getApplicationContext(), GotvProductsActivity.class);
        intent.putExtra("products", products.toString());
        startActivity(intent);

    }

    public void goBack(View view) {
        Navigator.moveTo(GotvNumberActivity.this, "bills");
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(GotvNumberActivity.this);
    }
}
