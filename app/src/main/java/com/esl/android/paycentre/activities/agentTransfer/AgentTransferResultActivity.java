package com.esl.android.paycentre.activities.agentTransfer;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SaveTransferTransactions;

public class AgentTransferResultActivity extends BaseActivity {

    private ImageView responseImage;

    private TextView responseText;

    private Button homeBtn;

    private String responseCode;

    private String responseMessage;

    private String transactionId;

    private String amount;

    private String convenienceFee;

    private String posAccount;

    private String total;

    private String appTime;

    private String transactionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_transfer_result);

        responseImage = findViewById(R.id.agentTransferResponseImage);
        responseText = findViewById(R.id.agentTransferTransactionStatus);
        homeBtn = findViewById(R.id.agentTransferHomeBtn);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });

        responseCode = getIntent().getStringExtra("responseCode");
        responseMessage = getIntent().getStringExtra("responseMessage");
        amount = getIntent().getStringExtra("amount");
        transactionId = getIntent().getStringExtra("transactionId");
        convenienceFee = getIntent().getStringExtra("convenienceFee");
        posAccount = getIntent().getStringExtra("posAccount");
        total = getIntent().getStringExtra("total");
        appTime = getIntent().getStringExtra("appTime");

        if(responseCode.equals("00")){
            // Save to Database
            responseImage.setImageResource(R.drawable.ic_check);
            responseText.setText(responseMessage);
        }else {
            responseImage.setImageResource(R.drawable.ic_error);
            responseText.setText(responseMessage);
        }


        long check = SaveTransferTransactions.updateTransferTransactions(AgentTransferResultActivity.this, "", "", "", "", Globals.vat, "", transactionId, responseCode, responseMessage, amount, convenienceFee, Globals.agentTransferType.toUpperCase());

        if(check < 0){
            Alert.showFailed(getApplicationContext(), "Agent Transfer transaction could not be saved");
        }

    }


    @Override
    public void onBackPressed() {

    }
}
