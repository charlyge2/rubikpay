package com.esl.android.paycentre.activities.dispute;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.esl.android.paycentre.R;

public class DisputeDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispute_details);
    }
}
