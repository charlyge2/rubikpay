package com.esl.android.paycentre.interfaces;

public interface ConvenienceFeeInterface {
    public void sendConvenienceFee(String fee);
    public void sendConvenienceFee(String fee, String StampDuty, boolean showStampDuty,String vat ,boolean showVat);
}
