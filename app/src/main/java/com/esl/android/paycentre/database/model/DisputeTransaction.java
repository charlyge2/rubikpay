package com.esl.android.paycentre.database.model;

import java.util.Date;

public class DisputeTransaction {
    public static final String TAG = DisputeTransaction.class.getSimpleName();
    public static final String TABLE = "dispute";

    // Labels Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_TRANSID = "transactionId";
    public static final String KEY_NAME = "name";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_STATUS = "status";
    public static final String KEY_DATE = "date";

    private int id;
    private String transactionId;
    private String name;
    private String phone;
    private String status;
    private long date;

    public DisputeTransaction() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDate() {
        return new Date(date);
    }

    public void setDate(Date date) {
        this.date = date.getTime();
    }

    public long getDateLong() {
        return date;
    }

    public void setDateLong(long date) {
        this.date = date;
    }
}
