package com.esl.android.paycentre.activities;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.esl.android.paycentre.PaycentreApplication;
import com.esl.android.paycentre.activities.activation.PosLoginActivity;
import com.esl.android.paycentre.services.InactivityService;
import com.esl.android.paycentre.interfaces.InactivityTimerListener;


public class BaseActivity extends AppCompatActivity {

    static Handler handler;
    static Runnable r;
    static boolean loginOnResume = false;
    static InactivityService timer =  new InactivityService( 5 * 60 * 1000, new InactivityTimerListener() {
        @Override
        public void onTimerCompleted() {
            if(PaycentreApplication.isInBackground) {
                loginOnResume = true;
                return;
            }

            handler.post(r);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        handler = new Handler();
        r = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BaseActivity.this, PosLoginActivity.class);
                startActivity(intent);
            }
        };
        timer.start();
    }

    @Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        super.onUserInteraction();
        Log.d("TIMER ", "Interaction detected!");
        timer.restart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(loginOnResume) {
            handler.post(r);
            loginOnResume = false;
        }
    }

}
