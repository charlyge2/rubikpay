package com.esl.android.paycentre.utils;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;
import com.esl.android.paycentre.services.HttpService;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Display {

    private static SharedPreferenceManager sharedPreferenceManager;

    private static String uuid;

    public static int checkStatus(Context context) {

        String preferencesName = ReferenceList.preference;

        MiscUtils.initContext(context);
        String activated = MiscUtils.getFromSharedPreferences(preferencesName, ReferenceList.activated, "");
        String agreed =  MiscUtils.getFromSharedPreferences(preferencesName, ReferenceList.agreed, "");
        String walkthrough = sharedPreferenceManager.getString(context, "walkthrough", "");
        String keyDownloaded = sharedPreferenceManager.getString(context, "initialization", "");
        String isAdminLogin = sharedPreferenceManager.getString(context, "adminLogin");
        String isPosOperationsPage = sharedPreferenceManager.getString(context, "posOperationsPage");
        String isPosLogin = sharedPreferenceManager.getString(context, "posLoginPage");

        Log.d("DISPLAY","ACTIVATED: "+activated + "\nAGREED : "+agreed + "\nWalkthrough : "+walkthrough + "\nKeydownloaded: "+keyDownloaded +
               "\nisAdminLogin "+isAdminLogin + "\nisPosLogin "+isPosLogin + "\nisPosOperations"+isPosOperationsPage);

        if (!activated.equals("true")) {

            return 1; //activation

        } else {

            if (!walkthrough.equals("true")) {

                return 2;  //walkthrough

            } else {

                if (!agreed.equals("true")) {

                    return 3;  //T&C

                } else {

                    if (!keyDownloaded.equals("true")) {

                        return 2;  //walkthrough

                    } else {

                        if (!isAdminLogin.equals("true")) {

                            return 4; //POSAdmin

                        } else {

                            if(!isPosOperationsPage.equals("true")){

                                return 4; // Admin

                            }else{

                                return 5;

                            }
                        }
                    }
                }
            }
        }
    }

    public static String getPhoneIdentification(Context context){

        uuid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        return uuid;
    }

    public static void sendToErrorLog(String activity, String errorMessage, String stackTrace) {

        try {

            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

            String formattedDate = df.format(c);

            JSONObject requestBody = new JSONObject();

            requestBody.put("activity", activity);
            requestBody.put("date", formattedDate);
            requestBody.put("agent", Globals.phoneNumber + "||" + Globals.username);
            requestBody.put("error", errorMessage);
            requestBody.put("stackTrace", stackTrace);

           // HttpService.log(requestBody, MainActivity.logErrorUrl);

        }catch (Exception ex){

        }
    }



}
