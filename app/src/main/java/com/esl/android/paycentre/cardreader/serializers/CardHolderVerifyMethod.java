 package com.esl.android.paycentre.cardreader.serializers;

public enum CardHolderVerifyMethod {
	CHNone,
	CHOfflinePlainPin,
	CHOfflineEncipheredPin,
	CHOnlinePin,
	CHSignature
}
