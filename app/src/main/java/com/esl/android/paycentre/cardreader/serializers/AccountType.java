package com.esl.android.paycentre.cardreader.serializers;

public enum AccountType{

/*
00 Default unspecified
10 Savings account 
20 Check account 
30 Credit account 
40 Universal account 
50 Investment account 
60 Electronic purse account (default) 
91 Mortgage loan 
92 Installment loan 
*/
    Default("00"),
    Savings("10"),
    Check("20"),
    Credit("30"),
    Universal("40"),
    Investment("50"),
    ElectronicPurse("60"),
    Mortgage("91"),
    Installment("92");
    
    private AccountType(final String text) {
        this.text = text;
    }

    private final String text;

    @Override
    public String toString() {
        return text;
    }

}