package com.esl.android.paycentre.cardreader.serializers;


public class EmvCardData {
	
	private String aid;
	private String appLabel;
	private String pan;
	private String iin;
	private String cardHolderName;
	private String cardSequenceNo;
	private String track2;
	private String track1;
	private String cardExpiryDate;
	
	
	
	public String getCardExpiryDate() {
		return cardExpiryDate;
	}
	public void setCardExpiryDate(String cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getAppLabel() {
		return appLabel;
	}
	public void setAppLabel(String appLabel) {
		this.appLabel = appLabel;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getIin() {
		return iin;
	}
	public void setIin(String iin) {
		this.iin = iin;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
	public String getCardSequenceNo() {
		return cardSequenceNo;
	}
	public void setCardSequenceNo(String cardSequenceNo) {
		this.cardSequenceNo = cardSequenceNo;
	}
	public String getTrack2() {
		return track2;
	}
	public void setTrack2(String Track2) {
		this.track2 = Track2;
	}
	public String getTrack1() {
		return track1;
	}
	public void setTrack1(String track1) {
		this.track1 = track1;
	}
	
	


}
