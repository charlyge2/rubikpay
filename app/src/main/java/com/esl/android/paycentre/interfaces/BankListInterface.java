package com.esl.android.paycentre.interfaces;

import org.json.JSONObject;

public interface BankListInterface {
    public void send(JSONObject bankList);
}
