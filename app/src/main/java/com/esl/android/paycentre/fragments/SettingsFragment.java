package com.esl.android.paycentre.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import androidx.annotation.Nullable;
import androidx.preference.SwitchPreference;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.preference.Preference;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.cardreader.network.HSMClient;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.cardreader.utils.MiscUtils;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


public class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {

    private static final String TAG = "SettingsFragment";
    public static String hidePurseBalanceStatus;

    public static String subscriptionStatus;

    private ProgressDialog mProgressDialog;

    private PinpadFacade pinpadFacade;

    SharedPreferenceManager sharedPreferenceManager;

    private AlertDialog alertDialog = null;
    private int logIntoDbCount;

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings_preferences, rootKey);

        pinpadFacade = new PinpadFacade(getActivity());

        mProgressDialog = new ProgressDialog(getActivity());

        final String subsribed = sharedPreferenceManager.getString(getContext(), "subscription_status", "false");

        SwitchPreference preference = (SwitchPreference) findPreference("hide_purse_balance");
   //     final SwitchPreference insurance = (SwitchPreference) findPreference("agent_insurance");
        Preference redownloadKeys = findPreference("redownload_keys");
        Preference changePort = findPreference("change_port");

        Preference hidden_pin = findPreference("hidden_pin");
        hidden_pin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                logIntoDbCount++;
                if(logIntoDbCount>9){
                    logIntoDbCount=0;
                    openLogintoDb();
                }
                return true;
            }
        });

        changePort.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                return false;
            }
        });
        redownloadKeys.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                //SettingsController.processor(getContext(), Constant.KEYS_REDOWNLOAD_URL, "REDOWNLOAD_KEYS");

               new UpdateTerminalAsyncTask().execute(" https://rubikcube.paypad.com.ng/api/v1/terminal/reactivate");
                return true;
            }
        });
        preference.setOnPreferenceChangeListener(this);

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object o) {
        hidePurseBalanceStatus = o.toString();
        String agentName = sharedPreferenceManager.getString(getContext(), "agentName", "");
        if (hidePurseBalanceStatus.equals("true")) {
            DashboardActivity.setBalance(agentName);
            DashboardActivity.refreshIcon.setVisibility(View.GONE);
        } else {
            DashboardActivity.refreshIcon.setVisibility(View.VISIBLE);
            new DashboardActivity.fetchPurseBalance(getContext()).execute("dsss");
        }
        return true;
    }

    private void launchPinpad() {

        if (!Globals.isPinpadConnected) {// pinpad not connected

            pinpadFacade.connect();

            //keys is downloaded in Activty's onActivityResult

        } else { // if pinpad is connected

            startDownloadKeys();

        }

    }

    private void startDownloadKeys() {
        Globals.redownloading = true;

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Downloading Keys ............");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        Intent msgIntent = new Intent(getActivity(), HSMClient.class);
        msgIntent.putExtra("receiver", new DownloadReceiver(new Handler()));
        getActivity().startService(msgIntent);

    }

    private class DownloadReceiver extends ResultReceiver {

        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if (resultCode == HSMClient.UPDATE_PROGRESS) {

                int progress = resultData.getInt("progress");
                mProgressDialog.setProgress(progress);

                mProgressDialog.dismiss();
                Alert.showSuccess(getActivity(), "Initialization Successful");
                pinpadFacade.doKeyLoad();

                sharedPreferenceManager.save(getContext(), "initialization", "true");

                Globals.autoConnect = true;

                MiscUtils.initContext(getActivity());
                String preferencesName = ReferenceList.preference;
                MiscUtils.storeInSharedPreferences(preferencesName,
                        ReferenceList.doKeyDownload, false);


            } else if (resultCode == HSMClient.UPDATE_FAILED) {

                int progress = resultData.getInt("progress");
                mProgressDialog.setProgress(progress);

                sharedPreferenceManager.save(getContext(), "initialization", "false");

                mProgressDialog.dismiss();

                String preferencesName = ReferenceList.preference;
                MiscUtils.initContext(getActivity());
                MiscUtils.storeInSharedPreferences(preferencesName,
                        ReferenceList.loadKeys, false);

            }
        }
    }

//    @Override
//    public void onNothingSelected(AdapterView<?> arg0) {
//
//        purposeErrorTextView.setVisibility(View.VISIBLE);
//
//    }

    class UpdateTerminalAsyncTask extends AsyncTask<String, String, String> {
        private long result = 0;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Updating details ...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String urlString = params[0];

            String responseCode = "XX";

            try {
                TrustManager tm = new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain,
                                                   String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain,
                                                   String authType) throws CertificateException {
                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                };

                SSLContext sc;

                sc = SSLContext.getInstance("TLS");

                sc.init(null, new TrustManager[]{tm}, null);

                SSLSocketFactory ssf = sc.getSocketFactory();

                URL url = new URL(urlString);

                HttpsURLConnection conn = (HttpsURLConnection) url
                        .openConnection();
                conn.setSSLSocketFactory(ssf);

                conn.setRequestMethod("GET");
                conn.setRequestProperty("Authorization", ConfigurationClass.getAccessToken(getActivity()));
                conn.setRequestProperty("x-phone-uuid", MainActivity.getUuid(getContext()));
                conn.setRequestProperty("Content-type", "application/json");

                result = (long) conn.getResponseCode();

                System.out.print("result: " + result);

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                StringBuilder sb = new StringBuilder();

                String output;

                while ((output = br.readLine()) != null) {

                    sb.append(output);
                }
                Log.d(TAG, "output: " + sb.toString());
                JSONObject response = new JSONObject(sb.toString());

                String responseMessage = response.getString("responseMessage");
                responseCode = response.getString("responseCode");

                ReferenceList.config = getActivity().getSharedPreferences(
                        ReferenceList.preference, 0);

                SharedPreferences.Editor editor = ReferenceList.config.edit();

                if (responseCode.equals("00")) {

                    editor.putString(ReferenceList.terminalId, response.getJSONObject("data").getString("terminalId"));
                   // editor.putString(ReferenceList.terminalId, "204473FN");
                    editor.apply();  //ends here for card reader
                    Globals.terminalId = response.getJSONObject("data").getString("terminalId");// 204473FN
                  //  Globals.terminalId =  "204473FN";

                  //  sharedPreferenceManager.save(getActivity(), "terminalId", "204473FN");
                    sharedPreferenceManager.save(getActivity(), "terminalId", response.getJSONObject("data").getString("terminalId"));
                    sharedPreferenceManager.save(getActivity(), "businessName", response.getJSONObject("data").getString("businessName"));
                    sharedPreferenceManager.save(getActivity(), "agentName", response.getJSONObject("data").getString("agentName"));
                    sharedPreferenceManager.save(getActivity(), "bankId", response.getJSONObject("data").getString("bankId"));
                    sharedPreferenceManager.save(getActivity(), "merchantId", response.getJSONObject("data").getString("merchantId"));
                    sharedPreferenceManager.save(getActivity(), "username", response.getJSONObject("data").getString("username"));
                    sharedPreferenceManager.save(getActivity(), "merchantPhoneNumber", response.getJSONObject("data").getString("merchantPhoneNumber"));
                    sharedPreferenceManager.save(getActivity(), "email", response.getJSONObject("data").getString("email"));
                    sharedPreferenceManager.save(getActivity(), "memberId", response.getJSONObject("data").getString("memberId"));
                    sharedPreferenceManager.save(getActivity(), "agentPhoneNumber", response.getJSONObject("data").getString("agentPhoneNumber"));

                }

            } catch (Exception e) {

                e.printStackTrace();
            }


            return responseCode;

        }

        protected void OnProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        // This is executed in the context of the main GUI thread
        protected void onPostExecute(String code) {

            if (result == 200 && "00".equals(code)) {

                progressDialog.dismiss();

                launchPinpad();


            } else if (result == 200 && !code.equals("00")) {

                progressDialog.dismiss();

                Alert.showFailed(getActivity(), "Could not update Agent's details");

            } else if (result != 200) {

                progressDialog.dismiss();

                Alert.showFailed(getActivity(), "Please try again later");
            }

        }
    }


    private void openLogintoDb() {
        if(getActivity()!=null){
            Toast.makeText(getActivity(), "Click, click", Toast.LENGTH_SHORT).show();
            DialogFragment dialogFragment = new InputPinFragment();
            dialogFragment.show(getActivity().getSupportFragmentManager(),"dialogFragment");
            dialogFragment.setCancelable(false);
        }

    }
}
