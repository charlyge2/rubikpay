package com.esl.android.paycentre.activities.airtimeVending;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.codinguser.android.contactpicker.ContactsPickerActivity;
import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.adapters.AirtimeProviderAdapter;
import com.esl.android.paycentre.controllers.AirtimeController;
import com.esl.android.paycentre.interfaces.ConvenienceFeeInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;
import com.esl.android.paycentre.utils.NumberTextWatcher;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Objects;


public class ValidateAirtimeAmountActivity extends BaseActivity implements ConvenienceFeeInterface {

    private static final int GET_PHONE_NUMBER = 3007;

    public static  final String TAG = "Permissions";

    private ArrayList<String> airtimeProviderArray = new ArrayList<>();

    private String selectedNetwork;

    public static Button airtimeProceedBtn;

    public static EditText airtimeAmount;

    public static EditText airtimePhoneNumber;

    private String amount;

    private String phone;

    private String formattedAmt;

    private String[] airtimeNetworkProviders = {"Select Network", "9MOBILE", "AIRTEL", "MTN", "GLO" };
    int images[] = { R.drawable.ic_airtime,R.drawable.etisalat, R.drawable.airtel,R.drawable.mtn, R.drawable.glo};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_airtime_amount);

        airtimeAmount = findViewById(R.id.airtimeAmount);
        airtimeAmount.addTextChangedListener(new NumberTextWatcher());
        airtimePhoneNumber = findViewById(R.id.airtimePhoneNumber);
        airtimeProceedBtn = findViewById(R.id.airtimeValidateBtn);
        airtimeProceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                amount = airtimeAmount.getText().toString();
                phone = airtimePhoneNumber.getText().toString();

                try {

                    formattedAmt = amount.replace(",","");

                    JSONObject airtimeParams = new JSONObject();

                    airtimeParams.put("amount",amount);
                    airtimeParams.put("phone",phone);
                    airtimeParams.put("network", selectedNetwork);

                    String result = AirtimeController.validateAmount(getApplicationContext(),airtimeParams);

                    if(result.equals("Please fill all fields")){
                        Alert.showWarning(getApplicationContext(),"Please fill all fields");
                    }else if(result.equals("failed")){
                        Alert.showWarning(getApplicationContext(),"Please fill all fields");
                    }else if(result.equals("Please select a network")){
                        Alert.showWarning(getApplicationContext(),"Please select a network");
                    }else if(result.equals("valid")){

                        try{

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("amount",formattedAmt);
                            jsonObject.put("transactionType","Recharge Top Up");

                            HttpService.fetchConvenienceFee(ValidateAirtimeAmountActivity.this,jsonObject);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        //airtimeProviderArray.addAll(Arrays.asList(airtimeNetworkProviders));


        Spinner spinner = (Spinner) findViewById(R.id.airtimeNetworkSpinner);
        // Create an ArrayAdapter using the string array and a default spinnern layout

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Alert.showWarning(getApplicationContext(), "You Select Position: "+position+" "+airtimeNetworkProviders[position]);
                selectedNetwork = airtimeNetworkProviders[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        AirtimeProviderAdapter customAdapter=new AirtimeProviderAdapter(getApplicationContext(),images,airtimeNetworkProviders);
        spinner.setAdapter(customAdapter);

    }

    @Override
    public void sendConvenienceFee(String fee) {

    }

    @Override
    public void sendConvenienceFee(String fee, String StampDuty, boolean showStampDuty, String vat, boolean showVat) {

        HttpService.progressDialog.dismiss();

        Intent intent = new Intent(getApplicationContext(),AirtimeAmountPreview.class);
        intent.putExtra("amount",formattedAmt);
        intent.putExtra("phone",phone);
        intent.putExtra("network",selectedNetwork);
        intent.putExtra("convenienceFee",fee);

        startActivity(intent);
    }



    public void goBack(View view) {
        Navigator.moveToDashboard(ValidateAirtimeAmountActivity.this);
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(ValidateAirtimeAmountActivity.this);
    }

    public void selectContact(View view) {

       if(!isContactsPermissionGranted()){
           return;
       }
       startActivityForResult(new Intent(this, ContactsPickerActivity.class), GET_PHONE_NUMBER);
    }

    // Listen for results.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        // See which child activity is calling us back.
        switch (requestCode) {
            case GET_PHONE_NUMBER:
                // This is the standard resultCode that is sent back if the
                // activity crashed or didn't doesn't supply an explicit result.
                if (resultCode == RESULT_CANCELED){
                    Alert.showFailed(getApplicationContext(),"No phone number found");
                } else {
                    String phoneNumber = (String) Objects.requireNonNull(data.getExtras()).get(ContactsPickerActivity.KEY_PHONE_NUMBER);
                    String name = (String) Objects.requireNonNull(data.getExtras()).get(ContactsPickerActivity.KEY_CONTACT_NAME);
                    //Do what you wish to do with phoneNumber e.g.
                    if (phoneNumber != null) {
                        airtimePhoneNumber.setText(phoneNumber.replace(" ", ""));
                    }
                    Alert.showSuccess(getApplicationContext(),"Name : "+name+"\nNumber : "+phoneNumber );
                }
            default:
                break;
        }
    }

    public boolean isContactsPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Granting..... permission");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Log.v(TAG,"Permission: "+permissions[0]+ " was "+grantResults[0]);
            startActivityForResult(new Intent(this, ContactsPickerActivity.class), GET_PHONE_NUMBER);
        }
    }



}
