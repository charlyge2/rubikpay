package com.esl.android.paycentre.activities.activation;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.constants.Constant;

public class TermsAndConditionsActivity extends AppCompatActivity {

    private WebView webView;

    private Button btn;

    private String type;

    private System termsUrl;

    public static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);

        type = getIntent().getStringExtra("type");

        progressDialog = new ProgressDialog(TermsAndConditionsActivity.this);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Loading ......");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        webView = (WebView) findViewById(R.id.webview1);
        btn = (Button) findViewById(R.id.backBtn);
        webView.getSettings().setJavaScriptEnabled(true);
        if(type.equals("loan")){
            webView.loadUrl(Constant.LOAN_TERMS_AND_CONDITIONS_URL);
        }else{
            webView.loadUrl(Constant.TERMS_AND_CONDITIONS_URL);
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                progressDialog.hide();
            }
        });

    }
}
