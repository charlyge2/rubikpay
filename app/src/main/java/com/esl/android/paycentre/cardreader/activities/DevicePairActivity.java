package com.esl.android.paycentre.cardreader.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.activation.AfterkeyExchangeActivity;
import com.esl.android.paycentre.cardreader.controllers.PinpadManager;
import com.esl.android.paycentre.cardreader.facade.PinpadFacade;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.utils.Alert;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class DevicePairActivity extends Activity {

    private PinpadFacade pinpadFacade;
    private String btAddress;
    private ProgressDialog mProgressDialog;


    private BluetoothDeviceAdapter mListAdapter;
    private final BroadcastReceiver mPairReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {

                    Globals.pairedDevice = true;

                    Globals.autoConnect = false;

                    connectToDevice();

                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED) {

                }

//                mListAdapter.notifyDataSetChanged();
            }
        }
    };
    private ListView mListView;
    private TextView titleTextView;
    private ArrayList<BluetoothDevice> mDeviceList;

    private PinpadManager mPinpadManager;
    private Handler deviceActivityHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.screen_device);

        Button pairButton = findViewById(R.id.pairButton);
        pairButton.setVisibility(View.GONE);

        pinpadFacade = new PinpadFacade(this);

        mProgressDialog = new ProgressDialog(DevicePairActivity.this);

        mDeviceList = getIntent().getExtras().getParcelableArrayList("device.list");

        mListAdapter = new BluetoothDeviceAdapter(this);
        mListView = findViewById(R.id.deviceListView);
        titleTextView = findViewById(R.id.textViewTitle);

        titleTextView.setText("SELECT PINPAD TO PAIR AND CONNECT");

        mListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parentView, View view,
                                    final int position, long id) {

                BluetoothDevice device = mListAdapter.getItem(position).device;
                Globals.bluetoothAddress = mListAdapter.getItem(position).address;

                mProgressDialog.setMessage("Pairing...");
                mProgressDialog.setCancelable(false);
                mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();


                    }
                });

                mProgressDialog.show();

                pairDevice(device);


            }
        });

        mListView.setAdapter(mListAdapter);

        registerReceiver(mPairReceiver, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));

        updateDeviceList();

        mPinpadManager = PinpadManager.getInstance(this);
        deviceActivityHandler = new Handler();
        mPinpadManager
                .setOnConnectionEstablishedListener(new PinpadManager.OnConnectionEstablishedListener() {
                    @Override
                    public void OnConnectionEstablished() {

                        runOnUiThread(new Runnable() {
                            public void run() {

                                // set this to check if connected
                                Globals.isPinpadConnected = true;

                                showSuccessToast(getResources().getString(R.string.msg_pinpad_connected));

                                setResult(RESULT_OK);
                                finish();
                            }
                        });
                    }
                });

    }

    private void pairDevice(BluetoothDevice mBluetoothDevice) {
        try {

            mProgressDialog.dismiss();

            Method method = mBluetoothDevice.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(mBluetoothDevice, (Object[]) null);

//            byte[] pin = (byte[]) BluetoothDevice.class.getMethod("convertPinToBytes", String.class).invoke(BluetoothDevice.class, "1234");
//            Method m = mBluetoothDevice.getClass().getMethod("setPin", byte[].class);
//            m.invoke(mBluetoothDevice, pin);
//            mBluetoothDevice.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(mBluetoothDevice, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connectToDevice() {
        String dialogMessage;
        // Construct a progress dialog to prevent user from actions until
        // connection is finished.
        final ProgressDialog dialog = new ProgressDialog(DevicePairActivity.this);


            dialogMessage = getString(R.string.msg_please_wait);

        dialog.setMessage(dialogMessage);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                return true;
            }
        });
        dialog.show();

        // Force connection to be execute in separate thread.
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    mPinpadManager.connect(Globals.bluetoothAddress, getApplicationContext());


                } catch (IOException e) {

                    e.printStackTrace();

                    showFailedToast(getResources().getString(R.string.msg_failed_to_connect_to_pinpad) + ": " + e.getMessage());


                } finally {

                        dialog.dismiss();
                }
            }
        });
        t.start();

    }


    // Populate list with paired bluetooth devices.
    private void updateDeviceList() {

        for (BluetoothDevice device : mDeviceList) {

            BluetootDeviceFacade pair = new BluetootDeviceFacade();
            pair.name = device.getName();
            pair.address = device.getAddress();
            pair.device = device;

            mListAdapter.add(pair);

        }


        mListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onDestroy() {
        unregisterReceiver(mPairReceiver);

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

        Intent intent = new Intent(DevicePairActivity.this, DeviceActivity.class);
        startActivity(intent);

        finish();
    }

    private void showSuccessToast(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Alert.showSuccess(getApplicationContext(), getResources().getString(R.string.msg_pinpad_connected));
            }
        });
    }

    private void showFailedToast(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Alert.showFailed(getApplicationContext(), getResources().getString(R.string.msg_failed_to_connect_to_pinpad));
            }
        });
    }

    private class BluetootDeviceFacade {

        String name;
        String address;
        BluetoothDevice device;
    }

    private class BluetoothDeviceAdapter extends ArrayAdapter<BluetootDeviceFacade> {

        public BluetoothDeviceAdapter(Context context) {

            super(context, android.R.layout.simple_list_item_2);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TwoLineListItem v = (TwoLineListItem) convertView;

            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = (TwoLineListItem) inflater.inflate(
                        android.R.layout.simple_list_item_2, null);
            }

            BluetootDeviceFacade device = getItem(position);
            v.getText1().setText(device.name);
            v.getText2().setText(device.address);

            return v;
        }
    }


}
