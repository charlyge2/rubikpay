package com.esl.android.paycentre.activities.activation;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.adapters.WalkthroughPageAdapter;
import com.esl.android.paycentre.utils.SharedPreferenceManager;


public class NewUpdateWalkthroughActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout dotsLayout;

    private ImageView[] dots;

    private ViewPager viewPager;

    private Button btnSkip, btnNext;

    private int[] layouts = {R.layout.welcome_slide, R.layout.fourth_slide};

    WalkthroughPageAdapter viewpagerAdapter;

    SharedPreferenceManager sharedPreferenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title

        setContentView(R.layout.activity_welcome);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewpagerAdapter = new WalkthroughPageAdapter(layouts, this);
        viewPager.setAdapter(viewpagerAdapter);

        dotsLayout = (LinearLayout) findViewById(R.id.dotsLayout);
        btnSkip = findViewById(R.id.btnSkip);
        btnNext = findViewById(R.id.btnNext);

        btnSkip.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        createDots(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);
                if (position == layouts.length - 1) {
                    btnNext.setText("Start");
                    btnSkip.setVisibility(View.INVISIBLE);
                } else {
                    btnNext.setText("Next");
                    btnSkip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void createDots(int current_position) {

        if (dotsLayout != null)
            dotsLayout.removeAllViews();

        dots = new ImageView[layouts.length];

        for (int i = 0; i < layouts.length; i++) {
            dots[i] = new ImageView(this);
            if (i == current_position) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.default_dots));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);

            dotsLayout.addView(dots[i], params);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                loadNextSlide();
                break;

            case R.id.btnSkip:
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                sharedPreferenceManager.save(getApplicationContext(),"viewedNewWalkthrough","false");
                startActivity(intent);
                break;
        }
    }


    private void loadNextSlide() {

        int nextSlide = viewPager.getCurrentItem() + 1;

        if (nextSlide < layouts.length) {
            viewPager.setCurrentItem(nextSlide);
        } else {

            sharedPreferenceManager.save(getApplicationContext(),"viewedNewWalkthrough","true");

            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);

        }
    }

    @Override
    public void onBackPressed() {

    }
}

