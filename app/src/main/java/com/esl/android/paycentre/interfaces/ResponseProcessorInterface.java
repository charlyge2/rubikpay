package com.esl.android.paycentre.interfaces;

public interface ResponseProcessorInterface<T> {

    public void processResponse(T response);
}
