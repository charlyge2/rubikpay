package com.esl.android.paycentre.cardreader.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class MiscUtils {

    private static Context context;
    private static SharedPreferences preferences;
    private static ProgressDialog progressDialog;

    /*
     * initialises the context
     *
     * @param context the application context
     */
    public static void initContext(Context context) {
        MiscUtils.context = context;
    }

    /**
     * Returns a preferences instance
     *
     * @param preferencesName The name of the preferences instance to return
     * @return Returns the preferences with the given name
     */
    private static SharedPreferences getSharedPreferences(String preferencesName) {
        SharedPreferences preferences = context.getSharedPreferences(preferencesName, 0);
        return preferences;
    }

    /**
     * Stores a key,value pair to a preferences instance
     *
     * @param preferencesName The name of the preferences instance for storage
     * @param key             The name of the preference to store
     * @param value           The value of the preference to store
     */
    public static void storeInSharedPreferences(String preferencesName, String key, String value) {
        preferences = getSharedPreferences(preferencesName);
        Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Retrieves a string preference from a preferences instance
     *
     * @param preferencesName The preferences instance to retrieve preference from
     * @param key             The name of the string preference to retrieve
     * @param defValue        The value of the retrieved string preference
     * @return Returns the string preference value
     */
    public static String getFromSharedPreferences(String preferencesName, String key, String defValue) {
        preferences = getSharedPreferences(preferencesName);
        String value = preferences.getString(key, defValue);
        return value;
    }

    /**
     * Stores a boolean preference in a preferences instance
     *
     * @param preferencesName The name of the preferences to store the boolean
     * @param key             The name of the boolean preference to store
     * @param value           The value of the boolean preference to store
     */
    public static void storeInSharedPreferences(String preferencesName, String key, boolean value) {
        preferences = getSharedPreferences(preferencesName);
        Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }


    /**
     * Retrieves a boolean preference from a preferences instance
     *
     * @param preferencesName The preferences instance to retrieve preference from
     * @param key             The name of the boolean preference to retrieve
     * @param defValue        The value of the retrieved boolean preference
     * @return Returns the boolean preference value
     */
    public static boolean getFromSharedPreferences(String preferencesName, String key, boolean defValue) {
        preferences = getSharedPreferences(preferencesName);
        boolean value = preferences.getBoolean(key, defValue);
        return value;
    }

    public static void storeInSharedPreferences(String preferencesName, String key, int value) {
        preferences = getSharedPreferences(preferencesName);
        Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getFromSharedPreferences(String preferencesName, String key, int defValue) {
        preferences = getSharedPreferences(preferencesName);
        return preferences.getInt(key, defValue);
    }


    /**
     * Returns a configured ProgressDialog
     *
     * @param context      The application context
     * @param title        The title of the dialog
     * @param message      The message to display in the body of the dialog
     * @param isCancelable dialog can be closed by user if set to true, and cannot be closed if otherwise
     * @return a configured ProgressDialog
     */
    public static ProgressDialog progressDialogConfig(Context context, String title, String message, boolean isCancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(isCancelable);

        return progressDialog;
    }

}
