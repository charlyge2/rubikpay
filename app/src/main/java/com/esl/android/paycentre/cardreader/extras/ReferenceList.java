package com.esl.android.paycentre.cardreader.extras;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.esl.android.paycentre.R;


public class ReferenceList extends Activity {
    public static final String preference = "Configuration";
    public static final String FolderName = "Paycentre";
    public static SharedPreferences config;
    public static final String storageLocation = "slocation";
    public static String pinpadVersion = "pinpadVersion";
    public static String loadKeys = "loadKeys";
    public static String nibssKeysDownloaded = "nibssKeysDownloaded";
    private String error = "Error in file";
    public static String amount = "0.00";
    public static String posAccountUsername = "posAccount";



    public static String POSSerialNumber = "psn";
    public static String pinpadMACAddress = "pinpadMACAddress";
    public static String pinpadSerialNumber = "pinpadSerialNumber";
    public static String activationCode = "activationCode";
    public static String isActivated = "isActivated";
    public static String isInit = "isInit";

    public static String TMK = "tmk";
    public static String TSK = "tsk";
    public static String TPK = "tpk";
    public static String bdk = "bdk";

    public static String dataKey = "dataKey";

    public static String TR31dupktKey = "TR31dupktKey";
    public static String TR31dataKey = "TR31dataKey";
    public static String TR31tmkKey = "TR31tmkKey";

    public static String acceptorID = "acceptorID";
    public static String acceptorName = "acceptorName";
    public static String merchantType = "merchantType";
    public static String terminalId = "terminalId";
    public static String activated = "activated";
    public static String agreed = "agreed";
    public static String walkthrough = "walkthrough";
    public static String keysDownload = "keysDownload";
    public static String isAdminLogin = "isAdminLogin";
    public static String isPosOperationsPage = "isPosOperationsPage";
    public static String isPosLogin = "isPosLogin";

    //    notification settings
    public static String nibssIp = "nibssIp";
    public static String nibssPort = "nibssPort";
    public static String hsmIp = "hsmIp";
    public static String hsmPort = "hsmPort";
    public static String doKeyDownload = "doKeyDownload";
    public static String updateTerminalDetail = "updateTerminalDetail";

    public ReferenceList() {
        // TODO Auto-generated constructor stub
    }

    public ReferenceList(String error) {
        // TODO Auto-generated constructor stub
        this.error = error;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub

        switch (id) {
            case 0:
                LayoutInflater inflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                View v = inflater.inflate(R.layout.alertview, null);
                EditText eText = v.findViewById(R.id.alertContent);
                eText.setText(error);

                return new AlertDialog.Builder(this)
                        //.setIcon(android.R.mipmap.ic_launcher)
                        .setTitle("A simple Dialog")
                        .setNeutralButton("OK", null)
                        .setView(v)
                        .create();

            default:
                return null;
        }
    }
}
