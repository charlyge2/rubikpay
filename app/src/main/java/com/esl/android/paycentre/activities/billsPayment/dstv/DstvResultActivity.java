package com.esl.android.paycentre.activities.billsPayment.dstv;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.database.model.Transaction;
import com.esl.android.paycentre.database.repo.TransactionRepo;
import com.esl.android.paycentre.utils.Alert;

import org.json.JSONObject;

import java.util.Date;

public class DstvResultActivity extends BaseActivity {


    private ImageView responseImage;

    private TextView responseText;

    private Button homeBtn;

    private String responseCode;

    private String responseMessage;

    private String cardNumber;

    private String customerName;

    private String customerNumber;


    private String amount;

    private String convenienceFee;

    private String posAccount;

    private String transactionId;

    private String total;

    private String appTime;

    private String transactionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dstv_result);

        responseImage = findViewById(R.id.dstvResponseImage);
        responseText = findViewById(R.id.dstvTransactionStatus);
        homeBtn = findViewById(R.id.dstvHomeBtn);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });

        responseCode = getIntent().getStringExtra("responseCode");
        responseMessage = getIntent().getStringExtra("responseMessage");

        cardNumber = getIntent().getStringExtra("cardNumber");
        customerName = getIntent().getStringExtra("customerName");
        customerNumber = getIntent().getStringExtra("customerNumber");;
        amount = getIntent().getStringExtra("amount");
        transactionId = getIntent().getStringExtra("transactionId");
        convenienceFee = getIntent().getStringExtra("convenienceFee");
        posAccount = getIntent().getStringExtra("posAccount");
        total = getIntent().getStringExtra("total");
        appTime = getIntent().getStringExtra("appTime");

        if(responseCode.equals("00")){
            responseImage.setImageResource(R.drawable.ic_check);
            responseText.setText(responseMessage);
        }else {
            responseImage.setImageResource(R.drawable.ic_error);
            responseText.setText(responseMessage);
        }

        long check = saveDstvTransaction();

        if(check < 0){
            Alert.showWarning(getApplicationContext(), "Dstv transaction could not be saved");
        }

    }

    public long saveDstvTransaction(){

        JSONObject params = new JSONObject();

        try {
            params.put("cardNumber", cardNumber);
            params.put("customerName", customerName);
            params.put("customerNumber",customerNumber);
        }catch (Exception e){
            e.printStackTrace();
        }

        Transaction transaction = new Transaction();
        TransactionRepo transactionRepo = new TransactionRepo();

        float floatAmount = Float.parseFloat(amount.replace(",", ""));
        float floatConvenience = Float.valueOf(convenienceFee);

        transaction.setAgentCut(0);
        transaction.setAmount(floatAmount);
        transaction.setConvenienceFee(floatConvenience);
        transaction.setDate(new Date());
        transaction.setPosAccount(posAccount);
        transaction.setTransactionId(transactionId);
        transaction.setResponseCode(responseCode);
        transaction.setResponseMsg(responseMessage);
        transaction.setVersion(MainActivity.appVersionUrl);
        transaction.setAgentCut(Float.valueOf("0.0"));
        transaction.setTransactionType(Transaction.Type.getByName("DSTV"));
        transaction.setData(params);

        long result = transactionRepo.insert(transaction);

        return result;

    }

    @Override
    public void onBackPressed() {

    }
}
