package com.esl.android.paycentre.interfaces;

public interface HttpResponseInterface {
    public void sendDefaultHttpResponse(String responseCode,String responseMessage);
}
