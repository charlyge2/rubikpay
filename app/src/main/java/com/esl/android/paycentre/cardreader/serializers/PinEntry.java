package com.esl.android.paycentre.cardreader.serializers;


public class PinEntry {
	public enum PinEntryState{
		SessionBegin,
		Entry,
		CloseSession,
		WrongPin,
		CorrectPin,
		LastPinTry
	}
	
	private PinEntryState state;
	private int stars;
	public PinEntryState getState() {
		return state;
	}
	public void setState(PinEntryState state) {
		this.state = state;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
	
	
}
