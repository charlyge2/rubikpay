package com.esl.android.paycentre.interfaces;

public interface StartimesNameLookupInterface {
    public void sendStartimesDetails(String customerName);
}

