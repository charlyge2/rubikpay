package com.esl.android.paycentre.activities.billsPayment.dstv;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.interfaces.NameLookupInterface;
import com.esl.android.paycentre.interfaces.PlanInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;

import org.json.JSONArray;
import org.json.JSONObject;

public class DstvNumberActivity extends BaseActivity implements NameLookupInterface, PlanInterface {

    private Button dstvAmountBtn;

    private EditText dstvCardNumber;

    public static String cardNumber;

    public static String dstvInvoicePeriod;

    public static String dstvCustomerName;

    public static String dstvCustomerNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dstv_number);

        dstvAmountBtn = findViewById(R.id.dstvAmountBtn);
        dstvCardNumber = findViewById(R.id.dstvCardNumber);

        dstvAmountBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cardNumber = dstvCardNumber.getText().toString();
                        if(TextUtils.isEmpty(cardNumber)){
                            Alert.showWarning(getApplicationContext(),"Please Enter DSTV card number");
                            return;
                        }

                        try {
                            JSONObject params = new JSONObject();

                            params.put("cardNumber",cardNumber);

                            String url = MainActivity.dstvInquiryUrl;

                            HttpService.doNameLookup(DstvNumberActivity.this,params,url);

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
        );
    }

    @Override
    public void sendAccountDetails(String customerName, String invoicePeriod, String customerNumber) {

        dstvCustomerName = customerName;
        dstvCustomerNumber = customerNumber;
        dstvInvoicePeriod = invoicePeriod;

        HttpService.progressDialog.dismiss();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DstvNumberActivity.this);
        alertDialogBuilder.setTitle("Dstv Account Details");
        alertDialogBuilder
                .setMessage("\nNAME : "+customerName +"\n\n\nCARD NUMBER : " + cardNumber + "\n\n\nINVOICE PERIOD : "+invoicePeriod)
                .setCancelable(false)
                .setPositiveButton("PROCEED",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                String url = MainActivity.dstvPlanUrl;

                                HttpService.fetchPlans(DstvNumberActivity.this,url);

                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Alert.showInfo(getApplicationContext(),"CANCELLED");
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public void sendPlans(JSONArray products) {

        HttpService.progressDialog.dismiss();

        Intent intent = new Intent(getApplicationContext(), DstvProductsActivity.class);
        intent.putExtra("products", products.toString());
        startActivity(intent);

    }

    public void goBack(View view) {
        Navigator.moveTo(DstvNumberActivity.this, "bills");
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(DstvNumberActivity.this);
    }
}
