package com.esl.android.paycentre.controllers.activationControllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.activation.PosLoginActivity;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.database.DBHelper;
import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.repo.PosAccountRepo;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePosPinController {

    String username,pin;

    SharedPreferenceManager sharedPreferenceManager;

    PosAccount posAccount = new PosAccount();

    PosAccountRepo posAccountRepo = new PosAccountRepo();

    public void doChangePosPin(final Context context, JSONObject userObject){
        final ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Status");
        progress.setMessage("Changing Pos Pin....");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        try {

            username = userObject.getString("username");
            pin = userObject.getString("pin");

            final JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("username", username);
                jsonObject.put("pin", pin);

                Log.d("PIN DETAILS", username + " sss " + pin);

                AndroidNetworking.post(MainActivity.changePosPinUrl)
                        .setPriority(Priority.IMMEDIATE)
                        .addHeaders("Authorization", ConfigurationClass.getAccessToken(context))
                        .addHeaders("x-phone-uuid",MainActivity.getUuid(context))
                        .addHeaders("Content-type", "application/json")
                        .addJSONObjectBody(jsonObject)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                progress.dismiss();

                                try {

                                    String responseMessage = response.getString("responseMessage");
                                    String responseCode = response.getString("responseCode");

                                    if (responseCode.equals("00")) {
                                        doPosAccountUpdate(context,username,pin);
                                    } else {
                                        Alert.showWarning(context,responseMessage);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError error) {
                                // handle errorfr
                                try {
                                    Log.d("POS_PIN_CHANGE",error.getErrorBody());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                progress.dismiss();
                                Alert.showWarning(context,"Could not change pos pin");
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void doPosAccountUpdate(final Context context, String usename, String pin){

        DBHelper dbHelper = new DBHelper(context);
        DatabaseManager.initializeInstance(dbHelper);

        posAccount.setUsername(usename);
        posAccount.setPassword(pin);

        try {
            posAccountRepo.update(posAccount);
            sharedPreferenceManager.save(context, "posLoginPage", "true");
            Intent intent = new Intent(context, PosLoginActivity.class);
            context.startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
