package com.esl.android.paycentre.activities.billsPayment.others;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.billsPayment.BillsActivity;
import com.esl.android.paycentre.activities.billsPayment.dstv.DstvNumberActivity;
import com.esl.android.paycentre.activities.billsPayment.gotv.GotvNumberActivity;
import com.esl.android.paycentre.activities.billsPayment.startimes.StartimesNumberActivity;
import com.esl.android.paycentre.adapters.BillList;
import com.esl.android.paycentre.adapters.ElectricityProviderAdapter;
import com.esl.android.paycentre.constants.Constant;
import com.esl.android.paycentre.controllers.BillsController;
import com.esl.android.paycentre.interfaces.ResponseProcessorInterface;
import com.esl.android.paycentre.services.HttpService;

import java.util.List;

public class ElectricityActivity extends AppCompatActivity implements ElectricityProviderAdapter.onBillItemClickListener, ResponseProcessorInterface<List<BillList>> {
    private RecyclerView electricityRecyclerView;
    private List<BillList> billLists;
    private ElectricityProviderAdapter providerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electricity);
        electricityRecyclerView = findViewById(R.id.electricityRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        electricityRecyclerView.setLayoutManager(linearLayoutManager);
        providerAdapter = new ElectricityProviderAdapter(this, this, billLists);
        electricityRecyclerView.setHasFixedSize(true);
        electricityRecyclerView.setAdapter(providerAdapter);
        HttpService.getElectricityBillers(this, this, MainActivity.powerUrl);
    }

    @Override
    public void onBillItemClicked(int position) {
        String billName = billLists.get(position).getBillName();

        Intent intent = new Intent(getApplicationContext(), OtherBillsInquiry.class);
        intent.putExtra("billName", billName);
        startActivity(intent);

    }

    @Override
    public void processResponse(List<BillList> response) {
        billLists = response;
        providerAdapter.setBillLists(response);
    }
}