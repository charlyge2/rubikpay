package com.esl.android.paycentre.activities.fundsTransfer;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.adapters.TransactionPreviewAdapter;
import com.esl.android.paycentre.adapters.TransactionPreviewList;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;
import com.esl.android.paycentre.utils.SaveTransferTransactions;
import com.esl.android.paycentre.utils.TransactionFormatter;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class FundsTransferAmountPreviewActivity extends BaseActivity {

    SharedPreferenceManager sharedPreferenceManager;

    private DecimalFormat DigitFormatter = new DecimalFormat("#,###,###.00");

    private TransactionPreviewAdapter transactionPreviewAdapter;

    private ArrayList<TransactionPreviewList> transactionPreview = new ArrayList<>();

    private ListView detailsView;

    private String formattedAmount;

    private String formattedAgentFee = "";
    private View vat_view;

    private Double total;

    private Button proceedBtn;

    private TextView amountTextView;

    private TextView nameTextView;

    private TextView bankTextView;

    private TextView accountTextView;

    private TextView convenienceFeeTextView;

    private TextView totalTextView;

    private TextView phoneTextView, vatTextView;
    private LinearLayout fund_vat_layout;

    private AlertDialog alertDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funds_transfer_amount_preview);

        amountTextView = findViewById(R.id.amountTextView);
        convenienceFeeTextView = findViewById(R.id.convenienceFeeTextView);
        totalTextView = findViewById(R.id.totalTextView);
        nameTextView = findViewById(R.id.nameTextView);
        bankTextView = findViewById(R.id.bankTextView);
        accountTextView = findViewById(R.id.accountTextView);
        phoneTextView = findViewById(R.id.phoneTextView);
        proceedBtn = findViewById(R.id.transferProceedBtn);
        fund_vat_layout = findViewById(R.id.fund_vat_layout);
        vatTextView = findViewById(R.id.vatTextView);
        vat_view = findViewById(R.id.vat_view);

        final String phone = getIntent().getStringExtra("phone");
        final String bankCode = getIntent().getStringExtra("bankCode");
        final String amount = getIntent().getStringExtra("amount");
        final String convenienceFee = getIntent().getStringExtra("convenienceFee");
        String narration = getIntent().getStringExtra("narration");
        final String bank = getIntent().getStringExtra("bank");
        final String accountName = getIntent().getStringExtra("accountName");
        final String accountNumber = getIntent().getStringExtra("accountNumber");
        final boolean showVat = getIntent().getBooleanExtra("showVat", false);
        final String vat = getIntent().getStringExtra("vat");
        final String amountFormatted = DigitFormatter.format(Double.parseDouble(amount.replace(",", "")));
        final String posAccountUsername = sharedPreferenceManager.getString(getApplicationContext(), "posAccountUsername", "");

        if (TextUtils.isEmpty(narration)) {
            narration = "Empty";
        } else {
            narration = narration;
        }

        final String finalNarration = narration;

        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedBtn.setEnabled(false);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FundsTransferAmountPreviewActivity.this);
                alertDialogBuilder.setTitle("Verify Account");
                alertDialogBuilder
                        .setMessage("\nAmount : ₦" + amountFormatted + "\n\n\nAccount Name : " + HttpService.name + "\n\n\nAccount Number : " + accountNumber + "\n\n\nBank : " + bank + "\n\n\nAre you sure you want to transfer to this account ?")
                        .setCancelable(true)
                        .setPositiveButton("PROCEED",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        try {

                                            JSONObject transferParams = new JSONObject();
                                            try {
                                                String transactionId = TransactionFormatter.generateTransactionId();
                                                String accountName = HttpService.name;
                                                transferParams.put("amount", amount.replace(",", ""));
                                                transferParams.put("convenienceFee", convenienceFee);
                                                transferParams.put("transactionId", transactionId);
                                                transferParams.put("accountName", accountName);
                                                transferParams.put("vat", vat);
                                                transferParams.put("accountNumber", accountNumber);
                                                transferParams.put("bankName", bank);
                                                transferParams.put("bankCode", bankCode);
                                                transferParams.put("narration", finalNarration);
                                                transferParams.put("phone", phone);
                                                transferParams.put("posAccount", posAccountUsername);
                                                transferParams.put("appTime", TransactionFormatter.generateAppTime());
                                                transferParams.put("version", MainActivity.getAppVersion(getApplicationContext()));

                                                String url = MainActivity.fundsTransferPaymentUrl;

                                                String type = "fundsTransfer";
                                                long check = SaveTransferTransactions.initialSaveTransactions(FundsTransferAmountPreviewActivity.this, accountName, accountNumber, bank, phone, vat, posAccountUsername, transactionId, amount, convenienceFee, Globals.fundsTransferType.toUpperCase());
                                                if (check < 0) {
                                                    Alert.showFailed(getApplicationContext(), "Funds Transfer transaction could not be saved");
                                                } else {
                                                    HttpService.makePayment(FundsTransferAmountPreviewActivity.this, transferParams, url, type);
                                                }


                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                })
                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        proceedBtn.setEnabled(true);
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        if (!TextUtils.isEmpty(narration)) {
            //transferDetails.add("Narration \t\t\t\t\t\t\t\t\t\t ₦" + narration);
        }

        formattedAmount = amount.replace(",", "");
        if (showVat) {
            fund_vat_layout.setVisibility(View.VISIBLE);
            vat_view.setVisibility(View.VISIBLE);
            total = Double.valueOf(formattedAmount) + Double.valueOf(convenienceFee) + Double.valueOf(vat);

            String formattedTotal = DigitFormatter.format(total);

            amountTextView.setText("₦" + amountFormatted);
            convenienceFeeTextView.setText("₦" + DigitFormatter.format(Double.valueOf(convenienceFee)));
            totalTextView.setText("₦" + formattedTotal);
            nameTextView.setText(HttpService.name);
            bankTextView.setText(bank);
            phoneTextView.setText(phone);
            accountTextView.setText(accountNumber);
            Globals.vat = vat;
            vatTextView.setText("₦" + vat);
        } else {
            total = Double.valueOf(formattedAmount) + Double.valueOf(convenienceFee);

            String formattedTotal = DigitFormatter.format(total);

            amountTextView.setText("₦" + amountFormatted);
            convenienceFeeTextView.setText("₦" + DigitFormatter.format(Double.valueOf(convenienceFee)));
            totalTextView.setText("₦" + formattedTotal);
            nameTextView.setText(HttpService.name);
            bankTextView.setText(bank);
            phoneTextView.setText(phone);
            accountTextView.setText(accountNumber);
        }


    }

    public void goBack(View view) {
        finish();
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(FundsTransferAmountPreviewActivity.this);
    }

}
