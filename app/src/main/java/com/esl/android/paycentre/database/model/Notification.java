package com.esl.android.paycentre.database.model;

import java.util.Date;

public class Notification {
    public static final String TAG = Notification.class.getSimpleName();
    public static final String TABLE = "notification";

    // Labels Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_TYPE = "type";
    public static final String KEY_DATE = "datecreated";

    private int id;
    private String title;
    private String message ;
    private String type;
    private long date;

    public Notification() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return new Date(date);
    }

    public void setDate(Date date) {
        this.date = date.getTime();
    }

    public long getDateLong() {
        return date;
    }

    public void setDateLong(long date) {
        this.date = date;
    }
}
