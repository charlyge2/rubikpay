package com.esl.android.paycentre.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import java.util.List;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.TransactionViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<TransactionHistory> transactionHistoryList;

    private static View.OnClickListener mOnItemClickListener;

    //getting the context and product list with constructor
    public TransactionHistoryAdapter(Context mCtx, List<TransactionHistory> transactionHistoryList) {
        this.mCtx = mCtx;
        this.transactionHistoryList = transactionHistoryList;
    }

    @Override
    public TransactionHistoryAdapter.TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.transaction_history_layout, null);
        return new TransactionHistoryAdapter.TransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TransactionHistoryAdapter.TransactionViewHolder holder, int position) {
        //getting the product of the specified position
        TransactionHistory history = transactionHistoryList.get(position);

        //binding the data with the viewholder views
        holder.transactionStatus.setText(history.getTransactionStatus());
        holder.transactionTime.setText(history.getTransactionTime());
        holder.transactionId.setText(history.getTransactionId());
        holder.transactionAmount.setText(history.getTransactionAmount());
        holder.transactionName.setText(history.getTransactionName());
        if(history.getTransactionId().equals("")){
            holder.transactionId.setVisibility(View.GONE);
        }
        if ("00".equals(history.getTransactionResponseCode())) {
            holder.transactionStatus.setTextColor(Color.parseColor("#00B653"));
        } else if ("XY".equals(history.getTransactionResponseCode())) {
            holder.transactionStatus.setTextColor(Color.parseColor("#4285F4"));
        } else {
            holder.transactionStatus.setTextColor(Color.parseColor("#B6042F"));
        }

        System.out.println(history);
    }


    @Override
    public int getItemCount() {
        Log.d("HISTORY_SIZE", String.valueOf(transactionHistoryList.size()));
      if(transactionHistoryList==null){
          return 0;
      }
         return transactionHistoryList.size();

    }


    public static void setOnItemClickListener(View.OnClickListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }


    class TransactionViewHolder extends RecyclerView.ViewHolder {

        TextView transactionId, transactionName, transactionAmount, transactionTime, transactionStatus, responseCode;

        public TransactionViewHolder(View itemView) {
            super(itemView);

            transactionAmount = itemView.findViewById(R.id.transactionAmount);
            transactionName = itemView.findViewById(R.id.transactionName);
            transactionId = itemView.findViewById(R.id.transactionId);
            transactionTime = itemView.findViewById(R.id.transactionTime);
            transactionStatus = itemView.findViewById(R.id.transactionStatus);
            responseCode = itemView.findViewById(R.id.responseCode);

            itemView.setTag(this);
            itemView.setOnClickListener(mOnItemClickListener);
        }
    }

   public void setTransactionHistoryList(List<TransactionHistory> transactionHistoryList) {
        this.transactionHistoryList = transactionHistoryList;
        notifyDataSetChanged();
    }
}

