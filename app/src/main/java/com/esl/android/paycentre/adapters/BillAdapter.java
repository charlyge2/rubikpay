package com.esl.android.paycentre.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.ArrayList;
import java.util.List;

public class BillAdapter extends ArrayAdapter<BillList> {

    private Context mcontext;

    private List<BillList> billList = new ArrayList<>();

    public BillAdapter(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<BillList> list) {
        super(context, 0 , list);
        mcontext = context;
        billList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mcontext).inflate(R.layout.billspaymentlist,parent,false);

        BillList billPreview = billList.get(position);

        ImageView image = (ImageView) listItem.findViewById(R.id.billImage);

        TextView name = (TextView) listItem.findViewById(R.id.billName);

        name.setText(billPreview.getBillName());

        image.setImageResource(billPreview.getBillImage());

        return listItem;
    }
}
