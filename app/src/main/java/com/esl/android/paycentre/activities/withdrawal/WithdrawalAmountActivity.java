package com.esl.android.paycentre.activities.withdrawal;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.activation.PosLoginActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.constants.Constant;
import com.esl.android.paycentre.interfaces.ConvenienceFeeInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;
import com.esl.android.paycentre.utils.NumberTextWatcher;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static com.esl.android.paycentre.cardreader.utils.Globals.amount;


public class WithdrawalAmountActivity extends BaseActivity implements View.OnClickListener, ConvenienceFeeInterface {

    private  EditText amount;

    private   EditText phoneNum;

    private  Button cfeeBtn;

    private TextView cashoutTitle;

    private TextView cashoutDescription;

    public static String type;

    public ImageView homeIcon;

    public ImageView backIcon;

    public String formattedAmount;

    private Double total;

    private String activatedTerminal;

    SharedPreferenceManager sharedPreferenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal_amount);
        Globals.stampDuty = "0";
        Intent intent = getIntent();
        type = intent.getStringExtra("type");

        cashoutTitle = (TextView) findViewById(R.id.cashoutTitle);
        cashoutDescription = (TextView) findViewById(R.id.cashoutDescription);
        amount = (EditText) findViewById(R.id.amount);
        phoneNum = (EditText) findViewById(R.id.phone);
        cfeeBtn = (Button) findViewById(R.id.withdawalAmtProceed);
        homeIcon = (ImageView) findViewById(R.id.homeeIcon);
        backIcon = (ImageView) findViewById(R.id.backIcon);

        cfeeBtn.setOnClickListener(this);
        homeIcon.setOnClickListener(this);
        backIcon.setOnClickListener(this);


        if ("Fund Purse".equalsIgnoreCase(Globals.withdrawalType)) {

            cashoutTitle.setText("Fund Purse");
            cashoutDescription.setText("Fund your account using your atm card");

            Globals.withdrawalType = "FUND PURSE";
            Globals.transactionTypeId = 10;
            Globals.withdrawalLimit = 100000.00;
            amount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
        }

        activatedTerminal = sharedPreferenceManager.getString(getApplicationContext(), "terminalId", "");

        String naira = getResources().getString(R.string.naira);

        amount.addTextChangedListener(new NumberTextWatcher(naira));

        phoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // phoneNum.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (phoneNum.getText().toString().trim().length() < 11) {
                    phoneNum.setError("Please Enter Valid Phone Number");
                } else {
                    phoneNum.setError(null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                //phoneNum.setError(null);
            }
        });


    }

    @Override
    public void onClick(View view) {
        Intent myIntent = null;

        switch (view.getId()) {

            case R.id.withdawalAmtProceed:

                String enteredAmount = amount.getText().toString();

                formattedAmount = enteredAmount.replace(",", "");

                if (TextUtils.isEmpty(amount.getText().toString())) {
                    Alert.showWarning(getApplicationContext(), "Please fill all fields");
                    return;
                } else if (Double.parseDouble(formattedAmount) < 10) {
                    Alert.showWarning(getApplicationContext(), "Amount entered is below minimum value");
                    return;
                } else if (Globals.withdrawalType.equals("FUND PURSE")) {
                    if (Double.parseDouble(formattedAmount) > Globals.withdrawalLimit) {
                        Alert.showWarning(getApplicationContext(), "You have Exceeded the maximum limit for fundpurse. The Maximum amount is ₦100,000");
                        return;
                    }
                } else if (Globals.withdrawalType.equals("CASH OUT")) {
                    Globals.withdrawalLimit = 200000.00;
                    if (Double.parseDouble(formattedAmount) > Globals.withdrawalLimit) {
                        Alert.showWarning(getApplicationContext(), "You have Exceeded the maximum limit for cashout. The Maximum amount is ₦200,000");
                        return;
                    }
                }

                total = Double.valueOf(formattedAmount);

                Globals.purchase = total;

                try {

                    JSONObject jsonObject = new JSONObject();

                    System.out.println("AMOUNT: " + total);

                    jsonObject.put("amount", total);
                    jsonObject.put("transactionType", Globals.withdrawalType);
                    jsonObject.put("activatedTerminal", activatedTerminal);


                    System.out.println("Json object sent" + jsonObject.toString());
                    HttpService.fetchConvenienceFee(WithdrawalAmountActivity.this, jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.homeIcon:

            case R.id.backIcon:
                myIntent = new Intent(this, DashboardActivity.class);
                startActivity(myIntent);
                break;

            default:
                myIntent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(myIntent);
                break;
        }
    }


    @Override
    public void sendConvenienceFee(String fee) {


    }

    @Override
    public void sendConvenienceFee(String fee, String StampDuty, boolean showStampDuty, String vat, boolean showVat) {
        HttpService.progressDialog.dismiss();

        Globals.convenienceFee = fee;

        System.out.println("CFEE: " + fee);

        Intent myIntent = new Intent(this, WithdrawalAmountDetailsActivity.class);

        myIntent.putExtra("phone", phoneNum.getText().toString());
        myIntent.putExtra("amount", amount.getText().toString());
        myIntent.putExtra("convenienceFee", fee);
        myIntent.putExtra("StampDuty", StampDuty);
        myIntent.putExtra("showStampDuty", showStampDuty);
        myIntent.putExtra("vat", vat);
        myIntent.putExtra("showVat", showVat);


        startActivity(myIntent);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String languageToLoad  = "en"; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
