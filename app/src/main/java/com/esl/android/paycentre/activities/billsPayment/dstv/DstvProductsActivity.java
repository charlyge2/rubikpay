package com.esl.android.paycentre.activities.billsPayment.dstv;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.adapters.ProductAdapter;
import com.esl.android.paycentre.adapters.ProductModel;
import com.esl.android.paycentre.services.HttpService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DstvProductsActivity extends BaseActivity {

    //a list to store all the products
    private List<ProductModel> productList;
    //the recyclerview
    private RecyclerView recyclerView;

    private DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Step 4 of 4: Finally call getTag() on the view.
            // This viewHolder will have all required values.
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();

            int position = viewHolder.getAdapterPosition();

            ProductModel thisItem = productList.get(position);

            String plan = thisItem.getPlan();
            String price = thisItem.getPrice();
            String code = thisItem.getPlanCode();

            HttpService.fetchDstvAddons(DstvProductsActivity.this,code,price);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dstv_products);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        ProductAdapter.setOnItemClickListener(onItemClickListener);

        Intent intent = getIntent();
        String products = intent.getStringExtra("products");

        Log.d("PRODUCTS",String.valueOf(products));

        JSONArray jsonArr = new JSONArray();

        //initializing the productsModellist
        productList = new ArrayList<>();

        try {

            jsonArr = new JSONArray(products);

            for (int i = 0; i < jsonArr.length(); i++)
            {
                JSONObject jsonObj = new JSONObject();

                try {

                    String plan = jsonArr.getJSONObject(i).getString("name");
                    String price = jsonArr.getJSONObject(i).getString("price");
                    String code = jsonArr.getJSONObject(i).getString("code");

                    double formattedPrice = Double.parseDouble(price);

                    String formattedString = "₦" +formatter.format(formattedPrice);

                    productList.add(
                            new ProductModel(
                                    i,
                                    plan,
                                    code,
                                    formattedString));

                    //creating recyclerview adapter
                    ProductAdapter adapter = new ProductAdapter(this, productList);

                    //setting adapter to recyclerview
                    recyclerView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println(jsonObj);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
