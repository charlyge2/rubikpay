package com.esl.android.paycentre.adapters;

public class AirtimeProviderList {
  // Network Image
  private int imageDrawable;
  // Network Name
  private String network;

  // Constructor that is used to create an instance of the PinpadList object
  public AirtimeProviderList(int imageDrawable,String network) {
    this.imageDrawable = imageDrawable;
    this.network = network;
  }


  public int getImageDrawable() {
    return imageDrawable;
  }

  public void setImageDrawable(int imageDrawable) {
    this.imageDrawable = imageDrawable;
  }


  public String getNetwork() {
    return network;
  }

  public void setNetwork(String name) {
    this.network = network;
  }

}


