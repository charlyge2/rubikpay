package com.esl.android.paycentre.activities.activation;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.controllers.activationControllers.CreatePosAccountController;
import com.esl.android.paycentre.services.InternetConnectionService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;


public class CreatePosAccountActivity extends AppCompatActivity {

    private Button createposBtn;

    private EditText firstname;

    private EditText lastname;

    private EditText username;

    private EditText pin;

    private EditText confirmPin;

    private TextView confirmText;

    private String pospin;

    private String confirmpin;

    SharedPreferenceManager sharedPreferenceManager;

    CreatePosAccountController createPosAccountController = new CreatePosAccountController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        //getSupportActionBar().hide(); // hide the title back
        setContentView(R.layout.activity_createposaccount);

        createposBtn = findViewById(R.id.createposBtn);
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        username = findViewById(R.id.username);
        pin = findViewById(R.id.pin);
        confirmPin = findViewById(R.id.confirmPin);

//        username.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                if(username.getText().toString().length() < 4) {
//                    username.setError("username cannot be less than 4 characters");
//                    createposBtn.setEnabled(false);
//                }else{
//                    username.setError(null);
//                    createposBtn.setEnabled(true);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        confirmPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                pospin = pin.getText().toString();
                confirmpin = confirmPin.getText().toString();

                Log.d("PIN_MATCHING", "POS_PIN - "+pospin+ " CONFIRM_PIN - "+confirmpin);

                if(TextUtils.isEmpty(pospin)){
                    confirmPin.setError("You need to enter pin to confirm pin");
                }else{
                    confirmPin.setError(null);
                }



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    public void createPosAccount(View view) {

        String agentPhoneNumber = sharedPreferenceManager.getString(getApplicationContext(), "agentPhoneNumber","");
        String agentPassword = sharedPreferenceManager.getString(getApplicationContext(), "password","");

        final JSONObject userObject = new JSONObject();

        if(TextUtils.isEmpty(firstname.getText().toString().trim())
                || TextUtils.isEmpty(lastname.getText().toString().trim())
                || TextUtils.isEmpty(pin.getText().toString().trim())
                || TextUtils.isEmpty(confirmPin.getText().toString().trim())
                || TextUtils.isEmpty(username.getText().toString().trim())){
            Alert.showWarning(getApplicationContext(),"Please fill all fields");
            return;
        }else if(pin.getText().toString().length() != 4){
            Alert.showWarning(getApplicationContext(),"Length of pin must be 4");
            return;
        }else if(!pospin.equals(confirmpin)){
            Alert.showWarning(getApplicationContext(),"Pin and Confirm Pin Does not Match");
            return;
        }else if(!InternetConnectionService.doInternetConnectionCheck(getApplicationContext())){
            Alert.showWarning(getApplicationContext(),"Please check your internet connection");
            return;
        }else{
            try {

                userObject.put("username",username.getText().toString().trim());
                userObject.put("lastname",lastname.getText().toString().trim());
                userObject.put("firstname",firstname.getText().toString().trim());
                userObject.put("pin",pin.getText().toString().trim());
                userObject.put("agentPhoneNumber",agentPhoneNumber);
                userObject.put("agentPassword",agentPassword);

                createPosAccountController.doCreatePosAccount(CreatePosAccountActivity.this,userObject,view);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
