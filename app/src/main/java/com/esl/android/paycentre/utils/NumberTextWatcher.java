package com.esl.android.paycentre.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

public class NumberTextWatcher implements TextWatcher {

    boolean mEditing;
    private String currency;

    public NumberTextWatcher() {
        mEditing = false;
    }

    public NumberTextWatcher(String currency) {
        mEditing = false;
        this.currency = currency;
    }

    public void afterTextChanged(Editable s) {
        if(!mEditing) {
            mEditing = true;
            String digits = s.toString().replaceAll("\\D", "");

            Currency naira = Currency.getInstance("NGN");
            NumberFormat nf = NumberFormat.getCurrencyInstance();

            try{
                String formatted = nf.format(Double.parseDouble(digits)/100);
                s.replace(0, s.length(), formatted);
                Log.i("FORMAT",String.valueOf(s));
            } catch (NumberFormatException nfe) {
                s.clear();
            }

            mEditing = false;
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    public void onTextChanged(CharSequence s, int start, int before, int count) { }

}