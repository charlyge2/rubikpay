package com.esl.android.paycentre.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.esl.android.paycentre.R;

public class PreferenceManager {

    private Context context;
    private SharedPreferences sharedPreferences;

    public PreferenceManager(Context context){
        this.context = context;
        getSharedPreference();
    }

    private void getSharedPreference(){
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.my_preference), Context.MODE_PRIVATE);
    }

    public void writePreferences(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.my_preference_key),"INIT_OK");
        editor.apply();
    }

    public boolean checkPreferences(){

        boolean status = false;

        if(sharedPreferences.getString(context.getString(R.string.my_preference_key),"null").equals("null")){
        }else{
            status = true;
        }

        return status;
    }

    public void clearPreference(){

        sharedPreferences.edit().clear().apply();

    }
}
