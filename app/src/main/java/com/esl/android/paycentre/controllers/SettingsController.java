package com.esl.android.paycentre.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import com.esl.android.paycentre.interfaces.ResponseProcessorInterface;
import com.esl.android.paycentre.services.HttpService;
import org.json.JSONObject;

import java.util.HashMap;

public class SettingsController {

    private static ProgressDialog progressDialog;

    public static void processor(final Context context, String url, final String type) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Status");
        progressDialog.setMessage("Processing....");
        progressDialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progressDialog.show();

        ResponseProcessorInterface<JSONObject> ipAndPortProcessor = new ResponseProcessorInterface<JSONObject>() {
            @Override
            public void processResponse(JSONObject response) {
                progressDialog.dismiss();

                Log.d(type + "_PROCESSOR", String.valueOf(response));
            }
        };

        ResponseProcessorInterface<JSONObject> terminalsProcessor = new ResponseProcessorInterface<JSONObject>() {
            @Override
            public void processResponse(JSONObject response) {
                progressDialog.dismiss();

                Log.d(type + "_PROCESSOR", String.valueOf(response));
            }
        };

        switch (type){
            case "PORT":
                HttpService.makeGet(context, url, new HashMap<String, String>(), new HashMap<String, String>(), true, ipAndPortProcessor);
                break;
            case "REDOWNLOAD_KEYS":
                HttpService.makeGet(context, url, new HashMap<String, String>(), new HashMap<String, String>(), true, terminalsProcessor);
                break;
        }
    }

}
