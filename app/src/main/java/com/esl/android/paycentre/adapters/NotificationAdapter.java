package com.esl.android.paycentre.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esl.android.paycentre.R;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationModelViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the NotificationModels in a list
    private List<Notification> NotificationModelList;

    private static View.OnClickListener onNotificationClickListener;

    //getting the context and Notification list with constructor
    public NotificationAdapter(Context mCtx, List<Notification> NotificationModelList) {
        this.mCtx = mCtx;
        this.NotificationModelList = NotificationModelList;
    }

    @Override
    public NotificationModelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.notification_layout, null);
        return new NotificationModelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationModelViewHolder holder, int position) {
        //getting the Notification of the specified position
        Notification NotificationModel = NotificationModelList.get(position);

        //binding the data with the viewholder views
        holder.notificationTitle.setText(NotificationModel.getTitle());
        holder.notificationDesc.setText(NotificationModel.getShortdesc());
        holder.notificationTime.setText(String.valueOf(NotificationModel.getTime()));


    }


    @Override
    public int getItemCount() {
        return NotificationModelList.size();
    }


    //TODO: Step 2 of 4: Assign itemClickListener to your local View.OnClickListener variable
    public static void setOnItemClickListener(View.OnClickListener itemClickListener) {
        onNotificationClickListener = itemClickListener;
    }


    class NotificationModelViewHolder extends RecyclerView.ViewHolder {

        TextView notificationTitle, notificationDesc, notificationTime;

        public NotificationModelViewHolder(View itemView) {
            super(itemView);

            notificationTitle = itemView.findViewById(R.id.notificationTitle);
            notificationDesc = itemView.findViewById(R.id.notificationDesc);
            notificationTime = itemView.findViewById(R.id.time);

            itemView.setTag(this);
            itemView.setOnClickListener(onNotificationClickListener);
        }
    }
}
