package com.esl.android.paycentre.adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.airtimeVending.ValidateAirtimeAmountActivity;
import com.esl.android.paycentre.activities.billsPayment.BillsActivity;
import com.esl.android.paycentre.activities.billsPayment.others.ElectricityActivity;
import com.esl.android.paycentre.activities.fundsTransfer.FundsTransferAmountActivity;
import com.esl.android.paycentre.activities.transactionHistory.HistoryActivity;
import com.esl.android.paycentre.activities.withdrawal.WithdrawalAmountActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;

public class DashboardSlideAdapter extends PagerAdapter implements View.OnClickListener{

    private int[] layouts;
    private Context context;
    private LayoutInflater layoutInflater;
    public DashboardSlideAdapter(int[] layouts, Context context){

        this.layouts = layouts;
        this.context = context;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return layouts.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        View view = layoutInflater.inflate(layouts[position],container,false);

        switch (position){
            case 0:
                // SLIDE 1

                LinearLayout withdrawalCard = view.findViewById(R.id.withdrawalCard);
                LinearLayout transferCard = view.findViewById(R.id.transferCard);
                LinearLayout airtimeCard = view.findViewById(R.id.airtimeCard);
                LinearLayout billCard = view.findViewById(R.id.billCard);
                LinearLayout transactionsCard = view.findViewById(R.id.transactionCard);
                LinearLayout electricityCard = view.findViewById(R.id.electricity);

                transferCard.setOnClickListener(this);
                airtimeCard.setOnClickListener(this);
                electricityCard.setOnClickListener(this);
                withdrawalCard.setOnClickListener(this);
                billCard.setOnClickListener(this);
                transactionsCard.setOnClickListener(this);

                break;
            case 1:
                // SLIDE 2
            //    LinearLayout loanCard = view.findViewById(R.id.loanCard);
                //CardView insuranceCard = view.findViewById(R.id.insuranceCard);

            //    loanCard.setOnClickListener(this);
                //insuranceCard.setOnClickListener(this);
                break;

        }


        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    Intent myIntent = null;

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.withdrawalCard:
                myIntent = new Intent(context, WithdrawalAmountActivity.class);
                myIntent.addFlags(myIntent.FLAG_ACTIVITY_NEW_TASK);
                Globals.withdrawalType = "CASH OUT";
                Globals.transactionTypeId = 3;
                Globals.withdrawalLimit = 41000.00;
                context.startActivity(myIntent);
                break;

            case R.id.transferCard:
                myIntent = new Intent(context, FundsTransferAmountActivity.class);
                myIntent.addFlags(myIntent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(myIntent);
                break;

            case R.id.airtimeCard:
                myIntent = new Intent(context, ValidateAirtimeAmountActivity.class);
                myIntent.addFlags(myIntent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(myIntent);
                break;

            case R.id.billCard:
                myIntent = new Intent(context, BillsActivity.class);
                myIntent.addFlags(myIntent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(myIntent);
                break;

            case R.id.transactionCard:
                myIntent = new Intent(context, HistoryActivity.class);
                myIntent.addFlags(myIntent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(myIntent);
                break;

            case R.id.electricity:
                myIntent = new Intent(context, ElectricityActivity.class);
                myIntent.addFlags(myIntent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(myIntent);
                break;



            default:
                Toast.makeText(context, "DEFAULT TOAST", Toast.LENGTH_LONG).show();
                break;

        }

    }
}

