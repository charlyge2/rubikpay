package com.esl.android.paycentre.cardreader.utils;

import com.datecs.emv.EmvTags;

import org.json.JSONObject;

public class Globals {

    public static int[] ONLINE_REQUEST_TAGS = {EmvTags.TAG_5F2A_TRANSACTION_CURRENCY_CODE,
            EmvTags.TAG_5F34_APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER,
            EmvTags.TAG_82_APPLICATION_INTERCHANGE_PROFILE, EmvTags.TAG_95_TERMINAL_VERIFICATION_RESULTS,
            EmvTags.TAG_9A_TRANSACTION_DATE, EmvTags.TAG_9C_TRANSACTION_TYPE, EmvTags.TAG_9F02_AMOUNT_AUTHORISED,
            EmvTags.TAG_9F03_AMOUNT_OTHER, EmvTags.TAG_9F10_ISSUER_APPLICATION_DATA,
            EmvTags.TAG_9F1A_TERMINAL_COUNTRY_CODE, EmvTags.TAG_9F26_APPLICATION_CRYPTOGRAM,
            EmvTags.TAG_9F27_CRYPTOGRAM_INFORMATION_DATA, EmvTags.TAG_9F36_APPLICATION_TRANSACTION_COUNTER,
            EmvTags.TAG_9F37_UNPREDICTABLE_NUMBER, EmvTags.TAG_57_TRACK_2_EQUIVALENT_DATA,
            EmvTags.TAG_5F20_CARDHOLDER_NAME, EmvTags.TAG_9F1F_TRACK_1_DISCRETIONARY_DATA,
            EmvTags.TAG_9F07_APPLICATION_USAGE_CONTROL, EmvTags.TAG_8E_CARDHOLDER_VERIFICATION_METHOD_LIST,
            EmvTags.TAG_9F0D_ISSUER_ACTION_CODE_DEFAULT, EmvTags.TAG_9F0E_ISSUER_ACTION_CODE_DENIAL,
            EmvTags.TAG_9F0F_ISSUER_ACTION_CODE_ONLINE, EmvTags.TAG_91_ISSUER_AUTHENTICATION_DATA,
            EmvTags.TAG_9F33_TERMINAL_CAPABILITIES, EmvTags.TAG_9F34_CARDHOLDER_VERIFICATION_METHOD_RESULTS,
            EmvTags.TAG_5A_APPLICATION_PRIMARY_ACCOUNT_NUMBER, EmvTags.TAG_9F35_TERMINAL_TYPE, EmvTags.TAG_9F06_APPLICATION_IDENTIFIER__TERMINAL,
            EmvTags.TAG_50_APPLICATION_LABEL
    };

    public static String ipAddress = "196.6.103.18";
    public static int port = 5009;
    public static String acceptorID = null;
    public static String acceptorName = null;
    public static String merchantType = null;
    public static String terminalCapabilities = "E0F8C8";
    public static boolean isPinpadConnected = false;
    public static boolean autoConnect = true;

    public static String authData = null;
    public static String script71 = null;
    public static String script72 = null;


    public static String returnMessage = "TRANSACTION NOT DONE";
    public static String responseCode = "XX";
    public static String appResponse = "FAILED";
    public static String authCode = "Not authenticated";
    public static String rrn = null;
    public static String stan = null;
    public static String entryMethod = "Chip";
    public static String tenderType = null;
    public static String cardholder = null;
    public static String aid = null;
    public static String cardExpiry = null;
    public static boolean pinpadKeysLoaded = false;
    public static boolean redownloading = false;
    public static boolean isSetup = false;
    public static String currencyAmount = null;
    public static String pinpadAmount = null;
    public static String transactionId = null;
    public static String transactionDate = null;
    public static String applicationVersion = null;
    public static String pan = null;
    public static String terminalId = null;
    public static String withdrawalType = "CASH OUT";
    public static String agentTransferType = "AGENT TRANSFER";
    public static String fundsTransferType = "FUNDS TRANSFER";
    public static String posAccount = null;
    public static String convenienceFee = null;
    public static long databaseId = -1;
    public static boolean pairedDevice = false;
    public static String bluetoothAddress;
    public static boolean checkFinalConnection = false;
    public static String sequenceNumber = null;
    public static String accountType = null;
    public static String purseBalance = "";
    public static int transactionTypeId = 3;
    public static String email = null;
    public static String username = null;
    public static Double amount = null;
    public static Double purchase = null;
    public static Double withdrawalLimit = 41000.00;
    public static boolean refreshToken = true;
    public static String disputeType = "CASHOUT";
    public static String playerId = null;
    public static String phoneNumber = "";
    public static String stampDuty = "0";
    public static boolean isValueDifferent = false;
    public static boolean showStampDuty = false;
    public static String vat;
    public static boolean showVat = false;
    public static String agentID ="";
}
