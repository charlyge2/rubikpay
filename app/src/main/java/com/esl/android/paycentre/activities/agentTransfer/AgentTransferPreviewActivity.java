package com.esl.android.paycentre.activities.agentTransfer;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.cardreader.utils.Globals;
import com.esl.android.paycentre.interfaces.AgentTransferVerificationInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Navigator;
import com.esl.android.paycentre.utils.SaveTransferTransactions;
import com.esl.android.paycentre.utils.TransactionFormatter;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import org.json.JSONObject;

import java.text.DecimalFormat;

public class AgentTransferPreviewActivity extends BaseActivity implements AgentTransferVerificationInterface {


    private Button agentTransferPaymentButton;

    private String formattedAmount;

    private String formattedTotal;

    private Double total;

    SharedPreferenceManager sharedPreferenceManager;

    private String convenienceFee;

    private static String posAccountUsername;

    private String amount;

    private TextView amountTextView,agent_transfer_vat_TextView;

    private TextView convenienceFeeTextView;

    private TextView totalTextView;
    private View agent_vat_view;
    private LinearLayout agent_transfer_vat_layout;

    private DecimalFormat DigitFormatter = new DecimalFormat("#,###,###.00");
    private boolean showVat;
    private String vat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_transfer_preview);

        amount = getIntent().getStringExtra("amount");
        convenienceFee = getIntent().getStringExtra("convenienceFee");

        showVat = getIntent().getBooleanExtra("showVat",false);
        vat = getIntent().getStringExtra("vat");
        Globals.vat = vat;

        formattedAmount = amount.replace(",","");

        posAccountUsername = sharedPreferenceManager.getString(getApplicationContext(), "posAccountUsername", "");

        amountTextView = findViewById(R.id.amountTextView);
        convenienceFeeTextView = findViewById(R.id.convenienceFeeTextView);
        totalTextView = findViewById(R.id.totalTextView);
        agent_transfer_vat_layout = findViewById(R.id.agent_transfer_vat_layout);
        agentTransferPaymentButton = findViewById(R.id.agentTransferPaymentBtn);
        agent_transfer_vat_TextView = findViewById(R.id.agent_transfer_vat_TextView);
        agent_vat_view = findViewById(R.id.agent_vat_view);
        agentTransferPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HttpService.verifyAgentAccountDetails(AgentTransferPreviewActivity.this);

            }
        });


        if(showVat){
            agent_vat_view.setVisibility(View.VISIBLE);
            agent_transfer_vat_layout.setVisibility(View.VISIBLE);
            total = Double.parseDouble(formattedAmount) + Double.parseDouble(convenienceFee) + Double.parseDouble(vat);

            formattedTotal = DigitFormatter.format(total);

            amountTextView.setText("₦"+DigitFormatter.format(Double.parseDouble(formattedAmount)));
            convenienceFeeTextView.setText("₦"+DigitFormatter.format(Double.parseDouble(convenienceFee)));
            totalTextView.setText("₦"+formattedTotal);
            agent_transfer_vat_TextView.setText("₦" + vat);
        }
        else {
            total = Double.parseDouble(formattedAmount) + Double.parseDouble(convenienceFee);

            formattedTotal = DigitFormatter.format(total);

            amountTextView.setText("₦"+DigitFormatter.format(Double.parseDouble(formattedAmount)));
            convenienceFeeTextView.setText("₦"+DigitFormatter.format(Double.parseDouble(convenienceFee)));
            totalTextView.setText("₦"+formattedTotal);
        }



    }

    @Override
    public void sendAgentAccountDetails(String accountName, String accountNumber, String bank) {

        HttpService.progressDialog.dismiss();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AgentTransferPreviewActivity.this);
        alertDialogBuilder.setTitle("Verify Account Details");
        alertDialogBuilder
                .setMessage("\nAmount : ₦"+amount +"\n\n\nAccount Number : " +accountNumber + "\n\n\nBank : "+bank + "\n\n\n" + accountName)
                .setCancelable(false)
                .setPositiveButton("PROCEED",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    String transactionId = TransactionFormatter.generateTransactionId();

                                    JSONObject agentTransferParams = new JSONObject();
                                    agentTransferParams.put("amount",formattedAmount);
                                    agentTransferParams.put("convenienceFee",convenienceFee);
                                    agentTransferParams.put("vat",vat);
                                    agentTransferParams.put("transactionId", transactionId);
                                    agentTransferParams.put("posAccount",posAccountUsername);
                                    agentTransferParams.put("total",total);
                                    agentTransferParams.put("appTime", TransactionFormatter.generateAppTime());
                                    agentTransferParams.put("agencyTransaction","Agent Transfer");
                                    agentTransferParams.put("agentCut","0");
                                    agentTransferParams.put("version", MainActivity.getAppVersion(getApplicationContext()));

                                    String url = MainActivity.agentTransferPaymentUrl;
                                    String type = "agentTransfer";
                                    SaveTransferTransactions.initialSaveTransactions(AgentTransferPreviewActivity.this,accountName,accountNumber,bank,"",vat,posAccountUsername,transactionId,amount,convenienceFee,Globals.agentTransferType.toUpperCase());
                                    HttpService.makePayment(AgentTransferPreviewActivity.this,agentTransferParams,url,type);

                                }catch (Exception e){

                                }
                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void goBack(View view) {
        finish();
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(AgentTransferPreviewActivity.this);
    }
}
