package com.esl.android.paycentre.activities;

import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.esl.android.paycentre.interfaces.TokenTimerListener;
import com.esl.android.paycentre.services.InactivityService;
import com.esl.android.paycentre.interfaces.InactivityTimerListener;
import com.esl.android.paycentre.services.TokenService;
import com.esl.android.paycentre.utils.SharedPreferenceManager;


public class TokenActivity extends AppCompatActivity {

    SharedPreferenceManager sharedPreferenceManager;
    static String expiredTime;
    static Handler handler;
    static Runnable r;
    static InactivityService timer =  new InactivityService(5 * 60 * 1000, new InactivityTimerListener() {
        @Override
        public void onTimerCompleted() {
            handler.post(r);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        expiredTime = sharedPreferenceManager.getString(getApplicationContext(), "expiresIn", "");

         TokenService timer =  new TokenService(Integer.valueOf(expiredTime), new TokenTimerListener() {
            @Override
            public void onTimerCompleted() {
                handler.post(r);
            }
        });

        handler = new Handler();
        r = new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                //Alert.showInfo(getApplicationContext(),"App timed out due to inactivity, Please Login again");
                // Refresh Token Here
            }
        };

        timer.start();
    }

    public void restart(){

//        if(expiredTime  TokenService.timeLeft < 8639){
//            timer.restart()
//        }
    }

    @Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        super.onUserInteraction();
        Log.d("TIMER ", "Interaction detected!");
        timer.restart();
    }

}
