package com.esl.android.paycentre.activities.activation;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.database.DBHelper;
import com.esl.android.paycentre.database.DatabaseManager;
import com.esl.android.paycentre.database.model.PosAccount;
import com.esl.android.paycentre.database.repo.PosAccountRepo;
import com.esl.android.paycentre.interfaces.HttpResponseInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.SharedPreferenceManager;

import java.util.ArrayList;
import java.util.Iterator;

public class DeletePosAccountActivity extends AppCompatActivity implements HttpResponseInterface {

    Spinner spinner;

    private String username;

    SharedPreferenceManager sharedPreferenceManager;

    PosAccountRepo posAccountRepo = new PosAccountRepo();

    PosAccount posAccount = new PosAccount();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_pos_account);

        spinner = (Spinner) findViewById(R.id.dateSpinner);

        final ArrayList<String> usernames = new ArrayList<>();

        DBHelper dbHelper;

        Button deletePos;

        dbHelper = new DBHelper(getApplicationContext());

        deletePos = findViewById(R.id.deletePosAccountBtn);
        deletePos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(spinner.getCount() == 0){
                    Alert.showWarning(getApplicationContext(),"Please select username");
                    return;
                }
                // HTTP CALL FIRST
                username = spinner.getSelectedItem().toString();
                final String url =  MainActivity.deletePosUrl + username;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeletePosAccountActivity.this);
                alertDialogBuilder.setTitle("Verify Delete");
                alertDialogBuilder
                        .setMessage("Are you sure you want to delete this POS acccount?")
                        .setCancelable(false)
                        .setPositiveButton("PROCEED",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        HttpService.deletePosAccount(DeletePosAccountActivity.this,url);
                                    }
                                })
                        .setNegativeButton("CANCEL",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        DatabaseManager.initializeInstance(dbHelper);

        Iterator posaccounts = posAccountRepo.getPosAccounts();

        while(posaccounts.hasNext()) {
            PosAccount account = (PosAccount) posaccounts.next();
            //Toast.makeText(getApplicationContext(),account.getUsername() + " ",Toast.LENGTH_LONG).show();
            usernames.add(account.getUsername());
        }

        // Create an ArrayAdapter using the string array and a default spinner layout

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, usernames);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(),""+adapterView.getItemAtPosition(i), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void deletePosAccount(String username){

        posAccount.setUsername(username);

        try {
            posAccountRepo.delete(posAccount);
            sharedPreferenceManager.save(getApplicationContext(), "posLoginPage", "true");
            Intent intent = new Intent(getApplicationContext(), PosLoginActivity.class);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendDefaultHttpResponse(String responseCode, String responseMessage) {
        HttpService.progressDialog.dismiss();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeletePosAccountActivity.this);
        alertDialogBuilder.setTitle("Confirmation");
        alertDialogBuilder
                .setMessage(responseMessage)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deletePosAccount(username);
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
}
