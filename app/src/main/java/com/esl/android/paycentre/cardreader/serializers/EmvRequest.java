package com.esl.android.paycentre.cardreader.serializers;

import java.util.Date;

public class EmvRequest {
	
	private long minorAmount;
	private OnlinePinMode pinMode;
	private String currencyCode;
	private Long otherAmount;

	private Date transactionDate;
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	private AccountType accountType;
	private EmvTransactionType transType;
	private String transactionCurrencyCode;
	private String transactionCategoryCode;
	private EmvTerminalDecision terminalDecision;

	
	public long getMinorAmount() {
		return minorAmount;
	}
	public void setMinorAmount(long minorAmount) {
		this.minorAmount = minorAmount;
	}
	
	public OnlinePinMode getPinMode() {
		return pinMode;
	}
	public void setPinMode(OnlinePinMode pinMode) {
		this.pinMode = pinMode;
	}
	public EmvTransactionType getTransType() {
		return transType;
	}
	public void setTransType(EmvTransactionType transType) {
		this.transType = transType;
	}
	public String getTransactionCurrencyCode() {
		return transactionCurrencyCode;
	}
	public void setTransactionCurrencyCode(String transactionCurrencyCode) {
		this.transactionCurrencyCode = transactionCurrencyCode;
	}
	public String getTransactionCategoryCode() {
		return transactionCategoryCode;
	}
	public void setTransactionCategoryCode(String transactionCategoryCode) {
		this.transactionCategoryCode = transactionCategoryCode;
	}
	public EmvTerminalDecision getTerminalDecision() {
		return terminalDecision;
	}
	public void setTerminalDecision(EmvTerminalDecision terminalDecision) {
		this.terminalDecision = terminalDecision;
	}
	
	public AccountType getAccountType() {
		return accountType;
	}	
	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public void setOtherAmount(Long otherAmount){
		this.otherAmount= otherAmount;
	}
	
	public Long getOtherAmount(){
		return otherAmount;
	}

}

