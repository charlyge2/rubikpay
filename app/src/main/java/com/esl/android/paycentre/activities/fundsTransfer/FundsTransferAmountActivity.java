package com.esl.android.paycentre.activities.fundsTransfer;

import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.BaseActivity;
import com.esl.android.paycentre.activities.MainActivity;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.controllers.FundsTransferController;
import com.esl.android.paycentre.interfaces.BankListInterface;
import com.esl.android.paycentre.interfaces.ConvenienceFeeInterface;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.services.InternetConnectionService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Navigator;
import com.esl.android.paycentre.utils.NumberTextWatcher;
import com.esl.android.paycentre.utils.SharedPreferenceManager;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class FundsTransferAmountActivity extends BaseActivity implements ConvenienceFeeInterface, BankListInterface {

    private ArrayList<String> bankNamesArray = new ArrayList<>();

    private Map<String, String> bankListAndCodes = new HashMap<String, String>();

    private String selectedBank;

    private String selectedBankCode;

    private EditText accountNumber;

    public static TextView accountName;

    public static View firstLine;

    private EditText narration;

    private EditText phone;

    private EditText amount;

    private Button proceedBtn;

    private boolean doubleBackToExitPressedOnce = false;

    private SharedPreferenceManager sharedPreferenceManager;

    private String formattedAmount;

    private String transferAmount;

    private String transferNarration;

    private String transferPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funds_transfer_amount);

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(
                this);
        String json = appSharedPrefs.getString("BankList", "");
        Gson gson=new Gson();
        JSONObject bankList =gson.fromJson(json, JSONObject.class);

        if(bankList!=null){
            processBankList(bankList);
        }
        else {
            HttpService.fetchBankList(this, MainActivity.bankListUrl);

        }

        accountName = findViewById(R.id.accountName);
        firstLine = findViewById(R.id.firstLine);
        accountNumber = findViewById(R.id.loanAmount);
        amount = findViewById(R.id.fundsTransferAmount);

        String naira = getResources().getString(R.string.naira);
        amount.addTextChangedListener(new NumberTextWatcher());
        phone = findViewById(R.id.customerTransferPhone);
        narration = findViewById(R.id.customerTransferNarration);
        proceedBtn = findViewById(R.id.transferBtn);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                transferAmount = amount.getText().toString();
                transferNarration = narration.getText().toString();
                transferPhone = phone.getText().toString();

                confirmAmount(transferAmount, transferNarration, transferPhone);
            }
        });

//        try{
//            Spinner spinner = findViewById(R.id.bankListSpinner);
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bankNamesArray);
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            spinner.setAdapter(adapter);
//            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    selectedBank = (String) adapterView.getItemAtPosition(i);
//                    selectedBankCode = bankListAndCodes.get(selectedBank);
//                    Log.d("BANK_CODE", "Bank - " + selectedBank + " Code - " + selectedBankCode);
//
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//
//                }
//            });
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }

        Spinner spinner = findViewById(R.id.bankListSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, bankNamesArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedBank = (String) adapterView.getItemAtPosition(i);
                selectedBankCode = bankListAndCodes.get(selectedBank);
                Log.d("BANK_CODE", "Bank - " + selectedBank + " Code - " + selectedBankCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (phone.getText().toString().trim().length() < 11) {
                    phone.setError("Input is not a valid phone number");
                    proceedBtn.setEnabled(false);
                } else {
                    phone.setError(null);
                    proceedBtn.setEnabled(true);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        accountNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(accountNumber.getText().toString())) {
                    int accountNumberLength = accountNumber.getText().toString().trim().length();
                    String accountNumberValidationResult = FundsTransferController.validateFundsTransfer(accountNumberLength);
                    if (accountNumberValidationResult.equals("invalid")) {
                        accountNumber.setError("Invalid Account Number");
                    } else if (accountNumberValidationResult.equals("valid")) {

                        accountNumber.setError(null);
                        if (TextUtils.isEmpty(selectedBank)) {
                            Alert.showWarning(getApplicationContext(), "You have to select a beneficiary bank");
                            return;
                        }

                        String url = MainActivity.fundsTransferNameVerificationUrl + selectedBankCode + "/" + accountNumber.getText().toString();
                        if (InternetConnectionService.doInternetConnectionCheck(getApplicationContext())) {
                            HttpService.getAccountName(FundsTransferAmountActivity.this, url);
                        }

                        else {
                            Alert.showWarning(getApplicationContext(), "No internet connection ");
                        }

                    } else {
                        accountNumber.setError(null);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        String checkPinStatus = sharedPreferenceManager.getString(getApplicationContext(),"fundTransferPin","false");
//        if(checkPinStatus.equals("false")){
//            Intent intent = new Intent(getApplicationContext(),FundsTransferPinActivity.class);
//            startActivity(intent);
//        }

    }

    public void confirmAmount(String amount, String narration, String phone) {

        if (TextUtils.isEmpty(amount) || TextUtils.isEmpty(phone)) {
            Alert.showWarning(getApplicationContext(), "Amount and Phone Fields are mandatory");
            return;
        } else if (TextUtils.isEmpty(accountNumber.getText().toString()) || TextUtils.isEmpty(selectedBank)) {
            Alert.showWarning(getApplicationContext(), "Please fill all fields");
            return;

        } else if (TextUtils.isEmpty(accountName.getText().toString())) {
            Alert.showWarning(getApplicationContext(), "Account Information is wrong. Please fetch valid account name");
            return;
        } else {

            formattedAmount = amount.replace(",", "");

            if (Double.valueOf(formattedAmount) > 100000) {
                Alert.showWarning(getApplicationContext(), "The maximum limit for funds transfer is ₦100,000");
                return;
            }

//            if (Double.valueOf(formattedAmount)>= Double.valueOf(currentBalance)){
//                Alert.showWarning(getApplicationContext(), "Insufficient fund, fund your account and try again.");
//                return;
//            }

            try {

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("amount", formattedAmount);
                jsonObject.put("transactionType", "Funds Transfer");

                HttpService.fetchConvenienceFee(FundsTransferAmountActivity.this, jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendConvenienceFee(String fee) {

    }
    @Override
    public void sendConvenienceFee(String fee, String StampDuty, boolean showStampDuty, String vat, boolean showVat) {
        HttpService.progressDialog.dismiss();

        Intent intent = new Intent(getApplicationContext(), FundsTransferAmountPreviewActivity.class);

        intent.putExtra("amount", transferAmount);
        intent.putExtra("bank", selectedBank);
        intent.putExtra("phone", transferPhone);
        intent.putExtra("bankCode", selectedBankCode);
        intent.putExtra("narration", transferNarration);
        intent.putExtra("accountName", accountName.getText().toString());
        intent.putExtra("accountNumber", accountNumber.getText().toString());
        intent.putExtra("convenienceFee", fee);
        intent.putExtra("showVat", showVat);
        intent.putExtra("vat", vat);

        startActivity(intent);
    }


    public void goBack(View view) {
        Navigator.moveToDashboard(FundsTransferAmountActivity.this);
    }

    public void goHome(View view) {
        Navigator.moveToDashboard(FundsTransferAmountActivity.this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }


    private void processBankList(JSONObject bankList) {
        if(bankList==null){
            return;
        }

        Log.d("BANKING_LIST", String.valueOf(bankList));
        Iterator<String> iter = bankList.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            Object value = bankList.opt(key);
            Log.d("BANKING_LIST", "KEY - " + key + " VALUE - " + value);
            bankListAndCodes.put(String.valueOf(value), key);
            bankNamesArray.add((String.valueOf(value)).toUpperCase());
            Collections.sort(bankNamesArray);
        }
    }

    @Override
    public void send(JSONObject bankList) {

        Log.d("BANKING_LIST", String.valueOf(bankList));
        Iterator<String> iter = bankList.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            Object value = bankList.opt(key);
            Log.d("BANKING_LIST", "KEY - " + key + " VALUE - " + value);
            bankListAndCodes.put(String.valueOf(value), key);
            bankNamesArray.add((String.valueOf(value)).toUpperCase());
            Collections.sort(bankNamesArray);

        }
    }
}
