package com.esl.android.paycentre.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.activation.PosAdminLoginActivity;
import com.esl.android.paycentre.activities.activation.PosLoginActivity;
import com.esl.android.paycentre.activities.activation.WalkthroughActivity;
import com.esl.android.paycentre.cardreader.extras.ReferenceList;
import com.esl.android.paycentre.config.ConfigurationClass;
import com.esl.android.paycentre.controllers.activationControllers.ActivationController;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.Display;


public class MainActivity extends AppCompatActivity {

    private Button activateButton;
    private static EditText activateInput;
   // private SharedPreferenceManager sharedPreferenceManager;
    private ActivationController activationController = new ActivationController();
    private boolean doubleBackToExitPressedOnce = false;
    private CheckBox termsCheckBox;
    public static final ConfigurationClass configurationClass = new ConfigurationClass("live");
    public static final String activationUrl = configurationClass.getEnviromentUrl("activation");
    public static final String adminLoginUrl = configurationClass.getEnviromentUrl("adminLogin");
    public static final String fetchPosUrl= configurationClass.getEnviromentUrl("fetchPos");;
    public static final String createPosUrl = configurationClass.getEnviromentUrl("createPos");
    public static final String changePosPinUrl = configurationClass.getEnviromentUrl("changePosPin");
    public static final String changePasswordUrl = configurationClass.getEnviromentUrl("changeAdminPassword");
    public static final String purseBalanceUrl = configurationClass.getEnviromentUrl("purseBalance");
    public static final String convenienceFeeUrl = configurationClass.getEnviromentUrl("convenienceFee");
    public static final String paycentreTransactionUrl = configurationClass.getEnviromentUrl("paycentreTransaction");
    public static final String fundsTransferNameVerificationUrl = configurationClass.getEnviromentUrl("fundsTransferNameVerification");
    public static final String fundsTransferPaymentUrl = configurationClass.getEnviromentUrl("fundsTransferPayment");
   // public static final String fundsTransferRequeryUrl = configurationClass.getEnviromentUrl("fundsTransferRequery");
    public static final String verifyAgentAccountUrl = configurationClass.getEnviromentUrl("verifyAgentAccount");
    public static final String agentTransferPaymentUrl = configurationClass.getEnviromentUrl("agentTransferPayment");
    public static final String ibetaTransactionUrl = configurationClass.getEnviromentUrl("ibetaTransaction");
    public static final String bankListUrl = configurationClass.getEnviromentUrl("bankList");
    public static final String airtimePaymentUrl = configurationClass.getEnviromentUrl("airtimePayment");
    public static final String dstvInquiryUrl = configurationClass.getEnviromentUrl("dstvInquiry");
    public static final String dstvPlanUrl = configurationClass.getEnviromentUrl("dstvPlan");
    public static final String dstvPaymentUrl = configurationClass.getEnviromentUrl("dstvPayment");
    public static final String gotvInquiryUrl = configurationClass.getEnviromentUrl("gotvInquiry");
    public static final String gotvPlanUrl = configurationClass.getEnviromentUrl("gotvPlan");
    public static final String gotvPaymentUrl = configurationClass.getEnviromentUrl("gotvPayment");
    public static final String startimesInquiryUrl = configurationClass.getEnviromentUrl("startimesInquiry");
    public static final String startimesPaymentUrl = configurationClass.getEnviromentUrl("startimesPayment");
    public static String appVersionUrl;
    public static final String loanEligibilityUrl = configurationClass.getEnviromentUrl("eligibility");
    public static final String loanUrl = configurationClass.getEnviromentUrl("loan");
    public static final String refreshTokenUrl = configurationClass.getEnviromentUrl("refreshToken");
    public static final String deletePosUrl = configurationClass.getEnviromentUrl("deletePos");
    public static final String createDisputeUrl = configurationClass.getEnviromentUrl("createDispute");
    public static final String fetchDisputesUrl = configurationClass.getEnviromentUrl("fetchDispute");
    public static final String sendEmailUrl = configurationClass.getEnviromentUrl("sendEmail");
    public static final String powerUrl = configurationClass.getEnviromentUrl("powerUrl");
    public static final String electricityInquiryUrl = configurationClass.getEnviromentUrl("electricityInquiryUrl");
    public static final String payPowerUrl = configurationClass.getEnviromentUrl("payPowerUrl");
    public static final String requeryPowerUrl = configurationClass.getEnviromentUrl("REQUERYPOWER");




    private static String uuid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);  //remove window title

        setContentView(R.layout.activity_main);
        appVersionUrl = getAppVersion(this);
        uuid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (uuid == null || TextUtils.isEmpty(uuid)){
            uuid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure._ID);
        }

        Log.i("UUID: ", uuid);

        termsCheckBox = findViewById(R.id.termsCheckBox);

        int getStatus = Display.checkStatus(getApplicationContext());

        Intent myIntent = null;
//
        if(getStatus == 1){

         //LOAD MAIN ACTIVITY

        }else if (getStatus == 2){
            myIntent = new Intent(MainActivity.this, WalkthroughActivity.class);
            startActivity(myIntent);

        }else if (getStatus == 3){

        }else if (getStatus == 4){

            myIntent = new Intent(MainActivity.this, PosAdminLoginActivity.class);
            startActivity(myIntent);

        }else if(getStatus == 5){

            myIntent = new Intent(MainActivity.this, PosLoginActivity.class);
            startActivity(myIntent);

        }
//
        activateButton = findViewById(R.id.activateBtn);
        activateInput = findViewById(R.id.activateInp);

        SpannableString spannableString = new SpannableString("Click to agree to Terms and Conditions");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
//                Intent intent = new Intent(getApplicationContext(), TermsAndConditionsActivity.class);
//                intent.putExtra("type","activation");
//                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(clickableSpan, 18, 38, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = findViewById(R.id.termsTextView);
        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);

        activateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (termsCheckBox.isChecked()){

                    ReferenceList.config = getSharedPreferences(
                            ReferenceList.preference, 0);

                    SharedPreferences.Editor editor = ReferenceList.config.edit();

                    editor.putString(ReferenceList.agreed, "true");
                    editor.apply();

                    activation();

                }else{
                    Alert.showWarning(MainActivity.this, "Kindly accept Terms of Service before proceeding");
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        uuid = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public void activation() {
        String activationResponse = activationController.validateActivation(MainActivity.this,activateInput.getText().toString());
        if(!TextUtils.isEmpty(activationResponse)){
            switch (activationResponse){
                case "valid":
                    HttpService.activateUser(MainActivity.this,activateInput.getText().toString());
                    break;
                case "Please Enter Activation Code":
                    Alert.showWarning(getApplicationContext(),"Please Enter Activation Code");
                    break;
                case "Activation Code Must be 8 digits":
                    Alert.showWarning(getApplicationContext(),"Activation Code Must be 8 digits");
                    break;
                case "Please Check Your Internet Connection":
                    Alert.showWarning(getApplicationContext(),"Please Check Your Internet Connection");
                    break;
                default:
                    Alert.showWarning(getApplicationContext(),"Could not activate");
                    break;
            }
        }
    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
        }

        this.doubleBackToExitPressedOnce = true;
        Alert.showWarning(getApplicationContext(),"Click back again to exit");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public static String getAppVersion(Context context){
        String version = null;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public static String getUuid(Context context){
        uuid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (uuid == null || TextUtils.isEmpty(uuid)){
            uuid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure._ID);
        }
        return uuid;
    }
}
