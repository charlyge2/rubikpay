package com.esl.android.paycentre.activities.loans;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.esl.android.paycentre.R;
import com.esl.android.paycentre.activities.dashboard.DashboardActivity;
import com.esl.android.paycentre.services.HttpService;
import com.esl.android.paycentre.utils.Alert;
import com.esl.android.paycentre.utils.NumberTextWatcher;

import org.json.JSONObject;

import java.text.DecimalFormat;

public class ExistingLoanActivity extends AppCompatActivity {

    private EditText loanAmount;

    private TextView interestTextView,paybackTextView,dailyPayableTextView,totalPayableTextView;

    private Double interest;

    private Double dailyPayable;

    private Double totalPayable;

    private Double interestRate;

    private Double eligibilityAmount;

    private TextView loanBalance;

    private int payBackDays;

    private DecimalFormat formatter = new DecimalFormat("#,###,###.00");

    private Button loanBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_loan);

        interestTextView = findViewById(R.id.interesttTextView);
        paybackTextView = findViewById(R.id.paybackkDTextView);
        totalPayableTextView = findViewById(R.id.totallPayableTextView);
        dailyPayableTextView = findViewById(R.id.dPayableeTextView);

        Intent intent = getIntent();
        interestRate = Double.valueOf(intent.getStringExtra("interestRate"));
        payBackDays = Integer.valueOf(intent.getStringExtra("payBackDays"));
        eligibilityAmount = Double.valueOf(intent.getStringExtra("eligibleAmount"));

        double formattedBalance = Double.valueOf(eligibilityAmount);

        String formattedLoanBalance = "₦" +formatter.format(formattedBalance);

        loanAmount = findViewById(R.id.loanAmount);
        loanAmount.addTextChangedListener(new NumberTextWatcher());
        loanBalance = findViewById(R.id.loanBalance);

        loanBalance.setText(formattedLoanBalance);
        loanBtn = (Button) findViewById(R.id.loanApplicationBtn);
        loanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(loanAmount.getText().toString())){
                    Alert.showWarning(getApplicationContext(),"Loan Amount cannot be empty");
                    return;
                }

                try {

                    JSONObject loanPayload = new JSONObject();

                    loanPayload.put("amount",loanAmount.getText().toString());
                    loanPayload.put("kycDocument","");
                    loanPayload.put("documentType","");
                    loanPayload.put("issuingDate","");
                    loanPayload.put("expiryDate","");

                    HttpService.doLoanApplication(ExistingLoanActivity.this,loanPayload);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        //loanAmount.addTextChangedListener(new NumberTextWatcher());
        loanAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String value = "".equals(String.valueOf(s))? "0": String.valueOf(s);
                interest = (interestRate / 100 ) * Double.valueOf(value);
                totalPayable = interest + Double.valueOf(value);
                dailyPayable = totalPayable / payBackDays;

                interestTextView.setText("₦"+formatter.format(interest));
                dailyPayableTextView.setText("₦"+formatter.format(dailyPayable));
                totalPayableTextView.setText("₦"+formatter.format(totalPayable));
                paybackTextView.setText(String.valueOf(payBackDays));

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        interestTextView.setText("₦0.00");
        dailyPayableTextView.setText("₦0.00");
        totalPayableTextView.setText("₦0.00");
        paybackTextView.setText(String.valueOf(payBackDays));


    }

    public void dashboard(View view) {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
    }
}
